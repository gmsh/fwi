// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard library
//GmshFEM Library
//GmshFWI Library

#include "configuration.h"
#include <gmsh.h>

namespace gmodel = gmsh::model;
namespace factory = gmsh::model::geo;

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::domain;

void ConfigurationInterface::mesh() const
{
  msg::print << "Generate meshes" << msg::endl;
  gmsh::option::setNumber("General.Terminal", 1);

  gmodel::add(_name);
  if (!_remesh)
  {
    gmsh::open(_name + ".msh");
    return;
  }

  wave_mesh();
  data_mesh();

  factory::synchronize();
  gmodel::mesh::generate();

  gmsh::write(_name + ".msh");
}

bool ConfigurationInterface::recIsValid(unsigned int shot,unsigned int rec) const
{
  if( !(shot<_ns) && !(rec<nr(shot)) )
  {
    throw Exception("Receiver "+std::to_string(rec)+ " is out of scope for shot "+std::to_string(shot)+".");
  }
  return true;
}
bool ConfigurationInterface::emIsValid(unsigned int shot,unsigned int em) const
{
  if( !(shot<_ns) && !(em<ne(shot)) )
  {
    throw Exception("Emitter "+std::to_string(em)+ " is out of scope for shot "+std::to_string(shot)+".");
  }
  return true;
}
bool ConfigurationInterface::pntIsValid(unsigned int e_r) const
{
  if( !(e_r<_np) )
  {
    throw Exception("Emitter-receiver "+std::to_string(e_r)+" is out of scope. There are only "+std::to_string(_np)+" points.");
  }
  return true;
}
bool ConfigurationInterface::shotIsValid(unsigned int shot) const
{
  if( !(shot<_ns) )
  {
    throw Exception("Shot "+std::to_string(shot)+" is out of scope.");
  }
  return true;
}
const std::vector<unsigned int>& ConfigurationInterface::emitter_idx(unsigned int shot) const
{
  shotIsValid(shot);
  return _emitter[shot];
}
const std::vector<unsigned int>& ConfigurationInterface::receiver_idx(unsigned int shot) const
{
  shotIsValid(shot);
  return _receiver[shot];
}
unsigned int ConfigurationInterface::isEmitter(unsigned int point) const
{
  std::vector<unsigned int> idx = all_emitter_idx();
  return (std::find(idx.begin(), idx.end(), point) != idx.end());
}
unsigned int ConfigurationInterface::isReceiver(unsigned int point) const
{
  std::vector<unsigned int> idx = all_receiver_idx();
  return (std::find(idx.begin(), idx.end(), point) != idx.end());
}
Domain ConfigurationInterface::emitter(unsigned int shot, unsigned int em) const
{
  emIsValid(shot,em);
  return _point[_emitter[shot][em]];
}
Domain ConfigurationInterface::receiver(unsigned int shot, unsigned int rec) const
{
  recIsValid(shot,rec);
  return _point[_receiver[shot][rec]];
}
typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree1 >::Object ConfigurationInterface::emitter_polarization(unsigned int shot, unsigned int em) const
{
  emIsValid(shot,em);
  return _emitter_polarization[shot][em];
}
typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree1 >::Object ConfigurationInterface::receiver_polarization(unsigned int shot, unsigned int rec) const
{
  recIsValid(shot,rec);
  return _receiver_polarization[shot][rec];
}
Domain ConfigurationInterface::point(unsigned int idx) const
{
  pntIsValid(idx);
  return _point[idx];
}
std::vector<unsigned int> ConfigurationInterface::all_emitter_idx(bool includeReceivers) const
{
  std::vector<unsigned int> em_idx;
  std::vector<unsigned int> rec_idx;
  if(!includeReceivers){rec_idx = all_receiver_idx(true);}
  for (unsigned int s = 0; s < ns(); s++)
  {
    for (unsigned int e = 0; e < ne(s); e++)
    {
      if
      (
          (std::find(em_idx.begin(), em_idx.end(), _emitter[s][e]) == em_idx.end())
          &&
          (std::find(rec_idx.begin(), rec_idx.end(), _emitter[s][e]) == rec_idx.end())
      )
      {
        em_idx.push_back(_emitter[s][e]);
      }
    }
  }
  return em_idx;
}
std::vector<unsigned int> ConfigurationInterface::all_receiver_idx(bool includeEmitters) const
{
  std::vector<unsigned int> rec_idx;
  std::vector<unsigned int> em_idx;
  if(!includeEmitters){em_idx = all_emitter_idx(true);}
  for (unsigned int s = 0; s < ns(); s++)
  {
    for (unsigned int r = 0; r < nr(s); r++)
    {
      if
      (
          (std::find(rec_idx.begin(), rec_idx.end(), _receiver[s][r]) == rec_idx.end())
          &&
          (std::find(em_idx.begin(), em_idx.end(), _receiver[s][r]) == em_idx.end())
      )
      {
        rec_idx.push_back(_receiver[s][r]);
      }
    }
  }
  return rec_idx;
}
std::vector<unsigned int> ConfigurationInterface::receiverPerShot() const
{
  std::vector<unsigned int> recPerShot;
  for (unsigned int s = 0; s < _ns; s++)
  {
    recPerShot.push_back(nr(s));
  }
  return recPerShot;
}
