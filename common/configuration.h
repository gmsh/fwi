// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_CONFIGURATION_INTERFACE
#define H_CONFIGURATION_INTERFACE

//GmshFEM Library
#include "GmshFem.h"
#include "Domain.h"
//Standard Library
#include <array>
#include <map>
//GmshFWI Library
#include "model/element.h"
#include "model/parametrization.h"
#include "enum.h"

using namespace gmshfem::function::literals;

class ConfigurationInterface //(abstract)
{
protected:
    const std::string _name;

    std::array<gmshfem::domain::Domain,2> _wave_omega;
    std::array<gmshfem::domain::Domain,2> _model_known;
    std::array<gmshfem::domain::Domain,2> _model_unknown;
    std::map<std::string, gmshfem::domain::Domain> _named_domains;
    gmshfem::domain::Domain _data_omega;

    unsigned int _np;
    std::vector< gmshfem::domain::Domain > _point;
    gmshfem::domain::Domain _points;
    unsigned int _ns;
    std::vector< std::vector<unsigned int> > _emitter;
    std::vector< std::vector<unsigned int> > _receiver;
    std::vector< std::vector<typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree1 >::Object> > _emitter_polarization;
    std::vector< std::vector<typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree1 >::Object> > _receiver_polarization;

    ParametrizationInterface* const _parametrization;
    std::vector<std::complex<double>> _mc;
    ModelFunction _m0;

    bool _remesh;

    virtual void wave_mesh() const = 0;
    virtual void data_mesh() const = 0;
public:
    ConfigurationInterface(std::string name, const ParametrizationInterface* const parametrization, const gmshfem::common::GmshFem& gmshFem) : _name(name), _parametrization(parametrization->clone()), _mc(model_size(),0.)
    {
        _parametrization->pulsation(0.);

        unsigned int remesh = true;
        gmshFem.userDefinedParameter(remesh, "remesh");
        _remesh = ((bool) remesh);
    };

    ConfigurationInterface() : _name("dummy"), _parametrization(nullptr), _mc(1,0.)
    {
        _remesh = true;
    };

    virtual ~ConfigurationInterface() {delete _parametrization;};

    virtual void mesh() const;
    virtual double area() const = 0;
    virtual double data_area() const {return gmshfem::post::integrate(1._d_sf,_data_omega,"Gauss0");};
    virtual double datapoint_area() const = 0;
    virtual double array() const {return 0.;};
    virtual double depth() const {return 0.;};

    virtual std::string wave_gmodel() const = 0;
    virtual std::string model_gmodel() const = 0;
    virtual std::string data_gmodel() const = 0;

    virtual std::array<unsigned int,2> data_coordinate_to_index(double xs, double xr) const = 0;
    virtual std::array<double,2> index_to_data_coordinate(unsigned int s, unsigned int r) const = 0;
    virtual bool data_coordinate_isValid(double xs,double xr) const = 0;

    gmshfem::domain::Domain wave_omega(Support support) const {return _wave_omega[support];};
    gmshfem::domain::Domain model_unknown(Support support) const {return _model_unknown[support];};
    gmshfem::domain::Domain model_known(Support support) const {return _model_known[support];};
    gmshfem::domain::Domain data_omega() const {return _data_omega;};

    gmshfem::domain::Domain model_evaldomain() const {return _model_unknown[Support::BLK] | _model_unknown[Support::BND];};
    gmshfem::domain::Domain wave_evaldomain() const {return _wave_omega[Support::BLK]  | _wave_omega[Support::BND] | _points;};
    gmshfem::domain::Domain data_evaldomain() const {return _data_omega;};
    const std::map<std::string, gmshfem::domain::Domain>& named_domains() const {return _named_domains;}

    gmshfem::domain::Domain emitter(unsigned int shot, unsigned int em) const;
    gmshfem::domain::Domain receiver(unsigned int shot, unsigned int rec) const;
    typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree1 >::Object emitter_polarization(unsigned int shot, unsigned int em) const;
    typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree1 >::Object receiver_polarization(unsigned int shot, unsigned int rec) const;
    gmshfem::domain::Domain point(unsigned int idx) const;
    const std::vector<unsigned int>& emitter_idx(unsigned int shot) const;
    const std::vector<unsigned int>& receiver_idx(unsigned int shot) const;
    std::vector<unsigned int> all_emitter_idx(bool includeReceivers = false) const;
    std::vector<unsigned int> all_receiver_idx(bool includeEmitters = false) const;
    unsigned int isEmitter(unsigned int point) const;
    unsigned int isReceiver(unsigned int point) const;
    unsigned int np() const {return _np;};
    unsigned int ns() const {return _ns;};
    unsigned int nr(unsigned int shot) const {return _receiver[shot].size();};
    unsigned int ne(unsigned int shot) const {return _emitter[shot].size();};
    bool recIsValid(unsigned int shot,unsigned int rec) const;
    bool emIsValid(unsigned int shot,unsigned int em) const;
    bool pntIsValid(unsigned int e_r) const;
    bool shotIsValid(unsigned int shot) const;
    std::vector<unsigned int> receiverPerShot() const;
    unsigned int model_size() const {return _parametrization->size();};
    ModelFunction m0() const {return _m0;};
    std::vector<std::complex<double>> mc() const {return _mc;};
    ModelFunction m0_natural(double pulsation) const
    {
        _parametrization->pulsation(pulsation);
        ModelFunction m0 = (*_parametrization)(_m0);
        _parametrization->pulsation(0.);
        return m0;
    };
    std::vector<std::complex<double>> mc_natural(double pulsation) const
    {
        _parametrization->pulsation(pulsation);
        std::vector<std::complex<double>> mc = (*_parametrization)(_mc);
        _parametrization->pulsation(0.);
        return mc;
    };
};


#endif // H_CONFIGURATION_INTERFACE
