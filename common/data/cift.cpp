// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFWI Library
#include "cift.h"

//GmshFEM Library
#include "Exception.h"

using namespace gmshfem;
using namespace gmshfem::common;

std::vector< double > to_time(const GmshFem& gmshFem, std::string suffix)
{
    unsigned int nt = 0;
    double T = 0.;
    if(
      !(
        gmshFem.userDefinedParameter(nt, "cift_nt") &&
        gmshFem.userDefinedParameter(T, "cift_T")
        )
    )
    {
      throw Exception("A continuous inverse Fourier transform parameter could not be found.");
    }
    if(nt<=0 || T<=0.)
    {
      throw Exception("Numer of time points (nt) and period (T) for continuous inverse Fourier transform parameter must be positive.");
    }

    std::vector< double > time(nt,0.);
    double dt = T / ((double)nt);
    for (unsigned int t = 0; t < nt; t++)
    {
        time[t] = ((double) t) * dt;
    }
    return time;
}

/*
* Cift
*/
template<Physic T_Physic>
Data<T_Physic> Cift<T_Physic>::frequency_to_time(const Data<T_Physic>& df, bool windowed) const
{
    isFrequencyCompatible(df);
    Data<T_Physic> dt(_time,df);
    for (unsigned int s = 0; s < df.ns(); s++)
    {
        for (unsigned int r = 0; r < df.nr(s); r++)
        {
            for (unsigned int t = 0; t < _time.size(); t++)
            {
                typename PointData<T_Physic>::Object tmp(0.);
                for (unsigned int f = 0; f < _freq.size(); f++)
                {
                    if(windowed)
                    {
                        tmp += _w[f] * _F[t][f] * df.value(f,s,r);
                    }
                    else
                    {
                        tmp += _F[t][f] * df.value(f,s,r);
                    }
                }
                dt.value(t,s,r,tmp+conj(tmp));
            }
        }
    }
    return dt;
};

template<Physic T_Physic>
Data<T_Physic> Cift<T_Physic>::time_to_frequency(const Data<T_Physic>& dt, bool unwindowed, bool normalized) const
{
    isTimeCompatible(dt);
    Data<T_Physic> df(_freq,dt);
    for (unsigned int s = 0; s < dt.ns(); s++)
    {
        for (unsigned int r = 0; r < dt.nr(s); r++)
        {
            for (unsigned int f = 0; f < _freq.size(); f++)
            {
                typename PointData<T_Physic>::Object tmp(0.);
                for (unsigned int t = 0; t < _time.size(); t++)
                {
                    tmp += 2. * _F[t][f] * dt.value(t,s,r);
                }
                if(unwindowed){tmp=std::conj(tmp);tmp /= 2.*_w[f];}
                else{tmp *= _w[f];}
                if(normalized){tmp *= _normalization;}
                df.value(f,s,r,tmp);
            }
        }
    }
    return df;
};

template<Physic T_Physic>
bool Cift<T_Physic>::isFrequencyCompatible(const Data<T_Physic>& df) const
{
    if(df.nf() != _freq.size())
    {
        throw Exception("Data has a different number of frequencies than continuous inverse Fourier transform.");
        return false;
    }
    for (unsigned int f = 0; f < df.nf(); f++)
    {
        if(df.frequency(f) != _freq[f])
        {
            throw Exception("Data frequency #"+std::to_string(f)+"is different from continuous inverse Fourier transform frequency #"+std::to_string(f)+".");
        }
        return false;
    }
    return true;
}

template<Physic T_Physic>
bool Cift<T_Physic>::isTimeCompatible(const Data<T_Physic>& dt) const
{
    if(dt.nf() != _time.size())
    {
        throw Exception("Data has a different number of times than continuous inverse Fourier transform.");
        return false;
    }
    for (unsigned int t = 0; t < dt.nf(); t++)
    {
        if(dt.frequency(t) != _time[t])
        {
            throw Exception("Data time #"+std::to_string(t)+"is different from continuous inverse Fourier transform time #"+std::to_string(t)+".");
        }
        return false;
    }
    return true;
}

template class Cift<Physic::acoustic>;
