// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_DATA_CIFT
#define H_COMMON_DATA_CIFT

//GmshFWI Library
#include "../data/element.h"
#include "../../specific/data/cift.h"

//GmshFEM Library
#include "GmshFem.h"
#include "Exception.h"

std::vector< double > to_time(const gmshfem::common::GmshFem& gmshFem, std::string suffix = "");

/*
* Cift
*/
template<Physic T_Physic>
class Cift
{
private:
    double _period;
    const std::vector< double > _time;
    const std::vector< double > _freq;
    const std::vector<std::complex<double>> _w;
    const std::vector<std::vector<std::complex<double>>> _F;
    const double _normalization;
public:
    Cift(const std::vector< double >& time, const std::vector< double >& freq, const gmshfem::common::GmshFem& gmshFem, std::string suffix = "") : _time(time), _freq(freq), _w(window(_freq)), _F(inverse_fourier_matrix(_time,_freq,gmshFem)), _normalization(pseudo_fourier_normalization(_time,_freq))
    {
        if(!gmshFem.userDefinedParameter(_period, "cift_T"))
        {
          throw gmshfem::common::Exception("Period of inverse Fourier transform parameter could not be found.");
        }
        if(_period<=0.)
        {
          throw gmshfem::common::Exception("Period for continuous inverse Fourier transform parameter must be positive.");
        }
    };
    Cift(const std::vector< double >& freq, const gmshfem::common::GmshFem& gmshFem, std::string suffix = "") : _time(to_time(gmshFem,suffix)), _freq(freq), _w(window(_freq)), _F(inverse_fourier_matrix(_time,_freq,gmshFem)), _normalization(pseudo_fourier_normalization(_time,_freq)) {};

    Data<T_Physic> frequency_to_time(const Data<T_Physic>& df,bool windowed) const;
    Data<T_Physic> time_to_frequency(const Data<T_Physic>& dt,bool unwindowed,bool normalized) const;

    bool isFrequencyCompatible(const Data<T_Physic>& df) const;
    bool isTimeCompatible(const Data<T_Physic>& dt) const;

    double period() const {return _period;};
};

#endif // H_COMMON_DATA_CiFT
