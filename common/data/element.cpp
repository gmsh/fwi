// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFem library
#include "Exception.h"
#include "CSVio.h"

using namespace gmshfem;
using namespace gmshfem::common;

//FWI Library
#include "element.h"
#include "../../specific/todata.h"

/*
* Class Data
*/
template<Physic T_Physic>
bool Data<T_Physic>::isValid(unsigned int f,unsigned int s,unsigned int r) const
{
  if ( !(f<nf()) )
  {
    throw Exception("Frequency "+std::to_string(f)+" is out of scope");
  }
  if ( !(s<ns()) )
  {
    throw Exception("Shot "+std::to_string(s)+" is out of scope");
  }
  if ( !(r<nr(s)) )
  {
    throw Exception("Receiver "+std::to_string(r)+" is out of scope ("+std::to_string(nr(s))+")");
  }
  return true;
}
template<Physic T_Physic>
Data<T_Physic>::Data(const std::vector< double >& freq, const ConfigurationInterface* const config) : _freq(freq), _config(config)
{
    unsigned int nf = _freq.size();
    if ( nf==0 )
    {
        msg::warning << "An empty data (nf=0) is created." << msg::endl;
    }
    unsigned int ns = _config->ns();
    _value.resize(nf);
    for (unsigned int f = 0; f < nf; f++)
    {
        _value[f].resize(ns);
        for (unsigned int s = 0; s < ns; s++)
        {
            unsigned int nr = _config->nr(s);
            _value[f][s].resize(nr,typename PointData<T_Physic>::Object());
        }
    }
}
template<Physic T_Physic>
Data<T_Physic>::Data(const std::vector<std::string>& filename, const std::vector< double >& freq, const ConfigurationInterface* const config) : Data(freq,config)
{
    read(filename);
}
template<Physic T_Physic>
Data<T_Physic>::Data(const Data<T_Physic>& other) : Data(other._freq,other._config) {*this = other;}

template<Physic T_Physic>
Data<T_Physic>::Data(const std::vector< double >& time, const Data<T_Physic>& other) : Data(time,other._config) {}

template<Physic T_Physic>
std::vector<double> subfrequency(const std::vector<unsigned int>& freq_idx, const Data<T_Physic>& d)
{
    std::vector<double> subfreq(freq_idx.size());
    for (unsigned int f = 0; f < freq_idx.size(); f++)
    {
        subfreq[f] = d.frequency(freq_idx[f]);
    }
    return subfreq;
}

template<Physic T_Physic>
Data<T_Physic>::Data(const std::vector<unsigned int>& freq_idx, const Data<T_Physic>& other) : Data(subfrequency(freq_idx,other),other._config)
{
    for (unsigned int f = 0; f < nf(); f++)
    {
        for (unsigned int s = 0; s < ns(); s++)
        {
            for (unsigned int r = 0; r < nr(s); r++)
            {
                _value[f][s][r] = other.value(freq_idx[f],s,r);
            }
        }
    }
}

template<Physic T_Physic> void Data<T_Physic>::read(const std::vector<std::string>& filename)
{
    if ( filename.size() != nf() )
    {
        throw Exception("Number of Data file to read is different from the number of frequencies.");
    }
    for (unsigned int f = 0; f < nf(); f++)
    {
        /* msg::print << "Read " + filename[f] << msg::endl; */
        CSVio file(filename[f],';',OpeningMode::Reading);
        for (unsigned int s = 0; s < ns(); s++)
        {
            if( file.isEOF() )
            {
                throw Exception("Data file "+filename[f]+" has not enough shots");
            }
            for (unsigned int r = 0; r < nr(s); r++)
            {
                if( file.isEOL() && r != 0)
                {
                    throw Exception("Data file "+filename[f]+" shot #"+std::to_string(s)+" has not enough receivers");
                }
                file >> _value[f][s][r];
            }
            if( !file.isEOL() )
            {
                throw Exception("Data file "+filename[f]+" shot #"+std::to_string(s)+" has too many receivers");
            }
        }
        typename PointData<T_Physic>::Object tmp;
        file >> tmp;
        if( !file.isEOF() )
        {
            throw Exception("Data file "+filename[f]+" has too many shots");
        }
        file.close();
    }
}

template<Physic T_Physic> void Data<T_Physic>::write(std::string filename,std::string type) const
{
    std::string fname = filename;
    for (unsigned int f = 0; f < nf(); f++)
    {
        if(nf()!=1){fname = filename+"f"+std::to_string(f);}
        if(type=="csv")
        {
            CSVio file(fname,';',OpeningMode::NewFile);
            for (unsigned int s = 0; s < ns(); s++)
            {
                for (unsigned int r = 0; r < nr(s); r++)
                {
                    file << _value[f][s][r];
                }
                file << csv::endl;
            }
            file.close();
        }
        else
        {
            msg::warning << "Impossible to save data in another format than .csv." << msg::endl;
        }
    }
}

template<Physic T_Physic>
typename PointData<T_Physic>::Object Data<T_Physic>::value(unsigned int f,unsigned int s,unsigned int r) const
{
    isValid(f,s,r);
    return _value[f][s][r];
}

template<Physic T_Physic>
typename PointData<T_Physic>::Object Data<T_Physic>::value(unsigned int f, double xs, double xr) const
{
    if(_config->data_coordinate_isValid(xs,xr))
    {
        std::array<unsigned int,2> index = _config->data_coordinate_to_index(xs,xr);
        unsigned int s = index[0];
        unsigned int r = index[1];
        isValid(f,s,r);
        return _value[f][s][r];
    }
    else
    {
        msg::warning << "Data coordinate: ("<< std::to_string(xs) << "," << std::to_string(xr) << ")" << " is not valid." << msg::endl;
        return typename PointData<T_Physic>::Object();
    }
}

template<Physic T_Physic>
void Data<T_Physic>::value(unsigned int f,unsigned int s,unsigned int r,const typename PointData<T_Physic>::Object& value)
{
  isValid(f,s,r);
  _value[f][s][r] = value;
  _constant = false;
}

template<Physic T_Physic>
void Data<T_Physic>::value(const typename PointData<T_Physic>::Object& value)
{
    for (unsigned int f = 0; f < nf(); f++)
    {
        for (unsigned int s = 0; s < ns(); s++)
        {
            for (unsigned int r = 0; r < nr(s); r++)
            {
                _value[f][s][r] = value;
            }
        }
    }
    _constant = true;
}

template<Physic T_Physic>
void Data<T_Physic>::value(unsigned int f, const WaveMultiField<T_Physic>& w)
{
    for (unsigned int s = 0; s < ns(); s++)
    {
        for (unsigned int r = 0; r < nr(s); r++)
        {
            _value[f][s][r] = wave_to_data(s,r,_config,w);
        }
    }
    _constant = false;
}

template<Physic T_Physic>
void Data<T_Physic>::operator=(const Data<T_Physic>& other)
{
  areCompatible(*this,other);
  for (unsigned int f = 0; f < nf(); f++)
  {
      for (unsigned int s = 0; s < ns(); s++)
      {
          for (unsigned int r = 0; r < nr(s); r++)
          {
              value(f,s,r,other.value(f,s,r));
          }
      }
  }
  _constant = other.constant();
}

template<Physic T_Physic>
Data<T_Physic> Data<T_Physic>::operator+(const Data<T_Physic>& other) const
{
    areCompatible(*this,other);
    Data<T_Physic> sum(*this);
    for (unsigned int f = 0; f < nf(); f++)
    {
        for (unsigned int s = 0; s < ns(); s++)
        {
            for (unsigned int r = 0; r < nr(s); r++)
            {
                sum.value(f,s,r, this->value(f,s,r)+other.value(f,s,r) );
            }
        }
    }
    return sum;
}

template<Physic T_Physic>
Data<T_Physic> Data<T_Physic>::operator-(const Data<T_Physic>& other) const
{
    areCompatible(*this,other);
    Data<T_Physic> sub(*this);
    for (unsigned int f = 0; f < nf(); f++)
    {
        for (unsigned int s = 0; s < ns(); s++)
        {
            for (unsigned int r = 0; r < nr(s); r++)
            {
                sub.value(f,s,r, this->value(f,s,r)-other.value(f,s,r) );
            }
        }
    }
    return sub;
}

template<Physic T_Physic>
Data<T_Physic> Data<T_Physic>::operator*(const Data<T_Physic>& other) const
{
    areCompatible(*this,other);
    Data<T_Physic> out(*this);
    for (unsigned int f = 0; f < nf(); f++)
    {
        for (unsigned int s = 0; s < ns(); s++)
        {
            for (unsigned int r = 0; r < nr(s); r++)
            {
                out.value(f,s,r, wise_multiply<T_Physic>(this->value(f,s,r),other.value(f,s,r)) );
            }
        }
    }
    return out;
}

template<Physic T_Physic>
Data<T_Physic> Data<T_Physic>::operator/(const Data<T_Physic>& other) const
{
    areCompatible(*this,other);
    Data<T_Physic> out(*this);
    for (unsigned int f = 0; f < nf(); f++)
    {
        for (unsigned int s = 0; s < ns(); s++)
        {
            for (unsigned int r = 0; r < nr(s); r++)
            {
                out.value(f,s,r, wise_divide<T_Physic>(this->value(f,s,r),other.value(f,s,r)) );
            }
        }
    }
    return out;
}

template<Physic T_Physic>
Data<T_Physic> Data<T_Physic>::operator*(std::complex<double> a) const
{
    Data<T_Physic> mul(*this);
    for (unsigned int f = 0; f < nf(); f++)
    {
        for (unsigned int s = 0; s < ns(); s++)
        {
            for (unsigned int r = 0; r < nr(s); r++)
            {
                mul.value(f,s,r, this->value(f,s,r)*a );
            }
        }
    }
    return mul;
}

template<Physic T_Physic>
Data<T_Physic> Data<T_Physic>::operator/(std::complex<double> a) const
{
    Data<T_Physic> div(*this);
    for (unsigned int f = 0; f < nf(); f++)
    {
        for (unsigned int s = 0; s < ns(); s++)
        {
            for (unsigned int r = 0; r < nr(s); r++)
            {
                div.value(f,s,r, this->value(f,s,r)/a );
            }
        }
    }
    return div;
}

template class Data<Physic::acoustic>;
template class Data<Physic::electromagnetic>;
template class Data<Physic::elastodynamic>;
