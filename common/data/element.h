// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_DATA_ELEMENT
#define H_COMMON_DATA_ELEMENT

//FWI Library
#include "../../specific/physic.h"
#include "../../specific/data/element.h"
#include "../configuration.h"
#include "../wave/element.h"

/*
* Data
*/
template<Physic T_Physic>
class Data final
{
private:
    const std::vector< double > _freq;
    std::vector< std::vector< std::vector<typename PointData<T_Physic>::Object> > > _value;
    const ConfigurationInterface* const _config;
    bool _constant;
public:
    Data(const std::vector< double >& freq, const ConfigurationInterface* const config);
    Data(const std::vector<std::string>& filename, const std::vector< double >& freq, const ConfigurationInterface* const config);
    Data(const Data<T_Physic>& other);
    Data(const std::vector< double >& time, const Data<T_Physic>& other);
    Data(const std::vector<unsigned int>& freq_idx, const Data<T_Physic>& other);
    void write(std::string filename,std::string type = "csv") const;
    void read(const std::vector<std::string>& filename);

    typename PointData<T_Physic>::Object value(unsigned int f, unsigned int s, unsigned int r) const;
    typename PointData<T_Physic>::Object value(unsigned int f, double  xs, double xr) const;
    void value(unsigned int f, unsigned int s, unsigned int r, const typename PointData<T_Physic>::Object& value);
    void value(const typename PointData<T_Physic>::Object& value);
    void value(unsigned int f, const WaveMultiField<T_Physic>& w);
    unsigned int nf() const {return _freq.size();};
    double frequency(unsigned int f) const {return _freq[f];};
    const std::vector< double >& frequency() const {return _freq;};
    unsigned int ns() const {return _config->ns();};
    unsigned int nr(unsigned int s) const {return _config->nr(s);};
    std::vector<unsigned int> receiverPerShot() const {return _config->receiverPerShot();};
    bool constant() const {return _constant;};

    void operator=(const Data<T_Physic>& other);
    Data<T_Physic> operator+(const Data<T_Physic>& other)const;
    Data<T_Physic> operator-(const Data<T_Physic>& other)const;
    Data<T_Physic> operator*(const Data<T_Physic>& other)const;
    Data<T_Physic> operator/(const Data<T_Physic>& other)const;
    Data<T_Physic> operator*(std::complex<double> a)const;
    Data<T_Physic> operator/(std::complex<double> a)const;
private:
    bool isValid(unsigned int f,unsigned int s,unsigned r) const;
};

template<Physic T_Physic>
bool areCompatible(const Data<T_Physic>& d1,const Data<T_Physic>& d2)
{
    bool areCompatible = (d1.nf()==d2.nf());
    if(!areCompatible)
    {
        throw gmshfem::common::Exception("Data1 and Data2 are imcompatible (number of frequencies).");
    }
    for (unsigned int f = 0; f < d1.nf(); f++)
    {
        areCompatible = d1.frequency(f) == d2.frequency(f);
        if(!areCompatible)
        {
            throw gmshfem::common::Exception("Data1 and Data2 are imcompatible (frequency #"+std::to_string(f)+").");
        }
    }
    areCompatible = (d1.ns()==d2.ns());
    if(!areCompatible)
    {
        throw gmshfem::common::Exception("Data1 and Data2 are imcompatible (number of shots).");
    }
    for (unsigned int s = 0; s < d1.ns(); s++)
    {
        areCompatible = (d1.nr(s)==d2.nr(s));
        if(!areCompatible)
        {
            throw gmshfem::common::Exception("Data1 and Data2 are imcompatible (number of receivers, shot "+std::to_string(s)+").");
        }
    }
    return areCompatible;
};
template<Physic T_Physic>
double l2norm2(const Data<T_Physic>& d)
{
    double nrm = 0.;
    for (unsigned int f = 0; f < d.nf(); f++)
    {
        for (unsigned int s = 0; s < d.ns(); s++)
        {
            for (unsigned int r = 0; r < d.nr(s); r++)
            {
                nrm += norm2<T_Physic>(d.value(f,s,r));
            }
        }
    }
    return nrm;
}
template<Physic T_Physic>
std::complex<double> l2scalp(const Data<T_Physic>& d1,const Data<T_Physic>& d2)
{
    areCompatible(d1,d2);
    std::complex<double> scalp = 0.;
    for (unsigned int f = 0; f < d1.nf(); f++)
    {
        for (unsigned int s = 0; s < d1.ns(); s++)
        {
            for (unsigned int r = 0; r < d1.nr(s); r++)
            {
                scalp += conj<T_Physic>(d1.value(f,s,r)) * d2.value(f,s,r);
            }
        }
    }
    return scalp;
}
template<Physic T_Physic>
std::complex<double> l2scalpNoConj(const Data<T_Physic>& d1,const Data<T_Physic>& d2)
{
    areCompatible(d1,d2);
    std::complex<double> scalp = 0.;
    for (unsigned int f = 0; f < d1.nf(); f++)
    {
        for (unsigned int s = 0; s < d1.ns(); s++)
        {
            for (unsigned int r = 0; r < d1.nr(s); r++)
            {
                scalp += d1.value(f,s,r) * d2.value(f,s,r);
            }
        }
    }
    return scalp;
}
// Inner product are not feasible for that physic
template<>
inline  std::complex<double> l2scalp<Physic::electromagnetic>(const Data<Physic::electromagnetic>& d1,const Data<Physic::electromagnetic>& d2)
{
    throw gmshfem::common::Exception("Electromagnetism isn't supported here");
}
template<>
inline std::complex<double> l2scalp<Physic::elastodynamic>(const Data<Physic::elastodynamic>& d1, const Data<Physic::elastodynamic>& d2)
{
    throw gmshfem::common::Exception("Elastodynamics isn't supported here");
}
template<>
inline  std::complex<double> l2scalpNoConj<Physic::electromagnetic>(const Data<Physic::electromagnetic>& d1,const Data<Physic::electromagnetic>& d2)
{
    throw gmshfem::common::Exception("Electromagnetism isn't supported here");
}
template<>
inline std::complex<double> l2scalpNoConj<Physic::elastodynamic>(const Data<Physic::elastodynamic>& d1, const Data<Physic::elastodynamic>& d2)
{
    throw gmshfem::common::Exception("Elastodynamics isn't supported here");
}

template<Physic T_Physic>
Data<T_Physic> conj(const Data<T_Physic>& d)
{
    Data<T_Physic> cnj(d);
    for (unsigned int f = 0; f < d.nf(); f++)
    {
        for (unsigned int s = 0; s < d.ns(); s++)
        {
            for (unsigned int r = 0; r < d.nr(s); r++)
            {
                cnj.value(f,s,r, conj<T_Physic>(d.value(f,s,r)) );
            }
        }
    }
    return cnj;
};
template<Physic T_Physic>
Data<T_Physic> abs(const Data<T_Physic>& d)
{
    Data<T_Physic> out(d);
    for (unsigned int f = 0; f < d.nf(); f++)
    {
        for (unsigned int s = 0; s < d.ns(); s++)
        {
            for (unsigned int r = 0; r < d.nr(s); r++)
            {
                out.value(f,s,r, abs<T_Physic>(d.value(f,s,r))  );
            }
        }
    }
    return out;
};

template<Physic T_Physic>
Data<T_Physic> real(const Data<T_Physic>& d)
{
    Data<T_Physic> out(d);
    for (unsigned int f = 0; f < d.nf(); f++)
    {
        for (unsigned int s = 0; s < d.ns(); s++)
        {
            for (unsigned int r = 0; r < d.nr(s); r++)
            {
                out.value(f,s,r, real<T_Physic>(d.value(f,s,r)) );
            }
        }
    }
    return out;
};

template<Physic T_Physic>
Data<T_Physic> arg(const Data<T_Physic>& d)
{
    Data<T_Physic> out(d);
    for (unsigned int f = 0; f < d.nf(); f++)
    {
        for (unsigned int s = 0; s < d.ns(); s++)
        {
            for (unsigned int r = 0; r < d.nr(s); r++)
            {
                out.value(f,s,r, arg<T_Physic>(d.value(f,s,r)) );
            }
        }
    }
    return out;
};

#endif // H_COMMON_DATA_ELEMENT
