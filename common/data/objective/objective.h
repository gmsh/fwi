// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_DATA_OBJECTIVE
#define H_COMMON_DATA_OBJECTIVE

//GmshFem Library
#include "Formulation.h"
//FWI Library
#include "../state.h"
#include "../../wave/element.h"
#include "../../configuration.h"

//Forward declaration
class ModelUpdater;
template<Physic T_Physic>
class DataUpdater;

/*
* ObjectiveInterface
*/
template<Physic T_Physic>
class ObjectiveInterface
{
protected:
    const Data<T_Physic> _d0;
    Data<T_Physic> _v;
public:
    ObjectiveInterface(const Data<T_Physic>& d0): _d0(d0), _v(d0) {};
    virtual ~ObjectiveInterface() = default;

    unsigned int nf() const {return _d0.nf();};
    virtual double performance(const Data<T_Physic>& d) = 0;
    virtual const Data<T_Physic>& update(Type type, const DataStateEvaluator<T_Physic>& ds) = 0;

    virtual void link(ModelUpdater* const mu, DataUpdater<T_Physic>* const du) {};
    virtual void unlink() {};
    virtual bool modelIsObsolete() {return false;};//Returns true, if objective depends on model

    virtual void scale(double scale) {};
};

#endif //H_COMMON_DATA_OBJECTIVE
