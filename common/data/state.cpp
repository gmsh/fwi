// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmeshFem Library
#include "Exception.h"

//FWI Library
#include "state.h"

using namespace gmshfem::common;
/*
* Class DataState
*/
template<Physic T_Physic>
const Data<T_Physic>& DataState<T_Physic>::state(Type type) const
{
    if(_isUpToDate[type]){return _state[type];}
    else
    {
        throw Exception(type_to_string(type) + " data is not up to date while required.");
    }
}

template class DataState<Physic::acoustic>;
template class DataState<Physic::electromagnetic>;
template class DataState<Physic::elastodynamic>;
