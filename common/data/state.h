// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_DATA_STATE
#define H_COMMON_DATA_STATE

//Standard Library
#include <array>

//GmshFWI Library
#include "../enum.h"
#include "element.h"
#include "../configuration.h"

//Forward declaration
template<Physic T_Physic>
class DataUpdater;

/*
* DataStateEvaluator
*/
template<Physic T_Physic>
class DataStateEvaluator
{
public:
    virtual const Data<T_Physic>& state(Type type) const = 0;
    void write(Type type, std::string name, std::string fmt = "csv") const {state(type).write(name,fmt);};
};

/*
* DataState
*/
template<Physic T_Physic>
class DataState final : public DataStateEvaluator<T_Physic>
{
private:
    std::array<Data<T_Physic>,5> _state;
    std::array<bool,5> _isUpToDate;
public:
    DataState(const std::vector< double >& freq, const ConfigurationInterface* const config):
    _state{
        Data<T_Physic>(freq,config),
        Data<T_Physic>(freq,config),
        Data<T_Physic>(freq,config),
        Data<T_Physic>(freq,config),
        Data<T_Physic>(freq,config)
    }, _isUpToDate{false,false,false,false,false} {};

    virtual const Data<T_Physic>& state(Type type) const;

    friend class DataUpdater<T_Physic>;
};

#endif // H_COMMON_DATA_STATE
