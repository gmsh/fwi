// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#include "updater.h"
#include "../wave/updater.h"

template<Physic T_Physic>
void DataUpdater<T_Physic>::update(Type type, const ModelStateEvaluator& m)
{
    if(_ds._isUpToDate[type]){return;}

    std::array<bool,4> waveNeedToBeUpToDate = {false,false,false,false};
    switch (type)
    {
        case Type::FS:
            waveNeedToBeUpToDate[Type::FS] = true;
            for (unsigned int f = 0; f < _nf; f++)
            {
                _ds._state[type].value(f, (_wu[f].get(waveNeedToBeUpToDate,m)).state(type) );
            }
            break;
        case Type::AS: case Type::DS:
            update(Type::FS,m);
            _ds._state[type] = _objective->update(type,_ds);
            break;
        case Type::PFS:
            waveNeedToBeUpToDate[Type::PFS] = true;
            for (unsigned int f = 0; f < _nf; f++)
            {
                _ds._state[type].value(f, (_wu[f].get(waveNeedToBeUpToDate,m)).state(type) );
            }
            break;
        case Type::PAS:
            update(Type::FS,m);
            update(Type::PFS,m);
            _ds._state[type] = _objective->update(type,_ds);
            break;
    }

    _ds._isUpToDate[type] = true;
}

template<Physic T_Physic>
const DataStateEvaluator<T_Physic>& DataUpdater<T_Physic>::get(std::array<bool,5> needToBeUpToDate, const ModelStateEvaluator& m)
{
    for (unsigned int t = 0; t < 5; t++)
    {
        Type type = ((Type) t);
        if(needToBeUpToDate[type]){update(type,m);}
    }
    return _ds;
}

template<Physic T_Physic>
void DataUpdater<T_Physic>::isObsolete(std::array<bool,5> NoMoreUpToDate)
{
    for (unsigned int t = 0; t < 5; t++)
    {
        if(NoMoreUpToDate[t]){_ds._isUpToDate[t] = false;}
    }
}

template class DataUpdater<Physic::acoustic>;
template class DataUpdater<Physic::electromagnetic>;
template class DataUpdater<Physic::elastodynamic>;
