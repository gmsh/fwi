// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_DATA_UPDATER
#define H_COMMON_DATA_UPDATER

//FWI Library
//#include "../../specific/physic.h"
#include "state.h"
#include "objective/objective.h"
#include "../model/state.h"
//#include "../configuration.h"

//Forward declaration
template<Physic T_Physic>
class WaveUpdater;

/*
* DataUpdater
*/
template<Physic T_Physic>
class DataUpdater final
{
private:
    const unsigned int _nf; //will disappear when equation is multifreq
    DataState<T_Physic> _ds;
    WaveUpdater<T_Physic>* const _wu;
    ObjectiveInterface<T_Physic>* const _objective;

    void update(Type type, const ModelStateEvaluator& m);
public:
    DataUpdater(const std::vector< double >& freq, const ConfigurationInterface* const config, WaveUpdater<T_Physic>* wu, ObjectiveInterface<T_Physic>* objective): _nf(freq.size()), _ds(freq,config), _wu(wu), _objective(objective)
    {
        if(_objective != nullptr)
        {
            if(_objective->nf() != _nf)
            {
                throw gmshfem::common::Exception("Objective and updater do not have the same number of frequencies");
            }
        }
    };

    const DataStateEvaluator<T_Physic>& get(std::array<bool,5> needToBeUpToDate, const ModelStateEvaluator& m);
    void isObsolete(std::array<bool,5> NoMoreUpToDate);
};

#endif // H_COMMON_DATA_UPDATER
