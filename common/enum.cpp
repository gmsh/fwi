// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFWI Library
#include "enum.h"

Type to_type(Order order)
{
    switch (order)
    {
        case Order::FST: return Type::AS;
        case Order::SCD: return Type::PAS;
        case Order::DIAG: return Type::DS;
    }
}

Order to_order(Type type)
{
    switch (type)
    {
        default:
        case Type::FS: return Order::FST;
        case Type::AS: return Order::FST;
        case Type::PFS: return Order::SCD;
        case Type::PAS: return Order::SCD;
    }
}

std::string type_to_string(Type type)
{
    switch (type)
    {
        case Type::FS: return "forward";
        case Type::AS: return "adjoint";
        case Type::PFS: return "perturbed forward";
        case Type::PAS: return "perturbed adjoint";
        case Type::DS: return "diagonal";
        default: return "";
    }
}


std::string order_to_string(Order order)
{
    switch (order)
    {
        default:
        case Order::FST: return "first";
        case Order::SCD: return "second";
        case Order::DIAG: return "diagonal";
    }
}

std::string support_to_string(Support support)
{
    switch (support)
    {
        case Support::BLK: return "bulk";
        case Support::BND: return "boundary";
        default: return "";
    }
}
