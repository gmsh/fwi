// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_TYPE
#define H_TYPE

//Standard Library
#include <string>

//GmshFWI Library

/* Forward, Perturbed Forward, Adjoint, Perturbed Adjoint */
enum Type : unsigned int {FS=0, PFS=1, AS=2, PAS=3, DS=4};
enum Order : unsigned int {FST=0, SCD=1, DIAG=2};
enum Support : unsigned int {BLK=0, BND=1};
Order to_order(Type type);
Type to_type(Order order);
std::string order_to_string(Order order);
std::string type_to_string(Type type);
std::string support_to_string(Support support);


#endif // H_TYPE
