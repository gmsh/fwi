// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_FUNCTIONAL
#define H_FUNCTIONAL

//GmshFWI Library
#include "model/element.h"
#include "model/innerproduct/innerproduct.h"

class FunctionalInterface
{
protected:
    unsigned int _iteration;
public:
    virtual ~FunctionalInterface() = default;

    virtual const InnerProductInterface* const innerproduct() const = 0;

    virtual void iteration(unsigned int iteration){_iteration=iteration;};
    virtual void setModel(const ModelField& m) = 0;
    virtual void setModel(const ModelFunction& m) = 0;
    virtual void setModelPerturbation(const ModelField& dm) = 0;
    virtual void setModelPerturbation(const ModelFunction& dm) = 0;

    virtual const ModelField& m() const = 0;
    virtual const ModelField& dm()const = 0;

    /* performance */
    virtual double performance() = 0;
    template<class M> double performance(const M &m)
    {
    setModel(m);
    return performance();
    };

    /*
    * First order
    */
    /* gradient */
    virtual const ModelField& gradient() = 0;
    template<class M> const ModelField& gradient(const M &m)
    {
        setModel(m);
        return gradient();
    };

    /* directional1 */
    virtual double directional1(const ModelFunction &dm) = 0;
    virtual double directional1(const ModelField &dm) = 0;

    template<class M,class DM>
    double directional1(const M &m,const DM &dm)
    {
        setModel(m);
        return directional1(dm);
    };

    /*
    * Second order
    */
    /* hessian */
    virtual const ModelField& hessian() = 0;
    template<class DM>
    const ModelField& hessian(const DM &dm2)
    {
        setModelPerturbation(dm2);
        return hessian();
    }
    template<class M,class DM>
    const ModelField& hessian(const M &m,const DM &dm2)
    {
        setModel(m);
        return hessian(dm2);
    }

    /* directional2 */
    virtual double directional2(const ModelFunction &dm2) = 0;
    virtual double directional2(const ModelField &dm2) = 0;

    template<class DM1,class DM2>
    double directional2(const DM1 &dm1,const DM2 &dm2)
    {
        setModelPerturbation(dm2);
        return directional2(dm1);
    }

    template<class M,class DM1,class DM2>
    double directional2(const M &m,const DM1 &dm1,const DM2 &dm2)
    {
        setModel(m);
        return directional2(dm1,dm2);
    }
};

#endif // H_FUNCTIONAL
