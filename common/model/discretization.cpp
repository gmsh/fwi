// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard library
//GmshFem library
#include "Exception.h"
#include "Message.h"
//FWI
#include "discretization.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::field;

namespace model
{
  /*
  *  class Discretization
  */
  Discretization::Discretization(const GmshFem& gmshFem, std::string suffix)
  {
    msg::print << "Read model discretization parameters" << msg::endl;
    std::string str_buffer;
    if(!(
            (
                gmshFem.userDefinedParameter(_functionSpaceDegree, "model_FunctionSpaceDegree")
                ||
                gmshFem.userDefinedParameter(_functionSpaceDegree, "model_FunctionSpaceDegree"+suffix)
            )
    ))
    {
      throw common::Exception("Model function space degree could not be found.");
    }
    else if(!(
                gmshFem.userDefinedParameter(str_buffer, "model_FunctionSpaceType")
                ||
                gmshFem.userDefinedParameter(str_buffer, "model_FunctionSpaceType"+suffix)
             )

         )
    {
     msg::warning << "Model function space type could not be found." << msg::endl;
    }
    else
    {
      if(str_buffer=="Lagrange"){_functionSpaceType = FunctionSpaceTypeForm0::Lagrange;}
      else if(str_buffer=="HierarchicalH1"){_functionSpaceType = FunctionSpaceTypeForm0::HierarchicalH1;}
      else
      {
          throw common::Exception("Unknown function space type: " + str_buffer);
      }
    }
  }
} // namespace model
