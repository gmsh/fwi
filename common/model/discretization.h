// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_MODEL_DISCRETIZATION
#define H_MODEL_DISCRETIZATION

//Standard library
#include <string>
//GmshFem library
#include "GmshFem.h"
#include "FunctionSpace.h"
#include "Message.h"

namespace model
{
  class Discretization
  {
  private:
      gmshfem::field::FunctionSpaceTypeForm0 _functionSpaceType;
      unsigned int _functionSpaceDegree;
  public:
      Discretization(const gmshfem::common::GmshFem& gmshFem, std::string suffix = "");
      gmshfem::field::FunctionSpaceTypeForm0 functionSpaceType() const {return _functionSpaceType;};
      unsigned int functionSpaceDegree() const {return _functionSpaceDegree;};
      void functionSpaceDegree(unsigned int order) {_functionSpaceDegree=order;};
  };
}; // namespace model

#endif // H_MODEL_DISCRETIZATION
