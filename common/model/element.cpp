// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard library

//GmshFem library
#include "Exception.h"
#include "Post.h"

//GmshFWI Library
#include "element.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::post;

/*
* ModelMonoField
*/
void ModelMonoField::oppose()
{
    for (auto it = this->begin(); it != this->end(); it++)
    {
        it->second = -it->second;
    }
}

void ModelMonoField::conjugate()
{
    for (auto it = this->begin(); it != this->end(); it++)
    {
        it->second = std::conj(it->second);
    }
}

void ModelMonoField::realify()
{
    for (auto it = this->begin(); it != this->end(); it++)
    {
        it->second = std::real(it->second);
    }
}

void ModelMonoField::imaginarify()
{
    for (auto it = this->begin(); it != this->end(); it++)
    {
        it->second = std::complex<double>(0.,std::imag(it->second));
    }
}
ModelMonoField ModelMonoField::operator*(double a) const
{
    ModelMonoField output = *this;
    for (auto it = output.begin(); it != output.end(); it++)
    {
        it->second *= a;
    }
    return output;
}
ModelMonoField ModelMonoField::operator*(std::complex<double> a) const
{
    ModelMonoField output = *this;
    for (auto it = output.begin(); it != output.end(); it++)
    {
        it->second *= a;
    }
    return output;
}
ModelMonoField ModelMonoField::operator/(double a) const
{
    ModelMonoField output = *this;
    for (auto it = output.begin(); it != output.end(); it++)
    {
        it->second /= a;
    }
    return output;
}
ModelMonoField ModelMonoField::operator/(std::complex<double> a) const
{
    ModelMonoField output = *this;
    for (auto it = output.begin(); it != output.end(); it++)
    {
        it->second /= a;
    }
    return output;
}

void ModelMonoField::operator+=(const ModelMonoField& other)
{
      if( *this->getFunctionSpace() != *other.getFunctionSpace())
      {
        throw common::Exception("Unable to apply 'operator+=' to fields having different function spaces");
      }

      for(auto it = other.begin(); it != other.end(); ++it)
      {
        const dofs::Dof *dof = it->first;
        std::complex<double> value = it->second;
        dofs::SearchDof sdof(dof->numType() - GMSHFEM_DOF_FIELD_OFFSET * other.tag() + GMSHFEM_DOF_FIELD_OFFSET * this->tag(), dof->entity());
        auto itFind = this->findDof(&sdof);
        if(itFind == this->end())
        {
            throw common::Exception("Unable to apply 'operator+=' to fields having different degrees of freedom (dofs)");
        }
        else
        {
          itFind->second += value;
        }
      }
}

ModelMonoField ModelMonoField::operator+(const ModelMonoField& other) const
{
      if( *this->getFunctionSpace() != *other.getFunctionSpace())
      {
        throw common::Exception("Unable to apply 'operator+' to fields having different function spaces");
      }

      ModelMonoField output = *this;

      for(auto it = other.begin(); it != other.end(); ++it)
      {
        const dofs::Dof *dof = it->first;
        std::complex<double> value = it->second;
        dofs::SearchDof sdof(dof->numType() - GMSHFEM_DOF_FIELD_OFFSET * other.tag() + GMSHFEM_DOF_FIELD_OFFSET * output.tag(), dof->entity());
        auto itFind = output.findDof(&sdof);
        if(itFind == output.end())
        {
            throw common::Exception("Unable to apply 'operator+' to fields having different degrees of freedom (dofs)");
        }
        else
        {
          itFind->second += value;
        }
      }

      output.name(this->name());

      return output;
}

void ModelMonoField::operator-=(const ModelMonoField& other)
{
      if( *this->getFunctionSpace() != *other.getFunctionSpace())
      {
        throw common::Exception("Unable to apply 'operator-=' to fields having different function spaces");
      }

      for(auto it = other.begin(); it != other.end(); ++it)
      {
        const dofs::Dof *dof = it->first;
        std::complex<double> value = it->second;
        dofs::SearchDof sdof(dof->numType() - GMSHFEM_DOF_FIELD_OFFSET * other.tag() + GMSHFEM_DOF_FIELD_OFFSET * this->tag(), dof->entity());
        auto itFind = this->findDof(&sdof);
        if(itFind == this->end())
        {
            throw common::Exception("Unable to apply 'operator-=' to fields having different degrees of freedom (dofs)");
        }
        else
        {
            itFind->second -= value;
        }
      }
}

ModelMonoField ModelMonoField::operator-(const ModelMonoField& other) const
{
      if( *this->getFunctionSpace() != *other.getFunctionSpace())
      {
        throw common::Exception("Unable to apply 'operator+' to fields having different function spaces");
      }

      ModelMonoField output = *this;

      for(auto it = other.begin(); it != other.end(); ++it)
      {
        const dofs::Dof *dof = it->first;
        std::complex<double> value = it->second;
        dofs::SearchDof sdof(dof->numType() - GMSHFEM_DOF_FIELD_OFFSET * other.tag() + GMSHFEM_DOF_FIELD_OFFSET * output.tag(), dof->entity());
        auto itFind = output.findDof(&sdof);
        if(itFind == output.end())
        {
            throw common::Exception("Unable to apply 'operator+' to fields having different degrees of freedom (dofs)");
        }
        else
        {
          itFind->second -= value;
        }
      }

      output.name(this->name());

      return output;
}

/*
* ModelField
*/
void ModelField::write(std::string name, std::string type, std::string path) const
{
    for (unsigned int c = 0; c < size(); c++)
    {
        (*this)[c].write(name+"c"+std::to_string(c),type,path);
    }
};

void ModelField::oppose()
{
    for (auto it = this->begin(); it != this->end(); it++)
    {
        it->oppose();
    }
}

void ModelField::conjugate()
{
    for (auto it = this->begin(); it != this->end(); it++)
    {
        it->conjugate();
    }
}

void ModelField::realify()
{
    for (auto it = this->begin(); it != this->end(); it++)
    {
        it->realify();
    }
}

void ModelField::imaginarify()
{
    for (auto it = this->begin(); it != this->end(); it++)
    {
        it->imaginarify();
    }
}
void ModelField::projection(std::vector<unsigned int> components)
{
    for (unsigned int c = 0; c < size(); c++)
    {
        if( std::find(components.begin(),components.end(),c) == components.end() )
        {
            (*this)[c].setValuesToZero();
        }
    }
}
ModelField ModelField::operator*(double a) const
{
    ModelField output = *this;
    for (auto it = output.begin(); it != output.end(); it++)
    {
        *it = *it * a;
    }
    return output;
}
ModelField ModelField::operator*(std::complex<double> a) const
{
    ModelField output = *this;
    for (auto it = output.begin(); it != output.end(); it++)
    {
        *it = *it * a;
    }
    return output;
}
ModelField ModelField::operator/(double a) const
{
    ModelField output = *this;
    for (auto it = output.begin(); it != output.end(); it++)
    {
        *it = *it / a;
    }
    return output;
}
ModelField ModelField::operator/(std::complex<double> a) const
{
    ModelField output = *this;
    for (auto it = output.begin(); it != output.end(); it++)
    {
        *it = *it / a;
    }
    return output;
}

void ModelField::operator+=(const ModelField& other)
{
      if( this->size() != other.size() )
      {
        throw common::Exception("Unable to apply 'operator+=' to multifields having different sizes");
      }

      for(unsigned int c = 0; c < this->size(); ++c)
      {
          (*this)[c] += other[c];
      }
}

ModelField ModelField::operator+(const ModelField& other) const
{
      if( this->size() != other.size() )
      {
        throw common::Exception("Unable to apply 'operator+' to multifields having different sizes");
      }

      ModelField output;
      for(unsigned int c = 0; c < this->size(); ++c)
      {
          output.emplace_back( (*this)[c] + other[c] );
      }
      return output;
}

void ModelField::operator-=(const ModelField& other)
{
      if( this->size() != other.size() )
      {
        throw common::Exception("Unable to apply 'operator-=' to multifields having different sizes");
      }

      for(unsigned int c = 0; c < this->size(); ++c)
      {
          (*this)[c] -= other[c];
      }
}

ModelField ModelField::operator-(const ModelField& other) const
{
      if( this->size() != other.size() )
      {
        throw common::Exception("Unable to apply 'operator+' to multifields having different sizes");
      }

      ModelField output;

      for(unsigned int c = 0; c < this->size(); ++c)
      {
          output.emplace_back( (*this)[c] - other[c] );
      }

      return output;
}

void ModelField::setValuesToZero()
{
    for (auto it = begin(); it < end(); it++)
    {
        it->setValuesToZero();
    }
}

/*
* ModelFunction
*/
ModelFunction ModelFunction::operator*(double a) const
{
    ModelFunction output = *this;
    for (auto it = output.begin(); it != output.end(); it++)
    {
        *it = *it * a;
    }
    return output;
}
ModelFunction ModelFunction::operator*(std::complex<double> a) const
{
    ModelFunction output = *this;
    for (auto it = output.begin(); it != output.end(); it++)
    {
        *it = *it * a;
    }
    return output;
}
ModelFunction ModelFunction::operator+(const ModelFunction& other) const
{
    compatible_model(*this,other);
    ModelFunction output;
    for (unsigned int c = 0; c < size(); c++)
    {
        output.emplace_back((*this)[c] + other[c]);
    }
    return output;
}
ModelFunction ModelFunction::operator-(const ModelFunction& other) const
{
    compatible_model(*this,other);
    ModelFunction output;
    for (unsigned int c = 0; c < size(); c++)
    {
        output.emplace_back((*this)[c] - other[c]);
    }
    return output;
}
ModelFunction ModelFunction::operator*(const ModelFunction& other) const
{
    compatible_model(*this,other);
    ModelFunction output;
    for (unsigned int c = 0; c < size(); c++)
    {
        output.emplace_back((*this)[c] * other[c]);
    }
    return output;
}
ModelFunction ModelFunction::operator/(const ModelFunction& other) const
{
    compatible_model(*this,other);
    ModelFunction output;
    for (unsigned int c = 0; c < size(); c++)
    {
        output.emplace_back((*this)[c] / other[c]);
    }
    return output;
}

bool compatible_model(const ModelFunction& m1,const ModelFunction& m2)
{
    if( m1.size() != m2.size() )
    {
        throw common::Exception("Multifields have different sizes");
        return false;
    }
    return true;
}
