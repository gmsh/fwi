// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_MODEL_ELEMENT
#define H_MODEL_ELEMENT

//GmshFEM Library
#include "FieldInterface.h"
#include "Function.h"
#include "Post.h"
//GmshFWI Library
#include "discretization.h"

/*
* ModelMonoField
*/
class ModelMonoField final : public gmshfem::field::Field< std::complex<double> , gmshfem::field::Form::Form0 >
{
public:
    ModelMonoField() : gmshfem::field::Field< std::complex<double> , gmshfem::field::Form::Form0 >() {};
    ModelMonoField(std::string name,const gmshfem::domain::Domain &domain, std::string gmodel, const model::Discretization& m_discret): gmshfem::field::Field< std::complex<double> , gmshfem::field::Form::Form0 >(name, domain, m_discret.functionSpaceType(), m_discret.functionSpaceDegree(), gmodel) {};
    ModelMonoField(const gmshfem::field::Field< std::complex<double> , gmshfem::field::Form::Form0 >& other) : gmshfem::field::Field< std::complex<double> , gmshfem::field::Form::Form0 >(other) {};

    void write(std::string name, std::string type="pos", std::string path="") const {gmshfem::post::save(*this,this->domain(),name,type,path);}

    unsigned int degree() const {return this->getFunctionSpace()->order();};

    void oppose();
    void conjugate();
    void realify();
    void imaginarify();
    void operator+=(const ModelMonoField& other);
    void operator-=(const ModelMonoField& other);
    ModelMonoField operator+(const ModelMonoField& other) const;
    ModelMonoField operator-(const ModelMonoField& other) const;
    ModelMonoField operator*(double a) const;
    ModelMonoField operator*(std::complex<double> a) const;
    ModelMonoField operator/(double a) const;
    ModelMonoField operator/(std::complex<double> a) const;
};

/*
* ModelField
*/
// If many components it might be more efficient to implement ModelField with a single field and a dofs marix. Similar to MultiWaveField
class ModelField final : public std::vector< ModelMonoField >
{
public:
    ModelField(){};

    ModelField(unsigned int c,const ModelMonoField& other): std::vector<ModelMonoField>(c,other) {};

    void write(std::string name, std::string type="pos", std::string path="") const;

    void oppose();
    void conjugate();
    void realify();
    void imaginarify();
    void projection(std::vector<unsigned int> components);
    void operator+=(const ModelField& other);
    void operator-=(const ModelField& other);
    ModelField operator+(const ModelField& other) const;
    ModelField operator-(const ModelField& other) const;
    ModelField operator*(double a) const;
    ModelField operator*(std::complex<double> a) const;
    ModelField operator/(double a) const;
    ModelField operator/(std::complex<double> a) const;

    void setValuesToZero();
};

/*
* ModelMonoFunction
*/
using ModelMonoFunction = gmshfem::function::ScalarFunction< std::complex<double> >;

/*
* ModelFunction
*/
class ModelFunction : public std::vector<ModelMonoFunction>
{
public:
    ModelFunction(unsigned int size = 0){resize(size);};
    ModelFunction(unsigned int c,const ModelMonoFunction& other): std::vector<ModelMonoFunction>(c,other) {};
    ModelFunction(const ModelField& modelfield)
    {
        unsigned int N = modelfield.size();
        this->resize(N);
        for (unsigned int c = 0; c < N; c++)
        {
            (*this)[c] = ModelMonoFunction(modelfield[c]);
        }
    };
    ModelFunction operator+(const ModelFunction& other) const;
    ModelFunction operator-(const ModelFunction& other) const;
    ModelFunction operator*(double a) const;
    ModelFunction operator*(std::complex<double> a) const;
    ModelFunction operator*(const ModelFunction& other) const;
    ModelFunction operator/(const ModelFunction& other) const;
};
bool compatible_model(const ModelFunction& m1,const ModelFunction& m2);
#endif //H_MODEL_ELEMENT
