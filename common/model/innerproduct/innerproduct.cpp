// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "AlgebraicFunctions.h"

//GmshFWI Library
#include "innerproduct.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::field;

static const std::complex< double > im = std::complex< double >(0., 1.);

/*
* DifferentialInnerProductInterface
*/
DifferentialInnerProductInterface::DifferentialInnerProductInterface(const ConfigurationInterface* const config, const gmshfem::common::GmshFem& gmshFem,std::string suffix)
: InnerProductInterface(config), _m_size(_config->model_size()), _j(), _systemAreUpToDate(false),_systemAreFactorized(false)
{
    for (unsigned int c = 0; c < _m_size; c++)
    {
         _formulation.emplace_back("model_formulation_component"+std::to_string(c));
    }
    if(!gmshFem.userDefinedParameter(_integrationType, "integration_type"))
    {
      throw Exception("Model integration type could not be found.");
    }
    if(!(
        gmshFem.userDefinedParameter(_integrationDegreeBlk, "innerproduct_integration_degree_blk")
        ||
        gmshFem.userDefinedParameter(_integrationDegreeBlk, "innerproduct_integration_degree_blk"+suffix)
    ))
    {
      throw Exception("Inner product integration degree (bulk) could not be found.");
    }
    if(!(
        gmshFem.userDefinedParameter(_integrationDegreeBnd, "innerproduct_integration_degree_bnd")
        ||
        gmshFem.userDefinedParameter(_integrationDegreeBnd, "innerproduct_integration_degree_bnd"+suffix)
    ))
    {
      throw Exception("Inner product integration degree (boundary) could not be found.");
    }
}

DifferentialInnerProductInterface::DifferentialInnerProductInterface(const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem,std::string suffix)
: DifferentialInnerProductInterface(config, gmshFem, suffix)
{
    _j = ModelField(_m_size,ModelMonoField("_j",_config->model_evaldomain(),_config->model_gmodel(),m_discret));
}

DifferentialInnerProductInterface::DifferentialInnerProductInterface(const ConfigurationInterface* const config, const ModelField& m, const gmshfem::common::GmshFem& gmshFem,std::string suffix)
: DifferentialInnerProductInterface(config, gmshFem, suffix)
{
    _j = m;
}

const ModelField& DifferentialInnerProductInterface::update(const Sensitivity& bulk_sensi, const Sensitivity& boundary_sensi)
{
    if(!_systemAreUpToDate)
    {
        for (unsigned int c = 0; c < _m_size; c++)
        {
            _formulation[c].removeSystem();
            _formulation[c].initSystem();
        }
        _systemAreFactorized=false;
        setLHS();
        for (unsigned int c = 0; c < _m_size; c++)
        {
            _formulation[c].pre();
            _formulation[c].assemble();
            _formulation[c].removeTerms();
        }
        _systemAreUpToDate=true;
    }

    setRHS(bulk_sensi,boundary_sensi);

    for (unsigned int c = 0; c < _m_size; c++)
    {
        _formulation[c].assemble();
        _formulation[c].removeTerms();

        _formulation[c].solve(_systemAreFactorized);
        _formulation[c].setRHSToZero();
    }
    _systemAreFactorized=true;

    return _j;
}

std::complex<double> DifferentialInnerProductInterface::product(const ModelField& m1, const ModelField& m2) const
{
    if( m1.size() != _m_size || m2.size() != _m_size)
    {
        throw Exception("Inner product model fields do not have the right number of components: m1.size = " + std::to_string(m1.size()) + " m2.size = " + std::to_string(m2.size()) + " _m_size = " + std::to_string(_m_size));
    }

    std::complex<double> sum = 0.;
    algebra::MatrixCRSFast< std::complex<double> > M;
    algebra::Vector< std::complex<double> > m1v, m2v;
    for(unsigned int c = 0; c < _m_size; c++)
    {
        _formulation[c].getLHS(M);

        (m1[c]).getAllVector(m1v);//getUnknown only?
        (m2[c]).getAllVector(m2v);//getUnknown only?
        for (unsigned int i = 0; i < m1v.size(); i++)
        {
            m1v[i] = std::conj(m1v[i]);
        }
        sum += bilinear(m1v, M, m2v);
    }
    return sum;
}
