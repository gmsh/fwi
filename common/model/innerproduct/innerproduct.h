// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_MODEL_INNERPRODUCT
#define H_COMMON_MODEL_INNERPRODUCT

//GmshFem Library
#include "Formulation.h"

//FWI Library
#include "../state.h"
#include "../discretization.h"
#include "../../configuration.h"
#include "../../sensitivity/updater.h"

//Forward declaration
class ModelPreconditionerUpdater;
class ModelUpdater;

/*
* InnerProductInterface
*/
class InnerProductInterface
{
protected:
    const ConfigurationInterface* const _config;
    unsigned int _iteration;
public:
    InnerProductInterface(const ConfigurationInterface* const config): _config(config) {};
    virtual ~InnerProductInterface() = default;

    virtual void link(ModelUpdater* const mu, SensitivityUpdater* const su) {};
    virtual void unlink() {};
    virtual const ModelField& update(const Sensitivity& bulk_sensi, const Sensitivity& boundary_sensi) = 0;

    virtual std::complex<double> product(const ModelField& m1, const ModelField& m2) const = 0;
    virtual std::complex<double> penalization(const ModelField& m1,const ModelField& m2) const {return product(m1,m2);};

    virtual void modelIsObsolete() {};

    virtual void scale(double scale) {};
    virtual bool iteration(double iteration) {_iteration=iteration;return false;}; //return: is obsolete at iteration ?
};

/*
* DifferentialInnerProductInterface
*/
class DifferentialInnerProductInterface: public InnerProductInterface
{
protected:
    const unsigned int _m_size;
    ModelField _j;

    std::vector<gmshfem::problem::Formulation< std::complex< double > >> _formulation;
    std::string _integrationType;
    unsigned int _integrationDegreeBlk;
    unsigned int _integrationDegreeBnd;
    bool _systemAreUpToDate;
    bool _systemAreFactorized;
private:
    DifferentialInnerProductInterface(const ConfigurationInterface* const config, const gmshfem::common::GmshFem& gmshFem, std::string suffix = "" );
public:
    DifferentialInnerProductInterface(const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix = "" );
    DifferentialInnerProductInterface(const ConfigurationInterface* const config, const ModelField& m, const gmshfem::common::GmshFem& gmshFem, std::string suffix = "" );
    virtual const ModelField& update(const Sensitivity& bulk_sensi, const Sensitivity& boundary_sensi);
    std::string integrationType(unsigned int degree) const {return _integrationType + std::to_string(degree);};

    virtual std::complex<double> product(const ModelField& m1, const ModelField& m2) const;
protected:
    virtual void setLHS() = 0;
    virtual void setRHS(const Sensitivity& bulk_sensi, const Sensitivity& boundary_sensi) = 0;
};

#endif //H_COMMON_MODEL_INNERPRODUCT
