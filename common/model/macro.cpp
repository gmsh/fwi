// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "CSVio.h"
#include "Formulation.h"
#include "Function.h"
#include "Exception.h"

//GmshFWI Library
#include "macro.h"

using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::equation;
using namespace gmshfem::function;

ModelMonoFunction bilinearInterpolation(std::string filename, bool isComplex)
{
    /*
    The 2D table has the following elements (nx = number of x values; ny = number of y values)

    nx ny
    x(1) x(2) x(3) ... x(nx)
    y(1) y(2) y(3) ... y(ny)
    g(x(1),y(1))  g(x(2),y(1))  g(x(3),y(1))  ... g(x(nx),y(1))
    g(x(1),y(2))  g(x(2),y(2))  g(x(3),y(2))  ... g(x(nx),y(2))
    g(x(1),y(3))  g(x(2),y(3))  g(x(3),y(3))  ... g(x(nx),y(3))
    ...
    g(x(1),y(ny)) g(x(2),y(ny)) g(x(3),y(ny)) ... g(x(nx),y(ny))
    */
    unsigned int nx;
    unsigned int ny;

    CSVio file(filename,' ',OpeningMode::Reading);
    file >> nx;
    file >> ny;
    if( !file.isEOL() )
    {
        throw Exception(filename+" does not have the appropriate format: first line has more than two values.");
    }
    std::vector<double> x(nx,0.);
    for (unsigned int j = 0; j < nx; j++)
    {
        file >> x[j];
        if( file.isEOL() ){break;}
    }

    std::vector<double> y(ny,0.);
    for (unsigned int i = 0; i < ny; i++)
    {
        file >> y[i];
        if( file.isEOL() ){break;}
    }
    if( (!std::is_sorted(x.begin(),x.end())) || (!std::is_sorted(y.begin(),y.end())) )
    {
        throw Exception(filename+" does not have the appropriate format: second and third line must be sorted");
    }
    if( (x.size() != nx) || (y.size() != ny) )
    {
        throw Exception(filename+" does not have the appropriate format: second and third line must have "+std::to_string(nx)+" and "+std::to_string(ny)+" values.");
    }

    std::vector< std::vector< std::complex<double> > > data(nx,std::vector< std::complex<double> >(ny,0.));
    for (unsigned int j = 0; j < ny; j++)
    {
        for (unsigned int i = 0; i < nx; i++)
        {
            double buffer_re=0.;
            double buffer_im=0.;
            file >> buffer_re;
            if(isComplex){file >> buffer_im;}
           data[i][j] = std::complex<double>(buffer_re,buffer_im);
        }
    }
    file.close();
    return gmshfem::function::bilinearInterpolation<std::complex<double>>(x,y,data);
}

ModelField rediscretize(const ModelFunction& m0, const ConfigurationInterface* const config, const model::Discretization& m_discret, std::string integrationType)
{
    integrationType += std::to_string(2*m_discret.functionSpaceDegree());
    ModelMonoField m("m",config->model_evaldomain(), config->model_gmodel(),m_discret);
    Formulation< std::complex< double > > formulation("Model rediscretization");
    formulation.integral(dof(m), tf(m),config->model_unknown(Support::BLK), integrationType);
    formulation.pre();
    formulation.assemble();
    formulation.removeTerms();
    ModelField mv;
    bool factorized = false;
    for (auto it = m0.begin(); it != m0.end(); it++)
    {
        formulation.integral(-(*it), tf(m),config->model_unknown(Support::BLK),integrationType);
        formulation.assemble();
        formulation.removeTerms();
        formulation.solve(factorized);
        factorized = true;
        formulation.setRHSToZero();
        mv.emplace_back(m);
    }
    return mv;
}

ModelField laplace_filter(const ModelFunction& m0, double scale, const ConfigurationInterface* const config, const model::Discretization& m_discret, std::string integrationType)
{
    double alpha1 = (scale / (2 * M_PI)) * (scale / (2 * M_PI));
    integrationType += std::to_string(2*m_discret.functionSpaceDegree());
    ModelMonoField m("m",config->model_evaldomain(), config->model_gmodel(),m_discret);
    Formulation< std::complex< double > > formulation("Model Laplace filter");
    formulation.integral(dof(m), tf(m),config->model_unknown(Support::BLK), integrationType);
    formulation.integral(alpha1*grad(dof(m)), grad(tf(m)),config->model_unknown(Support::BLK), integrationType);
    formulation.pre();
    formulation.assemble();
    formulation.removeTerms();
    ModelField mv;
    bool factorized = false;
    for (auto it = m0.begin(); it != m0.end(); it++)
    {
        formulation.integral(-(*it), tf(m),config->model_unknown(Support::BLK),integrationType);
        formulation.assemble();
        formulation.removeTerms();
        formulation.solve(factorized);
        factorized = true;
        formulation.setRHSToZero();
        mv.emplace_back(m);
    }
    return mv;
}
