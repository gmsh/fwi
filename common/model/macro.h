// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_MODEL_MACRO
#define H_COMMON_MODEL_MACRO

//GmshFWI
#include "element.h"
#include "../configuration.h"

ModelMonoFunction bilinearInterpolation(std::string filename, bool isComplex=false);

ModelField rediscretize(const ModelFunction& m0, const ConfigurationInterface* const config, const model::Discretization& m_discret, std::string integrationType);

ModelField laplace_filter(const ModelFunction & m0, double scale, const ConfigurationInterface* const config, const model::Discretization& m_discret, std::string integrationType);

#endif // H_COMMON_MODEL_MACRO
