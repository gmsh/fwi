// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_MODEL_PARAMETRIZATION
#define H_COMMON_MODEL_PARAMETRIZATION

//GmshFEM Library

//GmshFWI Library
#include "../sensitivity/state.h"
#include "../enum.h"

//Forward declaration
class ModelStateEvaluator;

/*
* ParametrizationInterface
*/
class ParametrizationInterface
{
protected:
    double _pulsation;

public:
    virtual ~ParametrizationInterface() = default;

    virtual ParametrizationInterface* clone() const = 0;

    virtual bool isModelIndependant() const {return false;};

    void pulsation(double pulsation) {_pulsation = pulsation;};

    /* operator() (Only forward): from external to internal */
    virtual ModelFunction operator()(const ModelFunction& m) const = 0;
    ModelFunction operator()(const ModelField& m) const
    {
        return operator()(ModelFunction(m));
    };
    std::vector<std::complex<double>> operator()(const std::vector<std::complex<double>>& m) const
    {
        ModelFunction in;
        for (unsigned int c = 0; c < size(); c++)
        {
            in.emplace_back(m[c]);
        }
        ModelFunction buffer = operator()(in);
        std::vector<std::complex<double>> out(buffer.size(),0.);
        for (unsigned int c = 0; c < buffer.size(); c++)
        {
            out[c] = gmshfem::post::evaluate(buffer[c],0.,0.,0.);
        }
        return out;
    };

    /* operator() (Only perturbed forward): from external to internal, ms in external parametrization. fct is the perturbation */
    virtual ModelFunction operator()(const ModelFunction& fct, const ModelFunction& m) const = 0;
    ModelFunction operator()(const ModelField& field, const ModelFunction& m) const
    {
        return operator()(ModelFunction(field),m);
    };

    /* operator() (Only adjoint and perturbed adjoint): from internal to external, ms is internal parametrization. Fct contains derivatives in internal. */
    virtual Sensitivity operator()(const SensitivityStateEvaluator& fct, Order order, Support support, const ModelStateEvaluator& ms) const = 0;

    /* size */
    virtual unsigned int size() const = 0;
};

#endif //H_COMMON_MODEL_PARAMETRIZATION
