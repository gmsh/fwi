// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library

//GmshFWI Library
#include "regularization.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::post;
using namespace gmshfem::equation;

/*
* RegularizationInterface
*/
RegularizationInterface::RegularizationInterface(const ConfigurationInterface* const config, const gmshfem::common::GmshFem& gmshFem, std::string suffix) : _config(config), _m_size(_config->model_size()), _lambda(_m_size,0.)
{
    if(!gmshFem.userDefinedParameter(_integrationType, "integration_type"))
    {
      throw Exception("Integration type could not be found.");
    }
    if(!(
        gmshFem.userDefinedParameter(_integrationDegreeBlk, "regularization_integration_degree_blk")
        ||
        gmshFem.userDefinedParameter(_integrationDegreeBlk, "regularization_integration_degree_blk"+suffix)
    ))
    {
      throw Exception("Regularization integration degree (bulk) could not be found.");
    }
    if(!(
        gmshFem.userDefinedParameter(_integrationDegreeBnd, "regularization_integration_degree_bnd")
        ||
        gmshFem.userDefinedParameter(_integrationDegreeBnd, "regularization_integration_degree_bnd"+suffix)
    ))
    {
      throw Exception("Regularization integration degree (boundary) could not be found.");
    }
    for (unsigned int c = 0; c < _m_size; c++)
    {
        std::string suffix1 = "c"+std::to_string(c);
        std::string root = "regularization_lambda";
        if(!(
            gmshFem.userDefinedParameter(_lambda[c], root) ||
            gmshFem.userDefinedParameter(_lambda[c], root+suffix)
            ||
            gmshFem.userDefinedParameter(_lambda[c], root+suffix1) ||
            gmshFem.userDefinedParameter(_lambda[c], root+suffix1+suffix)
        ))
        {
            throw Exception("Regularization strength could not be found.");
        }
    }
};

std::complex<double> RegularizationInterface::directional(const Sensitivity& sensitivity, const ModelFunction& dm2, Support support) const
{
    compatible_model(sensitivity,dm2);
    unsigned int N = sensitivity.size();
    std::complex<double> sum = 0.;
    std::string integration;
    switch (support)
    {
        case Support::BLK: integration = integrationType(_integrationDegreeBlk); break;
        case Support::BND: integration = integrationType(_integrationDegreeBnd); break;
    }
    for (unsigned int c = 0; c < N; c++)
    {
        sum += integrate( sensitivity[c] * dm2[c], _config->model_unknown(support), integration );
    }
    return sum;
}

void RegularizationInterface::lambda(double lambda,unsigned int c)
{
    if(c<_m_size) {_lambda[c]=lambda;}
    else
    {
        throw Exception("Component c"+std::to_string(c)+" does not exsit.");
    }
}

/*
* DifferentialRegularizationInterface
*/
DifferentialRegularizationInterface::DifferentialRegularizationInterface(const ConfigurationInterface* const config, const gmshfem::common::GmshFem& gmshFem, std::string suffix) : RegularizationInterface(config, gmshFem, suffix),_systemAreUpToDate(false), _systemAreFactorized(false)
{
    for (unsigned int c = 0; c < _m_size; c++)
    {
         _formulation.emplace_back("model_formulation_component"+std::to_string(c));
    }
};

DifferentialRegularizationInterface::DifferentialRegularizationInterface(const ConfigurationInterface* const config, const ModelField& m, const gmshfem::common::GmshFem& gmshFem, std::string suffix) : DifferentialRegularizationInterface(config, gmshFem, suffix)
{
    _k  = m;
}

Sensitivity DifferentialRegularizationInterface::update_sensitivity(Order order, Support support, const ModelState& ms)
{
    if(!_systemAreUpToDate)
    {
        for (unsigned int c = 0; c < _m_size; c++)
        {
            _formulation[c].removeSystem();
            _formulation[c].initSystem();
            _systemAreFactorized=false;
        }

        setLHS();

        for (unsigned int c = 0; c < _m_size; c++)
        {
            _formulation[c].pre();
            _formulation[c].assemble();
            _formulation[c].removeTerms();
            _systemAreUpToDate=true;
        }
    }

    setRHS(order,support,ms);

    for (unsigned int c = 0; c < _m_size; c++)
    {
        _formulation[c].assemble();
        _formulation[c].removeTerms();

        _formulation[c].solve(_systemAreFactorized);
        _formulation[c].setRHSToZero();
    }
    _systemAreFactorized=true;

    _j[order][support] = _k;
    _k.setValuesToZero();

    return _j[order][support];
}

void DifferentialRegularizationInterface::setLHS()
{
    for (unsigned int c = 0; c < _m_size; c++)
    {
        unsigned int degree = _k[c].degree();
        _formulation[c].integral(dof(_k[c]), tf(_k[c]), _config->model_unknown(Support::BLK), integrationType(2*degree));
    }
}
