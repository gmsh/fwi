// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_MODEL_REGULARIZATION
#define H_COMMON_MODEL_REGULARIZATION

//GmshFEM Library
#include "Formulation.h"

//GmshFWI Library
#include "../state.h"
#include "../../configuration.h"

/*
* RegularizationInterface
*/
class RegularizationInterface
{
protected:
    const ConfigurationInterface* const _config;
    const unsigned int _m_size;
    std::vector<double> _lambda;
    std::string _integrationType;
    unsigned int _integrationDegreeBlk;
    unsigned int _integrationDegreeBnd;
    std::string integrationType(unsigned int degree) const {return _integrationType + std::to_string(degree);};

public:
    RegularizationInterface(const ConfigurationInterface* const config, const gmshfem::common::GmshFem& gmshFem, std::string suffix = "");
    virtual ~RegularizationInterface() = default;

    virtual double performance(const ModelField& m) = 0;

    virtual Sensitivity update_sensitivity(Order order, Support support, const ModelState& ms) = 0;
    std::complex<double> directional(const Sensitivity& sensitivity, const ModelFunction& dm2, Support support) const;

    virtual void modelIsObsolete() {};
    virtual void modelPerturbationIsObsolete() {};

    void lambda(double lambda,unsigned int c=0);
};

/*
* DifferentialRegularizationInterface
*/
class DifferentialRegularizationInterface: public RegularizationInterface
{
protected:
    ModelField _k;
    std::array<std::array<ModelField,2>,3> _j;

    std::vector<gmshfem::problem::Formulation< std::complex< double > >> _formulation;

    bool _systemAreUpToDate;
    bool _systemAreFactorized;
private:
    DifferentialRegularizationInterface(const ConfigurationInterface* const config, const gmshfem::common::GmshFem& gmshFem, std::string suffix = "");
public:
    DifferentialRegularizationInterface(const ConfigurationInterface* const config, const ModelField& m, const gmshfem::common::GmshFem& gmshFem, std::string suffix = "" );

    virtual Sensitivity update_sensitivity(Order order, Support support, const ModelState& ms);

    virtual void setLHS();
    virtual void setRHS(Order order, Support support, const ModelState& ms) = 0;
};

#endif //H_COMMON_MODEL_REGULARIZATION
