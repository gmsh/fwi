// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "Exception.h"

//GmshFWI Library
#include "state.h"

using namespace gmshfem::common;


/*
* Class ModelState
*/
const ModelFunction& ModelState::state(Type type) const
{
    if(_isUpToDate[type]){return _state[type];}
    else
    {
        throw Exception(type_to_string(type) + " model is not up to date while required.");
    }
}

const ModelField& ModelState::field(Type type) const
{
    if(!_isUpToDate[type])
    {
        throw Exception(type_to_string(type) + " model is not up to date while required.");
    }
    if(!_useField[type])
    {
        throw Exception(type_to_string(type) + " model is set through a function and not a field.");
    }
    return _field[type];
}

void ModelState::write(Type type, std::string name) const
{
    if(_useField[type])
    {
        _field[type].write(name);
    }
    else
    {
        for (unsigned int c = 0; c < _state[type].size(); c++)
        {
            gmshfem::post::save(_state[type][c],_domain,name+"c"+std::to_string(c),"pos","");
        }
    }
}

bool ModelState::available(Type type, bool field) const
{
    if(field){return _isUpToDate[type] && _useField[type];}
    else{return _isUpToDate[type];}
}
