// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_MODEL_STATE
#define H_MODEL_STATE

//GmshFEM Library
//GmshFWI Library
#include "../configuration.h"
#include "element.h"
#include "../enum.h"
#include "parametrization.h"
/*
* ModelStateEvaluator
*/
class ModelStateEvaluator
{
public:
    virtual const ModelFunction& state(Type type) const = 0;
    virtual void write(Type type, std::string name) const = 0;
    virtual bool available(Type type) const = 0;
};

/*
* ModelState
*/
class ModelState final: public ModelStateEvaluator
{
private:
    std::array<ModelFunction,4> _state;
    std::array<bool,4> _isUpToDate;
    gmshfem::domain::Domain _domain;

    std::array<ModelField,4> _field;
    std::array<bool,4> _useField;
public:
    ModelState(const ConfigurationInterface* const config):
    _state{
        ModelFunction(),
        ModelFunction(),
        ModelFunction(),
        ModelFunction(),
    },
    _isUpToDate{ {false,false,false,false} },
    _domain(config->model_evaldomain()),
    _field{
        ModelField(),
        ModelField(),
        ModelField(),
        ModelField(),
    },
    _useField{ {false, false, false, false} } {};

    virtual const ModelFunction& state(Type type) const;
    virtual void write(Type type, std::string name) const;
    virtual bool available(Type type) const {return available(type,false);}
    bool available(Type type, bool field) const;

    const ModelField& field(Type type) const;

    friend class ModelUpdater;
};


#endif //H_MODEL_STATE
