// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//gmshFWI Library
#include "sweeper.h"

using namespace gmshfem::common;
//using namespace gmshfem::problem;
using namespace gmshfem::equation;
/*
* class ModelFieldSweeperInterface
*/
ModelFieldSweeperInterface::ModelFieldSweeperInterface(std::string name, const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem) : _config(config), _m_discret(m_discret), _formulation("sweeper_"+name+"_formulation"), _k(config->model_size(),ModelMonoField("m",config->model_evaldomain(), config->model_gmodel(),m_discret))
{
    if(!gmshFem.userDefinedParameter(_integrationType, "integration_type"))
    {
        throw Exception("Sweeper integration type could not be found");
    }
    _formulation.initSystem();
    _formulation.integral(dof(_k[0]), tf(_k[0]),_config->model_unknown(Support::BLK), _integrationType+std::to_string(2*_k[0].degree()));
    _formulation.pre();
    _formulation.assemble();
    _formulation.removeTerms();
    _formulation.solve(false);
    _k.setValuesToZero();
}
