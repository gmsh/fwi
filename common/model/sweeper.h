// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_MODEL_SWEEPER
#define H_COMMON_MODEL_SWEEPER

//gmshFEM Library
#include "Formulation.h"

//gmshFWI Library
#include "element.h"
#include "../configuration.h"

/*
* class ModelFunctionSweeperInterface
*/
class ModelFunctionSweeperInterface
{
public:
    virtual ~ModelFunctionSweeperInterface() = default;
    virtual ModelFunction operator()(unsigned int index1, unsigned int index2) const = 0;
};

/*
* class ModelFieldSweeperInterface
*/
class ModelFieldSweeperInterface
{
protected:
    const ConfigurationInterface* const _config;
    const model::Discretization _m_discret;
    mutable gmshfem::problem::Formulation< std::complex< double > > _formulation;
    mutable ModelField _k;
    std::string _integrationType;
public:
    ModelFieldSweeperInterface(std::string name, const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem);
    virtual ~ModelFieldSweeperInterface() = default;
    virtual ModelField operator()(unsigned int index1, unsigned int index2) const = 0;
    virtual std::array<double,2> value(unsigned int index1, unsigned int index2) const = 0;
    virtual std::array<unsigned int,2> max_idx() const = 0;
};

#endif // H_COMMON_MODEL_SWEEPER
