// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "Exception.h"
//GmshFWI Library
#include "updater.h"

using namespace gmshfem;
using namespace gmshfem::common;
/*
* ModelUpdater
*/
void ModelUpdater::update(Type type, const ModelFunction* const m, const ModelFunction* const dm)
{
    if(_ms._isUpToDate[type]){return;}

    switch (type)
    {
        case Type::FS:
            if(m==nullptr)
            {
                throw Exception("Model is required but not given.");
            }
            else
            {
                _ms._state[type] = *m;
                _ms._useField[false] = false;
            }
            break;
        case Type::AS:
            update(Type::FS,m,dm);
            _ms._field[type] = _innerproduct->update(
                _su->get(to_order(type),Support::BLK,_ms),
                _su->get(to_order(type),Support::BND,_ms)
            );
            _ms._useField[type] = true;
            _ms._state[type] = ((ModelFunction) _ms._field[type]);
            break;
        case Type::PFS:
            if(dm==nullptr)
            {
                throw Exception("Model perturbation is required but not given.");
            }
            else
            {
                _ms._state[type] = *dm;
                _ms._useField[type] = false;
            }
            break;
        case Type::PAS:
            update(Type::FS,m,dm);
            update(Type::PFS,m,dm);
            _ms._field[type] =
                _innerproduct->update(
                    _su->get(to_order(type),Support::BLK,_ms),
                    _su->get(to_order(type),Support::BND,_ms)
                );
            _ms._useField[type] = true;
            _ms._state[type] = ((ModelFunction) _ms._field[type]);
            break;
        case Type::DS: default:
            throw Exception("Impossible to update model state other than F,A,PF,PA");
            break;
    }

    _ms._isUpToDate[type] = true;
}

void ModelUpdater::update(Type type, const ModelField* const m , const ModelField* const dm )
{
    if(_ms._isUpToDate[type]){return;}
    assert(_innerproduct);
    assert(_su);    

    switch (type)
    {
        case Type::FS:
            if(m==nullptr)
            {
                throw Exception("Model is required but not given.");
            }
            else
            {
                _ms._field[type] = *m;
            }
            break;
        case Type::AS:
        {
            update(Type::FS, m, dm);
            auto &bulkSensitivity = _su->get(to_order(type), Support::BLK, _ms);
            auto &boundarySensitivity = _su->get(to_order(type), Support::BND, _ms);

            _ms._field[type] = _innerproduct->update(bulkSensitivity, boundarySensitivity);
        }
            break;
        case Type::PFS:
            if(dm==nullptr)
            {
                throw Exception("Model perturbation is required but not given.");
            }
            else
            {
                _ms._field[type] = *dm;
            }
            break;
        case Type::PAS:
            update(Type::FS,m,dm);
            update(Type::PFS,m,dm);
            _ms._field[type] =
                _innerproduct->update(
                    _su->get(to_order(type),Support::BLK,_ms),
                    _su->get(to_order(type),Support::BND,_ms)
                );
            break;
        case Type::DS: default:
            throw Exception("Impossible to update model state other than F,A,PF,PA");
            break;
    }

    _ms._useField[type] = true;
    _ms._state[type] = ((ModelFunction) _ms._field[type]);
    _ms._isUpToDate[type] = true;
}


const ModelState&  ModelUpdater::get(std::array<bool,4> needToBeUpToDate, const ModelFunction* const m, const ModelFunction* const dm )
{
    for (unsigned int t = 0; t < 4; t++)
    {
        Type type = ((Type) t);
        if(needToBeUpToDate[type]){update(type,m,dm);}
    }
    return _ms;
};

const ModelState& ModelUpdater::get(std::array<bool,4> needToBeUpToDate, const ModelField* const m , const ModelField* const dm )
{
    for (unsigned int t = 0; t < 4; t++)
    {
        Type type = ((Type) t);
        if(needToBeUpToDate[type]){update(type,m,dm);}
    }
    return _ms;
};

void ModelUpdater::isObsolete(std::array<bool,4> NoMoreUpToDate)
{
    for (unsigned int t = 0; t < 4; t++)
    {
        if(NoMoreUpToDate[t]){_ms._isUpToDate[t] = false;}
    }
}
