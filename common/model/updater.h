// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_MODEL_UPDATER
#define H_COMMON_MODEL_UPDATER
//GmshFEM Library
//GmshFWI Library
#include "state.h"
#include "innerproduct/innerproduct.h"
#include "../sensitivity/updater.h"

/*
* ModelUpdater
*/
class ModelUpdater final
{
private:
    ModelState _ms;
    SensitivityUpdater* const _su;
    InnerProductInterface* const _innerproduct;

    void update(Type type, const ModelFunction* const m, const ModelFunction* const dm = nullptr);
    void update(Type type, const ModelField* const m = nullptr, const ModelField* const dm = nullptr);

public:
    ModelUpdater(const ConfigurationInterface* const config, SensitivityUpdater* const su, InnerProductInterface* const innerproduct) : _ms(config), _su(su), _innerproduct(innerproduct) {};

    const ModelField& m() const
    {
        if(!_ms._useField[Type::FS])
        {
            throw gmshfem::common::Exception("Model updater m is required while not using fields");
        }
        return _ms.field(Type::FS);
    }

    const ModelField& dm() const
    {
        if(!_ms._useField[Type::PFS])
        {
            throw gmshfem::common::Exception("Model updater dm is required while not using fields");
        }
        return _ms.field(Type::PFS);
    }

    const ModelState&  get(std::array<bool,4> needToBeUpToDate, const ModelFunction* const m, const ModelFunction* const dm = nullptr);

    const ModelState&  get(std::array<bool,4> needToBeUpToDate, const ModelField* const m = nullptr, const ModelField* const dm = nullptr);

    void isObsolete(std::array<bool,4> NoMoreUpToDate);
};

#endif // H_COMMON_MODEL_UPDATER
