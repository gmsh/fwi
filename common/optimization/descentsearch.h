// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_OPTIMIZATION_DESCENTSEARCH
#define H_COMMON_OPTIMIZATION_DESCENTSEARCH

//GmshFWI Library
#include "../functional.h"
#include "../model/innerproduct/innerproduct.h"

/*
* class DescentSearchHistoryInterface
*/
class DescentSearchHistoryInterface
{
public:
    virtual void write(std::string filename) const = 0;
};

/*
* class DescentSearchInterface
*/
/*
 computes the descent direction p(m) where m = functional.m()
 stores the descent direction in functional.dm()
*/
class DescentSearchInterface
{
public:
    virtual ~DescentSearchInterface() = default;

    virtual void operator()(FunctionalInterface* const functional) const = 0;

    virtual const DescentSearchHistoryInterface* const history() const = 0;

    virtual void update(FunctionalInterface* const functional) const {};
    virtual void initialize(FunctionalInterface* const functional) const {};
    virtual bool need_steplength() const {return true;};
};

/*
* class TrustRegionDescentSearchInterface
*/
/*
 computes the descent direction p(m,Delta) where m = functional.m() and Delta = length is the size of the trust region
 stores the descent direction in functional.dm()
 returns a predicted decrease, steplength, on boundary?
*/
struct TrustRegionReturn
{
    double predicted_decrease;
    double radius;
    bool boundary;
};
class TrustRegionDescentSearchInterface: public DescentSearchInterface
{
public:
    virtual ~TrustRegionDescentSearchInterface() = default;

    virtual TrustRegionReturn operator()(FunctionalInterface* const functional, double trustRegionRadius) const = 0;
    virtual double predicted_performance(const ModelField& p, FunctionalInterface* const functional) const = 0;

    virtual void operator()(FunctionalInterface* const functional) const {(*this)(functional,0.);};

    virtual bool need_steplength() const override {return false;};
};
#endif // H_COMMON_OPTIMIZATION_DESCENTSEARCH
