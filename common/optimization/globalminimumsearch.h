// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_OPTIMIZATION_GLOBALMINIMUMSEARCH
#define H_COMMON_OPTIMIZATION_GLOBALMINIMUMSEARCH

//GmshFWI Library
#include "../functional.h"
#include "../model/element.h"

/*
* class GlobalMinimumSearchHistoryInterface
*/
class GlobalMinimumSearchHistoryInterface
{
public:
    virtual void write(std::string filename) const = 0;
};

/*
* class GlobalMinimumSearchInterface
*/
template<Physic T_Physic>
class GlobalMinimumSearchInterface
{
protected:
    std::string _name;
    const ConfigurationInterface* const _config;
public:
    GlobalMinimumSearchInterface(const std::string& name, const ConfigurationInterface* const config) : _name(name), _config(config) {};
    virtual ~GlobalMinimumSearchInterface() = default;
    void name(const std::string& name){_name=name;};

    virtual void operator()(ModelField* const m, const Data<T_Physic>& d0, const std::vector<EquationInterface<T_Physic>*>& equation) const = 0;

    virtual const GlobalMinimumSearchHistoryInterface* const history() const = 0;
};

#endif // H_COMMON_OPTIMIZATION_MINIMUMSEARCH
