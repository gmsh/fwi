// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_OPTIMIZATION_LINESEARCH
#define H_COMMON_OPTIMIZATION_LINESEARCH

//GmshFWI Library
#include "../functional.h"

/*
* class LineSearchHistoryInterface
*/
class LineSearchHistoryInterface
{
public:
    virtual void write(std::string filename) const = 0;
};

/*
* class LineSearchInterface
*/
/*
 returns argmin_a J(m0+a*p)
    where J = functional
    m0 = functional.m()
    p = functional.dm()
 At the end of the linesearch functional.m() is set to m+ap
*/
class LineSearchInterface
{
public:
    virtual ~LineSearchInterface() = default;

    virtual double operator()(FunctionalInterface* const functional) const = 0;

    virtual const LineSearchHistoryInterface* const history() const = 0;
};

#endif // H_COMMON_OPTIMIZATION_LINESEARCH
