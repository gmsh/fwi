// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_OPTIMIZATION_LOCALMINIMUMSEARCH
#define H_COMMON_OPTIMIZATION_LOCALMINIMUMSEARCH

//Standard Library
#include <limits>

//GmshFEM Library
#include "CSVio.h"
#include "Exception.h"

//GmshFWI Library
#include "../functional.h"
#include "../model/element.h"
#include "linesearch.h"
#include "descentsearch.h"

/*
* class LocalMinimumSearchHistoryInterface
*/
class LocalMinimumSearchHistoryInterface
{
public:
    virtual void write(std::string filename) const = 0;
};

/*
* class LocalMinimumSearchInterface
*/
class LocalMinimumSearchInterface
{
protected:
    std::string _name;

    unsigned int _maxIteration;
    unsigned int _writeInterval;
    double _absDecreaseThreshold;
    double _absGradient2NormThreshold;
    double _overallRelDecreaseThreshold;
    double _meanRelDecreaseThreshold;
    unsigned int _meanRelDecreaseMemory;
public:
    LocalMinimumSearchInterface(const gmshfem::common::GmshFem& gmshFem)
    {
        if(!gmshFem.userDefinedParameter(_maxIteration, "localminimum_maxIteration"))
        {
            gmshfem::msg::warning << "Maximum number of iterations for local minimum search (localminimum_maxIteration) could not be found." << gmshfem::msg::endl;
            _maxIteration = std::numeric_limits<unsigned int>::max();
        }
        if(!gmshFem.userDefinedParameter(_writeInterval, "localminimum_writeInterval"))
        {
            gmshfem::msg::warning << "Write interval for local minimum search (localminimum_writeInterval) could not be found." << gmshfem::msg::endl;
            _writeInterval = std::numeric_limits<unsigned int>::max();
        }
        if(!gmshFem.userDefinedParameter(_absDecreaseThreshold, "localminimum_absDecreaseThreshold"))
        {
            gmshfem::msg::warning << "Absolute decrease threshold (localminimum_absDecreaseThreshold) could not be found." << gmshfem::msg::endl;
            _absDecreaseThreshold = 0.;
        }
        if(!gmshFem.userDefinedParameter(_absGradient2NormThreshold, "localminimum_absGradient2NormThreshold"))
        {
            gmshfem::msg::warning << "Gradient norm 2 absolute threshold (localminimum_absGradient2NormThreshold) could not be found." << gmshfem::msg::endl;
            _absGradient2NormThreshold = 0.;
        }
        if(!gmshFem.userDefinedParameter(_overallRelDecreaseThreshold, "localminimum_overallRelDecreaseThreshold"))
        {
            gmshfem::msg::warning << "Overall realtive decrease threshold (localminimum_overallDecreaseThreshold) could not be found." << gmshfem::msg::endl;
            _overallRelDecreaseThreshold = 0.;
        }
        if(!gmshFem.userDefinedParameter(_meanRelDecreaseThreshold, "localminimum_meanRelDecreaseThreshold"))
        {
            gmshfem::msg::warning << "Mean relative decrease threshold (localminimum_meanRelDecreaseThreshold) could not be found." << gmshfem::msg::endl;
            _meanRelDecreaseThreshold = 0.;
        }
        if(!gmshFem.userDefinedParameter(_meanRelDecreaseMemory, "localminimum_meanRelDecreaseMemory"))
        {
            gmshfem::msg::warning << "Relative decrease memory (localminimum_meanRelDecreaseMemory) could not be found." << gmshfem::msg::endl;
            _meanRelDecreaseMemory = std::numeric_limits<unsigned int>::max();
        }
    }
    virtual ~LocalMinimumSearchInterface() = default;

    void name(const std::string& name){_name=name;};

    virtual void operator()(ModelField* const m, FunctionalInterface* const functional, const DescentSearchInterface* const descentsearch, const LineSearchInterface* const linesearch) const = 0;

    virtual const LocalMinimumSearchHistoryInterface* const history() const = 0;
};

/*
* class TrustRegionLocalMinimumSearchInterface
*/
class TrustRegionLocalMinimumSearchInterface: public LocalMinimumSearchInterface
{
public:
    TrustRegionLocalMinimumSearchInterface(const gmshfem::common::GmshFem& gmshFem) : LocalMinimumSearchInterface(gmshFem) {};
    virtual ~TrustRegionLocalMinimumSearchInterface() = default;

    virtual void operator()(ModelField* const m, FunctionalInterface* const functional, const DescentSearchInterface* const descentsearch, const LineSearchInterface* const linesearch) const
    {
        if(linesearch != nullptr)
        {
            throw gmshfem::common::Exception("Trust region local minimum search does not need linesearch.");
        }
        const TrustRegionDescentSearchInterface* const trdescentserach = dynamic_cast<const TrustRegionDescentSearchInterface*>(descentsearch);
        if(trdescentserach == nullptr)
        {
            throw gmshfem::common::Exception("Trust region local minimum search only works with trust region descentsearches.");
        }
        else
        {
            (*this)(m,functional,trdescentserach);
        }
    };

    virtual void operator()(ModelField* const m, FunctionalInterface* const functional, const TrustRegionDescentSearchInterface* const descentsearch) const = 0;
};

#endif // H_COMMON_OPTIMIZATION_MINIMUMSEARCH
