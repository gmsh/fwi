// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SENSITIVITY_ELEMENT
#define H_SENSITIVITY_ELEMENT

//GmshFem Library
#include "Function.h"
//FWI Library
#include "../model/element.h"

/*
* MonoSensitivity
*/
using MonoSensitivity = ModelMonoFunction;
/*
* Sensitivity
*/
using Sensitivity = ModelFunction;

#endif //H_SENSITIVITY_ELEMENT
