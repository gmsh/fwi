// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard library

//GmshFem library
#include "Exception.h"

//GmshFWI Library
#include "state.h"

using namespace gmshfem;
using namespace gmshfem::common;

/*
* SensitivityState
*/
const Sensitivity& SensitivityState::state(Order order, Support support) const
{
    if(!_isUpToDate[order][support])
    {
        throw Exception(order_to_string(order) + " sensitivity is not up to date while required.");
    }
    return _state[order][support];
}
