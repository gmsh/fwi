// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_SENSITIVITY_STATE
#define H_COMMON_SENSITIVITY_STATE

//Standerd Library
#include <array>

//GmshFWI Library
#include "element.h"
#include "../enum.h"
#include "../../specific/physic.h"

//Forward declaration
template<Physic T_Physic> class ParametrizedEquation;

/*
* SensitivityStateEvaluator
*/
class SensitivityStateEvaluator
{
public:
    virtual const Sensitivity& state(Order order, Support support) const = 0;
};

/*
* SensitivityState
*/
class SensitivityState: public SensitivityStateEvaluator
{
private:
    std::array<std::array<Sensitivity,2>,3> _state;
    std::array<std::array<bool,2>,3> _isUpToDate = {{ {false,false} , {false,false}, {false,false} }};
public:
    virtual const Sensitivity& state(Order order, Support support) const;
    friend class SensitivityUpdater;
    template<Physic T_Physic> friend class ParametrizedEquation;
};

#endif // H_COMMON_SENSITIVITY_STATE
