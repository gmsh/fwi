// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard library

//GmshFem library

//GmshFWI Library
#include "updater.h"

using namespace gmshfem;
using namespace gmshfem::common;

/*
* SensitivityUpdater
*/
const Sensitivity& SensitivityUpdater::get(Order order, Support support,const ModelState& ms)
{
    update(order,support,ms);
    return _ss.state(order,support);
}

void SensitivityUpdater::update(Order order, Support support, const ModelState& ms)
{
    if(_ss._isUpToDate[order][support]){return;}

    _ss._state[order][support] = sensitivity(order, support, ms);

    _ss._isUpToDate[order][support] = true;
}

void SensitivityUpdater::isObsolete(std::array<bool,3> NoMoreUpToDate)
{
    for (unsigned int o = 0; o < 3; o++)
    {
        if(NoMoreUpToDate[o])
        {
            _ss._isUpToDate[o] = { {false, false} };
        }
    }
}

/*
* SensitivityUpdaterFromEquation
*/
template<Physic T_Physic>
Sensitivity SensitivityUpdaterFromEquation<T_Physic>::sensitivity(Order order, Support support, const ModelState& ms)
{
    std::array<bool,4> waveNeedToBeUpToDate = {false,false,false,false};
    std::array<bool,5> dataNeedToBeUpToDate = {false,false,false,false,false};
    switch (order)
    {
        case Order::SCD:
            waveNeedToBeUpToDate[Type::PFS] = true;
            waveNeedToBeUpToDate[Type::PAS] = true;
        case Order::FST:
            waveNeedToBeUpToDate[Type::FS] = true;
            waveNeedToBeUpToDate[Type::AS] = true;
            break;
        case Order::DIAG:
            dataNeedToBeUpToDate[Type::DS] = true;
        default: break;
    }
    return _equation->update_sensitivity(order,support,_du->get(dataNeedToBeUpToDate,ms),ms,_wu->get(waveNeedToBeUpToDate,ms));
}

/*
* SensitivityUpdaterFromRegularization
*/
Sensitivity SensitivityUpdaterFromRegularization::sensitivity(Order order, Support support, const ModelState& ms)
{
    return _regularization->update_sensitivity(order,support,ms);
}

/*
* SensitivityUpdaterFromVector
*/
Sensitivity SensitivityUpdaterFromVector::sensitivity(Order order, Support support, const ModelState& ms)
{
    Sensitivity sum = _suv[0]->get(order,support,ms);
    for (unsigned int i = 1; i < _ns; i++)
    {
        sum = sum + _suv[i]->get(order,support,ms);
    }
    return sum;
}

template class SensitivityUpdaterFromEquation<Physic::acoustic>;
template class SensitivityUpdaterFromEquation<Physic::electromagnetic>;
template class SensitivityUpdaterFromEquation<Physic::elastodynamic>;
