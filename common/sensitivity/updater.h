// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_SENSITIVITY_UPDATER
#define H_COMMON_SENSITIVITY_UPDATER

//GmshFWI Library
#include "state.h"
#include "../../specific/physic.h"
#include "../wave/equation/equation.h"
#include "../model/regularization/regularization.h"
#include "../wave/updater.h"
#include "../data/updater.h"

/*
* SensitivityUpdater
*/
class SensitivityUpdater
{
private:
    SensitivityState _ss;
    void update(Order order, Support support, const ModelState& ms);
    virtual Sensitivity sensitivity(Order order, Support support, const ModelState& ms) = 0;
public:
    virtual ~SensitivityUpdater() = default;

    const Sensitivity& get(Order order, Support support,const ModelState& ms);
    void isObsolete(std::array<bool,3> NoMoreUpToDate);
};

/*
* SensitivityUpdaterFromEquation
*/
template<Physic T_Physic>
class SensitivityUpdaterFromEquation : public SensitivityUpdater
{
public:
    SensitivityUpdaterFromEquation(WaveUpdater<T_Physic>* const wu, DataUpdater<T_Physic>* const du, EquationInterface<T_Physic>* const equation) : _wu(wu), _du(du), _equation(equation) {};
private:
    WaveUpdater<T_Physic>* const _wu;
    DataUpdater<T_Physic>* const _du;
    EquationInterface<T_Physic>* const _equation;

    virtual Sensitivity sensitivity(Order order, Support support, const ModelState& ms);
};

/*
* SensitivityUpdaterFromRegularization
*/
class SensitivityUpdaterFromRegularization : public SensitivityUpdater
{
public:
    SensitivityUpdaterFromRegularization(RegularizationInterface* const regularization) : _regularization(regularization) {};
private:
    RegularizationInterface* const _regularization;

    virtual Sensitivity sensitivity(Order order, Support support, const ModelState& ms);
};

/*
* SensitivityUpdaterFromVector
*/
class SensitivityUpdaterFromVector : public SensitivityUpdater
{
public:
    SensitivityUpdaterFromVector(unsigned int ns, SensitivityUpdater* *const suv) : _ns(ns), _suv(suv) {};
private:
    unsigned int _ns;
    SensitivityUpdater* *const _suv;

    virtual Sensitivity sensitivity(Order order, Support support, const ModelState& ms);
};

#endif // H_COMMON_SENSITIVITY_UPDATER
