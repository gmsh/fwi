// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFWI Library
#include "statefunctional.h"

/* Will simplify when Equation is multifrequency */
template<Physic T_Physic>
StateFunctional<T_Physic>::StateFunctional(const ConfigurationInterface* const config, InnerProductInterface* const innerproduct, RegularizationInterface* const regularization, const std::vector< double >& freq, const std::vector<EquationInterface<T_Physic>*>& equation, ObjectiveInterface<T_Physic>* const objective): _innerproduct(innerproduct), _regularization(regularization), _equation(equation), _objective(objective), _nf(freq.size()), _mu(config,&_su,_innerproduct), _suv(new SensitivityUpdater*[(_regularization!=nullptr) ? (_nf+1):_nf]), _su((_regularization!=nullptr) ? (_nf+1):_nf,_suv), _wu( (WaveUpdater<T_Physic> *) std::malloc(_nf * sizeof(WaveUpdater<T_Physic>))), _du(freq,config,_wu,_objective)
{
    if(equation.size() != _nf)
    {
        throw gmshfem::common::Exception("Equation has not the same size as frequency vector");
    }
    if(_objective!=nullptr)
    {
        if(_objective->nf()!=_nf)
        {
            throw gmshfem::common::Exception("Objective has not the same size as frequency vector");
        }
        _objective->link(&_mu,&_du);
    }
    for (unsigned int f = 0; f < _nf; f++)
    {
        new(&_wu[f]) WaveUpdater<T_Physic>(config,&_du,_equation[f]);
        _suv[f] = new SensitivityUpdaterFromEquation<T_Physic>(&_wu[f],&_du,_equation[f]);
    }
    if(_regularization!=nullptr)
    {
        _suv[_nf] = new SensitivityUpdaterFromRegularization(_regularization);
    }
    if(_innerproduct!=nullptr){_innerproduct->link(&_mu,&_su);}
}

template<Physic T_Physic>
StateFunctional<T_Physic>::~StateFunctional()
{
    if(_innerproduct!=nullptr){_innerproduct->unlink();}
    if(_objective!=nullptr){_objective->unlink();}
    for (unsigned int f = 0; f < _nf; f++)
    {
        delete _suv[f];
    }
    if(_regularization!=nullptr)
    {
        delete _suv[_nf];
    }
    delete[] _suv;
    std::free(_wu);
}

template<Physic T_Physic>
void StateFunctional<T_Physic>::write_model(Type type, std::string name)
{
    std::array<bool,4> NeedToBeUpToDate = {false,false,false,false};
    NeedToBeUpToDate[type] = true;
    _mu.get(NeedToBeUpToDate).write(type,name);
}

template<Physic T_Physic>
void StateFunctional<T_Physic>::write_data(Type type, std::string name, std::string fmt)
{
    std::array<bool,5> DataNeedToBeUpToDate = {false,false,false,false,false};
    DataNeedToBeUpToDate[type] = true;
    std::array<bool,4> ModelNeedToBeUpToDate = {false,false,false,false};
    switch (type)
    {
        case Type::PFS: case Type::PAS:
            ModelNeedToBeUpToDate[Type::PFS] = true;
            //no break;
        case Type::FS: case Type::AS: case Type::DS:
            ModelNeedToBeUpToDate[Type::FS] = true;
    }
    _du.get(DataNeedToBeUpToDate,_mu.get(ModelNeedToBeUpToDate)).write(type,name,fmt);
}

template<Physic T_Physic>
void StateFunctional<T_Physic>::write_wave(Type type, std::string name)
{
    std::array<bool,4> WaveNeedToBeUpToDate = {false,false,false,false};
    WaveNeedToBeUpToDate[type] = true;
    std::array<bool,4> ModelNeedToBeUpToDate = {false,false,false,false};
    switch (type)
    {
        case Type::PFS: case Type::PAS:
            ModelNeedToBeUpToDate[Type::PFS] = true;
            //no break;
        case Type::FS: case Type::AS:
            ModelNeedToBeUpToDate[Type::FS] = true;
            break;
        case Type::DS: default:
            throw gmshfem::common::Exception("Impossible to write wave other than F,PF,A,PA");

    }
    for (unsigned int f = 0; f < _nf; f++)
    {
        std::string suffix_f = "f" + std::to_string(f);
        _wu[f].get(WaveNeedToBeUpToDate,_mu.get(ModelNeedToBeUpToDate)).write(type,name+suffix_f);
    }
}

template<Physic T_Physic>
void StateFunctional<T_Physic>::innerproductIsObsolete()
{
    std::array<bool,4> NoMoreUpToDate = {false, false, true, true};
    _mu.isObsolete(NoMoreUpToDate);
}

template<Physic T_Physic>
void StateFunctional<T_Physic>::objectiveIsObsolete()
{
    std::array<bool,5> dataNoMoreUpToDate = {false, false, true, true, true};
    _du.isObsolete(dataNoMoreUpToDate);

    std::array<bool,4> waveNoMoreUpToDate = {false, false, true, true};
    std::array<bool,3> sensitivityNoMoreUpToDate = {true, true, true};
    for (unsigned int f = 0; f < _nf; f++)
    {
        _wu[f].isObsolete(waveNoMoreUpToDate);
        _suv[f]->isObsolete(sensitivityNoMoreUpToDate);
    }
    _su.isObsolete(sensitivityNoMoreUpToDate);

    std::array<bool,4> modelNoMoreUpToDate = {false, false, true, true};
    _mu.isObsolete(modelNoMoreUpToDate);
}

template<Physic T_Physic>
void StateFunctional<T_Physic>::regularizationIsObsolete()
{
    if(_regularization != nullptr)
    {
        std::array<bool,3> sensitivityNoMoreUpToDate = {true, true, true};
        _suv[_nf]->isObsolete(sensitivityNoMoreUpToDate);
        _su.isObsolete(sensitivityNoMoreUpToDate);
        std::array<bool,4> modelNoMoreUpToDate = {false, false, true, true};
        _mu.isObsolete(modelNoMoreUpToDate);
    }
}

template<Physic T_Physic>
void StateFunctional<T_Physic>::modelIsObsolete()
{
    std::array<bool,5> dataNoMoreUpToDate = {true, true, true, true, true};
    dataNoMoreUpToDate[Type::DS] = false;
    if(_objective != nullptr){dataNoMoreUpToDate[Type::DS] = _objective->modelIsObsolete();}
    _du.isObsolete(dataNoMoreUpToDate);

    std::array<bool,4> waveNoMoreUpToDate = {true, true, true, true};
    std::array<bool,3> sensitivityNoMoreUpToDate = {true, true, true};
    bool sensitivityNoMoreUpToDateOverallDiag = false;

    for (unsigned int f = 0; f < _nf; f++)
    {
        _wu[f].isObsolete(waveNoMoreUpToDate);

        sensitivityNoMoreUpToDate[Order::DIAG]=false;
        sensitivityNoMoreUpToDate[Order::DIAG] = _equation[f]->modelIsObsolete();
        if(sensitivityNoMoreUpToDate[Order::DIAG]){sensitivityNoMoreUpToDateOverallDiag=true;}
        _suv[f]->isObsolete(sensitivityNoMoreUpToDate);
    }
    if(_regularization!=nullptr)
    {
        _regularization->modelIsObsolete();
        sensitivityNoMoreUpToDate[Order::DIAG]=false;
        _suv[_nf]->isObsolete(sensitivityNoMoreUpToDate);
    }
    sensitivityNoMoreUpToDate[Order::DIAG]=sensitivityNoMoreUpToDateOverallDiag;
    _su.isObsolete(sensitivityNoMoreUpToDate);

    std::array<bool,4> modelNoMoreUpToDate = {true, true, true, true};
    modelNoMoreUpToDate[Type::PFS]=false;
    if(_innerproduct != nullptr){_innerproduct->modelIsObsolete();}
    _mu.isObsolete(modelNoMoreUpToDate);
}

template<Physic T_Physic>
void StateFunctional<T_Physic>::modelPerturbationIsObsolete()
{
    std::array<bool,5> dataNoMoreUpToDate = {false, true, false, true, false};
    _du.isObsolete(dataNoMoreUpToDate);

    std::array<bool,4> waveNoMoreUpToDate = {false, true, false, true};
    std::array<bool,3> sensitivityNoMoreUpToDate = {false, true, false};
    for (unsigned int f = 0; f < _nf; f++)
    {
        _wu[f].isObsolete(waveNoMoreUpToDate);
        _equation[f]->modelPerturbationIsObsolete();
        _suv[f]->isObsolete(sensitivityNoMoreUpToDate);
    }
    if(_regularization!=nullptr)
    {
        _regularization->modelPerturbationIsObsolete();
        _suv[_nf]->isObsolete(sensitivityNoMoreUpToDate);
    }
    _su.isObsolete(sensitivityNoMoreUpToDate);

    std::array<bool,4> modelNoMoreUpToDate = {false, true, false, true};
    _mu.isObsolete(modelNoMoreUpToDate);
}


template<Physic T_Physic>
void StateFunctional<T_Physic>::iteration(unsigned int iteration)
{
    _iteration=iteration;
    if(_innerproduct->iteration(iteration)==true){innerproductIsObsolete();}
}

template<Physic T_Physic>
void StateFunctional<T_Physic>::setModel(const ModelField& m)
{
    modelIsObsolete();
    std::array<bool,4> ToBeUpdated = {true,false,false,false};
    _mu.get(ToBeUpdated,&m);
}

template<Physic T_Physic>
void StateFunctional<T_Physic>::setModel(const ModelFunction& m)
{
    modelIsObsolete();
    std::array<bool,4> ToBeUpdated = {true,false,false,false};
    _mu.get(ToBeUpdated,&m);
}

template<Physic T_Physic>
void StateFunctional<T_Physic>::setModelPerturbation(const ModelField& dm)
{
    modelPerturbationIsObsolete();
    // Update model PFS, e.g. copy the field to the model perturbation object
    std::array<bool,4> ToBeUpdated = {false,true,false,false};
    _mu.get(ToBeUpdated,nullptr,&dm);
}

template<Physic T_Physic>
void StateFunctional<T_Physic>::setModelPerturbation(const ModelFunction& dm)
{
    modelPerturbationIsObsolete();
    std::array<bool,4> ToBeUpdated = {false,true,false,false};
    _mu.get(ToBeUpdated,nullptr,&dm);
}

template<Physic T_Physic>
const ModelField& StateFunctional<T_Physic>::m() const
{
    return _mu.m();
}

template<Physic T_Physic>
const ModelField& StateFunctional<T_Physic>::dm() const
{
    return _mu.dm();
}

/* performance */
template<Physic T_Physic>
double StateFunctional<T_Physic>::performance()
{
    return performance_objective()+performance_regularization();
}

template<Physic T_Physic>
double StateFunctional<T_Physic>::performance_objective()
{
    if(_objective!=nullptr)
    {
        std::array<bool,5> DataToBeUpdated = {true,false,false,false,false};
        std::array<bool,4> ModelToBeUpdated = {true,false,false,false};

        return _objective->performance(
            _du.get(DataToBeUpdated,_mu.get(ModelToBeUpdated)).state(Type::FS)
        );
    }
    else
    {
        return 0.;
    }
}

template<Physic T_Physic>
double StateFunctional<T_Physic>::performance_regularization()
{

    if(_regularization!=nullptr)
    {
        std::array<bool,4> ModelToBeUpdated = {true,false,false,false};
        if(!_mu.get(ModelToBeUpdated).available(Type::FS,true))
        {
            throw gmshfem::common::Exception("Impossible to compute the regularization part of the performance functional for a model function. Only possible for fields.");
        }
        return _regularization->performance(
            _mu.get(ModelToBeUpdated).field(Type::FS)
        );
    }
    else
    {
        return 0.;
    }
}

/*
* First order
*/
/* gradient */
template<Physic T_Physic>
const ModelField& StateFunctional<T_Physic>::gradient()
{
    // Update the model adjoint state and return it.
    // TODO: no model sent but FS is updated -> Is it always up to date ?
    std::array<bool,4> ToBeUpdated = {false,false,true,false};
    return _mu.get(ToBeUpdated).field(Type::AS);
}

/* sensitivity */
template<Physic T_Physic>
const Sensitivity& StateFunctional<T_Physic>::sensitivity(Order order, Support support)
{
    std::array<bool,4> ModelToBeUpToDate = {false,false,false,false};
    switch (order)
    {
        case Order::SCD:
            ModelToBeUpToDate[Type::PFS] = true;
        case Order::FST: case Order::DIAG:
            ModelToBeUpToDate[Type::FS] = true;
    }
    return _su.get(order,support,_mu.get(ModelToBeUpToDate));
}

template<Physic T_Physic>
const Sensitivity& StateFunctional<T_Physic>::sensitivity(unsigned int f, Order order, Support support)
{
    std::array<bool,4> ModelToBeUpToDate = {false,false,false,false};
    switch (order)
    {
        case Order::SCD:
            ModelToBeUpToDate[Type::PFS] = true;
        case Order::FST: case Order::DIAG:
            ModelToBeUpToDate[Type::FS] = true;
    }
    return _suv[f]->get(order,support,_mu.get(ModelToBeUpToDate));
}

/* directional */
template<Physic T_Physic>
double StateFunctional<T_Physic>::directional(Order order, Support support, const ModelFunction &dm2)
{
    double sum = 0.;
    for (unsigned int f = 0; f < _nf; f++)
    {
        sum += std::real(
            _equation[f]->directional(
                sensitivity(f,order,support),
                dm2,
                support
            )
        );
    }
    if(_regularization!=nullptr)
    {
        sum += std::real(
            _regularization->directional(
                sensitivity(_nf,order,support),
                dm2,
                support
            )
        );
    }
    return sum;
}

/* directional1 */
template<Physic T_Physic>
double StateFunctional<T_Physic>::directional1(const ModelFunction &dm2)
{
    return directional(Order::FST, Support::BLK, dm2) + directional(Order::FST, Support::BND, dm2);
}

template<Physic T_Physic>
double StateFunctional<T_Physic>::directional1(const ModelField &dm2)
{
    return std::real( _innerproduct->product(gradient(),dm2) );
}

template <Physic T_Physic>
double StateFunctional<T_Physic>::directional1direct(const ModelField &dm)
{
    setModelPerturbation(dm);
    // Needs data AS (gradient kernel of the objective) and data PFS (perturbation on the receivers)
    std::array<bool, 5> DataToBeUpdated = {false, true, true, false, false};
    std::array<bool, 4> ModelToBeUpdated = {false, false, false, false};
    
    auto &du = _du.get(DataToBeUpdated, _mu.get(ModelToBeUpdated));
    auto &dataAS = du.state(Type::AS);
    auto &dataPFS = du.state(Type::PFS);

    return real(l2scalpNoConj(dataAS, dataPFS));
}

/*
* Second order
*/
/* hessian */
template<Physic T_Physic>
const ModelField& StateFunctional<T_Physic>::hessian()
{
    std::array<bool,4> ToBeUpdated = {false,false,false,true};
    return _mu.get(ToBeUpdated).field(Type::PAS);
}

/* directional2 */
template<Physic T_Physic>
double StateFunctional<T_Physic>::directional2(const ModelFunction &dm2)
{
    return directional(Order::SCD, Support::BLK, dm2) + directional(Order::SCD, Support::BND, dm2);
}

template<Physic T_Physic>
double StateFunctional<T_Physic>::directional2(const ModelField &dm2)
{
    return std::real( _innerproduct->product(hessian(),dm2) );
}

template class StateFunctional<Physic::acoustic>;
template class StateFunctional<Physic::electromagnetic>;
template class StateFunctional<Physic::elastodynamic>;
