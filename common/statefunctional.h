// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_STATEFUNCTIONAL
#define H_STATEFUNCTIONAL

//GmshFWI Library
#include "functional.h"
#include "model/updater.h"
#include "model/innerproduct/innerproduct.h"
#include "model/regularization/regularization.h"
#include "wave/updater.h"
#include "wave/equation/equation.h"
#include "data/updater.h"
#include "data/objective/objective.h"

template<Physic T_Physic>
class StateFunctional: public FunctionalInterface
{
private:
    InnerProductInterface* const _innerproduct;
    RegularizationInterface* const _regularization;
    const std::vector<EquationInterface<T_Physic>*> _equation;
    ObjectiveInterface<T_Physic>* const _objective;
    const unsigned int _nf;

    ModelUpdater _mu;

    SensitivityUpdater* *const _suv;
    SensitivityUpdaterFromVector _su;

    WaveUpdater<T_Physic> *const _wu;
    DataUpdater<T_Physic> _du;

    void modelIsObsolete();
    void modelPerturbationIsObsolete();
public:
    StateFunctional(const ConfigurationInterface* const config, InnerProductInterface* const innerproduct, RegularizationInterface* const regularization, const std::vector< double >& freq, const std::vector<EquationInterface<T_Physic>*>& equation, ObjectiveInterface<T_Physic>* const objective);

    ~StateFunctional();

    void innerproductIsObsolete();
    void objectiveIsObsolete();
    void regularizationIsObsolete();

    void write_model(Type type, std::string name);
    void write_data(Type type, std::string name, std::string fmt = "csv");
    void write_wave(Type type, std::string name);

    virtual const InnerProductInterface* const innerproduct() const {return _innerproduct;};

    virtual void iteration(unsigned int iteration) override;
    virtual void setModel(const ModelField& m);
    virtual void setModel(const ModelFunction& m);
    virtual void setModelPerturbation(const ModelField& dm);
    virtual void setModelPerturbation(const ModelFunction& dm);

    virtual const ModelField& m() const;
    virtual const ModelField& dm() const;

    /* performance */
    virtual double performance();
    double performance_objective();
    double performance_regularization();

    /*
    * First order
    */
    /* gradient */
    virtual const ModelField& gradient();

    /* sensitivity */
    const Sensitivity& sensitivity(Order order, Support support);
    const Sensitivity& sensitivity(unsigned int f, Order order, Support support);

    /* directional */
    double directional(Order order , Support support, const ModelFunction &dm);

    /* directional1 */
    virtual double directional1(const ModelFunction &dm);
    virtual double directional1(const ModelField &dm);
    // Solve the PFS for a direction and compute the variation of cost.
    virtual double directional1direct(const ModelField &dm);

    /*
    * Second order
    */
    /* hessian */
    virtual const ModelField& hessian();

    /* directional2 */
    virtual double directional2(const ModelFunction &dm2);
    virtual double directional2(const ModelField &dm2);
};

#endif // H_STATEFUNCTIONAL
