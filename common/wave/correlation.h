// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_WAVE_CORRELATION
#define H_COMMON_WAVE_CORRELATION

//Standard Library
#include <numeric>

//Gmsh Library
#include "gmsh.h"

//GmshFEM Library
#include "GmshFem.h"
#include "ExecutionTree.h"
#include "FieldInterface.h"
#include "FieldEvaluator.h"
#include "FieldNode.h"
#include "IndiceBucket.h"
#include "MathObject.h"
#include "OmpInterface.h"
#include "ScalarFunction.h"

#include "OperationsInterface.h"
#include "Function.h"
#include "Domain.h"
#include "CSVio.h"
#include "MathObject.h"

//GmshFWI Library
#include "element.h"

/*
* BiWaveMultiField Operation & Node
*/

namespace gmshfem
{
  namespace function
  {
    template<Physic T_Physic>
    class BiWaveMultiFieldOperation : public OperationsInterface< std::complex<double>, Degree::Degree0 >
    {
     public:
      typedef std::complex<double> D_Scalar;
      constexpr static Degree D_Degree = Degree::Degree0;

      BiWaveMultiFieldOperation() :
        OperationsInterface< std::complex<double>, Degree::Degree0 >()
      {
      }

      BiWaveMultiFieldOperation(const BiWaveMultiFieldOperation &other) :
        OperationsInterface< std::complex<double>, Degree::Degree0 >(other)
      {
      }

      virtual void operator()(const WaveMultiField<T_Physic> *fieldA, const WaveMultiField<T_Physic> *fieldB, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const = 0;
    };

    template< class T_Op, Physic T_Physic >
    class BiWaveMultiFieldNode final : public ExecutionTreeWithDegreeAndLeaves< typename T_Op::D_Scalar, T_Op::D_Degree >
    {
     protected:
      T_Op _op;
      const WaveMultiField<T_Physic> *_fieldA;
      const WaveMultiField<T_Physic> *_fieldB;

     public:
      BiWaveMultiFieldNode(const WaveMultiField<T_Physic> *fieldA, const WaveMultiField<T_Physic> *fieldB) :
        _op(), _fieldA(fieldA), _fieldB(fieldB)
      {
      }

      BiWaveMultiFieldNode(const T_Op &other, const WaveMultiField<T_Physic> *fieldA, const WaveMultiField<T_Physic> *fieldB) :
        _op(other), _fieldA(fieldA), _fieldB(fieldB)
      {
      }

      virtual ~BiWaveMultiFieldNode() {}

      virtual NodeType nodeType() const override
      {
          return NodeType::Nullary;
      }
      virtual common::Memory peakMemoryByGaussPoint(const domain::Domain &domain) const override
      {
        return 0.;
      }

      virtual bool operator==(const ExecutionTreeInterface &other) const override
      {
          //Could be implemented!
          return false;
      }

      virtual bool isConstant(const std::pair< int, int > &entity) const override
      {
        return false;
      }

      virtual void evaluate(OutputVector< typename T_Op::D_Scalar, T_Op::D_Degree > &values, const std::vector< scalar::Precision< typename T_Op::D_Scalar >, numa::allocator< scalar::Precision< typename T_Op::D_Scalar > > > &points, const std::vector< scalar::Precision< typename T_Op::D_Scalar > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const override
      {
        if(values.size() < points.size() / 3)
        {
#pragma omp barrier
            FunctionAllocator< typename MathObject< typename T_Op::D_Scalar, T_Op::D_Degree >::Object >::initalizeNUMAMemory(points.size() / 3);
#pragma omp master
            values.resize(points.size() / 3);
#pragma omp barrier
        }
        _op(_fieldA, _fieldB, values, points, gaussPoints, elementType, entity);
#pragma omp barrier
      }

      virtual BiWaveMultiFieldNode< T_Op,T_Physic > *copy() const override
      {
        BiWaveMultiFieldNode< T_Op,T_Physic > *cp = new BiWaveMultiFieldNode< T_Op,T_Physic >(_op, _fieldA, _fieldB);
        return cp;
      }

      virtual void exportTree(const domain::Domain &domain, common::HTMLNode *node) const override
      {
        node->name = _op.name() + " " + _fieldA->name() + " and " + _fieldB->name();
        node->scalar = scalar::Name< typename T_Op::D_Scalar >::name;
        node->localMemory = 0.;
        node->degree = T_Op::D_Degree;
      }
    };
  } // namespace function
} // namespace gmshfem

/*
* WaveMultiField Operation & Node
*/

namespace gmshfem
{
  namespace function
  {

    template<Physic T_Physic>
    class WaveMultiFieldOperation : public OperationsInterface< std::complex<double>, Degree::Degree0 >
    {
     public:
      typedef std::complex<double> D_Scalar;
      constexpr static Degree D_Degree = Degree::Degree0;

      WaveMultiFieldOperation() :
        OperationsInterface< std::complex<double>, Degree::Degree0 >()
      {
      }

      WaveMultiFieldOperation(const WaveMultiFieldOperation &other) :
        OperationsInterface< std::complex<double>, Degree::Degree0 >(other)
      {
      }

      virtual void operator()(const WaveMultiField<T_Physic>* field, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const = 0;
    };

    template< class T_Op, Physic T_Physic >
    class WaveMultiFieldNode final : public ExecutionTreeWithDegreeAndLeaves< typename T_Op::D_Scalar, T_Op::D_Degree >
    {
     protected:
      T_Op _op;
      const WaveMultiField<T_Physic> *_field;

     public:
      WaveMultiFieldNode(const WaveMultiField<T_Physic>* field) :
        _op(), _field(field)
      {
      }

      WaveMultiFieldNode(const T_Op &other, const WaveMultiField<T_Physic>* field) :
        _op(other), _field(field)
      {
      }

      virtual ~WaveMultiFieldNode() {}

      virtual NodeType nodeType() const override
      {
        return NodeType::Nullary;
      }
      virtual common::Memory peakMemoryByGaussPoint(const domain::Domain &domain) const override
      {
          return 0.;
      }

      virtual bool operator==(const ExecutionTreeInterface &other) const override
      {
          return false;
      }

      virtual bool isConstant(const std::pair< int, int > &entity) const override
      {
        return false;
      }

      virtual void evaluate(OutputVector< typename T_Op::D_Scalar, T_Op::D_Degree > &values, const std::vector< scalar::Precision< typename T_Op::D_Scalar >, numa::allocator< scalar::Precision< typename T_Op::D_Scalar > > > &points, const std::vector< scalar::Precision< typename T_Op::D_Scalar > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const override
      {
          if(values.size() < points.size() / 3)
          {
#pragma omp barrier
            FunctionAllocator< typename MathObject< typename T_Op::D_Scalar, T_Op::D_Degree >::Object >::initalizeNUMAMemory(points.size() / 3);
#pragma omp master
            values.resize(points.size() / 3);
#pragma omp barrier
      }

      _op(_field, values, points, gaussPoints, elementType, entity);
#pragma omp barrier
    }

      virtual WaveMultiFieldNode< T_Op,T_Physic > *copy() const override
      {
        WaveMultiFieldNode< T_Op,T_Physic > *cp = new WaveMultiFieldNode< T_Op,T_Physic >(_op, _field);
        return cp;
      }


        virtual void exportTree(const domain::Domain &domain, common::HTMLNode *node) const override
        {
            node->name = _op.name() + " " + _field->name();
            node->scalar = scalar::Name< typename T_Op::D_Scalar >::name;
            node->localMemory = 0.;
            node->degree = T_Op::D_Degree;
        }
    };
  } // namespace function
} // namespace gmshfem

#endif // H_COMMON_WAVE_CORRELATION
