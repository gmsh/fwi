// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFWI Library
#include "element.h"

/*
* WaveMultiField
*/
template<Physic T_Physic>
void WaveMultiField<T_Physic>::initializeField(const WaveField<T_Physic>& field)
{
    _field = field;
    for (unsigned i = 0; i < _dofValues.size(); ++i)
    {
      _dofValues[i].resize(_field.numberOfDofs());
    }
    _isInitialized = true;
}

template<Physic T_Physic>
const WaveField<T_Physic>& WaveMultiField<T_Physic>::operator[](const unsigned int index) const
{
    if(index!=_index || !_isUpToDate)
    {
        _field.setAllVector(_dofValues[index], gmshfem::dofs::RawOrder::Hash);
        _index=index;
        _isUpToDate=true;
    }
    return _field;
}

template<Physic T_Physic>
void WaveMultiField<T_Physic>::setValues(const unsigned int index, gmshfem::algebra::Vector< std::complex< double > > &vector)
{
  _dofValues[index] = std::move(vector);
}

template<Physic T_Physic>
void WaveMultiField<T_Physic>::write(std::string name) const
{
    for (unsigned int s = 0; s < size(); s++)
    {
        (*this)[s].write(name+"s"+std::to_string(s));
    }
}

/*
* WaveAuxilaryField
*/
template<Physic T_Physic>
void WaveAuxilaryField<T_Physic>::assignValues(const std::vector< std::complex<double> > &values)
{
    if(_systemValuesToDofValues.size() == 0) {
      _systemValuesToDofValues.reserve(_wmf->_field.numberOfDofs());
      _unknownIndices.reserve(values.size());
      unsigned counter = 0;
      for(auto it = _wmf->_field.begin(); it != _wmf->_field.end(); it++)
      {
        // Save all DOFs, including the fixed ones, but remember to only update the unknown ones
         _systemValuesToDofValues.push_back(it->first->numDof() - 1);
        if (it->first->type() == gmshfem::dofs::Type::Unknown) {
          _unknownIndices.push_back(counter);
        }
        ++counter;

      }
    }

    for (unsigned int i : _unknownIndices)
    {
      _wmf->_dofValues[_index][i] = values[_systemValuesToDofValues[i]];
    }
    if (_index == _wmf->_index)
    {
      _wmf->_isUpToDate = false;
    }
}

template class WaveMultiField<Physic::acoustic>;
template class WaveAuxilaryField<Physic::acoustic>;

template class WaveMultiField<Physic::electromagnetic>;
template class WaveAuxilaryField<Physic::electromagnetic>;

template class WaveMultiField<Physic::elastodynamic>;
template class WaveAuxilaryField<Physic::elastodynamic>;
