// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_WAVE_ELEMENT
#define H_COMMON_WAVE_ELEMENT

//GmshFWI Library
#include "../../specific/physic.h"
#include "../../specific/wave/element.h"
#include "../configuration.h"

//Forward declaration
template<Physic T_Physic> class WaveAuxilaryField;

/*
* WaveMultiField
*/
template<Physic T_Physic>
class WaveMultiField
{
private:
    mutable WaveField<T_Physic> _field;
    bool _isInitialized;
    mutable bool _isUpToDate;
    std::vector< gmshfem::algebra::Vector< std::complex< double > > > _dofValues;
    mutable unsigned int _index;
public:
    WaveMultiField(unsigned int n, const std::string& name = "") : _field(), _isInitialized(false), _isUpToDate(false), _dofValues(n), _index(n+1) {_field.name(name);};

    void initializeField(const WaveField<T_Physic>& field);
    bool isInitialized() const {return _isInitialized;};

    unsigned int size() const {return _dofValues.size();};

    const WaveField<T_Physic> &operator[](const unsigned int index) const;

    void setValues(const unsigned int index, gmshfem::algebra::Vector< std::complex< double > > &vector);

    void write(std::string name) const;
    std::string name() const {return _field.name();};

    friend class WaveAuxilaryField<T_Physic>;
};

/*
* WaveAuxilaryField
*/
template<Physic T_Physic>
class WaveAuxilaryField : public WaveField<T_Physic>
{
private:
    WaveMultiField<T_Physic>* _wmf;
    unsigned int _index;
    std::vector< unsigned int > _systemValuesToDofValues;
    std::vector< unsigned int > _unknownIndices;
public:
    WaveAuxilaryField(std::string name, gmshfem::domain::Domain domain, std::string gmodel, const wave::Discretization<T_Physic>& w_discret): WaveField<T_Physic>(name,domain,gmodel,w_discret), _wmf(nullptr), _index(0), _systemValuesToDofValues() {};
    void setField(WaveMultiField<T_Physic>* wmf){_wmf=wmf;_systemValuesToDofValues.clear();};
    void setIndex(unsigned int index){_index=index;};
    virtual void assignValues(const std::vector< std::complex<double> > &values) override;
};


#endif // H_COMMON_WAVE_ELEMENT
