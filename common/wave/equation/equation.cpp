// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFem Library
//#include "FieldRoutines.h"

//FWI Library
#include "equation.h"
#include "../../../specific/wave/equation/newEquation.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::post;

static const std::complex< double > im = std::complex< double >(0., 1.);

/*
* EquationInterface
*/
template<Physic T_Physic>
EquationInterface<T_Physic>::EquationInterface(const ConfigurationInterface* const config, const GmshFem& gmshFem,std::string suffix): _config(config)
{
    if(!gmshFem.userDefinedParameter(_integrationType, "integration_type"))
    {
      throw Exception("Integration type could not be found.");
    }
    if(!(
        gmshFem.userDefinedParameter(_integrationDegreeBlk, "equation_integration_degree_blk")
        ||
        gmshFem.userDefinedParameter(_integrationDegreeBlk, "equation_integration_degree_blk"+suffix)
    ))
    {
      throw Exception("Equation integration degree (bulk) could not be found.");
    }
    if(!(
        gmshFem.userDefinedParameter(_integrationDegreeBnd, "equation_integration_degree_bnd")
        ||
        gmshFem.userDefinedParameter(_integrationDegreeBnd, "equation_integration_degree_bnd"+suffix)
    ))
    {
      throw Exception("Equation integration degree (boundary) could not be found.");
    }
    int buffer=0;
    if(!gmshFem.userDefinedParameter(buffer, "equation_boundary"))
    {
        msg::warning << "Boundary are not considered in equations (default behaviour)." << msg::endl;
    }
    if(buffer==0){_boundary=false; msg::warning << "Boundary are not enabled in equations." << msg::endl;}
    else{_boundary=true; msg::warning << "Boundary are enabled in equations." << msg::endl;}
};

template<Physic T_Physic>
std::complex<double> EquationInterface<T_Physic>::directional(const Sensitivity& sensitivity, const ModelFunction& dm2, Support support) const
{
    compatible_model(sensitivity,dm2);
    unsigned int N = sensitivity.size();
    std::complex<double> sum = 0.;
    std::string integration;
    switch (support)
    {
        case Support::BLK: integration = integrationType(_integrationDegreeBlk); break;
        case Support::BND: integration = integrationType(_integrationDegreeBnd); break;
    }
    for (unsigned int n = 0; n < N; n++)
    {
        sum += integrate( sensitivity[n] * dm2[n], _config->model_unknown(support), integration );
    }
    return sum;
}
/*
* DifferentialEquationInterface
*/
template<Physic T_Physic>
DifferentialEquationInterface<T_Physic>::DifferentialEquationInterface(const ConfigurationInterface* const config, const wave::Discretization<T_Physic>& w_discret, const GmshFem& gmshFem, std::string suffix)
: EquationInterface<T_Physic>(config, gmshFem, suffix), _w_discret(w_discret), _v("_v",_config->wave_evaldomain(),_config->wave_gmodel(),_w_discret),_g(_config->np()),/* _dofg(_config->np(),gmshfem::algebra::Vector<std::complex<double>>()),*/_geAreUpToDate(false),_grAreUpToDate(false), _formulation("wave_formulation"),_systemIsUpToDate(false),_systemIsFactorized(false)
{
    /*
    unsigned int useGreen_buffer = 0;
    gmshFem.userDefinedParameter(useGreen_buffer,"useGreen");
    _useGreen = ((bool) useGreen_buffer);
    */
}

template<Physic T_Physic>
void DifferentialEquationInterface<T_Physic>::update_wave(WaveMultiField<T_Physic>& output, Type type, const DataStateEvaluator<T_Physic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<T_Physic>& ws)
{
    /*
    if(_useGreen){return updateWithGreen(type,s,d,m,w);}
    else {return updateWithSystem(type,s,d,m,w);}
    */
    updateWithSystem(output,type,ds,ms,ws);
}

template<Physic T_Physic>
void
DifferentialEquationInterface<T_Physic>::updateWithSystem(WaveMultiField<T_Physic>& output, Type type, const DataStateEvaluator<T_Physic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<T_Physic>& ws)
{
    if(!_systemIsUpToDate)
    {
      _formulation.removeSystem();
      _formulation.initSystem();

      _systemIsFactorized=false;
      setLHS(ms);
      _formulation.pre();
      _formulation.assemble();
      _systemIsUpToDate=true;
      _formulation.removeTerms();
    }

    if(!output.isInitialized()){output.initializeField(_v);};
    _v.setField(&output);

    for (unsigned int s = 0; s < _config->ns(); s++)
    {
        _v.setIndex(s);
        setRHS(type,s,ds,ms,ws);
        _formulation.assemble();
        _formulation.removeTerms();

        _formulation.solve(_systemIsFactorized);
        _systemIsFactorized=true;
        _formulation.setRHSToZero();
    }
}

/*
template<Physic T_Physic>
void
multiplyAddAssignementReceivers(algebra::Vector<std::complex<double>>& dofv, const std::vector< gmshfem::algebra::Vector<std::complex<double>> >& dofg, const Data<T_Physic>& d, const ConfigurationInterface* const config, unsigned int s)
{
    for (unsigned int r = 0; r < config->nr(s); r++)
    {
        for (unsigned int i = 0; i < dofv.size(); i++)
        {
            dofv[i] += dofg[config->receiver_idx(s)[r]][i] * d.value(s,r);
        }
    }
}

template<Physic T_Physic>
void
multiplyAddAssignementEmitters(algebra::Vector<std::complex<double>>& dofv, const std::vector< gmshfem::algebra::Vector<std::complex<double>> >& dofg, const ConfigurationInterface* const config, unsigned int s)
{
    for (unsigned int e = 0; e < config->ne(s); e++)
    {
        for (unsigned int i = 0; i < dofv.size(); i++)
        {
            dofv[i] += dofg[config->emitter_idx(s)[e]][i];
        }
    }
}
*/
/*
template<Physic T_Physic>
const WaveField<T_Physic>& DifferentialEquationInterface<T_Physic>::updateWithGreen(Type type, unsigned int s, const DataStateEvaluator<T_Physic>& d, const ModelStateEvaluator& m, const WaveStateEvaluator<T_Physic>& w)
{
    throw Exception("Update with green is slower than regular update. A faster version needs to be coded.");
    switch (type)
    {
      case Type::FS:
        {
            if(!_geAreUpToDate){updateGreenEmitter(m);}
            //algebra::Vector<std::complex<double>> _dofv(_v.size());
            //multiplyAddAssignementEmitters(_dofv,_dofg,_config,s);
            //_v.setVector(_dofv);
            return _v;
        }
      case Type::AS:
        {
            if(!_grAreUpToDate){updateGreenReceiver(m);}
            //algebra::Vector<std::complex<double>> _dofv(_v.size());
            //multiplyAddAssignementReceivers(_dofv,_dofg,d.state(Type::AS),_config,s);
            //_v.setVector(_dofv);
            return _v;
        }
      case Type::PFS: case Type::PAS: default:
        return updateWithSystem(type,s,d,m,w);
    }
}
*/
template<Physic T_Physic>
void DifferentialEquationInterface<T_Physic>::updateGreen(const ModelStateEvaluator& m, const std::vector<unsigned int>& p_idx)
{
    if(!_systemIsUpToDate)
    {
      _formulation.removeSystem();
      _formulation.initSystem();

      _systemIsFactorized=false;
      setLHS(m);
      _formulation.pre();
      _formulation.assemble();
      _systemIsUpToDate=true;
      _formulation.removeTerms();
    }

    if(!_g.isInitialized()){_g.initializeField(_v);};
    _v.setField(&_g);

    for (auto it = p_idx.begin(); it != p_idx.end(); it++)
    {
        _v.setIndex(*it);
        setGreenRHS(*it);
        _formulation.assemble();
        _formulation.removeTerms();

        _formulation.solve(_systemIsFactorized);
        _systemIsFactorized=true;
        _formulation.setRHSToZero();
    }
}

template<Physic T_Physic>
void DifferentialEquationInterface<T_Physic>::updateGreenEmitter(const ModelStateEvaluator& m)
{
    updateGreen(m,_config->all_emitter_idx(!_grAreUpToDate));
    _geAreUpToDate=true;
}

template<Physic T_Physic>
void DifferentialEquationInterface<T_Physic>::updateGreenReceiver(const ModelStateEvaluator& m)
{
    updateGreen(m,_config->all_receiver_idx(!_geAreUpToDate));
    _grAreUpToDate=true;
}

template<Physic T_Physic>
bool DifferentialEquationInterface<T_Physic>::modelIsObsolete()
{
    _systemIsUpToDate=false;
    _systemIsFactorized=false;
    _geAreUpToDate=false;
    _grAreUpToDate=false;
    return true;
}


/*
* Class ModelStateConverter
*/
ModelStateConverter::ModelStateConverter(const ConfigurationInterface* const config) : _state{
    ModelFunction(),
    ModelFunction()
}, _isUpToDate{ {false,false} }, _domain(config->model_evaldomain())
{

}
const ModelFunction& ModelStateConverter::state(Type type) const
{
    if ( !(type == Type::FS || type ==Type::PFS) )
    {
        throw Exception("Model state converter can only provide F and PF fields");
    }
    if(_isUpToDate[type]){return _state[type];}
    else
    {
        throw Exception(type_to_string(type) + " model is not up to date while required.");
    }
}
void ModelStateConverter::write(Type type, std::string name) const
{
    for (auto it = state(type).begin(); it != state(type).end(); it++)
    {
        gmshfem::post::save(*it,_domain,name,"pos","");
    }
}
bool ModelStateConverter::available(Type type) const
{
    if ( !(type == Type::FS || type ==Type::PFS) )
    {
        throw Exception("Model state converter can only provide F and PF fields");
        return false;
    }
    return _isUpToDate[type];
}

/*
* ParametrizationEquation
*/
template<Physic T_Physic>
ParametrizedEquation<T_Physic>::ParametrizedEquation(const ParametrizationInterface* const parametrization, unsigned int f_idx, double pulsation, const ConfigurationInterface* const config, const wave::Discretization<T_Physic>& w_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix) : EquationInterface<T_Physic>(config,gmshFem,suffix), _msc(config), _equation(newEquation(f_idx, pulsation,config,w_discret,gmshFem,suffix)),_parametrization(parametrization->clone())
{
    _parametrization->pulsation(pulsation);
    if(!_equation->compatible(_parametrization))
    {
        throw Exception("Equation and parametrization are not compatible.");
    }
};

//Wave related
template<Physic T_Physic>
void
ParametrizedEquation<T_Physic>::update_wave(WaveMultiField<T_Physic>& output, Type type, const DataStateEvaluator<T_Physic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<T_Physic>& ws)
{
    update_model(ms);
    _equation->update_wave(output,type,ds,_msc,ws);
}

//Model/sensitivity related
template<Physic T_Physic>
void ParametrizedEquation<T_Physic>::update_model(const ModelStateEvaluator& ms)
{
    if(ms.available(Type::FS) && !_msc._isUpToDate[Type::FS])
    {
        _msc._state[Type::FS] = (*_parametrization)(ms.state(Type::FS));
        _msc._isUpToDate[Type::FS] = true;
    }
    if(ms.available(Type::PFS) && !_msc._isUpToDate[Type::PFS])
    {
        _msc._state[Type::PFS] = (*_parametrization)(ms.state(Type::PFS),ms.state(Type::FS));
        _msc._isUpToDate[Type::PFS] = true;
    }
}
template<Physic T_Physic>
Sensitivity ParametrizedEquation<T_Physic>::update_sensitivity(Order order, Support support, const DataStateEvaluator<T_Physic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<T_Physic>& ws)
{
    update_model(ms);
    if(!_ssc._isUpToDate[order][support])
    {
        _ssc._state[order][support] = _equation->update_sensitivity(order,support,ds,_msc,ws);
        _ssc._isUpToDate[order][support] = true;
    }
    return (*_parametrization)(_ssc,order,support,_msc);

}
template<Physic T_Physic>
bool ParametrizedEquation<T_Physic>::modelIsObsolete()
{
    _msc._isUpToDate[Type::FS] = false;
    _ssc._isUpToDate[Order::FST] = {false,false};
    _ssc._isUpToDate[Order::SCD] = {false,false};

    bool diagNoMoreUpToDate = _equation->modelIsObsolete();
    if(_ssc._isUpToDate[Order::DIAG][Support::BLK])
    {
        _ssc._isUpToDate[Order::DIAG][Support::BLK] = !diagNoMoreUpToDate;
    }
    if(_ssc._isUpToDate[Order::DIAG][Support::BND])
    {
        _ssc._isUpToDate[Order::DIAG][Support::BND] = !diagNoMoreUpToDate;
    }

    if(!_parametrization->isModelIndependant())
    {
         //Because of parametrization
         diagNoMoreUpToDate = true;
        _msc._isUpToDate[Type::PFS] = false;
    }
    return diagNoMoreUpToDate;
}//Returns true, if sensitivity[DIAG] depends on model

template<Physic T_Physic>
void ParametrizedEquation<T_Physic>::modelPerturbationIsObsolete()
{
    _msc._isUpToDate[Type::PFS] = false;
    _ssc._isUpToDate[Order::SCD] = {false,false};
    _equation->modelPerturbationIsObsolete();
}

template class EquationInterface<Physic::acoustic>;
template class DifferentialEquationInterface<Physic::acoustic>;
template class ParametrizedEquation<Physic::acoustic>;

template class EquationInterface<Physic::electromagnetic>;
template class DifferentialEquationInterface<Physic::electromagnetic>;
template class ParametrizedEquation<Physic::electromagnetic>;

template class EquationInterface<Physic::elastodynamic>;
template class DifferentialEquationInterface<Physic::elastodynamic>;
template class ParametrizedEquation<Physic::elastodynamic>;
