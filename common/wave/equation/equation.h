// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_EQUATION
#define H_COMMON_EQUATION

//GmshFem Library
#include "Formulation.h"

//FWI Library
#include "../state.h"
#include "../../sensitivity/state.h"
#include "../../data/state.h"
#include "../../model/state.h"
#include "../../model/discretization.h"
#include "../../configuration.h"
#include "../../../specific/wave/discretization.h"

/*
* EquationInterface
*/
template<Physic T_Physic>
class EquationInterface
{
protected:
    const ConfigurationInterface* const _config;
    std::string _integrationType;
    unsigned int _integrationDegreeBlk;
    unsigned int _integrationDegreeBnd;

    bool _boundary;
public:
    EquationInterface(const ConfigurationInterface* const config, const gmshfem::common::GmshFem& gmshFem,std::string suffix="");

    virtual ~EquationInterface() = default;

    virtual bool compatible(const ParametrizationInterface* const parametrization) const = 0;

    //Wave related
    virtual void update_wave(WaveMultiField<T_Physic>& output, Type type, const DataStateEvaluator<T_Physic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<T_Physic>& ws) = 0;

    //Model/sensitivity related
    virtual Sensitivity update_sensitivity(Order order, Support support, const DataStateEvaluator<T_Physic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<T_Physic>& ws) = 0;
    std::complex<double> directional(const Sensitivity& sensitivity, const ModelFunction& dm2, Support support) const;
    std::string integrationType(unsigned int degree) const {return _integrationType + std::to_string(degree);};

    virtual bool modelIsObsolete() {return false;};//Returns true, if sensitivity[DIAG] depends on model
    virtual void modelPerturbationIsObsolete() {};
};

/*
* DifferentialEquationInterface
*/
template <Physic T_Physic>
class DifferentialEquationInterface: public EquationInterface<T_Physic>
{
protected:
    using EquationInterface<T_Physic>::_config;
    const wave::Discretization<T_Physic> _w_discret;

    WaveAuxilaryField<T_Physic> _v;

    //bool _useGreen;
    WaveMultiField<T_Physic> _g;
    //std::vector< gmshfem::algebra::Vector<std::complex<double>> > _dofg;
    bool _geAreUpToDate;
    bool _grAreUpToDate;

    gmshfem::problem::Formulation< std::complex< double > > _formulation;
    bool _systemIsUpToDate;
    bool _systemIsFactorized;
public:
    DifferentialEquationInterface(const ConfigurationInterface* const config, const wave::Discretization<T_Physic>& w_discret, const gmshfem::common::GmshFem& gmshFem,std::string suffix = "");

    //Wave related
    virtual void update_wave(WaveMultiField<T_Physic>& output, Type type, const DataStateEvaluator<T_Physic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<T_Physic>& ws);

    bool modelIsObsolete() override;//Returns true, if sensitivity[DIAG] depends on model
protected:
    void updateWithSystem(WaveMultiField<T_Physic>& output, Type type, const DataStateEvaluator<T_Physic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<T_Physic>& ws);
    //const WaveField<T_Physic>& updateWithGreen(Type type, unsigned int s, const DataStateEvaluator<T_Physic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<T_Physic>& ws);
    virtual void setLHS(const ModelStateEvaluator& m) = 0;
    virtual void setRHS(Type type,unsigned int s, const DataStateEvaluator<T_Physic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<T_Physic>& ws) = 0;

    void updateGreen(const ModelStateEvaluator& ms,const std::vector<unsigned int>& p_idx);
    void updateGreenEmitter(const ModelStateEvaluator& ms);
    void updateGreenReceiver(const ModelStateEvaluator& ms);
    virtual void setGreenRHS(unsigned int p) = 0;
};

/*
* ModelStateConverter
*/
class ModelStateConverter final: public ModelStateEvaluator
{
private:
    std::array<ModelFunction,2> _state;
    std::array<bool,2> _isUpToDate;

    gmshfem::domain::Domain _domain;
public:
    ModelStateConverter(const ConfigurationInterface* const config);

    virtual const ModelFunction& state(Type type) const;
    virtual void write(Type type, std::string name) const;
    virtual bool available(Type type) const;

    template<Physic T_Physic> friend class ParametrizedEquation;
};

/*
* ParametrizedEquation
*/
template<Physic T_Physic>
class ParametrizedEquation final: public EquationInterface<T_Physic>
{
protected:
    using EquationInterface<T_Physic>::_config;
private:
    ModelStateConverter _msc;
    SensitivityState _ssc;
    EquationInterface<T_Physic>* const _equation;
    ParametrizationInterface* const _parametrization;

public:
    ParametrizedEquation(const ParametrizationInterface* const parametrization, unsigned int f_idx, double pulsation, const ConfigurationInterface* const config, const wave::Discretization<T_Physic>& w_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix = "");

    ~ParametrizedEquation() {delete _parametrization; delete _equation;};

    virtual bool compatible(const ParametrizationInterface* const parametrization) const {return false;};

    //Wave related
    virtual void update_wave(WaveMultiField<T_Physic>& output, Type type, const DataStateEvaluator<T_Physic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<T_Physic>& ws);

    //Model/sensitivity related
    void update_model(const ModelStateEvaluator& ms);
    virtual Sensitivity update_sensitivity(Order order, Support support, const DataStateEvaluator<T_Physic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<T_Physic>& ws);
    virtual bool modelIsObsolete() override;//Returns true, if sensitivity[DIAG] depends on model
    virtual void modelPerturbationIsObsolete() override;
};

#endif //H_COMMON_EQUATION
