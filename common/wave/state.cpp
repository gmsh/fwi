// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmeshFem Library
#include "Exception.h"

//FWI Library
#include "state.h"

using namespace gmshfem::common;

/*
* Class WaveState
*/
template<Physic T_Physic>
const WaveMultiField<T_Physic>& WaveState<T_Physic>::state(Type type) const
{
    if(_isUpToDate[((unsigned int) type)]){return _state[((unsigned int) type)];}
    else
    {
        throw Exception(type_to_string(type) + " wave is not up to date while required.");
    }
}

template<Physic T_Physic>
const WaveField<T_Physic>& WaveState<T_Physic>::state(Type type, unsigned int index) const
{
    if(!(index < _state[((unsigned int) type)].size()))
    {
        throw Exception("Wave field index is bigger than vector size.");
    }
    if(!_isUpToDate[((unsigned int) type)])
    {
        throw Exception(type_to_string(type) + " wave is not up to date while required.");
    }
    return (_state[((unsigned int) type)])[index];
}

template class WaveState<Physic::acoustic>;
template class WaveState<Physic::electromagnetic>;
template class WaveState<Physic::elastodynamic>;
