// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_WAVE_STATE
#define H_COMMON_WAVE_STATE

//Standard Library
#include <array>

//GmshFWI Library
#include "../enum.h"
#include "element.h"
#include "../configuration.h"

//Forward declaration
template<Physic T_Physic>
class WaveUpdater;

/*
* WaveStateEvaluator
*/
template<Physic T_Physic>
class WaveStateEvaluator
{
public:
    virtual const WaveMultiField<T_Physic>& state(Type type) const = 0;
    virtual const WaveField<T_Physic>& state(Type type, unsigned int index) const = 0;
    void write(Type type, std::string name) const {state(type).write(name);};
};

/*
* WaveState
*/
template<Physic T_Physic>
class WaveState final: public WaveStateEvaluator<T_Physic>
{
private:
    std::array<WaveMultiField<T_Physic>,4> _state;
    std::array<bool,4> _isUpToDate;
public:
    WaveState(const ConfigurationInterface* const config):
    _state{
        WaveMultiField<T_Physic>(config->ns(),"u"),
        WaveMultiField<T_Physic>(config->ns(),"du"),
        WaveMultiField<T_Physic>(config->ns(),"lambda"),
        WaveMultiField<T_Physic>(config->ns(),"dlambda")
    }, _isUpToDate{false,false,false,false} {};

    virtual const WaveMultiField<T_Physic>& state(Type type) const;
    virtual const WaveField<T_Physic>& state(Type type, unsigned int index) const;

    friend class WaveUpdater<T_Physic>;
};

#endif // H_COMMON_WAVE_STATE
