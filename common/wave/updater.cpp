// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard library

//GmshFem library
#include "Exception.h"

//GmshFWI Library
#include "updater.h"
#include "../data/updater.h"

using namespace gmshfem;
using namespace gmshfem::common;


template<Physic T_Physic>
void WaveUpdater<T_Physic>::update(Type type, const ModelStateEvaluator& m)
{
    if(_ws._isUpToDate[type]){return;}

    std::array<bool,5> dataNeedToBeUpToDate = {false,false,false,false,false};
    switch (type)
    {
        case Type::FS:
            break;
        case Type::AS:
            dataNeedToBeUpToDate[Type::AS] = true;
            break;
        case Type::PFS:
            update(Type::FS,m);
            break;
        case Type::PAS:
            update(Type::AS,m);
            dataNeedToBeUpToDate[Type::PAS] = true;
            break;
        case Type::DS: default:
            throw Exception("Impossible to update wave state other than F,A,PF,PA");
            break;
    }

    if( (_du==nullptr) && (type != Type::FS) )
    {
        throw Exception("Wave updater can only update forward state when no data updater is provided (_du = nullptr)");
    }

    _equation->update_wave(_ws._state[type],type,_du->get(dataNeedToBeUpToDate,m),m,_ws);
    _ws._isUpToDate[type] = true;
}

template<Physic T_Physic>
const WaveStateEvaluator<T_Physic>& WaveUpdater<T_Physic>::get(std::array<bool,4> needToBeUpToDate, const ModelStateEvaluator& m)
{
    for (unsigned int t = 0; t < 4; t++)
    {
        Type type = ((Type) t);
        if(needToBeUpToDate[type]){update(type,m);}
    }
    return _ws;
}

template<Physic T_Physic>
void WaveUpdater<T_Physic>::isObsolete(std::array<bool,4> NoMoreUpToDate)
{
    for (unsigned int t = 0; t < 4; t++)
    {
        if(NoMoreUpToDate[t]){_ws._isUpToDate[t] = false;}
    }
}

/*
template<Physic T_Physic>
ModelFunction WaveUpdater<T_Physic>::update_sensitivity(Order order, Support support, const ModelStateEvaluator& ms)
{
    switch (order)
    {
        case Order::SCD:
            update(Type::PFS,ms);
            update(Type::PAS,ms);
            //no break;
        case Order::FST:
            update(Type::FS,ms);
            update(Type::AS,ms);
    }
    return _equation->update_sensitivity(order, support, ms, _ws);
}

template<Physic T_Physic>
ModelFunction WaveUpdater<T_Physic>::update_preconditioner(const ModelStateEvaluator& ms)
{
    return _equation->update_preconditioner(ms);
}
*/

template class WaveUpdater<Physic::acoustic>;
template class WaveUpdater<Physic::electromagnetic>;
template class WaveUpdater<Physic::elastodynamic>;
