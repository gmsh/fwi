// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_COMMON_WAVE_UPDATER
#define H_COMMON_WAVE_UPDATER

//GmshFWI Library
#include "equation/equation.h"
#include "element.h"
//#include "../configuration.h"

//Forward declaration
template<Physic T_Physic>
class DataUpdater;

/*
* WaveUpdater
*/
template<Physic T_Physic>
class WaveUpdater final
{
private:
    WaveState<T_Physic> _ws;
    DataUpdater<T_Physic>* const _du;
    EquationInterface<T_Physic>* const _equation;

    void update(Type type, const ModelStateEvaluator& ms);
public:
    WaveUpdater(const ConfigurationInterface* const config,DataUpdater<T_Physic>* du, EquationInterface<T_Physic>* equation) : _ws(config), _du(du), _equation(equation) {};

    const WaveStateEvaluator<T_Physic>& get(std::array<bool,4> needToBeUpToDate, const ModelStateEvaluator& ms);

    void isObsolete(std::array<bool,4> NoMoreUpToDate);
};

#endif // H_COMMON_WAVE_ELEMENT
