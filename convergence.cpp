//GmshFEM Library
#include "GmshFem.h"
#include "Post.h"
#include "CSVio.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::post;
using namespace gmshfem::function;

//FWI Library
#include "specific/physic.h"
#include "specific/configuration/newConfiguration.h"
#include "specific/model/parametrization/newParametrization.h"
#include "common/wave/equation/equation.h"
#include "specific/data/objective/newObjective.h"
#include "common/statefunctional.h"

static const std::complex< double > im = std::complex< double >(0., 1.);

enum class Interval: unsigned int { Linear=0, Log=1 };
Interval to_interval(const GmshFem& gmshFem)
{
  std::string interval_buffer;
  if(gmshFem.userDefinedParameter(interval_buffer, "interval"))
  {
    if(interval_buffer=="linear"){return Interval::Linear;}
    else if(interval_buffer=="log"){return Interval::Log;}
  }
  return Interval::Linear;
}

template <Physic T_Physic>
int convergence(const GmshFem& gmshFem)
{
    std::string name = "";
    gmshFem.userDefinedParameter(name, "prename");
    std::string suffix = "noname";
    gmshFem.userDefinedParameter(suffix, "name");
    name += suffix;

    ParametrizationInterface* const parametrization = newParametrization<T_Physic>(gmshFem);

    auto configuration = newConfiguration(name, parametrization, gmshFem);

    std::vector<double> frequency(1,0.);
    if(!gmshFem.userDefinedParameter(frequency[0], "frequency0"))
    {
        throw Exception("Frequency could not be found.");
    }
    std::vector<std::string> filename (1,"noname");
    gmshFem.userDefinedParameter(filename[0], "data0");
    const Data<T_Physic> d0(filename,frequency,configuration.get());

    ObjectiveInterface<T_Physic>* const objective = newObjective<T_Physic>(d0,configuration.get(),gmshFem);

    wave::Discretization<T_Physic> w_discret(gmshFem);

    std::vector<EquationInterface<T_Physic>*> pequation(1,new ParametrizedEquation<T_Physic>(parametrization,0,2.*M_PI*frequency[0],configuration.get(),w_discret,gmshFem));

    StateFunctional<T_Physic>* const statefunctional = new StateFunctional<T_Physic>(configuration.get(),nullptr,frequency,pequation,objective);
    FunctionalInterface* const functional = statefunctional;

    double eps0,epsN;
    unsigned int N;
    if(!(
        gmshFem.userDefinedParameter(N, "N") &&
        gmshFem.userDefinedParameter(eps0, "eps0") &&
        gmshFem.userDefinedParameter(epsN, "epsN")
    ) )
    {
        throw Exception("A directional parameter could not be found.");
    }
    ModelFunction dm(parametrization->size());

    for (unsigned int c = 0; c < parametrization->size(); c++)
    {
        std::string sfx = "c"+std::to_string(c);
        double Redm,Imdm;
        if(!(
            gmshFem.userDefinedParameter(Redm, "Re(dm"+sfx+")") &&
            gmshFem.userDefinedParameter(Imdm, "Im(dm"+sfx+")")
        ) )
        {
            throw Exception("A directional parameter (component "+sfx+" could not be found.");
        }

        ScalarPiecewiseFunction< std::complex< double > > spf_dm;
        spf_dm.addFunction(Redm+im*Imdm,configuration->model_unknown(Support::BLK) | configuration->model_unknown(Support::BND));

        dm[c] = spf_dm;
    }
    functional->setModelPerturbation(dm);



    CSVio output(name, ';', common::OpeningMode::NewFile);

    ModelFunction m = configuration->m0();

    msg::print << "Compute derivatives (adjoint state method)" << msg::endl;
    msg::indent();
    double j = functional->performance( m );
    msg::print << "j: "<< j << msg::endl;

    double dj_v = statefunctional->directional(Order::FST, Support::BLK, dm);
    msg::print << "dj_v: "<< dj_v << msg::endl;

    double dj_s = statefunctional->directional(Order::FST, Support::BND, dm);
    msg::print << "dj_s: "<< dj_s << msg::endl;
    msg::print << "jd: "<< dj_s+dj_v << msg::endl;

    double d2j_s = statefunctional->directional(Order::SCD, Support::BND, dm);
    msg::print << "d2j_s: "<< d2j_s << msg::endl;
    double d2j_v = statefunctional->directional(Order::SCD, Support::BLK, dm);
    msg::print << "d2j_v: "<< d2j_v << msg::endl;
    msg::print << "j2d: "<< d2j_s+d2j_v << msg::endl;
    output <<j << dj_s << dj_v << dj_s+dj_v << d2j_s << d2j_v << d2j_s+d2j_v << csv::endl;
    msg::unindent();

    Interval interval = to_interval(gmshFem);
    double base =  epsN / eps0;
    double span =  epsN - eps0;
    msg::print << "Compute derivatives (finite difference method)" << msg::endl;
    msg::indent();
    for (unsigned int n = 0; n <= N; n++)
    {
        double eps;
        switch (interval)
        {
          case Interval::Log:
            eps  = eps0 * std::pow(base, ((double) n) / ((double) N));
            break;
          case Interval::Linear:
            eps = eps0 + ((double)n) / ((double)N) * span;
            break;
        }
        msg::print << "--- Step length #" << n << ": eps = " << eps << " ---" << msg::endl;
        msg::indent();

        double jp =  functional->performance(m + dm*eps);
        msg::print << "jp: "<< jp << msg::endl;
        double jm =  functional->performance(m - dm*eps);
        msg::print << "jm: "<< jm << msg::endl;
        msg::print << "jFD: "<< (jp - jm) / 2. / eps << msg::endl;
        msg::print << "j2FD: "<< (jp - 2.*j + jm) / eps / eps << msg::endl;

        //msg::print << "j2FD: "<< (jp - 2.*j + jm) / eps /eps << msg::endl;

        output << eps << (jp - jm) / 2. / eps << std::abs(dj_s+dj_v - (jp - jm) / 2. / eps) << std::abs(dj_v - (jp - jm) / 2. / eps) << (jp - 2.*j + jm) / eps / eps  << std::abs(d2j_s+d2j_v - (jp - 2.*j + jm) / eps /eps) << csv::endl;
        msg::unindent();
    }
    msg::unindent();
    output.close();

    delete statefunctional;
    delete objective;
    delete pequation[0];
    delete parametrization;

    return 0;
}

int main(int argc, char **argv)
{
  GmshFem gmshFem(argc, argv);
  Physic T_Physic = to_physic(gmshFem);
  switch (T_Physic)
  {
    case Physic::acoustic: default:
      return convergence<Physic::acoustic>(gmshFem);
    case Physic::electromagnetic:
      return convergence<Physic::electromagnetic>(gmshFem);
    case Physic::elastodynamic:
      return convergence<Physic::elastodynamic>(gmshFem);
  }
}
