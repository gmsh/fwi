#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 19 14:22:17 2019

@author: xavier
"""
import numpy as np
from enum import Enum
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
#Value for 1 width figure
#H = 4.0;
#L = 5.0;
#Value for 2 width figure
H = 3.0;
L = 3.20;

class Config:
    weakly_inclusion=1
    weakly_background=2
    highly_inclusion=3
    highly_background=4

config = 3;
path = '../Paper1/Review/data/convergence/'
filename = {
    Config.weakly_inclusion: 'weakly_inclusion',
    Config.weakly_background: 'weakly_background',
    Config.highly_inclusion: 'highly_inclusion',
    Config.highly_background: 'highly_background',
}[config]

ref = np.loadtxt(path+filename+'_convergence.csv',delimiter=';',skiprows=0,max_rows=1);
x = np.loadtxt(path+filename+'_convergence.csv',delimiter=';',skiprows=1);


#Cost function
f0 = plt.figure(figsize=(L,H),tight_layout=False);
plt.subplots_adjust(top=0.93,right=0.95,bottom=0.2,left=0.25)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax = plt.gca().invert_xaxis()
plt.ylim(1e-9,2.)
plt.loglog(x[:,0],x[:,2],color='k',marker='+',markersize=5,linewidth=0)
plt.hlines(ref[1],x[0,0],x[-1,2],colors='tab:blue', linestyles='dashed')
plt.xlabel(r'$\epsilon$')
plt.grid()
plt.show()

f1 = plt.figure(figsize=(L,H),tight_layout=False);
plt.subplots_adjust(top=0.93,right=0.95,bottom=0.2,left=0.25)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax = plt.gca().invert_xaxis()
plt.ylim(1e-9,2.)
plt.loglog(x[:,0],x[:,3],color='k',marker='+',markersize=5,linewidth=0)
plt.hlines(ref[1],x[0,0],x[-1,0],colors='tab:blue', linestyles='dashed')
plt.xlabel(r'$\epsilon$')
plt.grid()
plt.show()
