#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  9 10:39:58 2021

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':12})
rc('text', usetex=True)

n_freq = 64
fmin = 0.25
fmax = 16.
freq = np.linspace(fmin,fmax,n_freq)
weight = 1j * 2. * np.pi * freq
n_s = 2
n_r = 2
data = np.zeros([n_freq,n_s,n_r])*1j
data_w = np.zeros([n_freq,n_s,n_r])*1j
snr = 200

name_synthetics = 'build/paper3_monoslab_synthetics'
for f in range(0,n_freq):
    suffix = str(f)+'.csv'
    tmp = np.loadtxt(name_synthetics+'_data'+suffix,delimiter=';')
    data[f,:,:] = (tmp[:,0:2*n_r:2] + 1j * tmp[:,1:2*n_r:2])
    data_w[f,:,:] = data[f,:,:] * weight[f]

data_w_mean_energy2 = np.linalg.norm(data_w)**2 / n_freq / n_s / n_r

noise_w = np.random.normal(loc=0.0, scale=np.sqrt(data_w_mean_energy2/2./snr), size=[n_freq,n_s,n_r]) + 1j * np.random.normal(loc=0.0, scale=np.sqrt(data_w_mean_energy2/2./snr), size=[n_freq,n_s,n_r])

noise_w_mean_energy2 = np.linalg.norm(noise_w)**2  / n_freq / n_s / n_r

noisy_data_w = (data_w+noise_w)

noisy_data = np.zeros([n_freq,n_s,n_r])*1j
noise = np.zeros([n_freq,n_s,n_r])*1j
for f in range(0,n_freq):
    noise[f,:,:] = noise_w[f,:,:] / weight[f]    
    noisy_data[f,:,:] = noisy_data_w[f,:,:] / weight[f]
    np.savetxt(name_synthetics+'_snr'+str(snr)+'_data'+str(f)+'.csv', noisy_data[f,:,:].view(float), fmt='%.18e', delimiter=';', newline='\n')