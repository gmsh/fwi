#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 18:11:08 2020

@author: xavier
"""
import numpy as np
import scipy as sp
from scipy import special
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)

f=1
ω=2.*np.pi*f
s0=1.
L=3.
nr=9600
xs=0.
Δxr = L/(nr-1)
xr=np.linspace(Δxr,L,nr)

def u(xs,xr,s):
    return -1j/4 * sp.special.hankel1(0,ω*s*np.abs(xs-xr))

def grad(d):
    dd = 1j*np.zeros(nr)
    dd[0] = (d[1]-d[0])/Δxr
    for i in range(1,nr-1):
        dd[i] = (d[i+1]-d[i-1])/2./Δxr
    dd[nr-1]=(d[nr-1]-d[nr-2])/Δxr
    return dd
    
def distL2(d,d0):
    return 1/nr*np.linalg.norm(d/d0-1)**2
def pdistH1(d,d0):
    r = d/d0-1
    dr = grad(r)
    return 1/nr*np.linalg.norm(dr)**2
def pdistH1_ph(d,d0):
    r_ph = 1-np.exp(1j*np.angle(d/d0))
    dr = grad(r_ph)
    return 1/nr*np.linalg.norm(dr)**2
def pdistH1_amp(d,d0):
    r_amp = 1-np.abs(d/d0)
    dr = grad(r_amp)
    return 1/nr*np.linalg.norm(dr)**2

ϵ=1.5
r_amp = np.abs(u(xs,xr,ϵ*s0)/u(xs,xr,s0))
r_ph = np.exp(1j*np.angle(u(xs,xr,ϵ*s0)/u(xs,xr,s0)))
r = u(xs,xr,ϵ*s0)/u(xs,xr,s0)
plt.plot(xr,np.real(r))
plt.plot(xr,np.real(r_amp))
plt.plot(xr,np.real(r_ph))
plt.plot(xr,np.real(grad(r)))
plt.plot(xr,np.real(grad(r_ph)))
plt.plot(xr,np.real(grad(r_amp)))

plt.plot(xr,np.imag(1-u(xs,xr,ϵ*s0)/u(xs,xr,s0)))
plt.plot(xr,np.real(grad(1-u(xs,xr,ϵ*s0)/u(xs,xr,s0))))
plt.plot(xr,np.imag(grad(1-u(xs,xr,ϵ*s0)/u(xs,xr,s0))))
plt.plot(xr,np.abs(u(xs,xr,ϵ*s0)))
plt.plot(xr,np.abs(u(xs,xr,s0)))

sv = np.linspace(s0/2,2*s0,100)
JL2 = np.zeros(np.size(sv))
JpH1 = np.zeros(np.size(sv))
JpH1_ph = np.zeros(np.size(sv))
JpH1_amp = np.zeros(np.size(sv))
d0 = u(xs,xr,s0)
for i in range(0,np.size(sv)):
    d = u(xs,xr,sv[i])
    JL2[i] = distL2(d,d0)
    JpH1[i] = pdistH1(d,d0)
    JpH1_ph[i] = pdistH1_ph(d,d0)
    JpH1_amp[i] = pdistH1_amp(d,d0)
plt.plot(sv,JL2)
plt.plot(sv,JpH1)
γD = 0.4*L**2/4
plt.plot(sv,(γD*JpH1+JL2) / np.max(γD*JpH1+JL2))
#plt.plot(sv,JpH1_ph)
#plt.plot(sv,JpH1_amp)
