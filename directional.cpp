//Standard Library
#include <numeric>

//GmshFem Library
#include "GmshFem.h"
#include "Post.h"
#include "CSVio.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::post;
using namespace gmshfem::function;

//FWI Library
#include "specific/physic.h"
#include "specific/configuration/newConfiguration.h"
#include "specific/model/parametrization/newParametrization.h"
#include "common/wave/equation/equation.h"
#include "specific/data/objective/newObjective.h"
#include "common/statefunctional.h"

static const std::complex< double > im = std::complex< double >(0., 1.);

enum class Interval: unsigned int { Linear=0, Log=1, Pi2Log1=2, ReLog=3 };
Interval to_interval(const GmshFem& gmshFem)
{
  std::string interval_buffer;
  if(gmshFem.userDefinedParameter(interval_buffer, "interval"))
  {
    if(interval_buffer=="linear"){return Interval::Linear;}
    else if(interval_buffer=="log"){return Interval::Log;}
    else if(interval_buffer=="re-log"){return Interval::ReLog;}
    else if(interval_buffer=="pi2log1"){return Interval::Pi2Log1;}
  }
  return Interval::Linear;
}

enum class SpatialDistribution: unsigned int { Constant=0, HeavisideY0=1 };
SpatialDistribution to_spatialdistribution(const GmshFem& gmshFem)
{
  std::string buffer;
  if(gmshFem.userDefinedParameter(buffer, "spatialdistribution"))
  {
    if(buffer=="constant"){return SpatialDistribution::Constant;}
    else if(buffer=="heavisidey0"){return SpatialDistribution::HeavisideY0;}
  }
  return SpatialDistribution::Constant;
}

template <Physic T_Physic>
int directional(const GmshFem& gmshFem)
{
    std::string name = "";
    gmshFem.userDefinedParameter(name, "prename");
    std::string suffix = "noname";
    gmshFem.userDefinedParameter(suffix, "name");
    name += suffix;

    ParametrizationInterface* const parametrization = newParametrization<T_Physic>(gmshFem);

    auto configuration = newConfiguration(name, parametrization, gmshFem);

    unsigned int n_freq_tot = 0;
    if(!gmshFem.userDefinedParameter(n_freq_tot, "n_freq"))
    {
        throw Exception("Total frequency number could not be found.");
    }
    std::vector<double> frequency(n_freq_tot);
    std::vector<std::string> filename(n_freq_tot);
    for (unsigned int f = 0; f < n_freq_tot; f++)
    {
        std::string suffix = std::to_string(f);
        if(!gmshFem.userDefinedParameter(frequency[f], "frequency"+suffix))
        {
            throw Exception("Frequency #"+suffix+" could not be found.");
        }
        if(!gmshFem.userDefinedParameter(filename[f], "data"+suffix))
        {
            msg::warning << "Data filename #"+suffix+" could not be found." << msg::endl;
            if(!gmshFem.userDefinedParameter(filename[f], "data"))
            {
                throw Exception("Data filename could not be found.");
            }
            filename[f] += suffix;
            msg::warning << "filename #"+suffix+" = "<< filename[f] << msg::endl;
        }
    }
    const Data<T_Physic> d0(filename,frequency,configuration.get());

    unsigned int n_group = 0;
    if(!gmshFem.userDefinedParameter(n_group, "n_group"))
    {
        throw Exception("Number of frequency groups could not be found.");
    }

    for (unsigned int g = 0; g < n_group; g++)
    {
        std::string suffix_g = std::to_string(g);
        msg::print << "--- Frequency group #" + suffix_g << " --- " << msg::endl;
        msg::indent();

        unsigned int n_freq = 0;
        std::vector<unsigned int> freq_idx;
        if(!gmshFem.userDefinedParameter(n_freq, "n_freq"+suffix_g))
        {
            msg::warning << "Total frequency number for frequency group #" << suffix_g << " could not be found." << msg::endl;
            if(n_group == n_freq_tot)
            {
                n_freq = 1;
                freq_idx.resize(n_freq);
                freq_idx[0] = g;
                msg::warning << "A single frequency is used for frequency group #" << suffix_g << msg::endl;
            }
            else if(n_group == 1)
            {
                n_freq = n_freq_tot;
                freq_idx.resize(n_freq);
                std::iota(freq_idx.begin(), freq_idx.end(), 0);
                msg::warning << "All frequencies are used for frequency group #" << suffix_g << msg::endl;
            }
            else
            {
                throw Exception("Simultaneous inversion of all frequencies requires n_group = 1. Sequential inversion of all frequencies requires n_group = n_freq_tot. Any intermediate case must be specified explicitely");
            }
        }
        else
        {
            freq_idx.resize(n_freq);
            for (unsigned int f = 0; f < n_freq; f++)
            {
                std::string suffix_f = std::to_string(f);
                if(!gmshFem.userDefinedParameter(freq_idx[f], "frequency"+suffix_g+suffix_f))
                {
                    freq_idx[f] = f;
                    msg::warning << "Frequency index #"+suffix_f+" for group #" + suffix_g + " could not be found. Index = " << f << "(default)." << msg::endl;
                }
                msg::print << "frequency #" + suffix_f << " = " << frequency[freq_idx[f]] << msg::endl;
            }
        }
        const Data<T_Physic> d0g(freq_idx,d0);

        auto objective = newObjective<T_Physic>(d0g,configuration.get(),gmshFem);

        std::vector<EquationInterface<T_Physic>*> pequation(n_freq);
        std::vector<double> subfrequency(n_freq);
        for (unsigned int f = 0; f < n_freq; f++)
        {
            std::string suffix_f = std::to_string(freq_idx[f]);
            subfrequency[f] = frequency[freq_idx[f]];

            wave::Discretization<T_Physic> w_discret(gmshFem,suffix_f);
            pequation[f] = new ParametrizedEquation<T_Physic>(parametrization,f,2.*M_PI*subfrequency[f],configuration.get(),w_discret,gmshFem,suffix_f);
        }

        StateFunctional<T_Physic>* const statefunctional = new StateFunctional<T_Physic>(configuration.get(),nullptr,nullptr,subfrequency,pequation,objective.get());
        FunctionalInterface* const functional = statefunctional;

        double eps;
        unsigned int N;
        if(!(
            gmshFem.userDefinedParameter(N, "N") &&
            gmshFem.userDefinedParameter(eps, "eps")
        ) )
        {
            throw Exception("A directional parameter could not be found.");
        }

        std::vector<std::complex<double>> mjp(parametrization->size());
        std::vector<double> yjp(parametrization->size());
        std::vector<std::complex<double>> mu0(parametrization->size());
        std::vector<std::complex<double>> muN(parametrization->size());
        std::vector<double> span(parametrization->size());
        std::vector<double> ratio(parametrization->size(),0.);
        std::vector<std::complex<double>> dir(parametrization->size());
        ModelFunction dm(parametrization->size());

        SpatialDistribution spatialdistribution = to_spatialdistribution(gmshFem);
        for (unsigned int c = 0; c < parametrization->size(); c++)
        {
            std::string sfx = "c"+std::to_string(c);
            /* code */
            double Remu0,Immu0,RemuN,ImmuN,Redm,Imdm;
            if(!(
                gmshFem.userDefinedParameter(Remu0, "Re(mu"+sfx+")") &&
                gmshFem.userDefinedParameter(Immu0, "Im(mu"+sfx+")") &&
                gmshFem.userDefinedParameter(RemuN, "Re(muN"+sfx+")") &&
                gmshFem.userDefinedParameter(ImmuN, "Im(muN"+sfx+")") &&
                gmshFem.userDefinedParameter(Redm, "Re(dm"+sfx+")") &&
                gmshFem.userDefinedParameter(Imdm, "Im(dm"+sfx+")")
            ) )
            {
                throw Exception("A directional parameter (component "+sfx+" could not be found.");
            }

            ScalarPiecewiseFunction< std::complex< double > > spf_dm;
            spf_dm.addFunction(Redm+im*Imdm,configuration.get()->model_unknown(Support::BLK) | configuration->model_unknown(Support::BND));

            dm[c] = spf_dm;

            double Remjp = 0.;
            double Immjp = 0.;
            switch (spatialdistribution)
            {
                case SpatialDistribution::HeavisideY0:
                if(!(
                    gmshFem.userDefinedParameter(Remjp, "Re(mjump"+sfx+")") &&
                    gmshFem.userDefinedParameter(Immjp, "Im(mjump"+sfx+")") &&
                    gmshFem.userDefinedParameter(yjp[c], "yjump"+sfx)
                )
            )
            {
                throw Exception("A spatial distribution parameter could not be found.");
            }
            mjp[c] = Remjp + im * Immjp;
            break;
            case SpatialDistribution::Constant:
            break;
        }

        mu0[c] = Remu0 + im * Immu0;
        muN[c] = RemuN + im * ImmuN;
        span[c] = std::abs(muN[c] - mu0[c]);
        if(span[c]!=0.){dir[c] = (muN[c] - mu0[c]) / span[c];}
        if(mu0[c]!=0.){ratio[c] = std::abs(muN[c] / mu0[c]);}
        else{dir[c]=0.;}

    }
    functional->setModelPerturbation(dm);

    CSVio output(name+"_directionalg"+suffix_g, ';', common::OpeningMode::NewFile);
    msg::print << "Compute directional derivatives" << msg::endl;
    msg::indent();
    Interval interval = to_interval(gmshFem);
    unsigned int write_data_fields = 0;
    gmshFem.userDefinedParameter(write_data_fields, "write_data_fields");
    for (unsigned int n = 0; n <= N; n++)
    {
        msg::print << "--- Validation point #" << n << " --- " << msg::endl;
        msg::indent();
        ModelFunction m(parametrization->size());
        std::vector<std::complex<double>> mn(parametrization->size());
        for (unsigned int c = 0; c < parametrization->size(); c++)
        {
            switch (interval)
            {
                case Interval::ReLog:
                {
                    double a = std::pow(ratio[c], ((double)n) / ((double)N) );
                    mn[c] = mu0[c]*a;
                    break;
                }
                case Interval::Log:
                {
                    double step = std::pow(span[c], ((double)n) / ((double)N) ) - 1.;
                    mn[c] = mu0[c] + step*dir[c];
                    break;
                }
                case Interval::Pi2Log1:
                {
                    double step = std::pow(span[c], ((double)n) / ((double)N) ) - 1.;
                    mn[c] = mu0[c] + step*dir[c];
                    if(c==1){mn[c] *= 2. * M_PI;}
                    break;
                }
                case Interval::Linear:
                {
                    double step = ((double)n) / ((double)N) * span[c];
                    mn[c] = mu0[c] + step*dir[c];
                    break;
                }
            }
            msg::print << "mn["+std::to_string(c)+ "] = " << mn[c] << msg::endl;
            ScalarPiecewiseFunction<std::complex<double>> spf_m;
            switch (spatialdistribution)
            {
                case SpatialDistribution::HeavisideY0:
                spf_m.addFunction(
                    mn[c] + (2. * heaviside( y<std::complex<double>>() - yjp[c] ) -1. ) * mjp[c],
                    configuration->model_unknown(Support::BLK) | configuration->model_unknown(Support::BND));
                    break;
                    case SpatialDistribution::Constant:
                    spf_m.addFunction(mn[c],configuration->model_unknown(Support::BLK) | configuration->model_unknown(Support::BND));
                    break;
                }
                m[c] = spf_m;
            }

            double j =  functional->performance(m);
            msg::print << "j: "<< j << msg::endl;
            if(write_data_fields){statefunctional->write_data(Type::FS,name+"_data_field"+std::to_string(n));}

            /*
            double dj_s = 0.;
            double dj_v = 0.;
            double jfd = 0.;
            double d2j_s = 0.;
            double d2j_v = 0.;
            double j2fd = 0.;
            */

            double dj_s = statefunctional->directional(Order::FST, Support::BND, dm);
            double dj_v = statefunctional->directional(Order::FST, Support::BLK, dm);
            double d2j_s = statefunctional->directional(Order::SCD, Support::BND, dm);
            double d2j_v = statefunctional->directional(Order::SCD, Support::BLK, dm);
            /*
            */

            double jp =  functional->performance(m + dm*eps);
            //msg::print << "jp: "<< jp << msg::endl;
            double jm =  functional->performance(m - dm*eps);
            //msg::print << "jm: "<< jm << msg::endl;
            double jfd = (jp - jm) / 2. / eps;
            msg::print << "dj_s: "<< dj_s << msg::endl;
            msg::print << "dj_v: "<< dj_v << msg::endl;
            msg::print << "jd: "<< dj_s+dj_v << msg::endl;
            msg::print << "jFD: "<< jfd << msg::endl;

            double jp1 =  functional->performance(m + dm*eps*2.);
            //msg::print << "jp1: "<< jp1 << msg::endl;
            double jm1 =  functional->performance(m - dm*eps*2.);
            //msg::print << "jm1: "<< jm1 << msg::endl;

            double j2fd = (jm1 -2.*jm +2.*j -2.*jp + jp1) / eps / eps / 2.;
            msg::print << "d2j_s: "<< d2j_s << msg::endl;
            msg::print << "d2j_v: "<< d2j_v << msg::endl;
            msg::print << "d2j: "<< d2j_s+d2j_v << msg::endl;
            msg::print << "j2FD: "<< j2fd << msg::endl;
            /*
            */

            for (unsigned int c = 0; c < parametrization->size(); c++)
            {
                output << mn[c];
            }
            output << j << dj_s << dj_v << dj_s+dj_v << jfd << dj_s+dj_v - jfd  << d2j_s << d2j_v << d2j_s+d2j_v << j2fd  << d2j_s+d2j_v - j2fd << csv::endl;
            msg::print << msg::endl;
            msg::print << msg::endl;
            msg::unindent();
        }
        output.close();

        delete statefunctional;
        for (unsigned int f = 0; f < n_freq; f++)
        {
            delete pequation[f];
        }
        msg::unindent();
    }

    delete parametrization;

    return 0;
}

int main(int argc, char **argv)
{
  GmshFem gmshFem(argc, argv);
  Physic T_Physic = to_physic(gmshFem);
  switch (T_Physic)
  {
    case Physic::acoustic: default:
      return directional<Physic::acoustic>(gmshFem);
    case Physic::electromagnetic:
      return directional<Physic::electromagnetic>(gmshFem);
    case Physic::elastodynamic:
      return directional<Physic::elastodynamic>(gmshFem);
  }
}
