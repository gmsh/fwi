//Standard Library
#include <numeric>

//GmshFem Library
#include "GmshFem.h"
#include "Post.h"
#include "CSVio.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::post;
using namespace gmshfem::function;

//FWI Library
#include "specific/physic.h"
#include "specific/configuration/newConfiguration.h"
#include "specific/model/parametrization/newParametrization.h"
#include "common/wave/equation/equation.h"
#include "specific/data/objective/newObjective.h"
#include "specific/model/regularization/newRegularization.h"
#include "specific/model/sweeper.h"
#include "common/statefunctional.h"

template <Physic T_Physic>
int directional(const GmshFem& gmshFem)
{
    std::string name = "";
    gmshFem.userDefinedParameter(name, "prename");
    std::string suffix = "noname";
    gmshFem.userDefinedParameter(suffix, "name");
    name += suffix;

    ParametrizationInterface* const parametrization = newParametrization<T_Physic>(gmshFem);

    auto configuration = newConfiguration(name, parametrization, gmshFem);

    unsigned int n_freq_tot = 0;
    if(!gmshFem.userDefinedParameter(n_freq_tot, "n_freq"))
    {
        throw Exception("Total frequency number could not be found.");
    }
    std::vector<double> frequency(n_freq_tot);
    std::vector<std::string> filename(n_freq_tot);
    for (unsigned int f = 0; f < n_freq_tot; f++)
    {
        std::string suffix = std::to_string(f);
        if(!gmshFem.userDefinedParameter(frequency[f], "frequency"+suffix))
        {
            throw Exception("Frequency #"+suffix+" could not be found.");
        }
        if(!gmshFem.userDefinedParameter(filename[f], "data"+suffix))
        {
            msg::warning << "Data filename #"+suffix+" could not be found." << msg::endl;
            if(!gmshFem.userDefinedParameter(filename[f], "data"))
            {
                throw Exception("Data filename could not be found.");
            }
            filename[f] += suffix;
            msg::warning << "filename #"+suffix+" = "<< filename[f] << msg::endl;
        }
    }
    const Data<T_Physic> d0(filename,frequency,configuration.get());

    unsigned int n_group = 0;
    if(!gmshFem.userDefinedParameter(n_group, "n_group"))
    {
        throw Exception("Number of frequency groups could not be found.");
    }

    for (unsigned int g = 0; g < n_group; g++)
    {
        std::string suffix_g = std::to_string(g);
        msg::print << "--- Frequency group #" + suffix_g << " --- " << msg::endl;
        msg::indent();

        unsigned int n_freq = 0;
        std::vector<unsigned int> freq_idx;
        if(!gmshFem.userDefinedParameter(n_freq, "n_freq"+suffix_g))
        {
            msg::warning << "Total frequency number for frequency group #" << suffix_g << " could not be found." << msg::endl;
            if(n_group == n_freq_tot)
            {
                n_freq = 1;
                freq_idx.resize(n_freq);
                freq_idx[0] = g;
                msg::warning << "A single frequency is used for frequency group #" << suffix_g << msg::endl;
            }
            else if(n_group == 1)
            {
                n_freq = n_freq_tot;
                freq_idx.resize(n_freq);
                std::iota(freq_idx.begin(), freq_idx.end(), 0);
                msg::warning << "All frequencies are used for frequency group #" << suffix_g << msg::endl;
            }
            else
            {
                throw Exception("Simultaneous inversion of all frequencies requires n_group = 1. Sequential inversion of all frequencies requires n_group = n_freq_tot. Any intermediate case must be specified explicitely");
            }
        }
        else
        {
            freq_idx.resize(n_freq);
            for (unsigned int f = 0; f < n_freq; f++)
            {
                std::string suffix_f = std::to_string(f);
                if(!gmshFem.userDefinedParameter(freq_idx[f], "frequency"+suffix_g+suffix_f))
                {
                    freq_idx[f] = f;
                    msg::warning << "Frequency index #"+suffix_f+" for group #" + suffix_g + " could not be found. Index = " << f << "(default)." << msg::endl;
                }
                msg::print << "frequency #" + suffix_f << " = " << frequency[freq_idx[f]] << msg::endl;
            }
        }
        const Data<T_Physic> d0g(freq_idx,d0);

        std::unique_ptr<ObjectiveInterface<T_Physic>> objective = (d0g.nf()!=0 ? newObjective<T_Physic>(d0g,configuration.get(),gmshFem) : nullptr);

        std::vector<EquationInterface<T_Physic>*> pequation(n_freq);
        std::vector<double> subfrequency(n_freq);
        for (unsigned int f = 0; f < n_freq; f++)
        {
            std::string suffix_f = std::to_string(freq_idx[f]);
            subfrequency[f] = frequency[freq_idx[f]];

            wave::Discretization<T_Physic> w_discret(gmshFem,suffix_f);
            pequation[f] = new ParametrizedEquation<T_Physic>(parametrization,f,2.*M_PI*subfrequency[f],configuration.get(),w_discret,gmshFem,suffix_f);
        }

        model::Discretization m_discret(gmshFem,suffix_g);
        ModelFieldSweeperInterface* sweeper_m = newModelFieldSweeper("m",configuration.get(),m_discret,gmshFem);
        ModelFieldSweeperInterface* sweeper_dm = newModelFieldSweeper("dm",configuration.get(),m_discret,gmshFem);

        RegularizationInterface* const regularization = newRegularization(configuration.get(),(*sweeper_m)(0,0), gmshFem);

        StateFunctional<T_Physic>* const statefunctional = new StateFunctional<T_Physic>(configuration.get(),nullptr,regularization,subfrequency,pequation,objective.get());
        FunctionalInterface* const functional = statefunctional;


        double eps;
        if(!gmshFem.userDefinedParameter(eps, "eps"))
        {
            throw Exception("Finite difference step (eps) could not be found.");
        }
        unsigned int N = sweeper_m->max_idx()[0];

        CSVio output(name+"_directionalg"+suffix_g, ';', common::OpeningMode::NewFile);
        msg::print << "Compute directional derivatives" << msg::endl;
        msg::indent();
        unsigned int write_data_fields = 0;
        gmshFem.userDefinedParameter(write_data_fields, "write_data_fields");
        for (unsigned int n = 0; n <= N; n++)
        {
            msg::print << "--- Validation point #" << n << " --- " << msg::endl;
            msg::indent();

            ModelField m = (*sweeper_m)(n,0);
            ModelField dm = (*sweeper_dm)(n,0);

            functional->setModel(m);
            functional->setModelPerturbation(dm);

            double j =  functional->performance();
            msg::print << "j: "<< j << msg::endl;
            if(write_data_fields){statefunctional->write_data(Type::FS,name+"_data_field"+std::to_string(n));}

            /*
            double dj_s = 0.;
            double dj_v = 0.;
            double jfd = 0.;
            double d2j_s = 0.;
            double d2j_v = 0.;
            double j2fd = 0.;
            */

            double dj_s = statefunctional->directional(Order::FST, Support::BND, dm);
            double dj_v = statefunctional->directional(Order::FST, Support::BLK, dm);
            double d2j_s = statefunctional->directional(Order::SCD, Support::BND, dm);
            double d2j_v = statefunctional->directional(Order::SCD, Support::BLK, dm);
            /*
            */

            double jp =  functional->performance(m + dm*eps);
            //msg::print << "jp: "<< jp << msg::endl;
            double jm =  functional->performance(m - dm*eps);
            //msg::print << "jm: "<< jm << msg::endl;
            double jfd = (jp - jm) / 2. / eps;
            msg::print << "dj_s: "<< dj_s << msg::endl;
            msg::print << "dj_v: "<< dj_v << msg::endl;
            msg::print << "jd: "<< dj_s+dj_v << msg::endl;
            msg::print << "jFD: "<< jfd << msg::endl;

            double jp1 =  functional->performance(m + dm*eps*2.);
            //msg::print << "jp1: "<< jp1 << msg::endl;
            double jm1 =  functional->performance(m - dm*eps*2.);
            //msg::print << "jm1: "<< jm1 << msg::endl;

            double j2fd = (jm1 -2.*jm +2.*j -2.*jp + jp1) / eps / eps / 2.;
            msg::print << "d2j_s: "<< d2j_s << msg::endl;
            msg::print << "d2j_v: "<< d2j_v << msg::endl;
            msg::print << "d2j: "<< d2j_s+d2j_v << msg::endl;
            msg::print << "j2FD: "<< j2fd << msg::endl;
            /*
            */

            output << sweeper_m->value(n,0)[0] << j << dj_s << dj_v << dj_s+dj_v << jfd << dj_s+dj_v - jfd  << d2j_s << d2j_v << d2j_s+d2j_v << j2fd  << d2j_s+d2j_v - j2fd << csv::endl;
            msg::print << msg::endl;
            msg::print << msg::endl;
            msg::unindent();
        }
        output.close();

        delete statefunctional;
        for (unsigned int f = 0; f < n_freq; f++)
        {
            delete pequation[f];
        }
        delete regularization;
        msg::unindent();
    }

    delete parametrization;

    return 0;
}

int main(int argc, char **argv)
{
  GmshFem gmshFem(argc, argv);
  Physic T_Physic = to_physic(gmshFem);
  switch (T_Physic)
  {
    case Physic::acoustic: default:
      return directional<Physic::acoustic>(gmshFem);
    case Physic::electromagnetic:
      return directional<Physic::electromagnetic>(gmshFem);
    case Physic::elastodynamic:
      return directional<Physic::elastodynamic>(gmshFem);
  }
}
