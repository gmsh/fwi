#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 19 14:22:17 2019

@author: xavier
"""
import numpy as np
from enum import Enum
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
#Value for 1 width figure
#H = 4.0;
#L = 5.0;
#Value for 2 width figure
H = 3.0;
L = 3.20;

class Config:
    weakly_inclusion=1
    weakly_background=2
    highly_inclusion=3
    highly_background=4
    dis_weakly_inclusion = 5

config = 5;
path = '../Paper1/Review/data/directional/'
filename = {
    Config.weakly_inclusion: 'weakly_inclusion',
    Config.weakly_background: 'weakly_background',
    Config.highly_inclusion: 'highly_inclusion',
    Config.highly_background: 'highly_background',
    Config.dis_weakly_inclusion: 'dis_weakly_inclusion',
}[config]

xc = {
    Config.weakly_inclusion: 1,
    Config.weakly_background: 0,
    Config.highly_inclusion: 0,
    Config.highly_background: 0,
    Config.dis_weakly_inclusion: 0,
}[config]

ax = {
    Config.weakly_inclusion: -1,
    Config.weakly_background: 1,
    Config.highly_inclusion: 1,
    Config.highly_background: 1,
    Config.dis_weakly_inclusion: 1,
}[config]

var = {
    Config.weakly_inclusion: '\\alpha_c',
    Config.weakly_background: 's^2_0',
    Config.highly_inclusion: 's^2_c',
    Config.highly_background: 's^2_0',
    Config.dis_weakly_inclusion: 's^2_c',
}[config]

x = np.loadtxt(path+filename+'_directional.csv',delimiter=';');


#Cost function
f0 = plt.figure(figsize=(L,H),tight_layout=False);
plt.subplots_adjust(top=0.93,right=0.95,bottom=0.2,left=0.25)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

plt.plot(ax*x[:,xc],x[:,2],color='k',marker='o',markersize=3,linewidth=0)
if config == Config.weakly_inclusion:
    plt.xscale('log')
plt.xlabel(r'$'+var+'$')
plt.ylabel(r'$\mathcal{J}('+var+')$')
plt.grid()
plt.show()

#First derivative
f1 = plt.figure(figsize=(L,H))
plt.subplots_adjust(top=0.93,right=0.95,bottom=0.2,left=0.25)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

#Finite difference
plt.plot(ax*x[:,xc],x[:,6],color='tab:green',marker='x',markersize=5,linewidth=0,label='FD')


#Adjoint state method
if config == Config.highly_inclusion:
    #Volume cylinder
    plt.plot(ax*x[:,xc],x[:,5],color='tab:brown',marker='o',markersize=3,linewidth=0,label='AS')
elif config == Config.weakly_inclusion:
    #Surface cylinder
    plt.plot(ax*x[:,xc],x[:,5],color='tab:brown',marker='o',markersize=3,linewidth=0,label='AS',fillstyle='none')
    plt.xscale('log')
elif config == Config.dis_weakly_inclusion:
    #Surface cylinder
    plt.plot(ax*x[:,xc],x[:,5],color='tab:brown',marker='o',markersize=3,linewidth=0,label='AS',fillstyle='none')
elif config == Config.highly_background or config == Config.weakly_background:
    #Volume background
    plt.plot(ax*x[:,xc],x[:,4],color='tab:blue',marker='o',markersize=3,linewidth=0,label='ASv')
    #Surface background
    plt.plot(ax*x[:,xc],x[:,3],color='tab:blue',marker='o',fillstyle='none',markersize=3,linewidth=0,label='ASs')

plt.grid()
#plt.legend(loc=3,ncol=1);
plt.xlabel(r'$'+var+'$')
plt.ylabel(r'$D_{'+var+'} \mathcal{J}('+var+')$')
plt.show();

#Second derivative
f2 = plt.figure(figsize=(L,H));
plt.subplots_adjust(top=0.93,right=0.95,bottom=0.2,left=0.25)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

#Finite difference
plt.plot(ax*x[:,xc],x[:,11],color='tab:green',marker='x',markersize=5,linewidth=0,label='FD')

#Adjoint state method
if config == Config.highly_inclusion:
    #Volume cylinder
    plt.plot(ax*x[:,xc],x[:,9],color='tab:brown',marker=r'$\bullet$',markersize=3,linewidth=0,label='AS')
elif config == Config.weakly_inclusion:
    #Surface cylinder
    plt.plot(ax*x[:,xc],x[:,8],color='tab:brown',marker=r'$\circ$',markersize=3,linewidth=0,label='AS')
    plt.xscale('log')
elif config == Config.dis_weakly_inclusion:
    #Surface cylinder
    plt.plot(ax*x[:,xc],x[:,8],color='tab:brown',marker=r'$\circ$',markersize=3,linewidth=0,label='AS')
elif config == Config.highly_background or config == Config.weakly_background:
    #Volume background
    plt.plot(ax*x[:,xc],x[:,9],color='tab:blue',marker=r'$\blacksquare$',markersize=3,linewidth=0,label='ASv')
    #Surface background
    plt.plot(ax*x[:,xc],x[:,8],color='tab:blue',marker=r'$\square$',markersize=3,linewidth=0,label='ASs')

plt.grid()
#plt.legend(loc=3,ncol=1);
plt.xlabel(r'$'+var+'$')
plt.ylabel(r'$D^2_{'+var+'} \mathcal{J}('+var+')$')
plt.show();
