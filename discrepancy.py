#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  6 12:42:21 2022

@author: xavier
"""
import numpy as np
import scipy as sp
from scipy import linalg
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Times New Roman'],'size':15})
rc('text', usetex=True)
#Value for 1 width figure
H = 3.;
W = 8.;

#
# Time domain
#

"""
path = "extended_marmousi_data/"
prename = "paper2_extended_synthetics_data"
snr_tag = 'snr2'
Nf = 3
"""

"""
path = "concrete3_data/"
prename = "paper2_concrete3_synthetics_data"
snr_tag = 'snrm3'
Nf=9
"""

path = "cross_data/"
prename = "cross_synthetics_data"
snr_tag = 'snr6'
Nf=7

noise = 0.
for f in range(0,Nf):
    tmp = np.loadtxt(path+prename+str(f)+".csv",delimiter=";");
    tmp_snr = np.loadtxt(path+prename+'_'+snr_tag+'f'+str(f)+".csv",delimiter=";");
    noise += sp.linalg.norm(tmp-tmp_snr)**2