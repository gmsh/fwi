//Gmsh Library
#include "gmsh.h"
//GmshFEM Library
#include "GmshFem.h"
#include "Post.h"
#include "Function.h"
#include "CSVio.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::function;
using namespace gmshfem::function::literals;
using namespace gmshfem::post;

//GmshFWI Library
#include "specific/configuration/newConfiguration.h"
#include "specific/model/parametrization/newParametrization.h"

enum class Error: unsigned int { Abs=0, Sqd=1, SqdInvRoot=2, Rel=3, ExpRel=4 };
Error to_error(const GmshFem& gmshFem)
{
  std::string error_buffer;
  if(gmshFem.userDefinedParameter(error_buffer, "error_type"))
  {
    if(error_buffer=="absolute_mean"){return Error::Abs;}
    else if(error_buffer=="root_mean_squared"){return Error::Sqd;}
    else if(error_buffer=="root_mean_squared_inverse_root"){return Error::SqdInvRoot;}
    else if(error_buffer=="relative_mean"){return Error::Rel;}
    else if(error_buffer=="exp_relative_mean"){return Error::ExpRel;}
  }
  return Error::Sqd;
}

template<Physic T_Physic>
int error(const GmshFem& gmshFem)
{
    std::string name = "";
    gmshFem.userDefinedParameter(name, "prename");
    std::string suffix = "noname";
    gmshFem.userDefinedParameter(suffix, "name");
    name += suffix;

    const ParametrizationInterface* const parametrization = newParametrization<T_Physic>(gmshFem);

    auto configuration = newConfiguration(name, parametrization, gmshFem);

    unsigned int write_error_field = 0;
    gmshFem.userDefinedParameter(write_error_field, "write_error_field");
    std::string inverted_model_path = "";
    gmshFem.userDefinedParameter(inverted_model_path, "inverted_model_path");

    Error error_type = to_error(gmshFem);

    std::string integration_type = "integration_type";
    unsigned int n_it = 0;
    unsigned int integration_degree = 0;
    if(!(
        gmshFem.userDefinedParameter(n_it, "inverted_model_number") &&
        gmshFem.userDefinedParameter(integration_degree, "integration_degree") &&
        gmshFem.userDefinedParameter(integration_type, "integration_type")
    ))
    {
        throw Exception("An error parameter could not be found");
    }

    std::string mask_type = "none";
    if(!gmshFem.userDefinedParameter(mask_type, "error_mask_type"))
    {
        msg::warning << "Error mask type oculd not be found. No mask is used (default)." << msg::endl;
    }
    ScalarFunction<std::complex<double>> mask;
    if(mask_type == "none")
    {
        mask = 1.;
    }
    else if(mask_type == "circle")
    {
        double r0 = 3.;

        mask = heaviside< std::complex<double> >
        (
          r0*r0  - pow(x< std::complex<double> >(),2) - pow(y< std::complex<double> >(),2)
        );
    }
    else if(mask_type == "horizontal")
    {
        double y0 = -2.;

        mask = heaviside< std::complex<double> >
        (
          y< std::complex<double> >()-y0
        );
    }
    else if(mask_type == "vertical-slice")
    {
        double x0 = 0.008;
        double x1 = 0.017;
        mask = heaviside< std::complex<double> >
        (
          (x< std::complex<double> >()-x0)*(x1-x< std::complex<double> >())
        );
    }
    else
    {
        throw Exception("Mask type "+mask_type+" is unknown.");
    }

    double area = std::real(integrate(mask, configuration->model_unknown(Support::BLK),"Gauss0"));
    msg::print << "Model mask unknown area = " << area << msg::endl;



    unsigned int list_buf = 0;
    if(!gmshFem.userDefinedParameter(list_buf, "inverted_model_list"))
    {
        msg::warning << "Error name list is not used (default)" << msg::endl;
    }
    bool list = ((bool)list_buf);

    unsigned int skip = 1;
    if(!gmshFem.userDefinedParameter(skip, "inverted_model_skip"))
    {
        msg::warning << "Skip number could not be found. Defautl value (1) is used." << msg::endl;
    }

    std::vector<std::string> filename(n_it+1);
    if(list)
    {
        for (unsigned int n = 0; n <= n_it; n++)
        {
            if(!gmshFem.userDefinedParameter(filename[n], "inverted_model_name"+std::to_string(n)))
            {
                throw Exception("File name #" + std::to_string(n) + " could not be found");
            }
            filename[n] = inverted_model_path + filename[n];
        }
    }
    else
    {
        std::string inverted_model_name = "noname";
        if(!gmshFem.userDefinedParameter(inverted_model_name, "inverted_model_name"))
        {
            throw Exception("Error model name could not be found");
        }
        std::string inverted_model_prefix = "";
        gmshFem.userDefinedParameter(inverted_model_prefix, "inverted_model_prefix");

        std::string inverted_model_suffix = "";
        if(gmshFem.userDefinedParameter(inverted_model_suffix, "inverted_model_suffix"))
        {
            for (unsigned int n = 0; n <= n_it; n++)
            {
                filename[n] = inverted_model_path+inverted_model_name+inverted_model_prefix+std::to_string(n*skip)+inverted_model_suffix;
            }
        }
        else
        {
            filename[0] = inverted_model_path+inverted_model_name+"_m_initial";
            for (unsigned int n = 1; n <= n_it; n++)
            {
                filename[n] = inverted_model_path+inverted_model_name+inverted_model_prefix+"m"+std::to_string(n*skip);
            }
        }

    }

    CSVio output(inverted_model_path+name, ';', common::OpeningMode::NewFile);
    int view_cnt = 0;
    for (unsigned int n = 0; n < n_it+1; n++)
    {
        for (unsigned int c = 0; c < parametrization->size(); c++)
        {
            std::string suffix = "c" + std::to_string(c);
            gmsh::merge(filename[n]+suffix+".pos");

            ScalarFunction<std::complex<double>> m = probeScalarView<std::complex<double>>(view_cnt);
            view_cnt++;

            gmsh::model::setCurrent(configuration->model_gmodel());

            ScalarFunction<std::complex<double>> error_density;
            switch (error_type)
            {
                case Error::Abs:
                    error_density = abs(configuration->m0()[c]-m);
                    break;
                case Error::Rel:
                    error_density = abs( (configuration->m0()[c]-m) / configuration->m0()[c] );
                    break;
                case Error::ExpRel:
                    error_density = abs( (exp(configuration->m0()[c])-exp(m)) / exp(configuration->m0()[c]) );
                    break;
                case Error::Sqd:
                    error_density = pow(abs(configuration->m0()[c]-m),2);
                    break;
                case Error::SqdInvRoot:
                    error_density = pow(abs(sqrt(1./configuration->m0()[c])-sqrt(1./m)),2);
                    break;
            }

            std::complex<double> error;
            switch (error_type)
            {
                case Error::Abs:
                case Error::Rel:
                case Error::ExpRel:
                    error = integrate(mask * error_density, configuration->model_unknown(Support::BLK),integration_type+std::to_string(integration_degree)) / area;
                    break;
                case Error::Sqd:
                case Error::SqdInvRoot:
                    error = std::sqrt(integrate(mask * error_density, configuration->model_unknown(Support::BLK),integration_type+std::to_string(integration_degree)) / area);
                    break;
            }

            msg::print << "Inverted model #" << n << " error = " << error << msg::endl;
            output << error;

            if(write_error_field)
            {
                save(error_density, configuration->model_unknown(Support::BLK), inverted_model_path+name+"_density"+std::to_string(n)+suffix, "pos", "");
                view_cnt++;
            }

        }
        output << csv::endl;
    }

    return 0;
}

int main(int argc, char **argv)
{
  GmshFem gmshFem(argc, argv);
  Physic T_Physic = to_physic(gmshFem);
  switch (T_Physic)
  {
    case Physic::acoustic: default:
      return error<Physic::acoustic>(gmshFem);
    case Physic::electromagnetic:
      return error<Physic::electromagnetic>(gmshFem);
    case Physic::elastodynamic:
      return error<Physic::elastodynamic>(gmshFem);
  }
}
