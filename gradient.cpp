//Standard Library
#include <numeric>

//GmshFem Library
#include "GmshFem.h"
//#include "Function.h"
//#include "Post.h"

using namespace gmshfem;
using namespace gmshfem::common;
//using namespace gmshfem::post;
//using namespace gmshfem::function;

//FWI Library
#include "specific/physic.h"
#include "specific/configuration/newConfiguration.h"
#include "specific/model/parametrization/newParametrization.h"
#include "common/wave/equation/equation.h"
#include "specific/data/objective/newObjective.h"
#include "specific/model/innerproduct/sobolev.h"
#include "common/statefunctional.h"

static const std::complex< double > im = std::complex< double >(0., 1.);

enum class Interval: unsigned int { Linear=0, Log=1 };
Interval to_interval(const GmshFem& gmshFem)
{
  std::string interval_buffer;
  if(gmshFem.userDefinedParameter(interval_buffer, "interval"))
  {
    if(interval_buffer=="linear"){return Interval::Linear;}
    else if(interval_buffer=="log"){return Interval::Log;}
  }
  return Interval::Linear;
}

template <Physic T_Physic>
int gradient(const GmshFem& gmshFem)
{
    std::string name = "";
    gmshFem.userDefinedParameter(name, "prename");
    std::string suffix = "noname";
    gmshFem.userDefinedParameter(suffix, "name");
    name += suffix;

    const ParametrizationInterface* const parametrization = newParametrization<T_Physic>(gmshFem);

    auto configuration = newConfiguration(name, parametrization, gmshFem);


    unsigned int n_freq_tot = 0;
    if(!gmshFem.userDefinedParameter(n_freq_tot, "n_freq"))
    {
        throw Exception("Total frequency number could not be found.");
    }
    std::vector<double> frequency(n_freq_tot);
    std::vector<std::string> filename(n_freq_tot);
    for (unsigned int f = 0; f < n_freq_tot; f++)
    {
        std::string suffix = std::to_string(f);
        if(!gmshFem.userDefinedParameter(frequency[f], "frequency"+suffix))
        {
            throw Exception("Frequency #"+suffix+" could not be found.");
        }
        if(!gmshFem.userDefinedParameter(filename[f], "data"+suffix))
        {
            msg::warning << "Data filename #"+suffix+" could not be found." << msg::endl;
            if(!gmshFem.userDefinedParameter(filename[f], "data"))
            {
                throw Exception("Data filename could not be found.");
            }
            filename[f] += suffix;
            msg::warning << "filename #"+suffix+" = "<< filename[f] << msg::endl;
        }
    }
    const Data<T_Physic> d0(filename,frequency,configuration.get());

    for (unsigned int c = 0; c < parametrization->size(); c++)
    {
        gmshfem::post::save(configuration->m0()[c], configuration->model_unknown(Support::BLK), name+"_m_blkc"+std::to_string(c)+"f0", "pos", "");
        gmshfem::post::save(configuration->m0()[c], configuration->model_unknown(Support::BND), name+"_m_bndc"+std::to_string(c)+"f0", "pos", "");
    }
    model::Discretization m_discret(gmshFem);
    sobolev_l2::InnerProduct innerproduct(configuration.get(),m_discret,gmshFem);

    unsigned int n_group = 0;
    if(!gmshFem.userDefinedParameter(n_group, "n_group"))
    {
        throw Exception("Number of frequency groups could not be found.");
    }

    for (unsigned int g = 0; g < n_group; g++)
    {
        std::string suffix_g = std::to_string(g);
        msg::print << "--- Frequency group #" + suffix_g << " --- " << msg::endl;
        msg::indent();

        unsigned int n_freq = 0;
        std::vector<unsigned int> freq_idx;
        if(!gmshFem.userDefinedParameter(n_freq, "n_freq"+suffix_g))
        {
            msg::warning << "Total frequency number for frequency group #" << suffix_g << " could not be found." << msg::endl;
            if(n_group == n_freq_tot)
            {
                n_freq = 1;
                freq_idx.resize(n_freq);
                freq_idx[0] = g;
                msg::warning << "A single frequency is used for frequency group #" << suffix_g << msg::endl;
            }
            else if(n_group == 1)
            {
                n_freq = n_freq_tot;
                freq_idx.resize(n_freq);
                std::iota(freq_idx.begin(), freq_idx.end(), 0);
                msg::warning << "All frequencies are used for frequency group #" << suffix_g << msg::endl;
            }
            else
            {
                throw Exception("Simultaneous inversion of all frequencies requires n_group = 1. Sequential inversion of all frequencies requires n_group = n_freq_tot. Any intermediate case must be specified explicitely");
            }
        }
        else
        {
            freq_idx.resize(n_freq);
            for (unsigned int f = 0; f < n_freq; f++)
            {
                std::string suffix_f = std::to_string(f);
                if(!gmshFem.userDefinedParameter(freq_idx[f], "frequency"+suffix_g+suffix_f))
                {
                    throw Exception("Frequency index #"+suffix_f+" for group #" + suffix_f + " could not be found.");
                }
                msg::print << "frequency #" + suffix_f << " = " << frequency[freq_idx[f]] << msg::endl;
            }
        }
        const Data<T_Physic> d0g(freq_idx,d0);

        auto objective = newObjective<T_Physic>(d0g,configuration.get(),gmshFem);

        std::vector<EquationInterface<T_Physic>*> pequation(n_freq);
        std::vector<double> subfrequency(n_freq);
        for (unsigned int f = 0; f < n_freq; f++)
        {
            std::string suffix_f = std::to_string(freq_idx[f]);
            subfrequency[f] = frequency[freq_idx[f]];

            wave::Discretization<T_Physic> w_discret(gmshFem,suffix_f);
            pequation[f] = new ParametrizedEquation<T_Physic>(parametrization,f,2.*M_PI*subfrequency[f],configuration.get(),w_discret,gmshFem,suffix_f);
        }

        StateFunctional<T_Physic>* const statefunctional = new StateFunctional<T_Physic>(configuration.get(),&innerproduct,nullptr,subfrequency,pequation,objective.get());
        FunctionalInterface* const functional = statefunctional;
        functional->setModel(configuration->m0());

        double scale0,scaleN;
        unsigned int N;
        if(!(
            gmshFem.userDefinedParameter(scale0, "scale0") &&
            gmshFem.userDefinedParameter(scaleN, "scaleN") &&
            gmshFem.userDefinedParameter(N, "N")
        ) )
        {
            throw common::Exception("A directional parameter could not be found.");
        }
        Interval interval = to_interval(gmshFem);

        double scale = 0.;

        double base =  scaleN / scale0;
        double span =  scaleN - scale0;
        msg::print << "Compute gradients" << msg::endl;
        msg::indent();
        for (unsigned int n = 0; n <= N; n++)
        {
            if(N==0){scale = scale0;}
            else
            {
                switch (interval)
                {
                    case Interval::Log:
                    scale  = scale0 * std::pow(base, ((double) n) / ((double) N));
                    break;
                    case Interval::Linear:
                    scale = scale0 + ((double)n) / ((double)N) * span;
                    break;
                }
            }
            msg::print << "--- Scale #"<< n << " = "<< scale << " --- " << msg::endl;
            msg::indent();

            innerproduct.scale(scale);
            innerproduct.bulkWeight(1.0);
            innerproduct.boundaryWeight(0.0);
            statefunctional->innerproductIsObsolete();
            functional->gradient().write(name+"_blk_gradientn"+std::to_string(n));

            innerproduct.bulkWeight(0.0);
            innerproduct.boundaryWeight(1.0);
            statefunctional->innerproductIsObsolete();
            functional->gradient().write(name+"_bnd_gradientn"+std::to_string(n));

            msg::unindent();
        }
        msg::unindent();

        delete statefunctional;
        for (unsigned int f = 0; f < n_freq; f++)
        {
            delete pequation[f];
        }
        msg::unindent();
    }
    delete parametrization;

    return 0;
}

int main(int argc, char **argv)
{
  GmshFem gmshFem(argc, argv);
  Physic T_Physic = to_physic(gmshFem);
  switch (T_Physic)
  {
    case Physic::acoustic: default:
      return gradient<Physic::acoustic>(gmshFem);
    case Physic::electromagnetic:
      return gradient<Physic::electromagnetic>(gmshFem);
    case Physic::elastodynamic:
      return gradient<Physic::elastodynamic>(gmshFem);
  }
}
