#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 12:12:50 2021

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Times New Roman'],'size':15})
rc('text', usetex=True)
#Value for 1 width figure
H = 3.;
L = 3.;

data = np.loadtxt('X0_Y250_Z0um.txt')
f1 = plt.figure(1)
plt.plot(data[:,0]*1e6,data[:,1])
f2 = plt.figure(2)
plt.plot(data[:,0]*1e6,data[:,2])
plt.plot(data[:,0],data[:,3])
