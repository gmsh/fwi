#Configuration
inclusion = none
#
unknown = background
#
#Objective
objective = l2distance
#
#Inner product
complex = 0
boundaryWeight = 0.
innerproduct = sobolev
diag_preconditioner = 1.
ref_preconditioner = 0.0002
#
n_group=1
#Data
data0 = EM2007Model1_synthetics_data0
#
#Discretization
h = 0.05
#Model
model_IntegrationType = Gauss
model_FunctionSpaceType = HierarchicalH1
model_FunctionSpaceDegree = 1
#Wave
wave_FunctionSpaceDegree = 1
#
#Global minimum search
globalminimumsearch = sequentialscale
globalminimum_relDecreaseThreshold = 0.0
globalminimum_n_scale=1
scale00 = 0.25
#
#Line search
linesearch = goldstein
rho = 0.5
c1 = 0.3
c2 = 0.7
