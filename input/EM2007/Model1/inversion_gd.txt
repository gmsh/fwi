name = inversion_gd
#
#Equation
gaussNewton=1
#
#Local minimum search
localminimumsearch = classic
localminimum_writeInterval = 30
localminimum_maxIteration = 20
localminimum_meanRelDecreaseMemory = 30
localminimum_meanRelDecreaseThreshold = 0
#
#Descent search
descentsearch = gradientdescent
