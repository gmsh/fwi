#!/bin/bash
#
#SBATCH --job-name=EM2007ModelX_synthetics
#SBATCH --output=EM2007ModelX_synthetics.log

#SBATCH --ntasks=1
#SBATCH --time=60:00
#SBATCH --mem-per-cpu=48000

time ./synthetics ../input/EM2007/ModelX/common.txt ../input/EM2007/ModelX/synthetics.txt -verbose 2 -maxThreads 1
