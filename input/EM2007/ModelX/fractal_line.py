#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 16:11:34 2020

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)

L=12.
ΔH = 4.

nx = 100
Δx = L/(nx-1)

wgn = ΔH * np.random.normal(0, 1, size=nx-1) / np.sqrt(nx)

#SPATIAL DOMAIN
x = np.linspace(Δx/2.,L-Δx/2.,nx-1)

#FREQUENCY DOMAIN
kx = np.fft.fftfreq(nx-1,Δx)

#FILTER
β = 2.1
def ffilter(kx):
    kr = np.abs((L*kx))
    kr = np.where(kr==0., np.nan, kr)
    out = 1./kr**(β/2)
    return np.nan_to_num(out)

fft_wgn = np.fft.fft(wgn)
fft_fgn = fft_wgn * ffilter(kx) 
#INVERSE TRANSFORM
fgn = np.fft.ifft(fft_fgn)

#fig = plt.figure()
y = np.zeros(nx);
x1 = np.linspace(0,L,nx)
for i in range(0,nx-1):
    y[i+1] = y[i] + np.real(fgn[i])
y[nx-1] = 0.
y =  y #np.sqrt(nx/2.) variance de la loi binomiale
plt.xlim(0,L)
plt.plot(x1,y)
plt.show()

np.savetxt('layercoord1.csv', (x1,y), header=str(nx), comments='' )