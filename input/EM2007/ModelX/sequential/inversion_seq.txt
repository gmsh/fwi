#Configuration
m0_scale = 1.0
unknown = all
#
#Objective
objective = l2distance
#Inner product
innerproduct = sobolev
innerproduct_weight_bnd = 0.
innerproduct_diag_preconditioner = 1
innerproduct_preconditioner_ref0 = 1e-3
innerproduct_preconditioner_ref1 = 1e-4
#
#Global minimum search
globalminimumsearch = sequentialscale
globalminimum_n_scale=1
scale0 = 8.0
scale1 = 6.66
scale2 = 5.
scale3 = 4.0
scale4 = 3.33
scale5 = 2.66
scale6 = 2.0
#Local minimum search
localminimum_absGradient2NormThreshold = 1e-5
#
#Discretization
h=0.1
#Model
innerproduct_integration_degree_blk0 = 3
innerproduct_integration_degree_bnd0 = 4
innerproduct_integration_degree_blk1 = 5
innerproduct_integration_degree_bnd1 = 6
innerproduct_integration_degree_blk2 = 5
innerproduct_integration_degree_bnd2 = 6
innerproduct_integration_degree_blk3 = 5
innerproduct_integration_degree_bnd3 = 6
innerproduct_integration_degree_blk4 = 7
innerproduct_integration_degree_bnd4 = 8
innerproduct_integration_degree_blk5 = 9
innerproduct_integration_degree_bnd5 = 10
innerproduct_integration_degree_blk6 = 11
innerproduct_integration_degree_bnd6 = 12
#
model_FunctionSpaceType = HierarchicalH1
model_FunctionSpaceDegree = 1
#
n_group=7
#Data
data0 = EM2007ModelX_synthetics_data0
data1 = EM2007ModelX_synthetics_data1
data2 = EM2007ModelX_synthetics_data2
data3 = EM2007ModelX_synthetics_data3
data4 = EM2007ModelX_synthetics_data4
data5 = EM2007ModelX_synthetics_data5
data6 = EM2007ModelX_synthetics_data6
