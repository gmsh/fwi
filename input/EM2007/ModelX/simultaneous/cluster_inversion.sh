#!/bin/bash
#
#SBATCH --job-name=EM2007ModelX_inversion_sim
#SBATCH --output=EM2007ModelX_inversion_sim.log

#SBATCH --ntasks=1
#SBATCH --time=800:00
#SBATCH --mem-per-cpu=48000

time ./inversion ../input/EM2007/ModelX/common.txt ../input/EM2007/ModelX/simultaneous/inversion.txt ../input/EM2007/ModelX/inversion_nscgB.txt -verbose 2 -maxThreads 1
