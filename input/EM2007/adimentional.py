#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 14:50:10 2020

@author: xavier
"""
import numpy as np


fc = np.array([75,90,120,145,180,230,300]) * 1e6
ωc = 2 * np.pi * fc
ϵ0 = 8.85418782*1e-12
μ0 = 4. * np.pi*1e-7
η0 = np.sqrt(μ0/ϵ0)
scaled_ωc = np.sqrt(ωc**2 * ϵ0 * μ0)
scaled_fc = scaled_ωc/2./np.pi


"""
    Model 4
"""
ϵr0 = 3.4
ϵr1 = 5.2
ϵr2 = 3.7
ϵr3 = 5.0

σ0 = 1.3*1e-3
σr0 = η0 * σ0
σ1 = 2.8*1e-3
σr1 = η0 * σ1
σ2 = 2.0*1e-3
σr2 = η0 * σ2
σ3 = 0.1*1e-3
σr3 = η0 * σ3 
