#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 14:50:10 2020

@author: xavier
"""
import numpy as np


fc = 150 * 1e6
ωc = 2 * np.pi * fc
ϵ0 = 8.85418782*1e-12
μ0 = 4. * np.pi*1e-7
scaled_ωc = np.sqrt(ωc**2 * ϵ0 * μ0)
scaled_fc = scaled_ωc/2./np.pi

"""
    Model 1
"""
ϵria = 5.
ϵr0 = 4.

σ0 = 0.1*1e-3
σr0 = σ0 / ωc / ϵ0 