#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 10:23:03 2021

@author: xavier
"""
import numpy as np

fc = 100 * 1e6
ωc = 2 * np.pi * fc
ϵ0 = 8.85418782*1e-12
μ0 = 4. * np.pi*1e-7
scaled_ωc = np.sqrt(ωc**2 * ϵ0 * μ0)
scaled_fc = scaled_ωc/2./np.pi

"""
    Model 2
"""
ϵr = np.flipud(np.loadtxt('mperm_synth2.txt'))
σ = np.exp(np.flipud(np.loadtxt('mcond_synth2.txt')))*1e-3
σr = σ / ωc / ϵ0
s2 = ϵr - 1j * σr

nx=np.size(s2,1)
ny=np.size(s2,0)
Δx = 0.05
Δy = 0.05
x=np.array([np.arange(0,nx)*Δx])
y=np.array([np.arange(0,ny)*Δy])

with open('../clayeytill.csv', 'w') as f:
  np.savetxt(f, np.array([[nx,ny]]), fmt='%d', delimiter=" ")
  np.savetxt(f, x, fmt='%.5f', delimiter=" ")
  np.savetxt(f, y, fmt='%.5f', delimiter=" ")
  np.savetxt(f, s2, delimiter=" ",fmt=(['%.5f %.5f'] * nx))
