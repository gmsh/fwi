#!/bin/bash
#
#SBATCH --job-name=timewave_synthetics_concrete3
#SBATCH --output=timewave_synthetics_concrete3.log

#SBATCH --ntasks=1
#SBATCH --time=300:00
#SBATCH --mem-per-cpu=16000

time ./time_synthetics_wave ../input/TimeWave/concrete3/common.txt ../input/TimeWave/concrete3/synthetics.txt -maxThreads 1 -verbose 2
