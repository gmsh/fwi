#Configuration
unknown = none
m0_typec0 = file.pos
#
#Wave
wave_FunctionSpaceDegree = 2
#Equation
equation_integration_degree_blk = 6
equation_integration_degree_bnd = 7
newmark_beta = 0.25
newmark_gamma = 0.5
newmark_dt = 8e-3
newmark_T = 6.0
excitation_fm = 6.
excitation_offset = 0.3
#Output
write_model_field = 1
write_wave_field = 1
write_interval = 10
