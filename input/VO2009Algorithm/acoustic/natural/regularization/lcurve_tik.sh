./inversion ../input/VO2009Algorithm/acoustic/natural/common.txt ../input/VO2009Algorithm/acoustic/natural/inversion.txt ../input/VO2009Algorithm/acoustic/natural/inversion_lbfgs.txt ../input/VO2009Algorithm/acoustic/natural/regularization_tik.txt -verbose 2 -maxThreads 1 -name reg_tik0 -regularization_lambda 6.25

./inversion ../input/VO2009Algorithm/acoustic/natural/common.txt ../input/VO2009Algorithm/acoustic/natural/inversion.txt ../input/VO2009Algorithm/acoustic/natural/inversion_lbfgs.txt ../input/VO2009Algorithm/acoustic/natural/regularization_tik.txt -verbose 2 -maxThreads 1 -name reg_tik1 -regularization_lambda 1.25

./inversion ../input/VO2009Algorithm/acoustic/natural/common.txt ../input/VO2009Algorithm/acoustic/natural/inversion.txt ../input/VO2009Algorithm/acoustic/natural/inversion_lbfgs.txt ../input/VO2009Algorithm/acoustic/natural/regularization_tik.txt -verbose 2 -maxThreads 1 -name reg_tik2 -regularization_lambda 0.25

./inversion ../input/VO2009Algorithm/acoustic/natural/common.txt ../input/VO2009Algorithm/acoustic/natural/inversion.txt ../input/VO2009Algorithm/acoustic/natural/inversion_lbfgs.txt ../input/VO2009Algorithm/acoustic/natural/regularization_tik.txt -verbose 2 -maxThreads 1 -name reg_tik3 -regularization_lambda 0.05

./inversion ../input/VO2009Algorithm/acoustic/natural/common.txt ../input/VO2009Algorithm/acoustic/natural/inversion.txt ../input/VO2009Algorithm/acoustic/natural/inversion_lbfgs.txt ../input/VO2009Algorithm/acoustic/natural/regularization_tik.txt -verbose 2 -maxThreads 1 -name reg_tik4 -regularization_lambda 0.01

./inversion ../input/VO2009Algorithm/acoustic/natural/common.txt ../input/VO2009Algorithm/acoustic/natural/inversion.txt ../input/VO2009Algorithm/acoustic/natural/inversion_lbfgs.txt ../input/VO2009Algorithm/acoustic/natural/regularization_tik.txt -verbose 2 -maxThreads 1 -name reg_tik5 -regularization_lambda 0.002

./inversion ../input/VO2009Algorithm/acoustic/natural/common.txt ../input/VO2009Algorithm/acoustic/natural/inversion.txt ../input/VO2009Algorithm/acoustic/natural/inversion_lbfgs.txt ../input/VO2009Algorithm/acoustic/natural/regularization_tik.txt -verbose 2 -maxThreads 1 -name reg_tik6 -regularization_lambda 0.0004

./inversion ../input/VO2009Algorithm/acoustic/natural/common.txt ../input/VO2009Algorithm/acoustic/natural/inversion.txt ../input/VO2009Algorithm/acoustic/natural/inversion_lbfgs.txt ../input/VO2009Algorithm/acoustic/natural/regularization_tik.txt -verbose 2 -maxThreads 1 -name reg_tik7 -regularization_lambda 0.00008
