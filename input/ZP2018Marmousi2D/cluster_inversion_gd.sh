#!/bin/bash
#
#SBATCH --job-name=ZP2018Marmousi2D_inversion_gd
#SBATCH --output=ZP2018Marmousi2D_inversion_gd.log

#SBATCH --ntasks=1
#SBATCH --time=1000:00
#SBATCH --mem-per-cpu=24000

time ./inversion  ../input/ZP2018Marmousi2D/common.txt ../input/ZP2018Marmousi2D/inversion.txt ../input/ZP2018Marmousi2D/inversion_gd.txt -verbose 1 -maxThreads 1
