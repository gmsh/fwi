#!/bin/bash
#
#SBATCH --job-name=ZP2018Marmousi2D_synthetics
#SBATCH --output=ZP2018Marmousi2D_synthetics.log

#SBATCH --ntasks=1
#SBATCH --time=30:00
#SBATCH --mem-per-cpu=48000

time ./synthetics ../input/ZP2018Marmousi2D/common.txt ../input/ZP2018Marmousi2D/synthetics.txt -verbose 1 -maxThreads 1
