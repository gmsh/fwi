#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  6 15:15:24 2021

@author: xavier
"""
import numpy as np
import scipy as sp
import scipy.special
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
#Value for 1 width figure
H = 1.5;
W = 6.5;
save = False;


L = 15.
f = 1.
ω = 2. * np.pi * f
v = 1.
k = ω / v
N = 11
r0 = 15.
x = np.linspace(0, L, N)

def d(xe,xr):
        r = np.sqrt( (xe-xr)**2+r0**2 )
        return np.conj( -1j/4*sp.special.hankel1(0,k*r) )

D = np.zeros([N, 2*N])
for e in range(0,N):
    for r in range(0,N):
        D[e,2*r] = np.real(d(x[e],x[r]))
        D[e,2*r+1] = np.imag(d(x[e],x[r]))

np.savetxt("paper1_reference_highly.csv",D,delimiter=";")
