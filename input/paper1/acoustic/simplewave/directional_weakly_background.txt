name = weakly_background
#Objective
objective = l2distance
#Configuration
unknown = background
Re(mb) = 0.
Im(mb) = 0.
#
xe = 5.0
xr = 5.0
inclusion_ni=1
inclusion_filled = 0
#Inclusion 1
inclusion0 = cylinder
inclusion_x0i0 = 12.5
inclusion_y0i0 = 12.5
inclusion_ri0 = 2.5
Re(mi0) = 1.2
Im(mi0) = -100
#
#Directional
Re(mu) = 0.9
Im(mu) = 0.
Re(muN) = 1.1
Im(muN) = 0.
Re(dm) = 1.
Im(dm) = 0.
N = 20
eps = 1e-5
#
#Data
data0 = empty_inclusion_data0
#
#Discretization
h = 0.25
