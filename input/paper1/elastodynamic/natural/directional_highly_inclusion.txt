name = highly_inclusion
#Objective
objective = l2distance
#Configuration
unknown = inclusion
Re(mbc0) = 1.
Im(mbc0) = 0.
Re(mbc1) = 1.
Im(mbc1) = 0.
Re(mbc2) = 1.
Im(mbc2) = 0.
#
xe = 5.0
xr = 20.0
inclusion_ni=1
inclusion_filled = 1
#Inclusion 1
inclusion0 = cylinder
inclusion_x0i0 = 12.5
inclusion_y0i0 = 12.5
inclusion_ri0 = 2.5
Re(mi0c0) = 0.
Im(mi0c0) = 0.
Re(mi0c1) = 0.
Im(mi0c1) = 0.
Re(mi0c2) = 0.
Im(mi0c2) = 0.
#
#Directional
Re(muc0) = 0.9
Im(muc0) = 0.
Re(muc1) = 2.
Im(muc1) = 0.
Re(muc2) = 2.
Im(muc2) = 0.
Re(muNc0) = 1.1
Im(muNc0) = 0.
Re(muNc1) = 2.
Im(muNc1) = 0.
Re(muNc2) = 2.
Im(muNc2) = 0.
Re(dmc0) = 0.
Im(dmc0) = 0.
Re(dmc1) = 0.
Im(dmc1) = 0.
Re(dmc2) = 1.
Im(dmc2) = 0.
interval = linear
N = 20
eps = 1e-4
#
#Data
data0 = filled_inclusion_data0
#
#Discretization
h = 0.4
