name = highly_background
#Objective
objective = l2distance
#Configuration
unknown = background
Re(mbc0) = 0.
Im(mbc0) = 0.
#
xe = 5.0
xr = 20.0
inclusion_ni=1
inclusion_filled = 1
#Inclusion 1
inclusion0 = cylinder
inclusion_x0i0 = 12.5
inclusion_y0i0 = 12.5
inclusion_ri0 = 2.5
Re(mi0c0) = 1.2
Im(mi0c0) = 0.
#
#Directional
Re(muc0) = 0.9
Im(muc0) = 0.
Re(muNc0) = 1.1
Im(muNc0) = 0.
Re(dmc0) = 1.
Im(dmc0) = 0.
interval = linear
N = 20
eps = 1e-5
#
#Data
data0 = filled_inclusion_data0
#
#Discretization
h = 0.4
