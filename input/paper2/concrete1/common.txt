prename = paper2_extended_robustness_
#Configuration
configuration = rectangular_acquisition
#
L = 0.0156
H = 0.0036
Ler = 0.015
Her = 0.003
hx = 0.00015
hy = 0.00015
em_offset = 0
em_step = 1
re_offset = 0
re_step = 1
n_excluded_points = 16
p_ex0 = 165
p_ex1 = 166
p_ex2 = 167
p_ex3 = 168
p_ex4 = 169
p_ex5 = 170
p_ex6 = 171
p_ex7 = 172
p_ex8 = 185
p_ex9 = 186
p_ex10 = 187
p_ex11 = 188
p_ex12 = 189
p_ex13 = 190
p_ex14 = 191
p_ex15 = 192
ReceiverOnEmitter=1
#
#Equation
physic = acoustic
equation = helmholtz
#
#Discretization
integration_type = Gauss
#Wave
wave_FunctionSpaceType = HierarchicalH1
#
#Frequencies
n_freq = 9
#Frequency0
frequency0 = 100.
#Frequency1
frequency1 = 125.
#Frequency2
frequency2 = 150.
#Frequency3
frequency3= 175.
#Frequency4
frequency4 = 200.
#Frequency5
frequency5 = 225.
#Frequency6
frequency6 = 250.
#Frequency7
frequency7 = 275.
#Frequency8
frequency8 = 300.
