#!/bin/bash
#
#SBATCH --job-name=paper2_extended_concrete2_inversion_sst_lbfgs
#SBATCH --output=paper2_extended_concrete2_inversion_sst_lbfgs.log

#SBATCH --ntasks=1
#SBATCH --time=500:00
#SBATCH --mem-per-cpu=16000

time ./inversion ../input/paper2/concrete2/common.txt ../input/paper2/concrete2/inversion.txt ../input/paper2/concrete2/parametrization_lns2.txt ../input/paper2/concrete2/innerproduct_sst_lns2.txt ../input/paper2/concrete2/inversion_lbfgs.txt -data paper2_extended_concrete2_synthetics_lns2_data -m0_pathc0 ../input/paper2/concrete2/hard_lnslowness2_initial -name inversion_sst_lbfgs -verbose 2 -maxThreads 1
