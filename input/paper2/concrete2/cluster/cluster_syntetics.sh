#!/bin/bash
#
#SBATCH --job-name=paper2_extended_concrete2
#SBATCH --output=paper2_extended_concrete2_synthetics.log

#SBATCH --ntasks=1
#SBATCH --time=10:00
#SBATCH --mem-per-cpu=16000

time ./synthetics ../input/paper2/concrete2/common.txt ../input/paper2/concrete2/synthetics.txt ../input/paper2/concrete2/parametrization_lns2.txt -m0_pathc0 ../input/paper2/concrete2/hard_lnslowness2 -name synthetics_lns2 -verbose 2 -maxThreads 1
