#Configuration
unknown = none
m0_typec0 = file
#
#Discretization
equation_integration_degree_blk0 = 5
equation_integration_degree_bnd0 = 6
equation_integration_degree_blk1 = 5
equation_integration_degree_bnd1 = 6
equation_integration_degree_blk2 = 5
equation_integration_degree_bnd2 = 6
equation_integration_degree_blk3 = 7
equation_integration_degree_bnd3 = 8
equation_integration_degree_blk4 = 7
equation_integration_degree_bnd4 = 8
equation_integration_degree_blk5 = 7
equation_integration_degree_bnd5 = 8
equation_integration_degree_blk6 = 9
equation_integration_degree_bnd6 = 10
equation_integration_degree_blk7 = 9
equation_integration_degree_bnd7 = 10
equation_integration_degree_blk8 = 9
equation_integration_degree_bnd8 = 10
#Wave
wave_FunctionSpaceDegree0 = 2
wave_FunctionSpaceDegree1 = 2
wave_FunctionSpaceDegree2 = 2
wave_FunctionSpaceDegree3 = 3
wave_FunctionSpaceDegree4 = 3
wave_FunctionSpaceDegree5 = 3
wave_FunctionSpaceDegree6 = 4
wave_FunctionSpaceDegree7 = 4
wave_FunctionSpaceDegree8 = 4
#Output
write_data_fields = 0
write_model_field0 = 1
write_wave_fields = 0
write_single_wave_fields = 1
write_single_wave_field0 = 137
