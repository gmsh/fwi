#!/bin/bash
#
#SBATCH --job-name=paper2_concrete3_inversion_s_lbfgs_ls
#SBATCH --output=paper2_concrete3_inversion_s_lbfgs_ls.log

#SBATCH --ntasks=1
#SBATCH --time=120:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/concrete3/common.txt ../input/paper2/concrete3/inversion.txt ../input/paper2/concrete3/innerproduct_s_lns2.txt ../input/paper2/concrete3/localminimum_classic.txt ../input/paper2/concrete3/descentsearch_lbfgs_ls.txt ../input/paper2/concrete3/linesearch_unit.txt -verbose 2 -maxThreads 1 -name s_lbfgs_ls
