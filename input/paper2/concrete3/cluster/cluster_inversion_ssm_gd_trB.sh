#!/bin/bash
#
#SBATCH --job-name=paper2_concrete3_inversion_ssm_gd_trB
#SBATCH --output=paper2_concrete3_inversion_ssm_gd_trB.log

#SBATCH --ntasks=1
#SBATCH --time=1800:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/concrete3/common.txt ../input/paper2/concrete3/inversion.txt ../input/paper2/concrete3/innerproduct_ssm_lns2.txt ../input/paper2/concrete3/localminimum_trB.txt ../input/paper2/concrete3/descentsearch_gd_tr.txt -localminimum_maxMu 4 -localminimum_retrospective 0 -verbose 2 -maxThreads 1 -name ssm_gd_trB
