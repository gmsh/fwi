#!/bin/bash
#
#SBATCH --job-name=paper2_concrete3_inversion_ssm_lbfgs_trBR
#SBATCH --output=paper2_concrete3_inversion_ssm_lbfgs_trBR.log

#SBATCH --ntasks=1
#SBATCH --time=500:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/concrete3/common.txt ../input/paper2/concrete3/inversion.txt ../input/paper2/concrete3/innerproduct_ssm_lns2.txt ../input/paper2/concrete3/localminimum_trB.txt ../input/paper2/concrete3/descentsearch_lbfgs_tr.txt ../input/paper2/concrete3/linesearch_unit.txt -localminimum_retrospective 1 -verbose 2 -maxThreads 1 -name ssm_lbfgs_trBR
