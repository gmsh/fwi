#!/bin/bash
#
#SBATCH --job-name=paper2_concrete3_inversion_ssm_ncg
#SBATCH --output=paper2_concrete3_inversion_ssm_ncg.log

#SBATCH --ntasks=1
#SBATCH --time=2160:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/concrete3/common.txt ../input/paper2/concrete3/inversion.txt ../input/paper2/concrete3/innerproduct_ssm_lns2.txt ../input/paper2/concrete3/localminimum_eisenstat.txt ../input/paper2/concrete3/descentsearch_ncg.txt ../input/paper2/concrete3/linesearch_unit.txt -gaussNewton 0 -verbose 2 -maxThreads 1 -name ssm_ncg
