#!/bin/bash
#
#SBATCH --job-name=paper2_concrete3_inversion_ssm_nscgBRgn
#SBATCH --output=paper2_concrete3_inversion_ssm_nscgBRgn.log

#SBATCH --ntasks=1
#SBATCH --time=2800:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/concrete3/common.txt ../input/paper2/concrete3/inversion.txt ../input/paper2/concrete3/innerproduct_ssm_lns2.txt ../input/paper2/concrete3/localminimum_trB.txt ../input/paper2/concrete3/descentsearch_nscg.txt -localminimum_retrospective 1 -gaussNewton 1 -verbose 2 -maxThreads 1 -name ssm_nscgBRgn
