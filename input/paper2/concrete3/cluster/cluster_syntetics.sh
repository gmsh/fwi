#!/bin/bash
#
#SBATCH --job-name=paper2_concrete3_synthetics
#SBATCH --output=paper2_concrete3_synthetics.log

#SBATCH --ntasks=1
#SBATCH --time=10:00
#SBATCH --mem-per-cpu=16000

time ./synthetics ../input/paper2/concrete3/common.txt ../input/paper2/concrete3/synthetics.txt -verbose 2 -maxThreads 1
