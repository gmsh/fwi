#!/bin/bash
#
#SBATCH --job-name=paper2_concrete3_inversion_us_lbfgs_ls_tik0
#SBATCH --output=paper2_concrete3_inversion_us_lbfgs_ls_tik0.log

#SBATCH --ntasks=1
#SBATCH --time=1000:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/concrete3/common.txt ../input/paper2/concrete3/inversion.txt ../input/paper2/concrete3/innerproduct_us_lns2.txt ../input/paper2/concrete3/localminimum_classic.txt ../input/paper2/concrete3/descentsearch_lbfgs_ls.txt ../input/paper2/concrete3/linesearch_unit.txt ../input/paper2/concrete3/regularization_tikhonov.txt -regularization_lambda 1e-4 -verbose 2 -maxThreads 1 -name us_lbfgs_ls_tik0
