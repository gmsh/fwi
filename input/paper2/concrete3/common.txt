prename = paper2_concrete3_
#Configuration
configuration = rectangular_acquisition
#
#Parametrization
parametrization = ln_slowness2_real
model_reference_frequency = 1.
#
Re(mbc0) = 2.4079
Im(mbc0) = 0.
#
L = 0.0312
H = 0.0042
Ler = 0.03
Her = 0.003
hx = 0.00015
hy = 0.00015
em_offset = 0
em_step = 1
re_offset = 0
re_step = 1
exclude_bottom_points = 1
n_excluded_points = 12
p_ex0 = 329
p_ex1 = 330
p_ex2 = 331
p_ex3 = 332
p_ex4 = 333
p_ex5 = 334
p_ex6 = 349
p_ex7 = 350
p_ex8 = 351
p_ex9 = 352
p_ex10 = 353
p_ex11 = 354
ReceiverOnEmitter=0
#
#Equation
physic = acoustic
equation = simplewave
#
#Discretization
integration_type = Gauss
#Wave
wave_FunctionSpaceType = HierarchicalH1
#
#Frequencies
n_freq = 9
#Frequency0
frequency0 = 100.
#Frequency1
frequency1 = 125.
#Frequency2
frequency2 = 150.
#Frequency3
frequency3= 175.
#Frequency4
frequency4 = 200.
#Frequency5
frequency5 = 225.
#Frequency6
frequency6 = 250.
#Frequency7
frequency7 = 275.
#Frequency8
frequency8 = 300.
