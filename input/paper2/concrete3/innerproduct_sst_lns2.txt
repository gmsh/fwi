#Local minimum search
#localminimum_absDecreaseThreshold = 30.
#Equation
equation_greenPreconditioner = 0
equation_const_preconditioner = 1.08e6
#Inner product
n_scale0 = 1
scale00 = 0.0
innerproduct = sobolev_l2
innerproduct_weight_bnd = 0.
innerproduct_preconditioner_ref = 0.
innerproduct_diag_preconditioner_scale = 1
innerproduct_stabilization = 38000
#stabilization = precond in concrete
