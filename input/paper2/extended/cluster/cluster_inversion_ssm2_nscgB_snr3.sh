#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_ssm2_nscgB_snr3
#SBATCH --output=paper2_extended_inversion_ssm2_nscgB_snr3.log

#SBATCH --ntasks=1
#SBATCH --time=1000:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/extended/common.txt ../input/paper2/extended/inversion.txt ../input/paper2/extended/innerproduct_ssm2.txt ../input/paper2/extended/localminimum_nscgB.txt ../input/paper2/extended/data_snr3.txt -verbose 2 -maxThreads 1 -gaussNewton 0 -localminimum_retrospective 0 -name ssm2_nscgB_snr3
