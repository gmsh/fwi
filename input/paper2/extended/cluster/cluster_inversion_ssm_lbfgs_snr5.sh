#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_ssm_lbfgs_snr5
#SBATCH --output=paper2_extended_inversion_ssm_lbfgs_snr5.log

#SBATCH --ntasks=1
#SBATCH --time=300:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/extended/common.txt ../input/paper2/extended/inversion.txt ../input/paper2/extended/innerproduct_ssm.txt ../input/paper2/extended/localminimum_lbfgs.txt ../input/paper2/extended/data_snr5.txt -verbose 2 -maxThreads 1 -name ssm_lbfgs_snr5
