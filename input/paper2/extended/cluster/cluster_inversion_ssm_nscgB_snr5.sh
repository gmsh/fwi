#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_ssm_nscgB_snr5
#SBATCH --output=paper2_extended_inversion_ssm_nscgB_snr5.log

#SBATCH --ntasks=1
#SBATCH --time=500:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/extended/common.txt ../input/paper2/extended/inversion.txt ../input/paper2/extended/innerproduct_ssm.txt ../input/paper2/extended/localminimum_nscgB.txt ../input/paper2/extended/data_snr5.txt -verbose 2 -maxThreads 1 -gaussNewton 0 -localminimum_retrospective 0 -name ssm_nscgB_snr5
