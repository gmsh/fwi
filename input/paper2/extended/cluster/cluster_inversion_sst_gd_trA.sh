#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_sst_gd_trA
#SBATCH --output=paper2_extended_inversion_sst_gd_trA.log

#SBATCH --ntasks=1
#SBATCH --time=1500:00
#SBATCH --mem-per-cpu=16000

time ./inversion ../input/paper2/extended/common.txt ../input/paper2/extended/inversion.txt ../input/paper2/extended/innerproduct_sst.txt ../input/paper2/extended/localminimum_trA.txt ../input/paper2/extended/descentsearch_gd_tr.txt ../input/paper2/extended/data.txt -localminimum_maxMu 5 -localminimum_retrospective 0 -verbose 2 -maxThreads 1 -name sst_gd_trA
