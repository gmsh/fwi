#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_sst_gd_trBR
#SBATCH --output=paper2_extended_inversion_sst_gd_trBR.log

#SBATCH --ntasks=1
#SBATCH --time=1200:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/extended/common.txt ../input/paper2/extended/inversion.txt ../input/paper2/extended/innerproduct_sst.txt ../input/paper2/extended/localminimum_trB.txt ../input/paper2/extended/descentsearch_gd_tr.txt ../input/paper2/extended/data.txt -localminimum_maxMu 4 -localminimum_retrospective 1 -verbose 2 -maxThreads 1 -name sst_gd_trBR
