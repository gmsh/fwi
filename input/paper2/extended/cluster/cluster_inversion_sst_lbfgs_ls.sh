#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_sst_lbfgs_ls
#SBATCH --output=paper2_extended_inversion_sst_lbfgs_ls.log

#SBATCH --ntasks=1
#SBATCH --time=300:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/extended/common.txt ../input/paper2/extended/inversion.txt ../input/paper2/extended/innerproduct_sst.txt ../input/paper2/extended/localminimum_classic.txt ../input/paper2/extended/descentsearch_lbfgs_ls.txt ../input/paper2/extended/linesearch_unit.txt ../input/paper2/extended/data.txt -verbose 2 -maxThreads 1 -name sst_lbfgs_ls
