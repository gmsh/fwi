#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_sst_lbfgs_trA
#SBATCH --output=paper2_extended_inversion_sst_lbfgs_trA.log

#SBATCH --ntasks=1
#SBATCH --time=300:00
#SBATCH --mem-per-cpu=16000

time ./inversion ../input/paper2/extended/common.txt ../input/paper2/extended/inversion.txt ../input/paper2/extended/innerproduct_sst.txt ../input/paper2/extended/localminimum_trA.txt ../input/paper2/extended/descentsearch_lbfgs_tr.txt ../input/paper2/extended/data.txt -localminimum_retrospective 0 -verbose 2 -maxThreads 1 -name sst_lbfgs_trA
