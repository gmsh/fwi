#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_sst_nscgAr
#SBATCH --output=paper2_extended_inversion_sst_nscgAr.log

#SBATCH --ntasks=1
#SBATCH --time=600:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/extended/common.txt ../input/paper2/extended/inversion.txt ../input/paper2/extended/innerproduct_sst.txt ../input/paper2/extended/localminimum_nscgA.txt ../input/paper2/extended/data.txt -verbose 2 -maxThreads 1 -gaussNewton 0 -localminimum_retrospective 1 -name sst_nscgAr
