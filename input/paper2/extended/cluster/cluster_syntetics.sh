#!/bin/bash
#
#SBATCH --job-name=paper2_extended_synthetics
#SBATCH --output=paper2_extended_synthetics.log

#SBATCH --ntasks=1
#SBATCH --time=10:00
#SBATCH --mem-per-cpu=16000

time ./synthetics ../input/paper2/extended/common.txt ../input/paper2/extended/synthetics.txt -verbose 2 -maxThreads 1
