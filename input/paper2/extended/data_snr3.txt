#Data
data0 = paper2_extended_synthetics_data0_snr3
data1 = paper2_extended_synthetics_data1_snr3
data2 = paper2_extended_synthetics_data2_snr3
#
#Local minimum search
localminimum_absDecreaseThreshold = 52.5
localminimum_meanRelDecreaseThreshold = 4e-5
localminimum_meanRelDecreaseMemory = 1
