name = inversion_lbfgs_w_error
#Configuration
unknown = subsurface
m0_typec0 = file
m0_pathc0 = ../input/paper2/marmousi_slowness2
#Error
error_type = root_mean_squared
integration_degree = 8
#Input
inverted_model_path = ../../Paper2/extended/S_lBFGS/
inverted_model_name = paper2_extended_inversion_lbfgs_w
inverted_model_prefix = g0s0
inverted_model_number = 28
#Output
write_error_field = 0
