#!/bin/bash
#
#SBATCH --job-name=paper2_inversion_ncg
#SBATCH --output=paper2_inversion_ncg.log

#SBATCH --ntasks=1
#SBATCH --time=2400:00
#SBATCH --mem-per-cpu=48000

time ./inversion  ../input/paper2/common.txt ../input/paper2/inversion.txt ../input/paper2/inversion_ncg.txt -verbose 1 -maxThreads 1
