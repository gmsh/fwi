#!/bin/bash
#
#SBATCH --job-name=paper2_inversion_nscgB
#SBATCH --output=paper2_inversion_nscgB.log

#SBATCH --ntasks=1
#SBATCH --time=1200:00
#SBATCH --mem-per-cpu=48000

time ./inversion  ../input/paper2/common.txt ../input/paper2/inversion.txt ../input/paper2/inversion_nscgB.txt -verbose 1 -maxThreads 1
