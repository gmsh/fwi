#!/bin/bash
#
#SBATCH --job-name=paper2_synthetics
#SBATCH --output=paper2_synthetics.log

#SBATCH --ntasks=1
#SBATCH --time=30:00
#SBATCH --mem-per-cpu=48000

time ./synthetics ../input/paper2/common.txt ../input/paper2/synthetics.txt -verbose 1 -maxThreads 1
