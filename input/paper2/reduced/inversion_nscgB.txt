name = inversion_nscgB
#
#Local minimum search
localminimumsearch = yuanfan
localminimum_retrospective = 0
localminimum_initMu = 1
localminimum_c0 = 0.0001
localminimum_c1 = 0.75
localminimum_c2 = 0.75
localminimum_c3 = 0.25
localminimum_c4 = 2.
localminimum_writeInterval = 5
localminimum_maxIteration = 50
#
#Descent search
descentsearch = newton_steihaug_conjugategradient
descent_maxIteration = 50
descent_eta0 = 0.4
