#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_sst_lbfgs_trBR_snr2
#SBATCH --output=paper2_extended_inversion_sst_lbfgs_trBR_snr2.log

#SBATCH --ntasks=1
#SBATCH --time=400:00
#SBATCH --mem-per-cpu=16000

time ./inversion ../input/paper2/extended/common.txt ../input/paper2/extended/inversion.txt ../input/paper2/extended/innerproduct_sst.txt ../input/paper2/extended/localminimum_trB.txt ../input/paper2/extended/descentsearch_lbfgs_tr.txt ../input/paper2/review/data_snr2.txt ../input/paper2/review/regularization_tikhonov.txt -localminimum_retrospective 1 -verbose 2 -maxThreads 1 -name sst_lbfgs_trBR_snr2
