#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_sst_ncg_snr2
#SBATCH --output=paper2_extended_inversion_sst_ncg_snr2.log

#SBATCH --ntasks=1
#SBATCH --time=800:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper2/extended/common.txt ../input/paper2/extended/inversion.txt ../input/paper2/extended/innerproduct_sst.txt ../input/paper2/extended/localminimum_ncg.txt ../input/paper2/review/data_snr2.txt ../input/paper2/review/regularization_tikhonov.txt -verbose 2 -maxThreads 1 -gaussNewton 0 -name sst_ncg_snr2
