#!/bin/bash
#
#SBATCH --job-name=paper2b_inversion_ncg
#SBATCH --output=paper2b_inversion_ncg.log

#SBATCH --ntasks=1
#SBATCH --time=1200:00
#SBATCH --mem-per-cpu=24000

time ./inversion ../input/paper2b/permcond_bireal/common.txt ../input/paper2b/permcond_bireal/inversion.txt ../input/paper2b/permcond_bireal/innerproduct_weight0.txt ../input/paper2b/permcond_bireal/inversion_ncg.txt -gaussNewton 0 -name inversion_ncg0 -verbose 2 -maxThreads 1
