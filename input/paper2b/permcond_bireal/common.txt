prename = paper2b_
#Equation
physic = acoustic
equation = helmholtz
equation_boundary = 0
#
#Parametrization
parametrization = permcond_bireal
model_reference_frequency = 1.
#
#Configuration
configuration = volume_acquisition
#
xe = 0.5
xr = 11.5
ye = 0.5
yr = 11.5
He = 10.0
Hr = 10.0
Le = 10.0
Lr = 10.0
#
nxe = 21
nxr = 21
nye = 21
nyr = 21
L = 12.
H = 12.
#
Re(mbc0) = 4.
Im(mbc0) = 0.
Re(mbc1) = 1.13
Im(mbc1) = 0.
#
n_freq = 7
#Frequency
frequency0 = 0.25
frequency1 = 0.3
frequency2 = 0.4
frequency3 = 0.5
frequency4 = 0.6
frequency5 = 0.75
frequency6 = 1.
#
#Discretization
integration_type = Gauss
wave_FunctionSpaceType = HierarchicalH1
#
wave_FunctionSpaceDegree0 = 1
integration_degree_blk0 = 3
integration_degree_bnd0 = 4
wave_FunctionSpaceDegree1 = 1
integration_degree_blk1 = 3
integration_degree_bnd1 = 4
wave_FunctionSpaceDegree2 = 2
integration_degree_blk2 = 5
integration_degree_bnd2 = 6
wave_FunctionSpaceDegree3 = 2
integration_degree_blk3 = 5
integration_degree_bnd3 = 6
wave_FunctionSpaceDegree4 = 2
integration_degree_blk4 = 5
integration_degree_bnd4 = 6
wave_FunctionSpaceDegree5 = 3
integration_degree_blk5 = 7
integration_degree_bnd5 = 8
wave_FunctionSpaceDegree6 = 3
integration_degree_blk6 = 7
integration_degree_bnd6 = 8
