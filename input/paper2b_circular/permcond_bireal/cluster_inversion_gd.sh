#!/bin/bash
#
#SBATCH --job-name=paper2b_inversion_gd
#SBATCH --output=paper2b_inversion_gd.log

#SBATCH --ntasks=1
#SBATCH --time=2400:00
#SBATCH --mem-per-cpu=24000

time ./inversion ../input/paper2b_circular/permcond_bireal/common.txt ../input/paper2b_circular/permcond_bireal/inversion.txt ../input/paper2b_circular/permcond_bireal/innerproduct_weight2.txt ../input/paper2b_circular/permcond_bireal/inversion_gd.txt -name inversion_gd -verbose 2 -maxThreads 1
