#!/bin/bash
#
#SBATCH --job-name=paper2b_inversion_gncg2
#SBATCH --output=paper2b_inversion_gncg2.log

#SBATCH --ntasks=1
#SBATCH --time=1800:00
#SBATCH --mem-per-cpu=24000

time ./inversion ../input/paper2b_circular/permcond_bireal/common.txt ../input/paper2b_circular/permcond_bireal/inversion.txt ../input/paper2b_circular/permcond_bireal/innerproduct_weight2.txt ../input/paper2b_circular/permcond_bireal/inversion_ncg.txt -gaussNewton 1 -name inversion_gncg2 -verbose 2 -maxThreads 1
