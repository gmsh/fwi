#!/bin/bash
#
#SBATCH --job-name=paper2b_synthetics
#SBATCH --output=paper2b_synthetics.log

#SBATCH --ntasks=1
#SBATCH --time=30:00
#SBATCH --mem-per-cpu=24000

time ./synthetics ../input/paper2b_circular/permcond_bireal/common.txt ../input/paper2b_circular/permcond_bireal/synthetics.txt -verbose 2 -maxThreads 1
