#!/bin/bash
#
#SBATCH --job-name=paper2_concrete3_inversion_ssm3_lbfgs_ls_snrm3
#SBATCH --output=paper2_concrete3_inversion_ssm3_lbfgs_ls_snrm3.log

#SBATCH --ntasks=1
#SBATCH --time=1000:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper3/concrete3/common.txt ../input/paper3/concrete3/inversion.txt ../input/paper3/concrete3/innerproduct_ssm.txt ../input/paper3/concrete3/localminimum_classic.txt ../input/paper3/concrete3/descentsearch_lbfgs_ls.txt ../input/paper3/concrete3/linesearch_unit.txt ../input/paper3/concrete3/data_snrm3.txt -scale0 0.007 -verbose 2 -maxThreads 1 -name ssm3_lbfgs_ls_snrm3
