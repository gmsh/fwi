#!/bin/bash
#
#SBATCH --job-name=paper2_concrete3_inversion_sst_lbfgs_ls_snrm3_tik
#SBATCH --output=paper2_concrete3_inversion_sst_lbfgs_ls_snrm3_tik.log

#SBATCH --ntasks=1
#SBATCH --time=1000:00
#SBATCH --mem-per-cpu=16000
#
#SBATCH --array=0-7

ARGS_R=( 1e-4 1e-3 0.005 0.025 0.05 0.1 0.25 0.5 )

time ./inversion  ../input/paper3/concrete3/common.txt ../input/paper3/concrete3/inversion.txt ../input/paper3/concrete3/innerproduct_sst.txt ../input/paper3/concrete3/localminimum_classic.txt ../input/paper3/concrete3/descentsearch_lbfgs_ls.txt ../input/paper3/concrete3/linesearch_unit.txt ../input/paper3/concrete3/data_snrm3.txt ../input/paper3/concrete3/regularization_tikhonov.txt -verbose 2 -maxThreads 1 -regularization_lambda ${ARGS_R[$SLURM_ARRAY_TASK_ID]} -name sst_lbfgs_ls_snrm3_tik$SLURM_ARRAY_TASK_ID
