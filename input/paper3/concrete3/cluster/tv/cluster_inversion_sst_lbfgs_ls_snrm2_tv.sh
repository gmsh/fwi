#!/bin/bash
#
#SBATCH --job-name=paper2_concrete3_inversion_sst_lbfgs_ls_snrm3_tv
#SBATCH --output=paper2_concrete3_inversion_sst_lbfgs_ls_snrm3_tv.log

#SBATCH --ntasks=1
#SBATCH --time=1000:00
#SBATCH --mem-per-cpu=13000
#
#SBATCH --array=6-7

ARGS_R=( 1e2 1.5e2 2.5e2 3.75e2 5e2 1e3 1e4 10.)

time ./inversion  ../input/paper3/concrete3/common.txt ../input/paper3/concrete3/inversion.txt ../input/paper3/concrete3/innerproduct_sst.txt ../input/paper3/concrete3/localminimum_classic.txt ../input/paper3/concrete3/descentsearch_lbfgs_ls.txt ../input/paper3/concrete3/linesearch_unit.txt ../input/paper3/concrete3/data_snrm3.txt ../input/paper3/concrete3/regularization_totalvariation.txt -verbose 2 -maxThreads 1 -regularization_lambda ${ARGS_R[$SLURM_ARRAY_TASK_ID]} -name sst_lbfgs_ls_snrm3_tv$SLURM_ARRAY_TASK_ID
