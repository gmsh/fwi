name = ssm2_lbfgs_ls_snrm3_error
#Configuration
unknown = all
m0_typec0 = file
m0_pathc0 = ../input/paper2/concrete3/large_concrete_lns2
#Error
error_type = exp_relative_mean
integration_degree = 8
#Input
inverted_model_path = ../../Manuscript/Part3bis/TC2/SNRM3/SSM_LBFGS_LS/
inverted_model_name = paper2_concrete3_ssm2_lbfgs_ls_snrm3
inverted_model_prefix = g0s0
inverted_model_number = 250
#Output
write_error_field = 0
