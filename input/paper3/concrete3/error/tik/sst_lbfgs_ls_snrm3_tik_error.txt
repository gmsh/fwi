name = sst_lbfgs_ls_snrm3_tik_error
#Configuration
unknown = all
m0_typec0 = file
m0_pathc0 = ../input/paper2/concrete3/large_concrete_lns2
#Error
error_type = exp_relative_mean
integration_degree = 8
#Input
inverted_model_path = ../../Manuscript/Part3bis/TC2/SNRM3/SST_LBFGS_LS_TIK/
inverted_model_number = 7
inverted_model_list = 1
inverted_model_name0 = paper2_concrete3_sst_lbfgs_ls_snrm3_tik0_mg0
inverted_model_name1 = paper2_concrete3_sst_lbfgs_ls_snrm3_tik1_mg0
inverted_model_name2 = paper2_concrete3_sst_lbfgs_ls_snrm3_tik2_mg0
inverted_model_name3 = paper2_concrete3_sst_lbfgs_ls_snrm3_tik3_mg0
inverted_model_name4 = paper2_concrete3_sst_lbfgs_ls_snrm3_tik4_mg0
inverted_model_name5 = paper2_concrete3_sst_lbfgs_ls_snrm3_tik5_mg0
inverted_model_name6 = paper2_concrete3_sst_lbfgs_ls_snrm3_tik6_mg0
inverted_model_name7 = paper2_concrete3_sst_lbfgs_ls_snrm3_tik7_mg0
#Output
write_error_field = 0
