name = synthetics
#Configuration
unknown = none
m0_typec0 = file
m0_pathc0 = ../input/paper2/concrete3/large_concrete_lns2
#
#Wave
wave_FunctionSpaceDegree = 2
#Equation
equation_integration_degree_blk = 6
equation_integration_degree_bnd = 7
newmark_beta = 0.25
newmark_gamma = 0.5
newmark_dt = 1e-4
newmark_T = 0.2
excitation_fm = 150.
excitation_offset = 0.0075
#Output
write_model_field = 1
