#!/bin/bash
#
#SBATCH --job-name=timewave_cross_synthetics
#SBATCH --output=timewave_cross_synthetics.log

#SBATCH --ntasks=1
#SBATCH --time=600:00
#SBATCH --mem-per-cpu=16000

time ./time_synthetics_wave ../input/paper3/cross/common.txt ../input/paper3/cross/synthetics.txt -maxThreads 1 -verbose 2
