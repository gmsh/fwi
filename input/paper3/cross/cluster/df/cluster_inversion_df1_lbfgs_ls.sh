#!/bin/bash
#
#SBATCH --job-name=cross_inversion_df1_lbfgs_ls_snr6
#SBATCH --output=cross_inversion_df1_lbfgs_ls_snr6.log

#SBATCH --ntasks=1
#SBATCH --time=700:00
#SBATCH --mem-per-cpu=4000

time ./inversion  ../input/paper3/cross/common.txt ../input/paper3/cross/inversion.txt ../input/paper3/cross/innerproduct_df.txt ../input/paper3/cross/localminimum_classic.txt ../input/paper3/cross/descentsearch_lbfgs_ls.txt ../input/paper3/cross/linesearch_unit.txt ../input/paper3/cross/data_snr6.txt -verbose 2 -maxThreads 1 -scale0 2.25 -name df1_lbfgs_ls_snr6
