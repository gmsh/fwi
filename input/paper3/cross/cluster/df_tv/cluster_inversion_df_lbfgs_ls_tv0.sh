#!/bin/bash
#
#SBATCH --job-name=cross_inversion_df_lbfgs_ls_snr7_tv0
#SBATCH --output=cross_inversion_df_lbfgs_ls_snr7_tv0.log

#SBATCH --ntasks=1
#SBATCH --time=400:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper3/cross/common.txt ../input/paper3/cross/inversion.txt ../input/paper3/cross/innerproduct_df.txt ../input/paper3/cross/localminimum_classic.txt ../input/paper3/cross/descentsearch_lbfgs_ls.txt ../input/paper3/cross/linesearch_unit.txt ../input/paper3/cross/data_snr7.txt ../input/paper3/cross/regularization_totalvariation.txt -regularization_lambdac0 5e-5 -regularization_lambdac1 5e-5 -verbose 2 -maxThreads 1 -name df_lbfgs_ls_snr7_tv0
