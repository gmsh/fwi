#!/bin/bash
#
#SBATCH --job-name=cross_inversion_ssm3_lbfgs_ls_snr7
#SBATCH --output=cross_inversion_ssm3_lbfgs_ls_snr7.log

#SBATCH --ntasks=1
#SBATCH --time=400:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper3/cross/common.txt ../input/paper3/cross/inversion.txt ../input/paper3/cross/innerproduct_ssm.txt ../input/paper3/cross/localminimum_classic.txt ../input/paper3/cross/descentsearch_lbfgs_ls.txt ../input/paper3/cross/linesearch_unit.txt ../input/paper3/cross/data_snr7.txt -verbose 2 -maxThreads 1 -scale0 5.25 -name ssm3_lbfgs_ls_snr7
