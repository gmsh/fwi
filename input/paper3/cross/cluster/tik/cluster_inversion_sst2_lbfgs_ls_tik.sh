#!/bin/bash
#
#SBATCH --job-name=cross_inversion_sst2_lbfgs_ls_snr6_tik
#SBATCH --output=cross_inversion_sst2_lbfgs_ls_snr6_tik.log

#SBATCH --ntasks=1
#SBATCH --time=1000:00
#SBATCH --mem-per-cpu=4000

#SBATCH --array=0-48

ARGS_R0=(1.75e-5 3e-5 5.25e-5 1e-4 1.75e-4 3e-4 5.25e-4)
ARGS_R1=(3e-6 1e-6 3e-6 1e-5 3e-5 1e-4 3e-4)

time ./inversion  ../input/paper3/cross/common.txt ../input/paper3/cross/inversion.txt ../input/paper3/cross/innerproduct_sst2.txt ../input/paper3/cross/localminimum_classic.txt ../input/paper3/cross/descentsearch_lbfgs_ls.txt ../input/paper3/cross/linesearch_unit.txt ../input/paper3/cross/data_snr6.txt ../input/paper3/cross/regularization_tikhonov.txt -regularization_lambdac0 ${ARGS_R0[$SLURM_ARRAY_TASK_ID % 7]} -regularization_lambdac1 ${ARGS_R1[$SLURM_ARRAY_TASK_ID / 7]} -verbose 2 -maxThreads 1 -name sst2_lbfgs_ls_snr6_tik$SLURM_ARRAY_TASK_ID
