name = timewave_initial
#Configuration
unknown = none
#
#Wave
wave_FunctionSpaceDegree = 3
#Equation
equation_integration_degree_blk = 8
equation_integration_degree_bnd = 9
newmark_beta = 0.25
newmark_gamma = 0.5
newmark_dt = 1e-2
newmark_T = 100
excitation_fm = 0.33
excitation_offset = 3.75
#Output
write_model_field = 1
