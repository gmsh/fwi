name = inversion_error_tv
#Configuration
unknown = all
inclusion_ni=1
#Inclusion 0
inclusion_position0 = 1.5
inclusion_depth0 = 1.0
Re(mi0c0) = 1.2
Im(mi0c0) = 0.
#Error
error_type = root_mean_squared
integration_degree = 8
#Input
inverted_model_path =
inverted_model_name = paper3_monoslab_inversion_iptv
inverted_model_prefix = g0s0
inverted_model_number = 26
#Output
write_error_field = 0
