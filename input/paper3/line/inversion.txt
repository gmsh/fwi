#Configuration
unknown = all
#
#Equation
gaussNewton=0
equation_boundary=1
#
n_group=1
#
#Discretization
#Model
model_FunctionSpaceType = HierarchicalH1
model_FunctionSpaceDegree = 1
#
#Objective
objective = l2distance
#
#Global minimum search
globalminimum_n_scale=1
globalminimumsearch = sequentialscale
globalminimum_relDecreaseThreshold = 0.0
