#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_df4_lbfgs_ls_snr2
#SBATCH --output=paper2_extended_inversion_df4_lbfgs_ls_snr2.log

#SBATCH --ntasks=1
#SBATCH --time=600:00
#SBATCH --mem-per-cpu=16000

time ./inversion  ../input/paper3/marmousi/common.txt ../input/paper3/marmousi/inversion.txt ../input/paper3/marmousi/innerproduct_df.txt ../input/paper3/marmousi/localminimum_classic.txt ../input/paper3/marmousi/descentsearch_lbfgs_ls.txt ../input/paper3/marmousi/linesearch_unit.txt ../input/paper3/marmousi/data_snr2.txt -localminimum_maxIteration 80 -scale0 4.0 -verbose 2 -maxThreads 1 -name df4_lbfgs_ls_snr2
