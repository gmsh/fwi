#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_df_lbfgs_ls_snr3_tv
#SBATCH --output=paper2_extended_inversion_df_lbfgs_ls_snr3_tv.log

#SBATCH --ntasks=1
#SBATCH --time=1200:00
#SBATCH --mem-per-cpu=16000
#
#SBATCH --array=0-35

ARGS_ADD=(0.4 0.2 0.1 0.05 0.02 0.005)
ARGS_ES=(0.0 0.25 0.5 1.0 2.0 4.0)

time ./inversion  ../input/paper3/marmousi/common.txt ../input/paper3/marmousi/inversion.txt ../input/paper3/marmousi/innerproduct_df.txt ../input/paper3/marmousi/localminimum_classic.txt ../input/paper3/marmousi/descentsearch_lbfgs_ls.txt ../input/paper3/marmousi/linesearch_unit.txt ../input/paper3/marmousi/data_snr3.txt ../input/paper3/marmousi/regularization_totalvariation.txt -localminimum_meanRelDecreaseThreshold 0.00001 -localminimum_meanRelDecreaseMemory 5 -localminimum_maxIteration 180 -verbose 2 -maxThreads 1 -regularization_lambda ${ARGS_ADD[$SLURM_ARRAY_TASK_ID % 6]} -scale0 ${ARGS_ES[$SLURM_ARRAY_TASK_ID / 6]} -name df_lbfgs_ls_snr3_tv_$SLURM_ARRAY_TASK_ID
