#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_ssm7_lbfgs_ls_snr2
#SBATCH --output=paper2_extended_inversion_ssm7_lbfgs_ls_snr2.log

#SBATCH --ntasks=1
#SBATCH --time=720:00
#SBATCH --mem-per-cpu=8000

time ./inversion ../input/paper3/marmousi/common.txt ../input/paper3/marmousi/inversion.txt ../input/paper3/marmousi/innerproduct_ssm.txt ../input/paper3/marmousi/localminimum_classic.txt ../input/paper3/marmousi/descentsearch_lbfgs_ls.txt ../input/paper3/marmousi/linesearch_unit.txt ../input/paper3/marmousi/data_snr2.txt -scale0 8.0 -localminimum_maxIteration 100 -verbose 2 -maxThreads 1 -name ssm7_lbfgs_ls_snr2
