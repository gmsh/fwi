#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_ssm_lbfgs_ls_snr2
#SBATCH --output=paper2_extended_inversion_ssm_lbfgs_ls_snr2.log

#SBATCH --ntasks=1
#SBATCH --time=360:00
#SBATCH --mem-per-cpu=16000

time ./inversion ../input/paper3/marmousi/common.txt ../input/paper3/marmousi/inversion.txt ../input/paper3/marmousi/innerproduct_ssm.txt ../input/paper3/marmousi/localminimum_classic.txt ../input/paper3/marmousi/descentsearch_lbfgs_ls.txt ../input/paper3/marmousi/linesearch_unit.txt ../input/paper3/marmousi/data_snr2.txt -scale0 0.5 -localminimum_maxIteration 50 -verbose 2 -maxThreads 1 -name ssm_lbfgs_ls_snr2
