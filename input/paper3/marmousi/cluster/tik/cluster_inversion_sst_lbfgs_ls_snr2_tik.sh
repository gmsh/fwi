#!/bin/bash
#
#SBATCH --job-name=paper2_extended_inversion_sst_lbfgs_ls_snr2_tik
#SBATCH --output=paper2_extended_inversion_sst_lbfgs_ls_snr2_tik.log

#SBATCH --ntasks=1
#SBATCH --time=400:00
#SBATCH --mem-per-cpu=16000
#
#SBATCH --array=0-14

ARGS_R=( 1.0 0.25 0.23 0.21 0.19 0.17 0.15 0.13 0.11 0.09 0.07 0.05 0.03 0.01 0.001 )

time ./inversion  ../input/paper3/marmousi/common.txt ../input/paper3/marmousi/inversion.txt ../input/paper3/marmousi/innerproduct_sst.txt ../input/paper3/marmousi/localminimum_classic.txt ../input/paper3/marmousi/descentsearch_lbfgs_ls.txt ../input/paper3/marmousi/linesearch_unit.txt ../input/paper3/marmousi/data_snr2.txt ../input/paper3/marmousi/regularization_tikhonov.txt -verbose 2 -maxThreads 1 -localminimum_maxIteration 60 -regularization_lambda ${ARGS_R[$SLURM_ARRAY_TASK_ID]} -name sst_lbfgs_ls_snr2_tik$SLURM_ARRAY_TASK_ID
