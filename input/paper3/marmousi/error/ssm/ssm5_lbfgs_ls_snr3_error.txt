name = ssm5_lbfgs_ls_snr2_error
#Configuration
unknown = subsurface
m0_typec0 = file
m0_pathc0 = ../input/paper2/marmousi_slowness2
#Error
error_type = relative_mean
integration_degree = 8
#Input
inverted_model_path = ../../Manuscript/Part3bis/TC1/SNR2/SSM_LBFGS_LS//
inverted_model_name = paper2_extended_ssm5_lbfgs_ls_snr2
inverted_model_prefix = g0s0
inverted_model_number = 100
#Output
write_error_field = 0
