#Inner product
innerproduct = sobolev_df
innerproduct_weight_bnd = 0.
innerproduct_diag_preconditioner_smooth = 0
innerproduct_diag_preconditioner_scale = 1
innerproduct_preconditioner_ref0 = 5400
innerproduct_stabilization = 5400
innerproduct_beta = 0.075
innerproduct_scale_structure = 0.125
