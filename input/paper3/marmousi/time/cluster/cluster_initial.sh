#!/bin/bash
#
#SBATCH --job-name=timewave_initial
#SBATCH --output=timewave_initial.log

#SBATCH --ntasks=1
#SBATCH --time=400:00
#SBATCH --mem-per-cpu=16000

time ./time_synthetics_wave ../input/paper3/marmousi/time/common.txt ../input/paper3/marmousi/time/initial.txt -maxThreads 1 -verbose 2
