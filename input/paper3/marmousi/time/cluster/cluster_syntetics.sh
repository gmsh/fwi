#!/bin/bash
#
#SBATCH --job-name=timewave_synthetics
#SBATCH --output=timewave_synthetics.log

#SBATCH --ntasks=1
#SBATCH --time=400:00
#SBATCH --mem-per-cpu=16000

time ./time_synthetics_wave ../input/paper3/marmousi/time/common.txt ../input/paper3/marmousi/time/synthetics.txt -maxThreads 1 -verbose 2
