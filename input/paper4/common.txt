prename = paper4_
#Configuration
configuration = volume_acquisition
#
#Parametrization
parametrization = natural
model_reference_frequency = 1.
#
ye = 0.0
yr = 0.0
xe = 5.0
xr = 20.0
nxe = 11
nxr = 11
nye = 0
nyr = 0
He = 15.0
Hr = 15.0
Le = 0.0
Lr = 0.0
L = 25.0
H = 25.0
#
Re(mbc0) = 1.
Im(mbc0) = 0.
#
inclusion_ni=1
inclusion_filled = 1
#Inclusion 1
inclusion0 = cylinder
inclusion_x0i0 = 12.5
inclusion_y0i0 = 12.5
inclusion_ri0 = 2.5
#
#Equation
physic = acoustic
equation = helmholtz
equation_boundary = 1
#
#Discretization
h = 0.125
integration_type = Gauss
#Wave
wave_FunctionSpaceType = HierarchicalH1
wave_FunctionSpaceDegree0 = 1
wave_FunctionSpaceDegree1 = 2
wave_FunctionSpaceDegree2 = 3
wave_FunctionSpaceDegree3 = 4
#
equation_integration_degree_blk0 = 2
equation_integration_degree_bnd0 = 3
equation_integration_degree_blk1 = 4
equation_integration_degree_bnd1 = 5
equation_integration_degree_blk2 = 6
equation_integration_degree_bnd2 = 7
equation_integration_degree_blk3 = 8
equation_integration_degree_bnd3 = 9
innerproduct_integration_degree_blk = 8
innerproduct_integration_degree_bnd = 9
#
n_freq=4
#Frequency
frequency0 = 0.25
frequency1 = 0.5
frequency2 = 0.75
frequency3 = 1.
