#!/bin/bash
#
#SBATCH --job-name=paper3_synthetics
#SBATCH --output=paper3_synthetics.log

#SBATCH --ntasks=1
#SBATCH --time=30:00
#SBATCH --mem-per-cpu=48000

time ./synthetics ../input/paper3/common.txt ../input/paper3/synthetics.txt -verbose 2 -maxThreads 1
