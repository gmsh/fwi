#!/bin/bash
#
#SBATCH --job-name=paper3_inversion_nscgA
#SBATCH --output=paper3_inversion_nscgA.log

#SBATCH --ntasks=1
#SBATCH --time=2400:00
#SBATCH --mem-per-cpu=48000

time ./inversion  ../input/paper3/common.txt ../input/paper3/convergence/inversion.txt ../input/paper3/convergence/inversion_nscgA.txt -verbose 2 -maxThreads 1
