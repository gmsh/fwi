#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  6 15:15:24 2021

@author: xavier
"""
import numpy as np
import scipy as sp
import scipy.special
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
#Value for 1 width figure
H = 1.5;
W = 6.5;
save = False;


"""
Example I: Wave number perturbation
"""
L = 8.712
f = np.array([4., 6., 8.])
ω = 2. * np.pi * f
v = 1.5
k = ω / v
N = 122
x = np.linspace(0, L, N)

def d(xe,xr,i):
        if(xe==xr): return 0.5 * (d(0.,1e-3,i)+d(0.,-1e-3,i)) #Must match numerical value at 0 (?)
        return np.conj( -1j/4*sp.special.hankel1(0,k[i]*np.abs(xe-xr)) )
for i in range(0,np.size(f)):
    D = np.zeros([N, 2*N])
    for e in range(0,N):
        for r in range(0,N):
            D[e,2*r] = np.real(d(x[e],x[r],i))
            D[e,2*r+1] = np.imag(d(x[e],x[r],i))
    np.savetxt("paper3_reference_data"+str(i)+".csv",D,delimiter=";")