#!/bin/bash
#
#SBATCH --job-name=RevAG2020
#SBATCH --output=RevAG2020.log

#SBATCH --ntasks=2
#SBATCH --time=180:00
#SBATCH --mem-per-cpu=2000

./synthetics ../input/review_paper1/common.txt ../input/review_paper1/synthetic_highly.txt -verbose 1
./directional ../input/review_paper1/common.txt ../input/review_paper1/directional_highly_inclusion.txt -verbose 1
./directional ../input/review_paper1/common.txt ../input/review_paper1/directional_highly_background.txt -verbose 1
./convergence ../input/review_paper1/common.txt ../input/review_paper1/convergence_highly_background.txt -verbose 1
./gradient ../input/review_paper1/common.txt ../input/review_paper1/gradient_highly.txt -verbose 1

./synthetics ../input/review_paper1/common.txt ../input/review_paper1/synthetic_weakly.txt -verbose 1
./directional ../input/review_paper1/common.txt ../input/review_paper1/directional_weakly_inclusion.txt -verbose 1
./directional ../input/review_paper1/common.txt ../input/review_paper1/directional_weakly_background.txt -verbose 1
./convergence ../input/review_paper1/common.txt ../input/review_paper1/convergence_weakly_background.txt -verbose 1
