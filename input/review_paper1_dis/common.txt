prename = RevAG2020_
#Equation
physic = acoustic
equation = helmholtz
useGreen = 1
#
#Configuration
configuration = inclusion
#
receiver_on_emitter = 0
ye = 0.0
yr = 0.0
nxe = 11
nxr = 11
nye = 0
nyr = 0
He = 15.0
Hr = 15.0
Le = 0.0
Lr = 0.0
L = 25.0
H = 25.0
#
#Discretization
wave_FunctionSpaceType = HierarchicalH1
wave_IntegrationType = Gauss
wave_FunctionSpaceDegree = 5
#
model_IntegrationType = Gauss
model_FunctionSpaceDegree = 0
#
#Frequency
frequency0 = 1.
