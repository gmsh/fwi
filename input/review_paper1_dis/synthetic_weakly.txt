name = synthetic_dis_weakly
#Configuration
xe = 5.0
xr = 5.0
inclusion = cylinder
filled = 0
r = 2.5
#
unknown = none
inclusion_spatialdistribution = heavisidey0
yjump = 12.5
Re(mjump) = 0.5
Im(mjump) = 1.
Re(mb) = 1.
Im(mb) = 0.
Re(mc) = 1.
Im(mc) = -2.
#
#Discretization
h = 0.2
#Output
write_data_fields = 0
write_wave_fields = 1
