prename = paper1_acoustic_helmholtz_
#Equation
physic = acoustic
equation = helmholtz
equation_boundary = 1
#
#Parametrization
parametrization = natural
model_reference_frequency = 1.
#
#Configuration
configuration = volume_acquisition
#
ye = 0.0
yr = 0.0
nxe = 11
nxr = 11
nye = 0
nyr = 0
He = 15.0
Hr = 15.0
Le = 0.0
Lr = 0.0
L = 25.0
H = 25.0
#
#Discretization
wave_FunctionSpaceType = HierarchicalH1
wave_FunctionSpaceDegree = 5
#
integration_type = Gauss
equation_integration_degree_blk = 10
equation_integration_degree_bnd = 10
#
#Frequency
n_freq=1
frequency0 = 1.
