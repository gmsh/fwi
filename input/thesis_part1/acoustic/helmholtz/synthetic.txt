name = synthetics
#Configuration
unknown = none
Re(mbc0) = 1.
Im(mbc0) = 0.
Re(mbc1) = 1.
Im(mbc1) = 0.
#
xe = 5.0
xr = 20.0
inclusion_ni=1
inclusion_filled = 1
#Inclusion 1
inclusion0 = cylinder
inclusion_x0i0 = 12.5
inclusion_y0i0 = 12.5
inclusion_ri0 = 2.5
Re(mi0c0) = 0.9
Im(mi0c0) = 0.
Re(mi0c1) = 1.1
Im(mi0c1) = 0.
#
#Discretization
h = 0.2
#Output
write_model_fields = 1
write_wave_fields = 1
write_data_fields = 1
