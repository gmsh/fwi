name = background_convergence
#Objective
objective = l2distance
#Configuration
unknown = background
Re(mbc0) = 0.93
Im(mbc0) = 0.
#
xe = 5.0
xr = 20.0
inclusion_ni=1
inclusion_filled = 1
#Inclusion 1
inclusion0 = cylinder
inclusion_x0i0 = 12.5
inclusion_y0i0 = 12.5
inclusion_ri0 = 2.5
Re(mi0c0) = 1.2
Im(mi0c0) = 0.
#
#Directional
Re(dmc0) = 1.
Im(dmc0) = 0.
interval = log
eps0 = 1e-10
epsN = 1e-1
N = 9
#
#Data
data0 = paper1_acoustic_simplewave_synthetics_data0
#
#Discretization
h = 0.25
