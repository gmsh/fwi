name = gradient
#
#Configuration
unknown = all
m0_typec0 = file
m0_pathc0 = ../input/thesis_part1/concrete3/large_concrete_s2_initial
#
#Objective
objective = l2distance
#
#InnerProduct
innerproduct_diag_preconditioner_scale = 0
innerproduct_diag_preconditioner_smooth = 0
innerproduct_preconditioner_ref = 1.
innerproduct_integration_degree_blk = 7
innerproduct_integration_degree_bnd = 8
#
#Scale
scale0 = 5e-4
scaleN = 5e-2
interval = log
N = 2
#
#Frequency
n_group=1
#
data = concrete3_synthetics_data
#
#Discretization
#Model
model_FunctionSpaceType = HierarchicalH1
model_FunctionSpaceDegree = 1
