name = backgroundc1_convergence
#Directional
Re(dmc0) = 0.
Im(dmc0) = 0.
Re(dmc1) = 1.
Im(dmc1) = 0.
Re(dmc2) = 0.
Im(dmc2) = 0.
interval = log
eps0 = 1e-10
epsN = 1e-1
N = 9
