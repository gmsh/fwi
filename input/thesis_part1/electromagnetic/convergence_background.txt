#Objective
objective = l2distance
#Configuration
unknown = background
Re(mbc0) = 1.1
Im(mbc0) = 0.
Re(mbc1) = 0.18849
Im(mbc1) = 0.
#
xe = 5.0
xr = 20.0
inclusion_ni=1
inclusion_filled = 1
#Inclusion 1
inclusion0 = cylinder
inclusion_x0i0 = 12.5
inclusion_y0i0 = 12.5
inclusion_ri0 = 2.5
Re(mi0c0) = 1.2
Im(mi0c0) = 0.
Re(mi0c1) = 0.31415
Im(mi0c1) = 0.
#
#Data
data0 = paper1_electromagnetic_maxwell_synthetics_data0
#
#Discretization
h = 0.25
