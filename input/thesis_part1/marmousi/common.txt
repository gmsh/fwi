prename = thesis_part1_marmousi_
#Configuration
configuration = surface_acquisition
#
#Parametrization
parametrization = slowness2_real
model_reference_frequency = 1.
#
xoffset = 0
Ler = 8.712
np = 243
re_start = 0
re_skip = 1
em_start = 0
em_skip = 2
ReceiverOnEmitter=0
ymax = 0.216
ymin = 0.072
L = 9.192
H = 2.904
#
Re(m_superc0) = 0.44
Im(m_superc0) = 0.
Re(m_subc0) = 0.25
Im(m_subc0) = 0.
#
#Equation
physic = acoustic
equation = simplewave
#
#Discretization
h = 0.036
integration_type = Gauss
#Wave
wave_FunctionSpaceType = HierarchicalH1
equation_integration_degree_blk0 = 5
equation_integration_degree_bnd0 = 6
equation_integration_degree_blk1 = 7
equation_integration_degree_bnd1 = 8
equation_integration_degree_blk2 = 9
equation_integration_degree_bnd2 = 10
#Wave
wave_FunctionSpaceDegree0 = 2
wave_FunctionSpaceDegree1 = 3
wave_FunctionSpaceDegree2 = 4
#
#Frequencies
n_freq = 3
#Frequency0
frequency0 = 4.
#Frequency1
frequency1 = 6.
#Frequency2
frequency2 = 8.
