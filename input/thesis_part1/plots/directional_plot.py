#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 18 16:37:05 2022

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
H = 3.0;
L = 3.20;

"""
path = 'Acoustics/Simplewave/'
filename = 'paper1_acoustic_simplewave_inclusion'
var = 's^2_c'
cylinder = True
background = False
n_param = 1
idx_param = 0
"""
"""
path = 'Acoustics/Simplewave/'
filename = 'paper1_acoustic_simplewave_background'
var = 's^2_0'
cylinder = False
background = True
n_param = 1
idx_param = 0
"""
"""
path = 'Acoustics/Helmholtz/'
filename = 'paper1_acoustic_helmholtz_inclusionc0'
var = '\\nu_c'
cylinder = True
background = False
n_param = 2
idx_param = 0
"""
"""
path = 'Acoustics/Helmholtz/'
filename = 'paper1_acoustic_helmholtz_inclusionc1'
var = 'K_c'
cylinder = True
background = False
n_param = 2
idx_param = 2
"""
"""
path = 'Acoustics/Helmholtz/'
filename = 'paper1_acoustic_helmholtz_backgroundc0'
var = '\\nu_0'
cylinder = False
background = True
n_param = 2
idx_param = 0
"""
"""
path = 'Acoustics/Helmholtz/'
filename = 'paper1_acoustic_helmholtz_backgroundc1'
var = 'K_0'
cylinder = False
background = True
n_param = 2
idx_param = 2
"""
"""
path = 'Elastodynamics/Navier/'
filename = 'paper1_elastodynamic_navier_inclusionc0'
var = '\\rho_c'
cylinder = True
background = False
n_param = 3
idx_param = 0
"""
"""
path = 'Elastodynamics/Navier/'
filename = 'paper1_elastodynamic_navier_inclusionc1'
var = '\\lambda_c'
cylinder = True
background = False
n_param = 3
idx_param = 2
"""
"""
path = 'Elastodynamics/Navier/'
filename = 'paper1_elastodynamic_navier_inclusionc2'
var = '\\mu_c'
cylinder = True
background = False
n_param = 3
idx_param = 4
"""
"""
path = 'Elastodynamics/Navier/'
filename = 'paper1_elastodynamic_navier_backgroundc0'
var = '\\rho_c'
cylinder = False
background = True
n_param = 3
idx_param = 0
"""
"""
path = 'Elastodynamics/Navier/'
filename = 'paper1_elastodynamic_navier_backgroundc1'
var = '\\lambda_c'
cylinder = False
background = True
n_param = 3
idx_param = 2
"""
"""
path = 'Elastodynamics/Navier/'
filename = 'paper1_elastodynamic_navier_backgroundc2'
var = '\\mu_c'
cylinder = False
background = True
n_param = 3
idx_param = 4
"""

x = np.loadtxt(path+filename+'_directionalg0.csv',delimiter=';');

#Cost function
f0 = plt.figure(figsize=(L,H),tight_layout=False);
plt.subplots_adjust(top=0.93,right=0.95,bottom=0.2,left=0.25)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

plt.plot(x[:,idx_param],x[:,2*n_param],color='k',marker='o',markersize=3,linewidth=0)
plt.xlabel(r'$'+var+'$')
plt.ylabel(r'$\mathcal{J}('+var+')$')
plt.grid()
plt.savefig(path+filename+'_misfit.eps')
plt.close()

#First derivative
f1 = plt.figure(figsize=(L,H))
plt.subplots_adjust(top=0.93,right=0.95,bottom=0.2,left=0.25)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

#Finite difference
plt.plot(x[:,idx_param],x[:,2*n_param+4],color='tab:green',marker='x',markersize=5,linewidth=0,label='FD')


#Adjoint state method
if cylinder:
    #Volume cylinder
    plt.plot(x[:,idx_param],x[:,2*n_param+3],color='tab:brown',marker='o',markersize=3,linewidth=0,label='AS')

if background:
    #Volume background
    plt.plot(x[:,idx_param],x[:,2*n_param+2],color='tab:blue',marker='o',markersize=3,linewidth=0,label='ASv')
    #Surface background
    plt.plot(x[:,idx_param],x[:,2*n_param+1],color='tab:blue',marker='o',fillstyle='none',markersize=3,linewidth=0,label='ASs')

plt.grid()
#plt.legend(loc=3,ncol=1);
plt.xlabel(r'$'+var+'$')
plt.ylabel(r'$D_{'+var+'} \mathcal{J}('+var+')$')
plt.savefig(path+filename+'_dj.eps')
plt.close()

#Second derivative
f2 = plt.figure(figsize=(L,H));
plt.subplots_adjust(top=0.93,right=0.95,bottom=0.2,left=0.25)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

#Finite difference
plt.plot(x[:,idx_param],x[:,2*n_param+9],color='tab:green',marker='x',markersize=5,linewidth=0,label='FD')

#Adjoint state method
if cylinder:
    #Volume cylinder
    plt.plot(x[:,idx_param],x[:,2*n_param+7],color='tab:brown',marker='o',markersize=3,linewidth=0,label='AS')

if background:
    #Volume background
    plt.plot(x[:,idx_param],x[:,2*n_param+7],color='tab:blue',marker='o',markersize=3,linewidth=0,label='ASv')
    #Surface background
    plt.plot(x[:,idx_param],x[:,2*n_param+6],color='tab:blue',marker='o',fillstyle='none',markersize=3,linewidth=0,label='ASs')

plt.grid()
#plt.legend(loc=3,ncol=1);
plt.xlabel(r'$'+var+'$')
plt.ylabel(r'$D^2_{'+var+'} \mathcal{J}('+var+')$')
plt.savefig(path+filename+'_d2j.eps')
plt.close()


#Convergence
if background:
    ref = np.loadtxt(path+filename+'_convergence.csv',delimiter=';',skiprows=0,max_rows=1);
    x = np.loadtxt(path+filename+'_convergence.csv',delimiter=';',skiprows=1);
    
    
    #Cost function
    f0 = plt.figure(figsize=(L,H),tight_layout=False);
    plt.subplots_adjust(top=0.93,right=0.95,bottom=0.2,left=0.25)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    ax = plt.gca().invert_xaxis()
    plt.ylim(5e-13,2.)
    plt.loglog(x[:,0],x[:,2],color='k',marker='+',markersize=5,linewidth=0)
    plt.hlines(np.abs(ref[1]),x[0,0],x[-1,2],colors='tab:blue', linestyles='dashed')
    plt.xlabel(r'$\epsilon$')
    plt.grid()
    plt.savefig(path+filename+'_error_all.eps')
    plt.close()
    
    f1 = plt.figure(figsize=(L,H),tight_layout=False);
    plt.subplots_adjust(top=0.93,right=0.95,bottom=0.2,left=0.25)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    ax = plt.gca().invert_xaxis()
    plt.ylim(5e-13,2.)
    plt.loglog(x[:,0],x[:,3],color='k',marker='+',markersize=5,linewidth=0)
    plt.hlines(np.abs(ref[1]),x[0,0],x[-1,0],colors='tab:blue', linestyles='dashed')
    plt.xlabel(r'$\epsilon$')
    plt.grid()
    plt.savefig(path+filename+'_error_blk.eps')
    plt.close()