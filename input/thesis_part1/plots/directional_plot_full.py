#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  7 18:22:34 2022

@author: xavier
"""
from directional_plot_partial import plot

from params import *
plot(data_embc1)
plot(data_embc0)
plot(data_emic1)
plot(data_emic0)


plot(data_aswi)
plot(data_aswb)
plot(data_aswwi)
plot(data_aswwb)
plot(data_ahic0)
plot(data_ahic1)
plot(data_ahbc0)
plot(data_ahbc1)
plot(data_enic0)
plot(data_enic1)
plot(data_enic2)
plot(data_enbc0)
plot(data_enbc1)
plot(data_enbc2)