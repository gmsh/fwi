#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  7 18:21:23 2022

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
plt.rcParams['ytick.right'] = plt.rcParams['ytick.labelright'] = True
plt.rcParams['ytick.left'] = plt.rcParams['ytick.labelleft'] = False
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb} \renewcommand{\familydefault}{\sfdefault} \usepackage{sansmathfonts}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
H = 3.0;
L = 3.0;

def plot(data):
    prepath = "/home/xavier/Academia/PhD/Manuscript/Part1/"
    x = np.loadtxt(prepath+data.path+data.filename+'_directionalg0.csv',delimiter=';');
    if(data.xnegative):
        x[:,data.idx_param] = -x[:,data.idx_param] 
    
    #Cost function
    f0 = plt.figure(figsize=(L,H),tight_layout=False);
    plt.subplots_adjust(top=0.93,right=0.85,bottom=0.2,left=0.15)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    if(data.xlog):
        plt.xscale('log')
    
    plt.plot(x[:,data.idx_param],x[:,2*data.n_param],color='k',marker='o',markersize=3,linewidth=0)
    plt.xlabel(r'$'+data.var+'$')
    plt.ylabel(r'$J('+data.var+')$')
    plt.grid()
    plt.savefig(prepath+data.path+data.filename+'_misfit.eps')
    plt.close()
    
    #First derivative
    f1 = plt.figure(figsize=(L,H))
    plt.subplots_adjust(top=0.93,right=0.85,bottom=0.2,left=0.15)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    if(data.xlog):
        plt.xscale('log')
        
    #Finite difference
    plt.plot(x[:,data.idx_param],x[:,2*data.n_param+4],color='tab:green',marker='x',markersize=5,linewidth=0,label='FD')
    
    
    #Adjoint state method
    if data.cylinder:
        if(not data.surface_cylinder):
            #Volume cylinder
            plt.plot(x[:,data.idx_param],x[:,2*data.n_param+2],color='tab:brown',marker='o',markersize=3,linewidth=0,label='ASv')
        else:
            #Surface cylinder
            plt.plot(x[:,data.idx_param],x[:,2*data.n_param+1],color='tab:brown',marker='o',markersize=3,linewidth=0,label='ASs',fillstyle='none')
    
    if data.background:
        #Volume background
        plt.plot(x[:,data.idx_param],x[:,2*data.n_param+2],color='tab:blue',marker='o',markersize=3,linewidth=0,label='ASv')
        #Surface background
        plt.plot(x[:,data.idx_param],x[:,2*data.n_param+1],color='tab:blue',marker='o',fillstyle='none',markersize=3,linewidth=0,label='ASs')
    
    plt.grid()
    #plt.legend(loc=3,ncol=1);
    plt.xlabel(r'$'+data.var+'$')
    plt.ylabel(r'$D_{'+data.var+'} J('+data.var+')$')
    plt.savefig(prepath+data.path+data.filename+'_dj.eps')
    plt.close()
    
    #Second derivative
    f2 = plt.figure(figsize=(L,H));
    plt.subplots_adjust(top=0.93,right=0.85,bottom=0.2,left=0.15)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    if(data.xlog):
        plt.xscale('log')
        
    #Finite difference
    plt.plot(x[:,data.idx_param],x[:,2*data.n_param+9],color='tab:green',marker='x',markersize=5,linewidth=0,label='FD')
    
    #Adjoint state method
    if data.cylinder:
        if(not data.surface_cylinder):
            #Volume cylinder
            plt.plot(x[:,data.idx_param],x[:,2*data.n_param+7],color='tab:brown',marker='o',markersize=3,linewidth=0,label='ASv')
        else:
            #Surface cylinder
            plt.plot(x[:,data.idx_param],x[:,2*data.n_param+6],color='tab:brown',marker='o',markersize=3,linewidth=0,fillstyle='none',label='ASv')    
    if data.background:
        #Volume background
        plt.plot(x[:,data.idx_param],x[:,2*data.n_param+7],color='tab:blue',marker='o',markersize=3,linewidth=0,label='ASv')
        #Surface background
        plt.plot(x[:,data.idx_param],x[:,2*data.n_param+6],color='tab:blue',marker='o',fillstyle='none',markersize=3,linewidth=0,label='ASs')
    
    plt.grid()
    #plt.legend(loc=3,ncol=1);
    plt.xlabel(r'$'+data.var+'$')
    plt.ylabel(r'$D^2_{'+data.var+'} J('+data.var+')$')
    plt.savefig(prepath+data.path+data.filename+'_d2j.eps')
    plt.close()
    
    
    #Convergence
    if data.background:
        ref = np.loadtxt(prepath+data.path+data.filename+'_convergence.csv',delimiter=';',skiprows=0,max_rows=1);
        x = np.loadtxt(prepath+data.path+data.filename+'_convergence.csv',delimiter=';',skiprows=1);
        
        
        #First derivative
        f0 = plt.figure(figsize=(L,H),tight_layout=False);
        plt.subplots_adjust(top=0.93,right=0.85,bottom=0.2,left=0.15)
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax = plt.gca().invert_xaxis()
        #plt.ylim(5e-13,2.)
        plt.loglog(x[:,0],x[:,2],color='k',marker='+',markersize=5,linewidth=0)
        plt.hlines(np.abs(ref[1]),x[0,0],x[-1,0],colors='tab:blue', linestyles='dashed')
        plt.xlabel(r'$\epsilon$')
        plt.grid()
        plt.savefig(prepath+data.path+data.filename+'_error_all.eps')
        plt.close()
        
        f1 = plt.figure(figsize=(L,H),tight_layout=False);
        plt.subplots_adjust(top=0.93,right=0.85,bottom=0.2,left=0.15)
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax = plt.gca().invert_xaxis()
        plt.ylim(5e-13,2.)
        plt.loglog(x[:,0],x[:,3],color='k',marker='+',markersize=5,linewidth=0)
        plt.hlines(np.abs(ref[1]),x[0,0],x[-1,0],colors='tab:blue', linestyles='dashed')
        plt.xlabel(r'$\epsilon$')
        plt.grid()
        plt.savefig(prepath+data.path+data.filename+'_error_blk.eps')
        plt.close()
        
        #Second derivative
        f2 = plt.figure(figsize=(L,H),tight_layout=False);
        plt.subplots_adjust(top=0.93,right=0.85,bottom=0.2,left=0.15)
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax = plt.gca().invert_xaxis()
        #plt.ylim(5e-8,200.)
        plt.loglog(x[:,0],x[:,5],color='k',marker='+',markersize=5,linewidth=0)
        plt.hlines(np.abs(ref[4]),x[0,0],x[-1,0],colors='tab:blue', linestyles='dashed')
        plt.xlabel(r'$\epsilon$')
        plt.grid()
        plt.savefig(prepath+data.path+data.filename+'_error2_all.eps')
        plt.close()
