General.BackgroundGradient = 0;
General.DisplayBorderFactor = 0.;
View[0].ShowScale = 0;
View[0].Axes = 3;
View[0].AxesFormatX = "";
View[0].AxesFormatY = "";
View[0].AxesTicsX = 0;
View[0].AxesTicsY = 0;
General.MenuWidth = 0;
View[0].ColormapNumber = 9;
General.GraphicsHeight = 480;
General.GraphicsWidth = 480;
General.SmallAxes = 0;
View[0].RangeType = 2;
View[0].SaturateValues = 1;
Mesh.SurfaceEdges = 0;
Mesh.SurfaceFaces = 0;
Geometry.Lines = 0;
Geometry.Points = 0;
General.ArrowHeadRadius = 0.1;
General.ArrowStemLength = 0.3;
General.ArrowStemRadius = 0.0;


path = "/home/xavier/Academia/PhD/Manuscript/Part1";

//Simple wave

//Wavefield
Merge StrCat(path,"/Acoustics/Simplewave/paper1_acoustic_simplewave_synthetics_u0s5.pos");
View[0].CustomMax = 0.12;
View[0].CustomMin = -0.12;
Save StrCat(path,"/Acoustics/Simplewave/simplewave_wavefieldRe.png");
View[0].TimeStep = 1;
Save StrCat(path,"/Acoustics/Simplewave/simplewave_wavefieldIm.png");
Delete View[0];

Merge StrCat(path,"/Acoustics/Simplewave/paper1_acoustic_simplewave_weakly_synthetics_u0s5.pos");
View[0].CustomMax = 0.12;
View[0].CustomMin = -0.12;
Save StrCat(path,"/Acoustics/Simplewave/simplewave_weakly_wavefieldRe.png");
View[0].TimeStep = 1;
Save StrCat(path,"/Acoustics/Simplewave/simplewave_weakly_wavefieldIm.png");
Delete View[0];

//Gradients
max_blk = {0.015,0.006,0.00016};
max_bnd = {0.003,0.0002,0.000007};
For n In {0:2}
    Merge Sprintf(StrCat(path,"/Acoustics/Simplewave/paper1_acoustic_simplewave_gradient_blk_gradientn%gc0.pos"),n);
    View[0].CustomMax = max_blk[n];
    View[0].CustomMin = -max_blk[n];
    Save Sprintf(StrCat(path,"/Acoustics/Simplewave/paper1_acoustic_simplewave_gradient_blk_gradientn%gc0.png"),n);
    Delete View[0];

    Merge Sprintf(StrCat(path,"/Acoustics/Simplewave/paper1_acoustic_simplewave_gradient_bnd_gradientn%gc0.pos"),n);
    View[0].CustomMax = max_bnd[n];
    View[0].CustomMin = -max_bnd[n];
    Save Sprintf(StrCat(path,"/Acoustics/Simplewave/paper1_acoustic_simplewave_gradient_bnd_gradientn%gc0.png"),n);
    Delete View[0];
EndFor


//Helmholtz

//Wavefield
Merge StrCat(path,"/Acoustics/Helmholtz/paper1_acoustic_helmholtz_synthetics_u0s5.pos");
View[0].CustomMax = 0.12;
View[0].CustomMin = -0.12;
Save StrCat(path,"/Acoustics/Helmholtz/helmholtz_wavefieldRe.png");
View[0].TimeStep = 1;
Save StrCat(path,"/Acoustics/Helmholtz/helmholtz_wavefieldIm.png");
Delete View[0];

//Gradients
max_blk = {0.015,0.015};
max_bnd = {0.003,0.003};
For c In {0:1}
    Merge Sprintf(StrCat(path,"/Acoustics/Helmholtz/paper1_acoustic_helmholtz_gradient_blk_gradientn0c%g.pos"),c);
    View[0].CustomMax = max_blk[c];
    View[0].CustomMin = -max_blk[c];
    Save Sprintf(StrCat(path,"/Acoustics/Helmholtz/paper1_acoustic_helmholtz_gradient_blk_gradientn0c%g.png"),c);
    Delete View[0];

    Merge Sprintf(StrCat(path,"/Acoustics/Helmholtz/paper1_acoustic_helmholtz_gradient_bnd_gradientn0c%g.pos"),c);
    View[0].CustomMax = max_bnd[c];
    View[0].CustomMin = -max_bnd[c];
    Save Sprintf(StrCat(path,"/Acoustics/Helmholtz/paper1_acoustic_helmholtz_gradient_bnd_gradientn0c%g.png"),c);
    Delete View[0];
EndFor

//Navier

//Wavefield
Merge StrCat(path,"/Elastodynamics/Navier/paper1_elastodynamic_navier_synthetics_u0s5.pos");
View[0].CustomMax = 0.05;
View[0].CustomMin = 0.;
View[0].ArrowSizeMax = 25;
View[0].ArrowSizeMin = 0;
Save StrCat(path,"/Elastodynamics/Navier/navier_wavefieldRe.png");
View[0].TimeStep = 1;
Save StrCat(path,"/Elastodynamics/Navier/navier_wavefieldIm.png");
Delete View[0];

//Gradients
max_blk = {0.008,0.00015,0.007};
max_bnd = {0.0016,0.00023,0.0015};
For c In {0:2}
    Merge Sprintf(StrCat(path,"/Elastodynamics/Navier/paper1_elastodynamic_navier_gradient_blk_gradientn0c%g.pos"),c);
    View[0].CustomMax = max_blk[c];
    View[0].CustomMin = -max_blk[c];
    Save Sprintf(StrCat(path,"/Elastodynamics/Navier/paper1_elastodynamic_navier_gradient_blk_gradientn0c%g.png"),c);
    Delete View[0];

    Merge Sprintf(StrCat(path,"/Elastodynamics/Navier/paper1_elastodynamic_navier_gradient_bnd_gradientn0c%g.pos"),c);
    View[0].CustomMax = max_bnd[c];
    View[0].CustomMin = -max_bnd[c];
    Save Sprintf(StrCat(path,"/Elastodynamics/Navier/paper1_elastodynamic_navier_gradient_bnd_gradientn0c%g.png"),c);
    Delete View[0];
EndFor

//Maxwell

//Wavefield
Merge StrCat(path,"/Electromagnetics/Maxwell/paper1_electromagnetic_maxwell_synthetics_u0s5.pos");
View[0].CustomMax = 0.008;
View[0].CustomMin = 0.;
View[0].ArrowSizeMax = 20;
View[0].ArrowSizeMin = 0;
Save StrCat(path,"/Electromagnetics/Maxwell/maxwell_wavefieldRe.png");
View[0].TimeStep = 1;
Save StrCat(path,"/Electromagnetics/Maxwell/maxwell_wavefieldIm.png");
Delete View[0];
Merge StrCat(path,"/Electromagnetics/Maxwell/paper1_electromagnetic_maxwell_synthetics_weakly_u0s5.pos");
View[0].CustomMax = 0.008;
View[0].CustomMin = 0.;
View[0].ArrowSizeMax = 20;
View[0].ArrowSizeMin = 0;
Save StrCat(path,"/Electromagnetics/Maxwell/maxwell_weakly_wavefieldRe.png");
View[0].TimeStep = 1;
Save StrCat(path,"/Electromagnetics/Maxwell/maxwell_weakly_wavefieldIm.png");
Delete View[0];

//Gradients
max_blk = {6.0e-5,2.3e-5};
max_bnd = {1.5e-5,1.8e-6};
For c In {0:1}
    Merge Sprintf(StrCat(path,"/Electromagnetics/Maxwell/paper1_electromagnetic_maxwell_gradient_blk_gradientn0c%g.pos"),c);
    View[0].CustomMax = max_blk[c];
    View[0].CustomMin = -max_blk[c];
    Save Sprintf(StrCat(path,"/Electromagnetics/Maxwell/paper1_electromagnetic_maxwell_gradient_blk_gradientn0c%g.png"),c);
    Delete View[0];

    Merge Sprintf(StrCat(path,"/Electromagnetics/Maxwell/paper1_electromagnetic_maxwell_gradient_bnd_gradientn0c%g.pos"),c);
    View[0].CustomMax = max_bnd[c];
    View[0].CustomMin = -max_bnd[c];
    Save Sprintf(StrCat(path,"/Electromagnetics/Maxwell/paper1_electromagnetic_maxwell_gradient_bnd_gradientn0c%g.png"),c);
    Delete View[0];
EndFor
