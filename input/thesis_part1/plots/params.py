#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  7 18:20:50 2022

@author: xavier
"""

class plot_data:
    path = ''
    filename = ''
    var = ''
    cylinder = False
    background = False
    n_param = 0
    idx_param = 0
    xlog = False
    xnegative = False
    surface_cylinder = False

#### Acoustics ####
    
####    
data_aswi = plot_data()
data_aswi.path = 'Acoustics/Simplewave/'
data_aswi.filename = 'paper1_acoustic_simplewave_inclusion'
data_aswi.var = 's^2_c'
data_aswi.cylinder = True
data_aswi.background = False
data_aswi.n_param = 1
data_aswi.idx_param = 0
####

####    
data_aswb = plot_data()
data_aswb.path = 'Acoustics/Simplewave/'
data_aswb.filename = 'paper1_acoustic_simplewave_background'
data_aswb.var = 's^2_0'
data_aswb.cylinder = False
data_aswb.background = True
data_aswb.n_param = 1
data_aswb.idx_param = 0
####

####    
data_aswwi = plot_data()
data_aswwi.path = 'Acoustics/Simplewave/'
data_aswwi.filename = 'paper1_acoustic_simplewave_weakly_inclusion'
data_aswwi.var = '\\alpha_c'
data_aswwi.cylinder = True
data_aswwi.background = False
data_aswwi.n_param = 1
data_aswwi.idx_param = 1
data_aswwi.xlog = True
data_aswwi.xnegative = True
data_aswwi.surface_cylinder = True
####

####    
data_aswwb = plot_data()
data_aswwb.path = 'Acoustics/Simplewave/'
data_aswwb.filename = 'paper1_acoustic_simplewave_weakly_background'
data_aswwb.var = 's^2_0'
data_aswwb.cylinder = False
data_aswwb.background = True
data_aswwb.n_param = 1
data_aswwb.idx_param = 0
####

####
data_ahic0 = plot_data()
data_ahic0.path = 'Acoustics/Helmholtz/'
data_ahic0.filename = 'paper1_acoustic_helmholtz_inclusionc0'
data_ahic0.var = '\\nu_c'
data_ahic0.cylinder = True
data_ahic0.background = False
data_ahic0.n_param = 2
data_ahic0.idx_param = 0
####

####
data_ahic1 = plot_data()
data_ahic1.path = 'Acoustics/Helmholtz/'
data_ahic1.filename = 'paper1_acoustic_helmholtz_inclusionc1'
data_ahic1.var = 'K_c'
data_ahic1.cylinder = True
data_ahic1.background = False
data_ahic1.n_param = 2
data_ahic1.idx_param = 2
####

####
data_ahbc0 = plot_data()
data_ahbc0.path = 'Acoustics/Helmholtz/'
data_ahbc0.filename = 'paper1_acoustic_helmholtz_backgroundc0'
data_ahbc0.var = '\\nu_0'
data_ahbc0.cylinder = False
data_ahbc0.background = True
data_ahbc0.n_param = 2
data_ahbc0.idx_param = 0
####

####
data_ahbc1 = plot_data()
data_ahbc1.path = 'Acoustics/Helmholtz/'
data_ahbc1.filename = 'paper1_acoustic_helmholtz_backgroundc1'
data_ahbc1.var = 'K_0'
data_ahbc1.cylinder = False
data_ahbc1.background = True
data_ahbc1.n_param = 2
data_ahbc1.idx_param = 2
####

#### Elastodynamics ####


####
data_enic0 = plot_data()
data_enic0.path = 'Elastodynamics/Navier/'
data_enic0.filename = 'paper1_elastodynamic_navier_inclusionc0'
data_enic0.var = '\\rho_c'
data_enic0.cylinder = True
data_enic0.background = False
data_enic0.n_param = 3
data_enic0.idx_param = 0
####

####
data_enic1 = plot_data()
data_enic1.path = 'Elastodynamics/Navier/'
data_enic1.filename = 'paper1_elastodynamic_navier_inclusionc1'
data_enic1.var = '\\lambda_c'
data_enic1.cylinder = True
data_enic1.background = False
data_enic1.n_param = 3
data_enic1.idx_param = 2
####

####
data_enic2 = plot_data()
data_enic2.path = 'Elastodynamics/Navier/'
data_enic2.filename = 'paper1_elastodynamic_navier_inclusionc2'
data_enic2.var = '\\mu_c'
data_enic2.cylinder = True
data_enic2.background = False
data_enic2.n_param = 3
data_enic2.idx_param = 4
####

####
data_enbc0 = plot_data()
data_enbc0.path = 'Elastodynamics/Navier/'
data_enbc0.filename = 'paper1_elastodynamic_navier_backgroundc0'
data_enbc0.var = '\\rho_c'
data_enbc0.cylinder = False
data_enbc0.background = True
data_enbc0.n_param = 3
data_enbc0.idx_param = 0
####

####
data_enbc1 = plot_data()
data_enbc1.path = 'Elastodynamics/Navier/'
data_enbc1.filename = 'paper1_elastodynamic_navier_backgroundc1'
data_enbc1.var = '\\lambda_c'
data_enbc1.cylinder = False
data_enbc1.background = True
data_enbc1.n_param = 3
data_enbc1.idx_param = 2
####

####
data_enbc2 = plot_data()
data_enbc2.path = 'Elastodynamics/Navier/'
data_enbc2.filename = 'paper1_elastodynamic_navier_backgroundc2'
data_enbc2.var = '\\mu_c'
data_enbc2.cylinder = False
data_enbc2.background = True
data_enbc2.n_param = 3
data_enbc2.idx_param = 4
####

#### Electrodynamics ####

####
data_emic0 = plot_data()
data_emic0.path = 'Electromagnetics/Maxwell/'
data_emic0.filename = 'paper1_electromagnetic_maxwell_inclusionc0'
data_emic0.var = '\\epsilon_c'
data_emic0.cylinder = True
data_emic0.background = False
data_emic0.n_param = 2
data_emic0.idx_param = 0
####

####
data_emic1 = plot_data()
data_emic1.path = 'Electromagnetics/Maxwell/'
data_emic1.filename = 'paper1_electromagnetic_maxwell_inclusionc1'
data_emic1.var = '\\sigma_c'
data_emic1.cylinder = True
data_emic1.background = False
data_emic1.n_param = 2
data_emic1.idx_param = 2
data_emic1.xlog = True
####

####
data_embc0 = plot_data()
data_embc0.path = 'Electromagnetics/Maxwell/'
data_embc0.filename = 'paper1_electromagnetic_maxwell_backgroundc0'
data_embc0.var = '\\epsilon_0'
data_embc0.cylinder = False
data_embc0.background = True
data_embc0.n_param = 2
data_embc0.idx_param = 0
####

####
data_embc1 = plot_data()
data_embc1.path = 'Electromagnetics/Maxwell/'
data_embc1.filename = 'paper1_electromagnetic_maxwell_backgroundc1'
data_embc1.var = '\\sigma_0'
data_embc1.cylinder = False
data_embc1.background = True
data_embc1.n_param = 2
data_embc1.idx_param = 2
data_embc1.xlog = True
####
