#!/bin/bash
#
#SBATCH --job-name=cross_inversion_sst2_lbfgs_ls
#SBATCH --output=cross_inversion_sst2_lbfgs_ls.log

#SBATCH --ntasks=1
#SBATCH --time=600:00
#SBATCH --mem-per-cpu=24000

time ./inversion  ../input/thesis_part2/cross/common.txt ../input/thesis_part2/cross/inversion.txt ../input/thesis_part2/cross/innerproduct_sst.txt ../input/thesis_part2/cross/localminimum_classic.txt ../input/thesis_part2/cross/descentsearch_lbfgs_ls.txt ../input/thesis_part2/cross/linesearch_unit.txt  -verbose 2 -maxThreads 1 -innerproduct_weight0 1.0 -innerproduct_weight1 3.0 -name inversion_sst2_lbfgs_ls
