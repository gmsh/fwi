#!/bin/bash
#
#SBATCH --job-name=cross_inversion_sst2_lbfgs_trBR
#SBATCH --output=cross_inversion_sst2_lbfgs_trBR.log

#SBATCH --ntasks=1
#SBATCH --time=600:00
#SBATCH --mem-per-cpu=24000

time ./inversion  ../input/thesis_part2/cross/common.txt ../input/thesis_part2/cross/inversion.txt ../input/thesis_part2/cross/innerproduct_sst.txt ../input/thesis_part2/cross/localminimum_trB.txt ../input/thesis_part2/cross/descentsearch_lbfgs_tr.txt -verbose 2 -maxThreads 1 -localminimum_retrospective 1 -innerproduct_weight0 1.0 -innerproduct_weight1 3.0 -name inversion_sst2_lbfgs_trBR
