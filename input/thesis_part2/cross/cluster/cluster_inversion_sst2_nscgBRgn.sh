#!/bin/bash
#
#SBATCH --job-name=cross_inversion_sst2_nscgBRgn
#SBATCH --output=cross_inversion_sst2_nscgBRgn.log

#SBATCH --ntasks=1
#SBATCH --time=2000:00
#SBATCH --mem-per-cpu=24000

time ./inversion  ../input/thesis_part2/cross/common.txt ../input/thesis_part2/cross/inversion.txt ../input/thesis_part2/cross/innerproduct_sst.txt ../input/thesis_part2/cross/localminimum_trB.txt ../input/thesis_part2/cross/descentsearch_nscg.txt -verbose 2 -maxThreads 1 -innerproduct_weight0 1.0 -innerproduct_weight1 3.0 -gaussNewton 1 -localminimum_retrospective 1 -name inversion_sst2_nscgBRgn
