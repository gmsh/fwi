#!/bin/bash
#
#SBATCH --job-name=cross_synthetics
#SBATCH --output=cross_synthetics.log

#SBATCH --ntasks=1
#SBATCH --time=10:00
#SBATCH --mem-per-cpu=24000

time ./synthetics ../input/thesis_part2/cross/common.txt ../input/thesis_part2/cross/synthetics.txt -verbose 2 -maxThreads 1
