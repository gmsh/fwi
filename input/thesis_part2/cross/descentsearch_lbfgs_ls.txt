#Local minimum search
localminimum_maxIteration = 500
localminimum_writeInterval = 5
#
#Descent search
descentsearch = lbfgs
descent_memory = 10
