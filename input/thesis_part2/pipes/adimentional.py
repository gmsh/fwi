#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 14:50:10 2020

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
plt.rcParams['ytick.right'] = plt.rcParams['ytick.labelright'] = True
plt.rcParams['ytick.left'] = plt.rcParams['ytick.labelleft'] = False
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
H = 3.0;
L = 3.0;


fc = np.array([75,90,120,145,180,230,300]) * 1e6
ωc = 2 * np.pi * fc
ϵ0 = 8.85418782*1e-12
μ0 = 4. * np.pi*1e-7
η0 = np.sqrt(μ0/ϵ0)
scaled_ωc = np.sqrt(ωc**2 * ϵ0 * μ0)
scaled_fc = scaled_ωc/2./np.pi

k0 = 2 * np.pi * 0.5

"""
    Model 4
"""
ϵr = np.array([5.2, 3.7, 5.0, 80.0])
σ = np.array([2.8, 2.0, 0.1, 10.]) * 1e-3
σr = σ * η0 / k0

plt.figure(figsize=(2*L,2*L),tight_layout=False);
plt.subplots_adjust(top=0.95,right=0.95,bottom=0.05,left=0.05)
plt.plot(ϵr,σr,'o',color='r')
plt.plot(np.log(ϵr),np.log(σr),'o',color='b')
plt.plot(6/5*np.log(ϵr),4/5*np.log(σr),'x',color='b')
plt.plot(ϵr,np.log(σr),'o',color='g')
plt.xlim([-10,10])
plt.ylim([-10,10])
plt.grid()
