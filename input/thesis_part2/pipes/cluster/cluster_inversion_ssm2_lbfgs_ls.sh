#!/bin/bash
#
#SBATCH --job-name=pipes_inversion_ssm2_lbfgs_ls
#SBATCH --output=pipes_inversion_ssm2_lbfgs_ls.log

#SBATCH --ntasks=1
#SBATCH --time=480:00
#SBATCH --mem-per-cpu=24000

time ./inversion  ../input/thesis_part2/pipes/common.txt ../input/thesis_part2/pipes/inversion.txt ../input/thesis_part2/pipes/innerproduct_ssm.txt ../input/thesis_part2/pipes/localminimum_classic.txt ../input/thesis_part2/pipes/descentsearch_lbfgs_ls.txt ../input/thesis_part2/pipes/linesearch_unit.txt  -verbose 2 -maxThreads 1 -innerproduct_weight0 1.0 -innerproduct_weight1 4.0 -name inversion_ssm2_lbfgs_ls
