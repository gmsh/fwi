#!/bin/bash
#
#SBATCH --job-name=pipes_synthetics
#SBATCH --output=pipes_synthetics.log

#SBATCH --ntasks=1
#SBATCH --time=30:00
#SBATCH --mem-per-cpu=24000

time ./synthetics ../input/thesis_part2/pipes/common.txt ../input/thesis_part2/pipes/synthetics.txt -verbose 2 -maxThreads 1
