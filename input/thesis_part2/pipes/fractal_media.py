#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 14:20:06 2020

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)

L=12.
H=22.
Δp = 0.1

nx = 101
ny = 201
Δx = L/(nx-1)
Δy = H/(ny-1)

wgn = np.random.normal(0, 1, size=[nx,ny])

#SPATIAL DOMAIN
x = np.linspace(0,L,nx)
y = np.linspace(0,H,ny)
[X,Y] = np.meshgrid(x,y)

#FREQUENCY DOMAIN
kx = np.fft.fftfreq(nx,Δx) #no 2π
ky = np.fft.fftfreq(ny,Δy) #no 2π
[KX,KY] = np.meshgrid(kx,ky)

#FILTER
lcx = 1.
lcy = 0.2
β = 2.5
def ffilter(kx,ky):
    kr = np.sqrt((kx*lcx)**2+(ky*lcy)**2)
    kr = np.where(kr==0., np.nan, kr)
    out = 1./kr**(β/2)
    return np.nan_to_num(out)

fft_wgn = np.fft.fft2(wgn)
fft_fgn = np.transpose(np.conj(fft_wgn)) * ffilter(KX,KY) 
#INVERSE TRANSFORM
fgn = np.fft.ifft2(fft_fgn)
fgn = Δp * fgn / np.linalg.norm(fgn) * np.sqrt( nx * ny )

fig = plt.figure()
plt.xlim(0,L)
plt.ylim(0,H)
plt.axis('equal')
plt.pcolormesh(X,Y,np.real(fgn))
plt.colorbar()
plt.show()

filename='DRe_mb1c0.csv'
np.savetxt(filename, [x], header=str(nx)+' '+str(ny), comments='',fmt='%.3f')
with open(filename, "ab") as f:
    np.savetxt(f, [y],fmt='%.3f')
    np.savetxt(f, np.real(fgn),fmt='%.3f')
