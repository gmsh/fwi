#Inner product
innerproduct = sobolev_l2
innerproduct_weight_bnd = 0.
innerproduct_diag_preconditioner_smooth = 1
innerproduct_diag_preconditioner_scale = 1
innerproduct_update = 1
innerproduct_stabilization0 = 8e-3
innerproduct_stabilization1 = 2.5e-5
#innerproduct_weight0 = 1.0
#innerproduct_weight1 = 1.0
n_scale0 = 1
scale0 = 0.0
