name = ip_comparison
#Configuration
m0_scale = 1.0
unknown = all
#
#Objective
objective = l2distance
#
#Discretization
h = 0.1
#Model
innerproduct_integration_degree_blk = 5
innerproduct_integration_degree_bnd = 6
#
model_FunctionSpaceType = HierarchicalH1
model_FunctionSpaceDegree = 1
#
n_group=1
# Group 0
n_freq0=7
frequency00=0
frequency01=1
frequency02=2
frequency03=3
frequency04=4
frequency05=5
frequency06=6
#
#Data
data0 = pipes_synthetics_data0
data1 = pipes_synthetics_data1
data2 = pipes_synthetics_data2
data3 = pipes_synthetics_data3
data4 = pipes_synthetics_data4
data5 = pipes_synthetics_data5
data6 = pipes_synthetics_data6
