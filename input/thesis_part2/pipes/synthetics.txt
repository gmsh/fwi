name = synthetics
#Configuration
unknown = none
#
inclusion_ni = 4
inclusion_filled = 1
inclusion = cylinder
#Inclusion 0
inclusion_x0i0 = 5.
inclusion_y0i0 = 9.
inclusion_ri0 = 1.
Re(mi0c0) = 4.38
Re(mi0c1) = 0.18
Im(mi0c0) = 0.
Im(mi0c1) = 0.
#Inclusion 1
inclusion_x0i1 = 4
inclusion_y0i1 = 14.5
inclusion_ri1 = 0.25
Re(mi1c0) = 4.38
Re(mi1c1) = 0.18
Im(mi1c0) = 0.
Im(mi1c1) = 0.
#Inclusion 2
inclusion_x0i2 = 6
inclusion_y0i2 = 14.5
inclusion_ri2 = 0.25
Re(mi2c0) = 4.38
Re(mi2c1) = 0.18
Im(mi2c0) = 0.
Im(mi2c1) = 0.
#Inclusion 3
inclusion_x0i3 = 8
inclusion_y0i3 = 14.5
inclusion_ri3 = 0.25
Re(mi3c0) = 4.38
Re(mi3c1) = 0.18
Im(mi3c0) = 0.
Im(mi3c1) = 0.
#
#Output
write_data_fields = 0
write_model_field0 = 1
write_single_wave_fields = 3
write_single_wave_field0 = 0
write_single_wave_field1 = 20
write_single_wave_field2 = 40
