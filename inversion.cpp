//GmshFem Library
#include "GmshFem.h"
#include "Post.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::post;

//Standard Library
#include <numeric>

//FWI Library
#include "specific/physic.h"
#include "specific/configuration/newConfiguration.h"
#include "common/wave/equation/equation.h"
#include "specific/model/parametrization/newParametrization.h"
#include "common/model/macro.h"
#include "specific/optimization/globalminimumsearch/newGlobalMinimumSearch.h"

template <Physic T_Physic>
int inversion(const GmshFem& gmshFem)
{
    std::string name = "";
    gmshFem.userDefinedParameter(name, "prename");
    std::string suffix = "noname";
    gmshFem.userDefinedParameter(suffix, "name");
    name += suffix;

    const ParametrizationInterface* const parametrization = newParametrization<T_Physic>(gmshFem);

    auto configuration = newConfiguration(name, parametrization, gmshFem);

    for (unsigned int c = 0; c < parametrization->size(); c++)
    {
        save(configuration->m0()[c], configuration->model_unknown(Support::BLK), name+"_m_blkc"+std::to_string(c), "pos", "");
        save(configuration->m0()[c], configuration->model_unknown(Support::BND), name+"_m_bndc"+std::to_string(c), "pos", "");
    }

    unsigned int n_freq_tot = 0;
    if(!gmshFem.userDefinedParameter(n_freq_tot, "n_freq"))
    {
        throw Exception("Total frequency number could not be found.");
    }
    std::vector<double> frequency(n_freq_tot);
    std::vector<std::string> filename(n_freq_tot);
    for (unsigned int f = 0; f < n_freq_tot; f++)
    {
        std::string suffix = std::to_string(f);
        if(!gmshFem.userDefinedParameter(frequency[f], "frequency"+suffix))
        {
            throw Exception("Frequency #"+suffix+" could not be found.");
        }
        if(!gmshFem.userDefinedParameter(filename[f], "data"+suffix))
        {
            msg::warning << "Data filename #"+suffix+" could not be found." << msg::endl;
            if(!gmshFem.userDefinedParameter(filename[f], "data"))
            {
                throw Exception("Data filename could not be found.");
            }
            filename[f] += suffix;
            msg::warning << "filename #"+suffix+" = "<< filename[f] << msg::endl;
        }
    }
    const Data<T_Physic> d0(filename,frequency,configuration.get());

    unsigned int n_group = 0;
    if(!gmshFem.userDefinedParameter(n_group, "n_group"))
    {
        throw Exception("Number of frequency group could not be found.");
    }


    msg::print << "Inverse data" << msg::endl;
    ModelField m;
    for (unsigned int g = 0; g < n_group; g++)
    {
        std::string suffix_g = std::to_string(g);
        msg::print << "--- Frequency group #" + suffix_g << " --- " << msg::endl;
        msg::indent();

        unsigned int n_freq = 0;
        std::vector<unsigned int> freq_idx;
        if(!gmshFem.userDefinedParameter(n_freq, "n_freq"+suffix_g))
        {
            msg::warning << "Total frequency number for frequency group #" << suffix_g << " could not be found." << msg::endl;
            if(n_group == n_freq_tot)
            {
                n_freq = 1;
                freq_idx.resize(n_freq);
                freq_idx[0] = g;
                msg::warning << "A single frequency is used for frequency group #" << suffix_g << msg::endl;
            }
            else if(n_group == 1)
            {
                n_freq = n_freq_tot;
                freq_idx.resize(n_freq);
                std::iota(freq_idx.begin(), freq_idx.end(), 0);
                msg::warning << "All frequencies are used for frequency group #" << suffix_g << msg::endl;
            }
            else
            {
                throw Exception("Simultaneous inversion of all frequencies requires n_group = 1. Sequential inversion of all frequencies requires n_group = n_freq_tot. Any intermediate case must be specified explicitely");
            }
        }
        else
        {
            freq_idx.resize(n_freq);
            for (unsigned int f = 0; f < n_freq; f++)
            {
                std::string suffix_f = std::to_string(f);
                if(!gmshFem.userDefinedParameter(freq_idx[f], "frequency"+suffix_g+"_"+suffix_f))
                {
                    freq_idx[f] = f;
                    msg::warning << "Frequency index #"+suffix_f+" for group #" + suffix_g + " could not be found. Index = " << f << "(default)." << msg::endl;
                }
                msg::print << "frequency #" + suffix_f << " = " << frequency[freq_idx[f]] << msg::endl;
            }
        }

        std::string integrationType;
        if(!(
        gmshFem.userDefinedParameter(integrationType, "integration_type")
        ||
        gmshFem.userDefinedParameter(integrationType, "integration_type"+suffix_g)
        ))
        {
            throw Exception("Integration type could not be found.");
        }
        model::Discretization m_discret(gmshFem,suffix_g);
        if(g==0)
        {
            double m0_scale=0.;
            if(gmshFem.userDefinedParameter(m0_scale, "m0_scale"))
            {
                m = laplace_filter(configuration->m0(),m0_scale,configuration.get(), m_discret, integrationType);
            }
            else
            {
                m = rediscretize(configuration->m0(), configuration.get(), m_discret, integrationType);
            }
            m.write(name+"_m_initial", "pos", "");
        }
        else
        {
            m = rediscretize(m,configuration.get(), m_discret, integrationType);
        }

        const Data<T_Physic> d0g(freq_idx,d0);
        std::vector<EquationInterface<T_Physic>*> pequation(n_freq);
        for (unsigned int f = 0; f < n_freq; f++)
        {
            std::string suffix_f = std::to_string(freq_idx[f]);

            wave::Discretization<T_Physic> w_discret(gmshFem,suffix_f);
            pequation[f] = new ParametrizedEquation<T_Physic>(parametrization,f,2.*M_PI*frequency[freq_idx[f]],configuration.get(),w_discret,gmshFem,suffix_f);
        }

        GlobalMinimumSearchInterface<T_Physic>* const globalminimumsearch = newGlobalMinimumSearch<T_Physic>(name,configuration.get(), gmshFem, suffix_g);
        (*globalminimumsearch)(&m,d0g,pequation);
        m.write(name+"_mg"+suffix_g, "pos", "");

        globalminimumsearch->history()->write(name+"_globalminimum_historyg" + suffix_g);

        for (unsigned int f = 0; f < n_freq; f++)
        {
            delete pequation[f];
        }


        msg::unindent();
    }

    delete parametrization;
    return 0;
}

int main(int argc, char **argv)
{
  GmshFem gmshFem(argc, argv);
  Physic T_Physic = to_physic(gmshFem);
  switch (T_Physic)
  {
    case Physic::acoustic: default:
      return inversion<Physic::acoustic>(gmshFem);
    case Physic::electromagnetic:
      return inversion<Physic::electromagnetic>(gmshFem);
    case Physic::elastodynamic:
      return inversion<Physic::elastodynamic>(gmshFem);
  }
}
