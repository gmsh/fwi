#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 09:51:40 2020

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':6})
rc('text', usetex=True)
#Value for 1 width figure
H = 1.5;
L = 4.5;

path = '../Presentation2/PS_NCG/'
filename = 'ZP2018Marmousi2D_inversion_ncg'
freqf = np.array([4.,6.,9.])
n_freq = np.size(freqf)
n_scalef = [[]]*n_freq;
jfs = [[]]*n_freq;
n_itfs = [[]]*n_freq;
scalefs = [[]]*n_freq;
for f in range(0,n_freq):
    n_scalef[f] = np.loadtxt(path+filename+'_globalminimum_historyg'+str(f)+'.csv',delimiter=';',max_rows=1,skiprows=3,dtype=np.integer)[1];
    scalefs[f] = np.loadtxt(path+filename+'_globalminimum_historyg'+str(f)+'.csv',delimiter=';',max_rows=np.int(n_scalef[f]),skiprows=5,dtype=np.float)[:,2];
    jfs[f] = [[]]*n_scalef[f];
    n_itfs[f] = [[]]*n_scalef[f];
    line = 3
    for s in range(0,n_scalef[f]):
        n_itfs[f][s] = np.loadtxt(path+filename+'_localminimum_historyg'+str(f)+'.csv',delimiter=';',max_rows=1,skiprows=line,dtype=np.integer)[1];
        line=line+2
        jfs[f][s] = np.loadtxt(path+filename+'_localminimum_historyg'+str(f)+'.csv',delimiter=';',max_rows=np.int(n_itfs[f][s]+1),skiprows=line,dtype=np.float)[:,0];
        line=line+n_itfs[f][s]+1+1

n_it=0
linefs = [[]]*n_freq
f0 = plt.figure(figsize=(L,H),tight_layout=False);
plt.subplots_adjust(top=0.97,right=0.88,bottom=0.15,left=0.12)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
for f in range(0,n_freq):
    linefs[f] = [[]]*n_scalef[f];
    plt.plot(n_it,jfs[f][0][0],color='k',marker='o',zorder=3,markersize=3.)
    for s in range(0,n_scalef[f]):
        linefs[f][s] = plt.plot(range(n_it,n_it+n_itfs[f][s]+1),jfs[f][s],color='lightgray',zorder=2)
        plt.plot(n_it+n_itfs[f][s],jfs[f][s][n_itfs[f][s]],color='k',marker='x',zorder=3,markersize=3.)
        n_it=n_it+n_itfs[f][s]
    plt.grid(zorder=1)
    plt.xlim([0,n_it])
    plt.ylabel(r'$\mathcal{J}_\omega(m_n)$')
    plt.yscale('log')
    plt.legend()


for f in range(0,n_freq):
    freq_str=r'$f=\makebox[6mm][r]{'+str(freqf[f])+'}$ [Hz]'
    for s in range(0,n_scalef[f]):
        linefs[f][s][0].set_color('k')
        scale_str=r'$s=\makebox[6mm][r]{'+str(scalefs[f][s])+'}$ [km]'
        linefs[f][s][0].set_label(freq_str+'\n'+scale_str)
        plt.legend()
        plt.pause(0.5)
        plt.savefig(filename+'_plotg'+str(f)+'s'+str(s)+'.eps')
        linefs[f][s][0].set_color('lightgray')
        linefs[f][s][0].set_label('')
