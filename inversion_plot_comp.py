#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 16:43:19 2021

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':6})
rc('text', usetex=True)
#Value for 1 width figure
H = 1.5;
L = 4.5;

n_comp = 3
path = ['EisenstatChoiceA/','YuanFanChoiceA/','YuanFanChoiceB/']
filename = ['paper2_inversion_ncg','paper2_inversion_nscg','paper2_inversion_nscgB']
glob = ['linesearch','trust-region','trust-region']
order = [ 2 , 2, 2 ]
col = ['k','k', 'k']
lin = [':','--','-']
max_forward = 300

freqf = np.array([4.,6.,9.])
n_freq = 2#np.size(freqf)
jcfs = []
n_itcf = []
n_scalecf = []
n_itcfs = []

for i in range(0,n_comp):
    n_scalef = [[]]*n_freq; 
    jfs = [[]]*n_freq;
    n_itfs = [[]]*n_freq;
    n_itf = [0]*n_freq
    scalefs = [[]]*n_freq;
    for f in range(0,n_freq):
        n_scalef[f] = np.loadtxt(path[i]+filename[i]+'_globalminimum_historyg'+str(f)+'.csv',delimiter=';',max_rows=1,skiprows=3,dtype=np.integer)[1];
        scalefs[f] = np.loadtxt(path[i]+filename[i]+'_globalminimum_historyg'+str(f)+'.csv',delimiter=';',usecols=2,max_rows=np.int(n_scalef[f]),skiprows=6,dtype=np.float);
        jfs[f] = [[]]*n_scalef[f];
        n_itfs[f] = np.zeros(n_scalef[f],np.integer);
        line = 3 
        for s in range(0,n_scalef[f]):        
            n_itfs[f][s] = np.loadtxt(path[i]+filename[i]+'_localminimum_historyg'+str(f)+'.csv',delimiter=';',usecols = 1, max_rows=1,skiprows=line,dtype=np.integer);
            n_itf[f] = n_itf[f]+n_itfs[f][s]
            line=line+2
            jfs[f][s] = np.loadtxt(path[i]+filename[i]+'_localminimum_historyg'+str(f)+'.csv',delimiter=';',max_rows=np.int(n_itfs[f][s]+1),skiprows=line,dtype=np.float)[:,0];
            line=line+n_itfs[f][s]+1+1
    jcfs.append(jfs)
    n_itcf.append(n_itf)
    n_itcfs.append(n_itfs)
    n_scalecf.append(n_scalef)

it_linecf = [[]*n_freq]*n_comp;
it_descentcf = [[]*n_freq]*n_comp; 
n_forwardcf = [[]*n_freq]*n_comp;
for i in range(0,n_comp):
    it_linef = [[]]*n_freq
    it_descentf = [[]]*n_freq
    n_forwardf = [[]]*n_freq
    for f in range(0,n_freq):
        n_forwardf[f] = np.zeros(n_itcf[i][f],np.integer)
        if glob[i] == 'linesearch': 
            it_linef[f] = np.loadtxt(path[i]+filename[i]+'_linesearch_historyg'+str(f)+'.csv',delimiter=';',usecols = 5, skiprows=1,dtype=np.integer)
            n_forwardf[f] += it_linef[f] 
        if order[i] == 2:
            it_descentf[f] = np.zeros(n_itcf[i][f],np.integer)            
            line = 3 
            for n in range(0,n_itcf[i][f]):        
                it_descentf[f][n] = np.loadtxt(path[i]+filename[i]+'_descent_historyg'+str(f)+'.csv',delimiter=';',usecols=3,max_rows=1,skiprows=line,dtype=np.integer);
                line=line+it_descentf[f][n]+3
            n_forwardf[f] += 2*it_descentf[f]
        n_forwardf[f] += 2
        n_forwardf[f] = np.cumsum(n_forwardf[f])
        n_forwardf[f] = np.insert(n_forwardf[f],0,0)
    n_forwardcf[i] = n_forwardf    
    it_descentcf[i] = it_descentf
    it_linecf[i] = it_linef
        
fig = [[]]*n_freq
for f in range(0,n_freq):
    fig[f]=plt.figure(figsize=(L,H),tight_layout=False);
    plt.subplots_adjust(top=0.95,right=0.98,bottom=0.14,left=0.08)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.grid(zorder=0)
    plt.xlim([0,max_forward])
    plt.ylim([1e-4,10])
    plt.yscale('log')
    for i in range(0,n_comp):
        n_it=0
        lines = [[]]*n_scalecf[i][f]
        plt.plot(n_forwardcf[i][f][n_it],jcfs[i][f][0][0],color=col[i],marker='o',zorder=3,markersize=3.)
        for s in range(0,n_scalecf[i][f]):
            lines[s] = plt.plot(n_forwardcf[i][f][range(n_it,n_it+n_itcfs[i][f][s]+1)],jcfs[i][f][s][0:n_itcfs[i][f][s]+1],color=col[i],linestyle=lin[i],zorder=2)
            plt.plot(n_forwardcf[i][f][n_it+n_itcfs[i][f][s]],jcfs[i][f][s][n_itcfs[i][f][s]],color=col[i],marker='x',zorder=3,markersize=3.)
            n_it=n_it+n_itcfs[i][f][s]
        n_it = 0


f1,axs =plt.subplots(1,n_freq,figsize=(L,H),tight_layout=False);
plt.subplots_adjust(top=0.95,right=0.98,bottom=0.14,left=0.08)
for f in range(0,n_freq):
    if f != 0:
        plt.setp(axs[f].get_yticklabels(), visible=False)
    axs[f].grid(zorder=0)
    axs[f].set_xlim([0,max_forward])
    axs[f].set_ylim([1e-4,10])
    axs[f].set_yscale('log')
    for i in range(0,n_comp):
        n_it=0
        lines = [[]]*n_scalecf[i][f]
        axs[f].plot(n_forwardcf[i][f][n_it],jcfs[i][f][0][0],color=col[i],marker='o',zorder=3,markersize=3.)
        for s in range(0,n_scalecf[i][f]):
            lines[s] = axs[f].plot(n_forwardcf[i][f][range(n_it,n_it+n_itcfs[i][f][s]+1)],jcfs[i][f][s][0:n_itcfs[i][f][s]+1],color=col[i],linestyle=lin[i],zorder=2)
            axs[f].plot(n_forwardcf[i][f][n_it+n_itcfs[i][f][s]],jcfs[i][f][s][n_itcfs[i][f][s]],color=col[i],marker='x',zorder=3,markersize=3.)
            n_it=n_it+n_itcfs[i][f][s]
        n_it = 0 
        

f0 = plt.figure(figsize=(L,H),tight_layout=False);
plt.subplots_adjust(top=0.97,right=0.88,bottom=0.15,left=0.12)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.grid(zorder=0)
plt.xlim([0,max_it*n_freq])
plt.ylabel(r'$\mathcal{J}_\omega(m_n)$')
plt.yscale('log')
plt.legend()
t = np.arange(0,max_it*n_freq+2,3)
plt.xticks(ticks=t,labels=t%(max_it+1))
for i in range(0,n_comp):
    n_it=0
    linefs = [[]]*n_freq
    for f in range(0,n_freq):
        linefs[f] = [[]]*n_scalecf[i][f];
        plt.plot(n_it,jcfs[i][f][0][0],color=col[i],marker='o',zorder=3,markersize=3.)
        for s in range(0,n_scalecf[i][f]):
            linefs[f][s] = plt.plot(range(n_it,n_it+n_itcfs[i][f][s]+1),jcfs[i][f][s],color=col[i],zorder=2)
            plt.plot(n_it+n_itcfs[i][f][s],jcfs[i][f][s][n_itcfs[i][f][s]],color=col[i],marker='x',zorder=3,markersize=3.)
            n_it=n_it+n_itcfs[i][f][s]
        n_it = max_it+1
        plt.vlines(n_it-0.5,1e-3,10)


f1= plt.figure(figsize=(L,H),tight_layout=False);
plt.subplots_adjust(top=0.97,right=0.88,bottom=0.15,left=0.12)
plt.grid(zorder=0)
plt.ylim([-10,35])
ax1 = plt.gca()
ax1.set_ylabel(r'LS it. \hspace{1cm} CG it.',va='bottom')
t = np.arange(0,max_it*n_freq,3)
plt.xticks(ticks=t,labels=t%(max_it+1))
uwd=0.6
wd = uwd/n_comp
for i in range(0,n_comp):
    if order[i] == 2:
        n_it=0
        for f in range(0,n_freq):
            plt.bar(np.arange(n_it,n_it+n_itcf[i][f])+(i-0.5)*wd,it_descentcf[i][f],zorder=5,width=wd,color=col[i])
            if glob[i] == 'linesearch':
                plt.bar(np.arange(n_it,n_it+n_itcf[i][f])+(i-0.5)*wd,-it_linecf[i][f],zorder=5,width=wd,color=col[i])
            n_it += max_it+1
            plt.vlines(n_it-0.5,-20,35)    
