#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 11 15:49:20 2022

@author: xavier
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
plt.rcParams['ytick.right'] = plt.rcParams['ytick.labelright'] = False
plt.rcParams['ytick.left'] = plt.rcParams['ytick.labelleft'] = True
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb} \renewcommand{\familydefault}{\sfdefault} \usepackage{sansmathfonts}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
lw = 1.5
ms=5
H = 2.0;
L = 3.0;

"""
#Mamrousi early stop regularization
j0 = 1
jmrzv = 21.06701972868763
n_comp = 3
prepath = [''] * 3 
best = [17,26,43]
mrz = [11,14,22]
path = ['SST_LBFGS_LS/','SSM_LBFGS_LS/','DF_LBFGS_LS/',]
filename = ['paper2_extended_sst_lbfgs_ls_snr2','paper2_extended_ssm_lbfgs_ls_snr2','paper2_extended_df3_lbfgs_ls_snr2']
lab = ['SST','SSM','DF']
glob = ['linesearch'] * 3
order = [1.5] * 3
col = ['C0','C1','C2'] 
mrk = [''] * 3
lin = ['-'] * 3
n_freq = 1
"""

"""
#Mamrousi additive regularization
j0 = 1. #35.6633
jmrzv = 17.303269765664275
n_comp = 3
prepath = [''] * 3 
best = [0] * 3
mrz = [ 0 ] * 3
ms=0
path = ['SST_LBFGS_LS_SNR3/','TIK/SST_LBFGS_LS_SNR3_TIK10/','TV/SST_LBFGS_LS_SNR3_TV11/']
filename = ['paper2_extended_sst_lbfgs_ls_snr3','paper2_extended_sst_lbfgs_ls_snr3_tik10','paper2_extended_sst_lbfgs_ls_snr3_tv11']
lab = ['SST','TIK','TV']
glob = ['linesearch'] * 3
order = [1.5] * 3
col = ['C0','C1','C2'] 
mrk = [''] * 3
lin = ['-'] * 3
n_freq = 1
"""

"""
#Mamrousi df
j0 = 1. #35.6633
jmrzv = 21.06701972868763
n_comp = 7
best = [ 22, 21, 30, 43, 61, 119,  69 ]
mrz = [ 12, 12, 15, 22, 34, 62,  110 ]
prepath = [''] * 7 
path = ['DF_LBFGS_LS/'] * 7
filename = ['paper2_extended_df1_lbfgs_ls_snr2','paper2_extended_df_lbfgs_ls_snr2','paper2_extended_df2_lbfgs_ls_snr2','paper2_extended_df3_lbfgs_ls_snr2','paper2_extended_df4_lbfgs_ls_snr2','paper2_extended_df5_lbfgs_ls_snr2','paper2_extended_df6_lbfgs_ls_snr2']
lab = ['DF1','DF','DF2','DF3','DF4','DF5','DF6']
glob = ['linesearch'] * 7
order = [1.5] * 7
col = ['#061606','#124212','#1C661C','#2ca02c','#78C278','#BAE0BA','#E3F2E3'] 
mrk = [''] * 7
lin = ['-'] * 7
n_freq = 1
β = 1.
A = 25.994976
"""

"""
#Mamrousi ssm
j0 = 1. #35.6633
jmrzv = 21.06701972868763
n_comp = 7
best = [21, 26, 36, 59, 48, 82, 0]
mrz = [12, 14, 20, 33, 57, np.nan, np.nan]
prepath = [''] * n_comp
path = ['SSM_LBFGS_LS/'] * n_comp
filename = ['paper2_extended_ssm1_lbfgs_ls_snr2','paper2_extended_ssm_lbfgs_ls_snr2','paper2_extended_ssm2_lbfgs_ls_snr2','paper2_extended_ssm5_lbfgs_ls_snr2','paper2_extended_ssm6_lbfgs_ls_snr2','paper2_extended_ssm7_lbfgs_ls_snr2','paper2_extended_ssm8_lbfgs_ls_snr2']
lab = ['ssm1','ssm','ssm2','ssm5','ssm6','ssm7','ssm8']
glob = ['linesearch'] * n_comp
order = [1.5] * n_comp
col = ['#492200','#974700','#d26300','#ff8922','#ff9e49','#ffbe84','#ffd2ab'] 
mrk = [''] * n_comp
lin = ['-'] * n_comp
n_freq = 1
β = 0.075
A = 25.994976
"""

"""
#Concrete 3 additive regularization
j0 = 1. 
jmrzv = 5608.4871201510405
n_comp = 5
prepath = [''] * 5 
best = [0] * 5
mrz = [ 0 ] * 5
ms=0
path = [ 'snrm3/SST_LBFGS_LS_TV_snrm3/' ] * 5
filename = ['paper2_concrete3_sst_lbfgs_ls_snrm3_tv0','paper2_concrete3_sst_lbfgs_ls_snrm3_tv1','paper2_concrete3_sst_lbfgs_ls_snrm3_tv2','paper2_concrete3_sst_lbfgs_ls_snrm3_tv3','paper2_concrete3_sst_lbfgs_ls_snrm3_tv4']
lab = ['TV0','TV1','TV2','TV3','TV4']
glob = ['linesearch'] * 5
order = [1.5] * 5
col = ['C0','C1','C2','C3','C4'] 
mrk = [''] * 5
lin = ['-'] * 5
n_freq = 1
"""

"""
#Concrete 3 early stop regularization
j0 = 1.
jmrzv = 11177.374443381555

n_comp = 2;
best = [226,247]
mrz = [40,51]
col = ['C1','C2'] 

prepath = [''] * n_comp
path = [ 'SSM_LBFGS_LS/' , 'DF_LBFGS_LS/' ] * 2
filename = [ 'paper2_concrete3_ssm2_lbfgs_ls_snrm3', 'paper2_concrete3_df4_lbfgs_ls_snrm3' ]
lab = [ 'SSM2','DF4' ]
glob = ['linesearch'] * n_comp
order = [1.5] * n_comp

mrk = [''] * n_comp
lin = ['-'] * n_comp
n_freq = 1
"""

"""
#Concrete 3 df or ssm
j0 = 1.
jmrzv = 11177.374443381555

#n_comp = 6; β = 1.; A = 0.00013104; substr = 'df'
#best = [200,141,193,234,247,250]
#mrz = [25,26,34,42,51,56]
#col = ['#061606','#124212','#1C661C','#2ca02c','#78C278','#BAE0BA','#E3F2E3'] 

n_comp = 6; β = 300.; A = 0.00013104; substr = 'ssm'
best = [150,193,226,250,250,250]
mrz = [28,33,40,50,59,70]
col = ['#492200','#974700','#d26300','#ff8922','#ff9e49','#ffbe84','#ffd2ab'] 

prepath = [''] * n_comp
path = [ substr.upper()+'_LBFGS_LS/' ] * n_comp
filename = ['paper2_concrete3_'+substr.lower()+'0_lbfgs_ls_snrm3','paper2_concrete3_'+substr.lower()+'1_lbfgs_ls_snrm3','paper2_concrete3_'+substr.lower()+'2_lbfgs_ls_snrm3','paper2_concrete3_'+substr.lower()+'3_lbfgs_ls_snrm3','paper2_concrete3_'+substr.lower()+'4_lbfgs_ls_snrm3','paper2_concrete3_'+substr.lower()+'5_lbfgs_ls_snrm3']
lab = ['DF0','DF1','DF2','DF3','DF4','DF5']
glob = ['linesearch'] * n_comp
order = [1.5] * n_comp

mrk = [''] * n_comp
lin = ['-'] * n_comp
n_freq = 1
"""

#Cross
j0 = 1.
jmrzv = 0.6931585837012904

n_comp = 4; β = 1.; A = 1.;
best = [0] * n_comp
mrz = [0] * n_comp
ms = 0
col = ['C1','C2','C1','C2'] 
     
prepath = [''] * n_comp
path = [ 'SST_LBFGS_LS_TIK/', 'SST_LBFGS_LS_TV/', 'SSM_LBFGS_LS/', 'DF_LBFGS_LS/' ]
filename = [ 'cross_sst2_lbfgs_ls_snr6_tik22', 'cross_sst2_lbfgs_ls_snr6_tv24', 'cross_ssm1_lbfgs_ls_snr6', 'cross_df2_lbfgs_ls_snr6' ]
lab = [ 'TIK','TV','SSM','DF' ]
glob = ['linesearch'] * n_comp
order = [1.5] * n_comp

mrk = [''] * n_comp
lin = [ '--','--','-','-' ]
n_freq = 1


"""
LOAD
"""
jcfs = []
mcfs = []
p_ratiocfs = []
r_ratiocfs = []
n_itcf = []
n_scalecf = []
n_itcfs = []

for i in range(0,n_comp):
    path_tmp = prepath[i]+path[i]
    n_scalef = [[]]*n_freq; 
    jfs = [[]]*n_freq;
    mfs = [[]]*n_freq;
    p_ratiofs = [[]]*n_freq;
    r_ratiofs = [[]]*n_freq;
    n_itfs = [[]]*n_freq;
    n_itf = [0]*n_freq
    scalefs = [[]]*n_freq;
    for f in range(0,n_freq):
        n_scalef[f] = np.loadtxt(path_tmp+filename[i]+'_globalminimum_historyg'+str(f)+'.csv',delimiter=';',max_rows=1,skiprows=3,dtype=np.integer)[1];
        scalefs[f] = np.loadtxt(path_tmp+filename[i]+'_globalminimum_historyg'+str(f)+'.csv',delimiter=';',usecols=2,max_rows=np.int(n_scalef[f]),skiprows=6,dtype=np.float);
        jfs[f] = [[]]*n_scalef[f];
        mfs[f] = [[]]*n_scalef[f];
        p_ratiofs[f] = [[]]*n_scalef[f];
        r_ratiofs[f] = [[]]*n_scalef[f];
        n_itfs[f] = np.zeros(n_scalef[f],np.integer);
        line = 3 
        for s in range(0,n_scalef[f]):        
            n_itfs[f][s] = np.loadtxt(path_tmp+filename[i]+'_localminimum_historyg'+str(f)+'.csv',delimiter=';',usecols = 1, max_rows=1,skiprows=line,dtype=np.integer);
            n_itf[f] = n_itf[f]+n_itfs[f][s]
            line=line+2
            jfs[f][s] = np.loadtxt(path_tmp+filename[i]+'_localminimum_historyg'+str(f)+'.csv',delimiter=';',max_rows=np.int(n_itfs[f][s]+1),skiprows=line,dtype=np.float)[:,0] / j0;
            mfs[f][s] = np.loadtxt(path_tmp+filename[i]+'_localminimum_historyg'+str(f)+'.csv',delimiter=';',max_rows=np.int(n_itfs[f][s]+1),skiprows=line,dtype=np.float)[:,4];
            if glob[i] == 'trust-region' or glob[i] == 'trust-region-retro':
                p_ratiofs[f][s] = np.loadtxt(path_tmp+filename[i]+'_localminimum_historyg'+str(f)+'.csv',delimiter=';',max_rows=np.int(n_itfs[f][s]+1),skiprows=line,dtype=np.float)[:,6];
                r_ratiofs[f][s] = np.loadtxt(path_tmp+filename[i]+'_localminimum_historyg'+str(f)+'.csv',delimiter=';',max_rows=np.int(n_itfs[f][s]+1),skiprows=line,dtype=np.float)[:,7];
                r_ratiofs[f][s][p_ratiofs[f][s] < 0] = np.nan;
            line=line+n_itfs[f][s]+1+1
    jcfs.append(jfs)
    mcfs.append(mfs)
    p_ratiocfs.append(p_ratiofs)
    r_ratiocfs.append(r_ratiofs)
    n_itcf.append(n_itf)
    n_itcfs.append(n_itfs)
    n_scalecf.append(n_scalef)

it_linecf = [[]*n_freq]*n_comp;
it_descentcf = [[]*n_freq]*n_comp;
bnd_descentcf = [[]*n_freq]*n_comp; 
n_forwardcf = [[]*n_freq]*n_comp;
successcf = [[]*n_freq]*n_comp;
negativecf = [[]*n_freq]*n_comp;
etacf = [[]*n_freq]*n_comp;
for i in range(0,n_comp):
    path_tmp = prepath[i]+path[i]
    it_linef = [[]]*n_freq
    it_descentf = [[]]*n_freq
    bnd_descentf = [[]]*n_freq
    n_forwardf = [[]]*n_freq
    successf = [[]]*n_freq
    negativef = [[]]*n_freq
    etaf = [[]]*n_freq
    for f in range(0,n_freq):
        n_forwardf[f] = np.zeros(n_itcf[i][f],np.integer)

        if glob[i] == 'linesearch': 
            it_linef[f] = np.loadtxt(path_tmp+filename[i]+'_linesearch_historyg'+str(f)+'.csv',delimiter=';',usecols = 5, skiprows=1,dtype=np.integer)
            n_forwardf[f] += it_linef[f] 

        if order[i] == 1.5 and (glob[i] == 'trust-region' or glob[i] == 'trust-region-retro') :
            successf[f] = np.zeros(n_itcf[i][f],np.integer)
            bnd_descentf[f] = np.zeros(n_itcf[i][f],np.integer)
            line = 2
            for n in range(0,n_itcf[i][f]):
                bnd_descentf[f][n] = np.loadtxt(path_tmp+filename[i]+'_descent_historyg'+str(f)+'.csv',delimiter=';',usecols=4,max_rows=1,skiprows=line,dtype=np.integer);            
                successf[f][n] = np.loadtxt(path_tmp+filename[i]+'_descent_historyg'+str(f)+'.csv',delimiter=';',usecols=0,max_rows=1,skiprows=line,dtype=np.integer);
                line=line+2        
        if order[i] == 2:
            it_descentf[f] = np.zeros(n_itcf[i][f],np.integer)
            successf[f] = np.zeros(n_itcf[i][f],np.integer)
            etaf[f] = np.zeros(n_itcf[i][f],np.float)
            negativef[f] = np.zeros(n_itcf[i][f],np.integer)
            bnd_descentf[f] = np.zeros(n_itcf[i][f],np.integer)
            line = 3 
            for n in range(0,n_itcf[i][f]):        
                it_descentf[f][n] = np.loadtxt(path_tmp+filename[i]+'_descent_historyg'+str(f)+'.csv',delimiter=';',usecols=3,max_rows=1,skiprows=line,dtype=np.integer);
                if glob[i] == 'trust-region' or glob[i] == 'trust-region-retro':
                    bnd_descentf[f][n] = np.loadtxt(path_tmp+filename[i]+'_descent_historyg'+str(f)+'.csv',delimiter=';',usecols=6,max_rows=1,skiprows=line,dtype=np.integer);
                etaf[f][n] = np.loadtxt(path_tmp+filename[i]+'_descent_historyg'+str(f)+'.csv',delimiter=';',usecols=1,max_rows=1,skiprows=line,dtype=np.float);            
                successf[f][n] = np.loadtxt(path_tmp+filename[i]+'_descent_historyg'+str(f)+'.csv',delimiter=';',usecols=0,max_rows=1,skiprows=line,dtype=np.integer);
                line=line+it_descentf[f][n]+1
                negativef[f][n] = (np.loadtxt(path_tmp+filename[i]+'_descent_historyg'+str(f)+'.csv',delimiter=';',usecols=1,max_rows=1,skiprows=line,dtype=np.float)<0);
                line=line+2
            n_forwardf[f] += 2*it_descentf[f]
            if glob[i] == 'trust-region-retro':
                n_forwardf[f] += 2 #retrospective Hessian-vector product
        n_forwardf[f] += 2
        n_forwardf[f] = np.cumsum(n_forwardf[f])
        n_forwardf[f] = np.insert(n_forwardf[f],0,0)
    n_forwardcf[i] = n_forwardf    
    it_descentcf[i] = it_descentf
    bnd_descentcf[i] = bnd_descentf
    it_linecf[i] = it_linef
    successcf[i] = successf
    etacf[i] = etaf
    negativecf[i] = negativef

"""
DATA CONVERGENCE PLOT
"""
fig = [[]]*n_freq
for f in range(0,n_freq):
    plt.figure(figsize=(2*L,H),tight_layout=False);
    plt.subplots_adjust(top=0.92,right=0.92,bottom=0.13,left=0.08)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.grid(zorder=0)

    plt.xlim([0,250])
    plt.xticks(np.arange(0,251,50))
    """    
    plt.xlim([0,200])
    plt.xticks(np.arange(0,201,40))
    """
    """
    plt.xlim([5,50])
    plt.xticks(np.arange(0,51,10))
    """
    #plt.ylim([0.95,1.1])
    plt.ylim([0.99,1.02])
    """
    plt.xlim([0,30])
    plt.xticks(np.arange(0,31,10))
    plt.ylim([0.95,1.5])
    """
    
    for i in range(0,n_comp,1):
        plt.plot(jcfs[i][f][0]/jmrzv,color=col[i],marker=mrk[i],linestyle=lin[i],zorder=3,markersize=3, linewidth=lw,label=lab[i])
        plt.plot(best[i],jcfs[i][f][0][best[i]]/jmrzv,color=col[i],marker='o',zorder=3,markersize=ms)
        if ~np.isnan(mrz[i]): 
            plt.plot(mrz[i],jcfs[i][f][0][mrz[i]]/jmrzv,color=col[i],marker='d',zorder=3,markersize=ms)
    #plt.legend()
    plt.hlines(1.0, 0, 1e4, colors='k', linestyles='dashed', zorder=2, linewidth=lw/2.)
    #plt.savefig('convergence_comp'+str(f)+'.eps')
    
"""
MODEL CONVERGENCE PLOT
"""
errorc = []
for i in range(0,n_comp):
    error = np.loadtxt(prepath[i]+path[i]+filename[i]+'_error.csv',delimiter=';')[:,0];
    error = error + np.loadtxt(prepath[i]+path[i]+filename[i]+'_error.csv',delimiter=';')[:,2]; 
    error = error / 2       
    errorc.append(error)

#error
plt.figure(figsize=(2*L,H),tight_layout=False);
plt.subplots_adjust(top=0.92,right=0.92,bottom=0.13,left=0.08)
#plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.grid(zorder=0)



#plt.ylim([0.1,0.25])
#plt.ylim([0.,6.])
plt.ylim([0.5,1.4])
plt.yticks(np.arange(0.5,1.4,0.25))

"""
plt.xlim([5,50])
plt.xticks(np.arange(0,51,10))
"""
plt.xlim([0,200])
plt.xticks(np.arange(0,201,40))
skip=10
"""
plt.xlim([0,250])
plt.xticks(np.arange(0,251,50))
"""

plt.grid(zorder=0)
for i in (0,2,3):
    #np.arange(0,251,skip),
    plt.plot(np.arange(0,201,skip),errorc[i],color=col[i],linestyle=lin[i],zorder=2,label=lab[i],marker=mrk[i])
    plt.plot(best[i],errorc[i][best[i]],color=col[i],marker='o',zorder=3,markersize=ms)
    if ~np.isnan(mrz[i]): 
        plt.plot(mrz[i],errorc[i][mrz[i]],color=col[i],marker='d',zorder=3,markersize=ms)
"""
    plt.plot(jcfs[i][0][0],errorc,color=col[i],linestyle=lin[i],zorder=2,label=lab[i])
plt.xscale('log')
plt.gca().invert_xaxis()
"""
#plt.legend()

#L-curve
plt.figure(figsize=(2*L,H),tight_layout=False);
plt.subplots_adjust(top=0.92,right=0.92,bottom=0.13,left=0.08)
for i in range(0,n_comp):
    plt.plot( (mcfs[i][0][0])/A/β**2, jcfs[i][0][0]/jmrzv, color=col[i],linestyle=lin[i],zorder=2)
    plt.plot( (mcfs[i][0][0][best[i]])/A/β**2, (jcfs[i][0][0][best[i]]/jmrzv), linestyle=lin[i],color=col[i],marker='o',zorder=3,markersize=ms)
    if ~np.isnan(mrz[i]): 
        plt.plot( (mcfs[i][0][0][mrz[i]])/A/β**2,(jcfs[i][0][0][mrz[i]]/jmrzv), color=col[i],marker='d',zorder=3,markersize=ms)
    plt.hlines(1.0, 0, 1e4, colors='k', linestyles='dashed', zorder=2, linewidth=lw/2.)

plt.ylim([0.95,1.1])
#plt.xlim([0.,10.])
plt.xlim([0.,100.])
#plt.xlim([0.,30.])
#plt.xlim([15.,120.])
#plt.ylim([0.99,1.02])
plt.grid()