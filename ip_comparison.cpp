//Standard Library
#include <numeric>

//GmshFem Library
#include "GmshFem.h"
#include "Post.h"
#include "Formulation.h"
using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::post;

//GmshFWI Library
#include "specific/physic.h"
#include "specific/configuration/newConfiguration.h"
#include "common/wave/equation/equation.h"
#include "specific/data/objective/newObjective.h"
#include "specific/model/innerproduct/newInnerProduct.h"
#include "specific/model/regularization/newRegularization.h"
#include "common/statefunctional.h"
#include "specific/model/parametrization/newParametrization.h"
#include "common/model/macro.h"

template<Physic T_Physic>
int ip_comparison(const GmshFem& gmshFem)
{
    std::string name = "";
    gmshFem.userDefinedParameter(name, "prename");
    std::string suffix = "noname";
    gmshFem.userDefinedParameter(suffix, "name");
    name += suffix;

    const ParametrizationInterface* const parametrization = newParametrization<T_Physic>(gmshFem);

    auto configuration = newConfiguration(name, parametrization, gmshFem);

    std::string integrationType;
    gmshFem.userDefinedParameter(integrationType, "integration_type");

    model::Discretization m_discret(gmshFem);

    double filter_scale = 0.;
    gmshFem.userDefinedParameter(filter_scale, "m0_scale");

    unsigned int n_freq_tot = 0;
    if(!gmshFem.userDefinedParameter(n_freq_tot, "n_freq"))
    {
        throw Exception("Total frequency number could not be found.");
    }
    std::vector<double> frequency(n_freq_tot);
    std::vector<std::string> filename(n_freq_tot);
    for (unsigned int f = 0; f < n_freq_tot; f++)
    {
        std::string suffix = std::to_string(f);
        if(!gmshFem.userDefinedParameter(frequency[f], "frequency"+suffix))
        {
            throw Exception("Frequency #"+suffix+" could not be found.");
        }
        if(!gmshFem.userDefinedParameter(filename[f], "data"+suffix))
        {
            msg::warning << "Data filename #"+suffix+" could not be found." << msg::endl;
            if(!gmshFem.userDefinedParameter(filename[f], "data"))
            {
                throw Exception("Data filename could not be found.");
            }
            filename[f] += suffix;
            msg::warning << "filename #"+suffix+" = "<< filename[f] << msg::endl;
        }
    }
    const Data<T_Physic> d0(filename,frequency,configuration.get());

    unsigned int n_group = 0;
    if(!gmshFem.userDefinedParameter(n_group, "n_group"))
    {
        throw Exception("Number of frequency groups could not be found.");
    }

    for (unsigned int g = 0; g < n_group; g++)
    {
        std::string suffix_g = std::to_string(g);
        msg::print << "--- Frequency group #" + suffix_g << " --- " << msg::endl;
        msg::indent();

        unsigned int n_freq = 0;
        std::vector<unsigned int> freq_idx;
        if(!gmshFem.userDefinedParameter(n_freq, "n_freq"+suffix_g))
        {
            msg::warning << "Total frequency number for frequency group #" << suffix_g << " could not be found." << msg::endl;
            if(n_group == n_freq_tot)
            {
                n_freq = 1;
                freq_idx.resize(n_freq);
                freq_idx[0] = g;
                msg::warning << "A single frequency is used for frequency group #" << suffix_g << msg::endl;
            }
            else if(n_group == 1)
            {
                n_freq = n_freq_tot;
                freq_idx.resize(n_freq);
                std::iota(freq_idx.begin(), freq_idx.end(), 0);
                msg::warning << "All frequencies are used for frequency group #" << suffix_g << msg::endl;
            }
            else
            {
                throw Exception("Simultaneous inversion of all frequencies requires n_group = 1. Sequential inversion of all frequencies requires n_group = n_freq_tot. Any intermediate case must be specified explicitely");
            }
        }
        else
        {
            freq_idx.resize(n_freq);
            for (unsigned int f = 0; f < n_freq; f++)
            {
                std::string suffix_f = std::to_string(f);
                if(!gmshFem.userDefinedParameter(freq_idx[f], "frequency"+suffix_g+suffix_f))
                {
                    throw Exception("Frequency index #"+suffix_f+" for group #" + suffix_f + " could not be found.");
                }
                msg::print << "frequency #" + suffix_f << " = " << frequency[freq_idx[f]] << msg::endl;
            }
        }
        const Data<T_Physic> d0g(freq_idx,d0);

        msg::print << "Perform comparison" << msg::endl;
        msg::indent();

        ModelField m = laplace_filter(configuration.get()->m0(),filter_scale,configuration.get(),m_discret,integrationType);
        m.write(name+"_m_smooth"+suffix_g);

        auto objective = newObjective<T_Physic>(d0g,configuration.get(),gmshFem);

        std::vector<EquationInterface<T_Physic>*> pequation(n_freq);
        std::vector<double> subfrequency(n_freq);
        for (unsigned int f = 0; f < n_freq; f++)
        {
            std::string suffix_f = std::to_string(freq_idx[f]);
            subfrequency[f] = frequency[freq_idx[f]];

            wave::Discretization<T_Physic> w_discret(gmshFem,suffix_f);
            pequation[f] = new ParametrizedEquation<T_Physic>(parametrization,f,2.*M_PI*subfrequency[f],configuration.get(),w_discret,gmshFem,suffix_f);
        }

        auto innerproduct = newInnerProduct(configuration.get(),m_discret,gmshFem,suffix_g);
        RegularizationInterface* const regularization = newRegularization(configuration.get(),m,gmshFem,suffix_g);
        StateFunctional<T_Physic>* const statefunctional = new StateFunctional<T_Physic>(configuration.get(),innerproduct.get(),regularization,subfrequency,pequation,objective.get());
        statefunctional->setModel(m);
        double j = statefunctional->performance();
        msg::print << "performance = " << j << msg::endl;

        msg::print << "Compute sensitivity" << msg::endl;
        msg::indent();
        for (unsigned int c = 0; c < parametrization->size(); c++)
        {
            std::string suffix_c = "c"+std::to_string(c);
            save(statefunctional->sensitivity(Order::FST,Support::BLK)[c], configuration->model_unknown(Support::BLK), name+"_sensi"+suffix_g+suffix_c, "pos", "");
        }
        msg::unindent();

        unsigned int diag_preconditioner_scale_buf = 0;
        gmshFem.userDefinedParameter(diag_preconditioner_scale_buf, "innerproduct_diag_preconditioner_scale");
        bool diag_preconditioner_scale = ((bool) diag_preconditioner_scale_buf);

        unsigned int diag_preconditioner_smooth_buf = 0;
        gmshFem.userDefinedParameter(diag_preconditioner_smooth_buf, "innerproduct_diag_preconditioner_smooth");
        bool diag_preconditioner_smooth = ((bool) diag_preconditioner_smooth_buf);

        if(diag_preconditioner_scale || diag_preconditioner_smooth)
        {
            msg::print << "Compute preconditioner" << msg::endl;
            msg::indent();
            for (unsigned int c = 0; c < parametrization->size(); c++)
            {
                std::string suffix_c = "c"+std::to_string(c);
                save(statefunctional->sensitivity(Order::DIAG,Support::BLK)[c], configuration->model_unknown(Support::BLK), name+"_precond"+suffix_g+suffix_c, "pos", "");
            }
            msg::unindent();

        }

        if(diag_preconditioner_scale)
        {
            msg::print << "Compute scaled gradient" << msg::endl;
            msg::indent();
            innerproduct->scale(0.);
            statefunctional->innerproductIsObsolete();
            statefunctional->write_model(Type::AS,name+"_pgradient"+suffix_g);
            msg::unindent();
        }

        msg::print << "Compute smoothed gradient" << msg::endl;
        msg::indent();
        unsigned int n_scale = 1;
        gmshFem.userDefinedParameter(n_scale, "n_scale"+suffix_g);
        for (unsigned int s = 0; s < n_scale; s++)
        {
            std::string suffix_s = std::to_string(s);
            double scale = 0.;
            gmshFem.userDefinedParameter(scale, "scale"+suffix_g+suffix_s);
            gmshFem.userDefinedParameter(scale, "scale"+suffix_s);
            msg::print << "Scale #" << suffix_g << " = "<< scale << msg::endl;
            msg::indent();
            innerproduct->scale(scale);
            statefunctional->innerproductIsObsolete();
            statefunctional->write_model(Type::AS,name+"_sgradient"+suffix_g+suffix_s);
            msg::unindent();
        }
        msg::unindent();

        delete statefunctional;
        for (unsigned int f = 0; f < n_freq; f++)
        {
            delete pequation[f];
        }
        delete regularization;
        msg::unindent();
    }
    msg::unindent();
    delete parametrization;

    return 0;
}

int main(int argc, char **argv)
{
  GmshFem gmshFem(argc, argv);
  Physic T_Physic = to_physic(gmshFem);
  switch (T_Physic)
  {
    case Physic::acoustic: default:
      return ip_comparison<Physic::acoustic>(gmshFem);
    case Physic::electromagnetic:
      return ip_comparison<Physic::electromagnetic>(gmshFem);
    case Physic::elastodynamic:
      return ip_comparison<Physic::elastodynamic>(gmshFem);
  }
}
