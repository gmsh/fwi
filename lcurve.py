#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 14 18:02:59 2022

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
plt.rcParams['ytick.right'] = plt.rcParams['ytick.labelright'] = False
plt.rcParams['ytick.left'] = plt.rcParams['ytick.labelleft'] = True
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb} \renewcommand{\familydefault}{\sfdefault} \usepackage{sansmathfonts}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
lw = 1.5
ms=5
H = 2.0;
L = 3.0;

"""
#Marmousi
jmrzv = 21.06701972868763
n_comp = 2
reg = [np.array([1.0, 0.25, 0.23, 0.21, 0.19, 0.17, 0.15, 0.13, 0.11, 0.09, 0.07, 0.05, 0.03, 0.01, 0.001])] * 2
mrz = [1,1]
best = [8,10]
path = ['SST_LBFGS_LS_TIK/','SST_LBFGS_LS_TV/']
filename = ['paper2_extended_sst_lbfgs_ls_snr2_tik','paper2_extended_sst_lbfgs_ls_snr2_tv']
lab = ['TIK','TV']
col = ['C1','C2']
mrk = [''] * 2
lin = ['-'] * 2
"""

#Concrete 3
jmrzv = 11177.374443381555
n_comp = 4
ms=0
reg = [ np.array([1e-4, 1e-3, 0.005, 0.025, 0.05, 0.1, 0.25, 0.5]), np.array([1e2, 1.5e2, 2.5e2, 3.75e2, 5e2, 1e3, 1e4, 10.]), np.array([1e-4, 1e-3, 0.005, 0.025, 0.05, 0.1, 0.25, 0.5, 1e-5, 1e-6, 1e-7]), np.array([1e2, 1.5e2, 2.5e2, 3.75e2, 5e2, 1., 10., 1e3, 1e4, 0.1, 0.01]) ]
mrz = [0,0,0,0]
best = [0,0,0,0]
path = ['SST_LBFGS_LS_TIK/','SST_LBFGS_LS_TV/','SSM_LBFGS_LS_TIK/','DF_LBFGS_LS_TV/']
filename = ['paper2_concrete3_sst_lbfgs_ls_snrm3_tik','paper2_concrete3_sst_lbfgs_ls_snrm3_tv','paper2_concrete3_ssm_lbfgs_ls_snrm3_tik','paper2_concrete3_df_lbfgs_ls_snrm3_tv']
lab = ['TIK','TV','SSM-TIK','DF-TV']
col = ['C1','C2'] * 2
mrk = [''] * 2
lin = ['-'] * 2 + ['-.'] * 2

performance = [[]] * n_comp
objective = [[]] * n_comp
regularization = [[]] * n_comp
error = [[]] * n_comp

for i in range(0,n_comp):
    n_reg = np.size(reg[i])
    performance[i] = np.zeros(n_reg)
    objective[i] = np.zeros(n_reg)
    regularization[i] = np.zeros(n_reg)
    for r in range(0,n_reg):
        tmp = np.loadtxt(path[i]+filename[i]+str(r)+'_globalminimum_historyg0.csv',delimiter=';',max_rows=1,skiprows=6,dtype=np.float);
        performance[i][r] = tmp[0]
        objective[i][r] = tmp[3]
        regularization[i][r] = tmp[4]
    tmp = np.loadtxt(path[i]+filename[i]+"_error.csv",delimiter=';',dtype=np.float);
    error[i] = tmp[:,0]
        
#
# OBJECTIVE
#
plt.figure(figsize=(2*L,H),tight_layout=False);
plt.subplots_adjust(top=0.92,right=0.92,bottom=0.13,left=0.08)
#plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.grid(zorder=0)

#plt.xlim([1.,0.001])
#plt.ylim([0.95,1.10])
plt.xlim([1e4,1e-7])
plt.ylim([0.99,1.02])

plt.xscale('log')
#plt.xlabel(r'$\lambda$')
#plt.ylabel(r'$J(\lambda)$')
for i in range(0,n_comp,1):
    idx=np.argsort(reg[i])
    plt.plot(reg[i][idx],objective[i][idx]/jmrzv,color=col[i],marker='o',zorder=3,markersize=0,linewidth=lw,linestyle=lin[i],label=lab[i])
    plt.plot(reg[i][best[i]],objective[i][best[i]]/jmrzv,color=col[i],marker='o',zorder=3,markersize=ms)
    plt.plot(reg[i][mrz[i]],objective[i][mrz[i]]/jmrzv,color=col[i],marker='d',zorder=3,markersize=ms)
#plt.legend()
plt.hlines(1., 1e-7, 1e4, colors='k', linestyles='dashed', zorder=2, linewidth=lw/2.)

#
# PERFORMANCE
#
plt.figure(figsize=(2*L,H),tight_layout=False);
plt.subplots_adjust(top=0.92,right=0.92,bottom=0.13,left=0.08)
#plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.grid(zorder=0)

#plt.xlim([1.,0.001])
#plt.ylim([0.95,1.10])
plt.xlim([1e4,1e-7])
plt.ylim([0.99,1.02])

plt.xscale('log')
#plt.xlabel(r'$\lambda$')
#plt.ylabel(r'$J(\lambda)$')
for i in range(0,n_comp,1):
    idx=np.argsort(reg[i])
    plt.plot(reg[i][idx],performance[i][idx]/jmrzv,color=col[i],marker='o',zorder=3,markersize=0,linewidth=lw,linestyle=lin[i],label=lab[i])
    plt.plot(reg[i][best[i]],performance[i][best[i]]/jmrzv,color=col[i],marker='o',zorder=3,markersize=ms)
    plt.plot(reg[i][mrz[i]],performance[i][mrz[i]]/jmrzv,color=col[i],marker='d',zorder=3,markersize=ms)
#plt.legend()
plt.hlines(1., 1e-7, 1e4, colors='k', linestyles='dashed', zorder=2, linewidth=lw/2.)
    
#
# ERROR
#
plt.figure(figsize=(2*L,H),tight_layout=False);
plt.subplots_adjust(top=0.92,right=0.92,bottom=0.13,left=0.08)
#plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.grid(zorder=0)

#plt.ylim([0.10,0.25])
#plt.xlim([1.,0.001])
plt.ylim([0.,6.])
plt.xlim([1e4,1e-7])

plt.xscale('log')
#plt.xlabel(r'$\lambda$')
#plt.ylabel(r'$J(\lambda)$')
for i in range(0,n_comp,1):
    idx=np.argsort(reg[i])
    plt.plot(reg[i][idx],error[i][idx],color=col[i],marker='o',zorder=3,markersize=0,linewidth=lw,linestyle=lin[i],label=lab[i])
    plt.plot(reg[i][mrz[i]],error[i][mrz[i]],color=col[i],marker='d',zorder=3,markersize=ms)
    plt.plot(reg[i][best[i]],error[i][best[i]],color=col[i],marker='o',zorder=3,markersize=ms)
#plt.legend()

#
# L-CURVE
#
plt.figure(figsize=(2*L,H),tight_layout=False);
plt.subplots_adjust(top=0.95,right=0.98,bottom=0.20,left=0.15)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.grid(zorder=0)
#plt.xlim([0,max_forward])
#plt.xticks(np.arange(0,max_forward+1,Δmax_forward))
#plt.ylim([0.425,1.5])
plt.yscale('log')
plt.xscale('log')
plt.xlabel(r'$J(m)$')
plt.ylabel(r'$R(m)$')
for i in range(0,n_comp,1):
    idx=np.argsort(reg[i])
    plt.plot(objective[i][idx]/jmrzv,np.divide(regularization[i][idx],reg[i][idx]),color=col[i],marker='o',linewidth=lw,linestyle=lin[i],zorder=3,markersize=3)
plt.vlines(1., 0, 1e4, colors='k', linestyles='dashed', zorder=1, linewidth=lw/2.)
#plt.legend()        