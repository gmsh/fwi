#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  1 16:56:40 2022

@author: xavier
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
plt.rcParams['ytick.right'] = plt.rcParams['ytick.labelright'] = False
plt.rcParams['ytick.left'] = plt.rcParams['ytick.labelleft'] = True
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb} \renewcommand{\familydefault}{\sfdefault} \usepackage{sansmathfonts}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
lw = 1.5
ms=5
H = 2.0;
L = 3.0;

# Cross: TIK
jmrzv = 0.6931585837012904
reg0 = np.array([1.75e-5, 3e-5, 5.25e-5, 1e-4, 1.75e-4, 3e-4, 5.25e-4])
reg1 = np.array([3e-6, 1e-6, 3e-6, 1e-5, 3e-5, 1e-4, 3e-4])
N0 = np.size(reg0)
N1 = np.size(reg1)

path = 'SST_LBFGS_LS_TIK/'
filename = 'cross_sst2_lbfgs_ls_snr6_tik'

"""
# Cross: TV
jmrzv = 0.6931585837012904
reg0 = np.array([3e-5, 5.25e-5, 1e-4, 1.75e-4, 3e-4, 5.25e-4, 1e-3])
reg1 = np.array([5.25e-6, 1e-5, 1.75e-5, 3e-5, 5.25e-5, 1e-4, 1.75e-4])
N0 = np.size(reg0)
N1 = np.size(reg1)

path = 'SST_LBFGS_LS_TV/'
filename = 'cross_sst2_lbfgs_ls_snr6_tv'
"""

performance = np.zeros([N0,N1])
objective = np.zeros([N0,N1])
regularization = np.zeros([N0,N1])
error = np.zeros([N0,N1,2])

tmp_error = np.loadtxt(path+filename+"_error.csv",delimiter=';',dtype=np.float);
for n1 in range(0,N1):
    for n0 in range(0,N0):
        tmp = np.loadtxt(path+filename+str(n1*N1+n0)+'_globalminimum_historyg0.csv',delimiter=';',max_rows=1,skiprows=6,dtype=np.float);
        performance[n0,n1] = tmp[0]
        objective[n0,n1] = tmp[3]
        regularization[n0,n1] = tmp[4]
        error[n0,n1,0] = tmp_error[n1+N0*n0,0]
        error[n0,n1,1] = tmp_error[n1+N0*n0,2]

#
# ERROR
#
border0 = np.zeros(N0+1)
for n0 in range(0,N0-1):
    border0[n0+1] = (np.log10(reg0[n0])+np.log10(reg0[n0+1]))/2.
    border0[n0+1] = 10**border0[n0+1]
border0[0] = np.log10(border0[1]) - (np.log10(border0[2])-np.log10(border0[1]))
border0[0] = 10**border0[0]
border0[N0] = np.log10(border0[N0-1]) + (np.log10(border0[N0-1])-np.log10(border0[N0-2]))
border0[N0] = 10**border0[N0]

border1 = np.zeros(N1+1)
for n1 in range(0,N1-1):
    border1[n1+1] = (np.log10(reg1[n1])+np.log10(reg1[n1+1]))/2.
    border1[n1+1] = 10**border1[n1+1]
border1[0] = np.log10(border1[1]) - (np.log10(border1[2])-np.log10(border1[1]))
border1[0] = 10**border1[0]
border1[N1] = np.log10(border1[N1-1]) + (np.log10(border1[N1-1])-np.log10(border1[N1-2]))
border1[N1] = 10**border1[N1]

figure=plt.figure(figsize=(L,H),tight_layout=False)
plt.subplots_adjust(top=0.90,right=0.95,bottom=0.13,left=0.13)
ax=plt.subplot()
ax.yaxis.set_label_position("right")
ax.set_ylabel('Y', rotation=-90, labelpad=15)
ax.xaxis.set_label_position("top")
ax.set_xlabel('Y', rotation=0, labelpad=5)
#vmax=1.5
plt.pcolor(border0,border1,(1.*error[:,:,0]+1.*error[:,:,1])/2,vmin=0.5,vmax=1.0)
plt.xlabel(r'$\lambda_{\epsilon}$')
plt.ylabel(r'$\lambda_{\sigma}$')
plt.xscale('log')
plt.yscale('log')
plt.colorbar(pad=0.15,ticks=[0.5, 0.75, 1.0])


