# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
header = np.loadtxt('marmousi_velocity.csv',skiprows=0,max_rows=1,dtype=np.integer).reshape(1, 2)
headerX = np.loadtxt('marmousi_velocity.csv',skiprows=1,max_rows=1,dtype=np.float).reshape(1, header[0][0]) / 1000.
headerY = np.loadtxt('marmousi_velocity.csv',skiprows=2,max_rows=1,dtype=np.float).reshape(1, header[0][1]) / 1000.
data = 1. / (np.loadtxt('marmousi_velocity.csv',skiprows=3,dtype=np.float) / 1000.)**2

with open('marmousi_slownesssquared.csv', "ab") as file:
    np.savetxt(file,header,fmt='%d')
    np.savetxt(file,headerX)
    np.savetxt(file,headerY)
    np.savetxt(file,data)