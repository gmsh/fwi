//GmshFem Library
#include "GmshFem.h"
#include "Post.h"
#include "Formulation.h"
#include "CSVio.h"
#include "FieldRoutines.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::function;
using namespace gmshfem::problem;
using namespace gmshfem::post;

//GmshFWI Library
#include "specific/physic.h"
#include "specific/configuration/newConfiguration.h"
#include "common/wave/equation/equation.h"
#include "specific/data/objective/newObjective.h"
#include "specific/model/sweeper.h"
#include "common/statefunctional.h"
#include "specific/model/parametrization/newParametrization.h"
#include "common/model/macro.h"

static const std::complex< double > im = std::complex< double >(0., 1.);

template<Physic T_Physic>
int multiscale(const GmshFem& gmshFem)
{
    std::string name = "";
    gmshFem.userDefinedParameter(name, "prename");
    std::string suffix = "noname";
    gmshFem.userDefinedParameter(suffix, "name");
    name += suffix;

    ParametrizationInterface* const parametrization = newParametrization<Physic::acoustic>(gmshFem);
    auto configuration = newConfiguration(name, parametrization, gmshFem);

    std::string integrationType;
    gmshFem.userDefinedParameter(integrationType, "model_IntegrationType");

    unsigned int write_fields = 1;
    gmshFem.userDefinedParameter(write_fields, "write_fields");
    unsigned int write_data = 0;
    gmshFem.userDefinedParameter(write_data, "write_data");

    unsigned int n_freq = 1;
    if(!gmshFem.userDefinedParameter(n_freq, "n_freq"))
    {
        throw Exception("Total frequency number could not be found.");
    }
    std::vector<double> frequency(n_freq);
    std::vector<std::string> filename(n_freq);
    for (unsigned int f = 0; f < n_freq; f++)
    {
        std::string suffix = std::to_string(f);
        if(!gmshFem.userDefinedParameter(frequency[f], "frequency"+suffix))
        {
            throw Exception("Frequency #"+suffix+" could not be found.");
        }
        if(!gmshFem.userDefinedParameter(filename[f], "data"+suffix))
        {
            msg::warning << "Data filename #"+suffix+" could not be found." << msg::endl;
            if(!gmshFem.userDefinedParameter(filename[f], "data"))
            {
                throw Exception("Data filename could not be found.");
            }
            filename[f] += suffix;
            msg::warning << "filename #"+suffix+" = "<< filename[f] << msg::endl;
        }
    }
    const Data<T_Physic> d0(filename,frequency,configuration.get());

    auto objective = newObjective<T_Physic>(d0,configuration.get(),gmshFem);

    std::vector<EquationInterface<T_Physic>*> pequation(n_freq);
    for (unsigned int f = 0; f < n_freq; f++)
    {
        std::string suffix_f = std::to_string(f);

        wave::Discretization<T_Physic> w_discret(gmshFem,suffix_f);
        pequation[f] = new ParametrizedEquation<T_Physic>(parametrization,f,2.*M_PI*frequency[f],configuration.get(),w_discret,gmshFem,suffix_f);
    }
    StateFunctional<T_Physic>* const statefunctional = new StateFunctional<T_Physic>(configuration.get(),nullptr,nullptr,frequency,pequation,objective.get());
    FunctionalInterface* const functional = statefunctional;

    msg::print << "Perform multiscale analysis" << msg::endl;
    msg::indent();
    ModelFunctionSweeperInterface* sweeper = newModelFunctionSweeper(gmshFem);

    unsigned int N1 = 0;
    unsigned int N2 = 0;
    gmshFem.userDefinedParameter(N1, "sweep_N1");
    gmshFem.userDefinedParameter(N2, "sweep_N2");

    CSVio output(name+suffix, ';', common::OpeningMode::NewFile);
    output << N1 << N2 << csv::endl;
    for (unsigned int n1 = 0; n1 <= N1; n1++)
    {
        for (unsigned int n2 = 0; n2 <= N2; n2++)
        {
            std::string suffix = 'n'+std::to_string(n1)+'m'+std::to_string(n2);

            msg::print << "Point #" << n1 << " #" << n2 << msg::endl;
            msg::indent();

            ModelFunction m = (*sweeper)(n1,n2);

            double j = functional->performance(m);
            msg::print << "j = " << j << msg::endl;
            output << n1 << n2 << j << csv::endl;
            if(write_fields)
            {
                save(m[0],configuration->model_evaldomain(),name+"_m"+suffix,"pos","");
                statefunctional->write_wave(Type::FS,name+"_u"+suffix);
            }
            if(write_data)
            {
                statefunctional->write_data(Type::FS,name+"_d"+suffix);
            }
            msg::unindent();
        }
    }
    output.close();


    delete statefunctional;
    for (unsigned int f = 0; f < n_freq; f++)
    {
        delete pequation[f];
    }
    delete sweeper;
    delete parametrization;

    return 0;
}

int main(int argc, char **argv)
{
  GmshFem gmshFem(argc, argv);
  Physic T_Physic = to_physic(gmshFem);
  switch (T_Physic)
  {
    case Physic::acoustic: default:
      return multiscale<Physic::acoustic>(gmshFem);
    case Physic::electromagnetic:
      return multiscale<Physic::electromagnetic>(gmshFem);
    case Physic::elastodynamic:
      return multiscale<Physic::elastodynamic>(gmshFem);
  }
}
