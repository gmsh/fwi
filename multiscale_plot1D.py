#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 14:47:47 2020

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':6})
rc('text', usetex=True)
#Value for 1 width figure
H = 1.5;
L = 4.5;

freq = np.array([4.,6.,9.])
n_freq= 3#np.size(freq)

tmp = np.loadtxt('MP2008LinearVelocity2D_multiscale0.csv',delimiter=';',max_rows=1,dtype=np.integer);
Nv = tmp[0];
Na = tmp[1];
N = Na;
j_tot = np.zeros(N+1)

f0 = plt.figure(figsize=(L,H),tight_layout=False);
plt.subplots_adjust(top=0.9,right=0.95,bottom=0.225,left=0.1)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
for i in range(0,n_freq):
    tmp = np.loadtxt('MP2008LinearVelocity2D_multiscale'+str(i)+'.csv',delimiter=';',skiprows=1);
    alpha = tmp[:,1];
    j = tmp[:,2];
    j_tot += j
    plt.plot(alpha,j,label=r'$f = ' + str(freq[i]) +'$ [Hz]')

plt.xlabel(r'$\gamma$')
plt.ylabel(r'$\mathcal{J}_\omega(\gamma)$')
plt.xlim([alpha[0], alpha[Na]])
plt.legend(loc=4)
plt.grid()
plt.show()
#plt.plot(alpha,j_tot/3)
