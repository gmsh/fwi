//GmshFem Library
#include "GmshFem.h"
#include "Post.h"
#include "Formulation.h"
using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::post;

//GmshFWI Library
#include "common/data/updater.h"
#include "common/wave/updater.h"
#include "common/model/updater.h"
#include "specific/physic.h"
#include "specific/configuration/newConfiguration.h"
#include "specific/wave/equation/newEquation.h"
#include "specific/data/objective/continuousl2distance.h"
#include "specific/model/parametrization/newParametrization.h"
#include "common/model/macro.h"

enum class Interval: unsigned int { Linear=0, Log=1 };
Interval to_interval(const GmshFem& gmshFem)
{
  std::string interval_buffer;
  if(gmshFem.userDefinedParameter(interval_buffer, "interval"))
  {
    if(interval_buffer=="linear"){return Interval::Linear;}
    else if(interval_buffer=="log"){return Interval::Log;}
  }
  return Interval::Linear;
}

template<Physic T_Physic>
int ob_comparison(const GmshFem& gmshFem)
{
    std::string name = "";
    gmshFem.userDefinedParameter(name, "prename");
    std::string suffix = "noname";
    gmshFem.userDefinedParameter(suffix, "name");
    name += suffix;

    ParametrizationInterface* const parametrization = newParametrization<T_Physic>(gmshFem);

    auto configuration = newConfiguration(name, parametrization, gmshFem);

    std::string integrationType;
    gmshFem.userDefinedParameter(integrationType, "model_IntegrationType");

    model::Discretization m_discret(gmshFem);

    double filter_scale = 0.;
    gmshFem.userDefinedParameter(filter_scale, "filter_scale");

    ModelUpdater mu(configuration.get(),nullptr,nullptr);

    unsigned int n_freq = 1;
    gmshFem.userDefinedParameter(n_freq, "n_freq");
    msg::print << "Perform comparison" << msg::endl;
    msg::indent();
    for (unsigned int f = 0; f < n_freq; f++)
    {
        std::string suffix = std::to_string(f);

        double frequency;
        if(!gmshFem.userDefinedParameter(frequency, "frequency"+suffix))
        {
            throw Exception("Frequency "+suffix+" could not be found.");
        }
        msg::print << "--- Frequency #"+suffix+" = " << frequency << " --- " << msg::endl;
        msg::indent();

        ModelField m = laplace_filter(configuration->m0(),filter_scale,configuration.get(),m_discret,integrationType);
        m.write(name+"_m_smooth"+suffix);

        wave::Discretization<T_Physic> w_discret(gmshFem,suffix);
        parametrization->pulsation(2.*M_PI*frequency);
        EquationInterface<T_Physic>* const equation = newEquation(2.*M_PI*frequency,configuration.get(),w_discret,gmshFem);
        EquationInterface<T_Physic>* const pequation = new ParametrizedEquation<T_Physic>(configuration.get(),equation,parametrization,gmshFem);

        std::string filename = "noname";
        gmshFem.userDefinedParameter(filename, "data"+suffix);
        Data<T_Physic> d0(filename,configuration.get());

        continuousl2distance::Objective<T_Physic>* const objective = new continuousl2distance::Objective<T_Physic>(&d0,configuration.get(),gmshFem,suffix);

        WaveUpdater<T_Physic> wu(configuration.get(),nullptr,pequation);
        DataUpdater<T_Physic> du(configuration.get(),&wu,objective);
        std::array<bool,5> dataNeedUpToDate = {true,false,false,false,false};
        std::array<bool,4> modelNeedUpToDate = {true,false,false,false};
        Data<T_Physic> d = (du.get(dataNeedUpToDate,mu.get(modelNeedUpToDate,&m))).state(Type::FS);

        d0.write(name+"_d0"+suffix, "pos");
        d.write(name+"_d"+suffix, "pos");

        Data<T_Physic> dw = d / objective->w();
        Data<T_Physic> d0w = d0 / objective->w();
        d0w.write(name+"_d0w"+suffix, "pos");
        dw.write(name+"_dw"+suffix, "pos");
        (dw.phase() / d0w.phase()).write(name+"_phase"+suffix, "pos");

        double scale0,scaleN;
        unsigned int N;
        if(!(
        gmshFem.userDefinedParameter(scale0, "scale0") &&
        gmshFem.userDefinedParameter(scaleN, "scaleN") &&
        gmshFem.userDefinedParameter(N, "N")
        ) )
        {
            throw common::Exception("A scale parameter could not be found.");
        }
        Interval interval = to_interval(gmshFem);

        double scale = 0.;

        double base =  scaleN / scale0;
        double span =  scaleN - scale0;
        for (unsigned int n = 0; n <= N; n++)
        {
            std::string suffix1 = suffix + "s" + std::to_string(n);
            if(N==0){scale = scale0;}
            else
            {
                switch (interval)
                {
                    case Interval::Log:
                    scale  = scale0 * std::pow(base, ((double) n) / ((double) N));
                    break;
                    case Interval::Linear:
                    scale = scale0 + ((double)n) / ((double)N) * span;
                    break;
                }
            }
            msg::print << "--- Scale #"<< n << " = "<< scale << " --- " << msg::endl;
            msg::indent();

            objective->scale(scale);
            Data<T_Physic> sdw = objective->inv_operator_s(dw);
            Data<T_Physic> sd0w = objective->inv_operator_s(d0w);
            sd0w.write(name+"_sd0w"+suffix1, "pos");
            sdw.write(name+"_sdw"+suffix1, "pos");
            (sdw.phase() / sd0w.phase()).write(name+"_phase"+suffix1, "pos");

            msg::unindent();
        }
        msg::unindent();

        delete objective;
        delete equation;
        delete pequation;
        msg::unindent();
    }
    msg::unindent();
    delete parametrization;

    return 0;
}

int main(int argc, char **argv)
{
  GmshFem gmshFem(argc, argv);
  Physic T_Physic = to_physic(gmshFem);
  switch (T_Physic)
  {
    case Physic::acoustic: default:
      return ob_comparison<Physic::acoustic>(gmshFem);
  }
}
