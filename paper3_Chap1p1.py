#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 09:20:00 2021

@author: xavier
"""
import numpy as np
import scipy as sp
import scipy.special
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
#Value for 1 width figure
H = 1.5;
W = 6.5;
save = False;

"""
Common
"""
h=5.

"""
Example I: Wave number perturbation
"""
name = 'example1'
L = 10.
λ = 1.
k = 2*np.pi / λ
N = 2**8+1
Δxr = L/N
xr = np.linspace(-(L-Δxr)/2, (L-Δxr)/2, N)
ξr = (np.arange(0,N)-np.floor(N/2))/L

Nϵ=38
ϵmax=0.2
ϵmin=0
ξmin=0
ξmax=2/λ
xmin=0
xmax=L/2

Jmax=3.
def J_l2(ϵ):
    return 1-np.sinc(ϵ*k*L/2/np.pi)
s=λ
def J_sob(ϵ):
    return 0.5*s**2*k**2*ϵ**2
amplitude=True
if amplitude:
    def d(xr,ϵ):
        return -1j/4*sp.special.hankel1(0,k*(1+ϵ)*np.abs(xr))
else:
    def d(xr,ϵ):
        return np.exp(1j*k*(1+ϵ)*np.abs(xr))
"""
Example II: Plane reflector depth perturbation

name = 'example2'
L = 10.
λ = 1.
k = 2*np.pi / λ
s=λ
N = 2**8+1
Δxr = L/N
xr = np.linspace(-(L-Δxr)/2, (L-Δxr)/2, N)
ξr = (np.arange(0,N)-np.floor(N/2))/L

Nϵ=38
ϵmax=0.075
ϵmin=0
ξmin=0
ξmax=1/λ
xmin=0
xmax=L/2

Jmax=2.
def J_l2(ϵ):
    return 2*np.sin(ϵ*k*h)**2
def J_sob(ϵ):
    return 0.*ϵ
def d(xr,ϵ):
    return np.exp(1j*k*2*h*(1+ϵ))*np.ones(N)
"""

"""
Example III: Point reflector depth perturbation

name = 'example3'
L = 10.
λ = 1.
k = 2*np.pi / λ
h=5.
s=λ
N = 2**8+1
Δxr = L/N
xr = np.linspace(-(L-Δxr)/2, (L-Δxr)/2, N)
ξr = (np.arange(0,N)-np.floor(N/2))/L

Nϵ=38
ϵmax=0.075
ϵmin=0
ξmin=0
ξmax=1/λ
xmin=0
xmax=L/2

Jmax=2.
def J_l2(ϵ):
    coeff = 0.
    if(L/2/h > 2):
        coeff = 1/2
    elif(L/2/h < 2):
        coeff = 1
    else:
        coeff = 3/4
    return 2*np.sin(coeff*ϵ*k*h)**2
def f_array(l):
    return np.nan_to_num(1/8/l * (np.arctan(l) - l*(1-l**2) / (1+l**2)**2))
def J_sob(ϵ):
    return f_array(L/2/h)*(k)**2*0.5*ϵ**2
def d(xr,ϵ):
    r = np.sqrt(xr**2 + (h*(1+ϵ))**2)
    return np.exp(1j*k*(r+h*(1+ϵ)))
"""


"""
SPATIAL DOMAIN ANALYSIS
"""
vϵ = np.linspace(ϵmin,ϵmax,Nϵ)
vc = np.linspace(0.95,0.05,Nϵ)
fig1 = plt.figure(1,figsize=(W,H),tight_layout=False);
plt.subplots_adjust(top=0.9,right=0.975,bottom=0.3,left=0.1)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlim([xmin,xmax])
plt.xlabel(r'$x_r$')
plt.ylabel(r'Re($d_0$)')
plt.grid()
for i in range(0,Nϵ):
    plt.plot(xr,np.real(d(xr,vϵ[i])),color=plt.cm.Spectral(vc[i]))
if save: plt.savefig(name+'_d.eps')

def w(ϵ):
    return d(xr,ϵ) / d(xr,0)
def r(ϵ):
    return 1-w(ϵ)

fig2 = plt.figure(2,figsize=(W,H),tight_layout=False);
plt.subplots_adjust(top=0.9,right=0.975,bottom=0.3,left=0.1)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlim([xmin,xmax])
plt.xlabel(r'$x_r$')
plt.ylabel(r'$\left\vert r \right\vert$')
plt.grid()
for i in range(0,Nϵ):
    plt.plot(xr,np.abs(r(vϵ[i])),color=plt.cm.Spectral(vc[i]))
if save: plt.savefig(name+'_r.eps')

"""
FREQUENCY DOMAIN ANALYSIS
"""
Nint = 8
ξintr = np.linspace(ξr[0],ξr[N-1],N+(N-1)*Nint)
def cfft(q,ξr):
    out = np.zeros(np.size(ξr))*1j
    for i in range(0,N):
        out += np.nan_to_num(q[i]) * np.exp(-1j*2*np.pi*xr[i]*ξr)
    return out / N

def f_d(ξr,ϵ):
    return cfft(d(xr,ϵ),ξr)

fig3 = plt.figure(3,figsize=(W,H),tight_layout=False);
plt.subplots_adjust(top=0.9,right=0.975,bottom=0.3,left=0.1)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlabel(r'$\xi_r$')
plt.ylabel(r'$| \hat{d_0} |$')
plt.xlim([ξmin,ξmax])
plt.grid()
for i in range(0,Nϵ):
    plt.plot(ξr,np.abs(f_d(ξr,vϵ[i])),color=plt.cm.Spectral(vc[i]),marker='o',markersize=3.5)
if save: plt.savefig(name+'_fd.eps')
    
def f_r(ξr,ϵ):
    return cfft(r(ϵ),ξr)

fig4 = plt.figure(4,figsize=(W,H),tight_layout=False);
plt.subplots_adjust(top=0.9,right=0.975,bottom=0.3,left=0.1)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlabel(r'$\xi_r$')
plt.ylabel(r'$| \hat{r} |$')
plt.xlim([ξmin,ξmax])
plt.grid()
for i in range(0,Nϵ):
    plt.plot(ξr,np.abs(f_r(ξr,vϵ[i])),color=plt.cm.Spectral(vc[i]),marker='o',markersize=3.5)
if save: plt.savefig(name+'_fr.eps')    

"""
COST FUNCTION
"""
ϵ = np.linspace(2*ϵmin,2*ϵmax,100)

def σ(ξr):
    return s*2*np.pi*1j*ξr

def distance1(ϵ):
    return 0.5*(np.linalg.norm(f_r(ξr,ϵ)))**2
v_distance1 = np.vectorize(distance1)

def distance2(ϵ):
    return 0.5*(np.linalg.norm(σ(ξr)*f_r(ξr,ϵ)))**2
v_distance2 = np.vectorize(distance2)

"""
COST FUNCTION
"""
#Least squares
fig5 = plt.figure(5,figsize=(W,H),tight_layout=False);
plt.subplots_adjust(top=0.9,right=0.975,bottom=0.3,left=0.1)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlabel(r'$\epsilon$')
plt.xlim([2*ϵmin,2*ϵmax])
plt.ylim([0,Jmax])
plt.grid()

#plt.plot(ϵ,v_distance1(ϵ),'k')
plt.plot(ϵ,J_l2(ϵ),'tab:gray')
for i in range(0,Nϵ):
    plt.plot(vϵ[i],v_distance1(vϵ[i]),color=plt.cm.Spectral(vc[i]),marker='o',markersize=3.5)
if save: plt.savefig(name+'_j_l2.eps')
   
fig6 = plt.figure(6,figsize=(W,H),tight_layout=False);
plt.subplots_adjust(top=0.9,right=0.975,bottom=0.3,left=0.1)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlabel(r'$\epsilon$')
plt.xlim([2*ϵmin,2*ϵmax])
if name=='example1':plt.ylim([0,Jmax])
else: plt.ylim([0,Jmax/10])
plt.grid()

#plt.plot(ϵ,v_distance2(ϵ),'k')
plt.plot(ϵ,J_sob(ϵ),'tab:gray')
for i in range(0,Nϵ):
    plt.plot(vϵ[i],v_distance2(vϵ[i]),color=plt.cm.Spectral(vc[i]),marker='o',markersize=3.5)
if save: plt.savefig(name+'_j_sob.eps')

plt.close('all')


"""
SENSITIVITY RATIO
"""
from paper3_Chap1p2 import α

"""
SYMBOL MIXING
"""
#Numerical
def σ3(ξr,α,s):
    return (1 + 1j*2*np.pi*α*L*ξr) / (1 + 1j*2*np.pi*s*L*ξr)

def distance3(ϵ,α,s):
    return 0.5*(np.linalg.norm(σ3(ξr,α,s)*f_r(ξr,ϵ)))**2
v_distance3 = np.vectorize(distance3)

Nα=21
αmin=α(L/2/h)/2
αmax=α(L/2/h)*3/2
vα = np.linspace(αmin,αmax,Nα)
vαc = np.linspace(0.95,0.05,Nα)

fig7 = plt.figure(7,figsize=(W,H),tight_layout=False);
plt.subplots_adjust(top=0.9,right=0.95,bottom=0.3,left=0.05)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlim([2*ϵmin,2*ϵmax])
plt.xlabel(r'$\epsilon$')
#plt.ylim([0,Jmax])
plt.grid()
#plt.plot(ϵ,(α(1)*L)**2*J_sob(ϵ),'tab:gray')
#plt.plot(ϵ,J_l2(ϵ),'tab:gray')
plt.plot(ϵ,J_l2(ϵ)+(α(L/2/h)*L)**2*J_sob(ϵ),'tab:gray')

for i in range(0,Nα):
    plt.plot(ϵ,v_distance3(ϵ,vα[i],0),color=plt.cm.Spectral(vαc[i]))
    #plt.plot(ϵ,J_l2(ϵ)+vα[i]**2*J_sob(ϵ),'tab:gray')
plt.plot(ϵ,v_distance3(ϵ,vα[10],0),'k--')

Ns=21
smin=0
smax=α(L/2/h)
vs = np.linspace(smin,smax,Ns)
vsc = np.linspace(0.95,0.05,Ns)

fig8 = plt.figure(8,figsize=(W,H),tight_layout=False);
plt.subplots_adjust(top=0.9,right=0.95,bottom=0.3,left=0.05)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlim([2*ϵmin,2*ϵmax])
plt.xlabel(r'$\epsilon$')
#plt.ylim([0,Jmax])
plt.grid()

for i in range(0,Ns):
    plt.plot(ϵ,v_distance3(ϵ,2*α(L/2/h),vs[i]),color=plt.cm.Spectral(vsc[i]))