#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 16:07:03 2021

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
#Value for 1 width figure
H = 1.5;
W = 6.5;

"""
Example III: Point reflector depth perturbation
"""

lmin=1e-1
lmax=10.
l=np.linspace(lmin,lmax,100)

def f_array_dise(l):
    return np.nan_to_num(1/8/l * (np.arctan(l) - l*(1-l**2) / (1+l**2)**2))
def f_array_conv(l):
    if(l>=1):
        return 3 * (3/l+1)/l**2
    elif(l<=0):
        return np.nan
    elif(l<1):
        return np.nan_to_num(12./l**2)
f_array_conv = np.vectorize(f_array_conv)

def α(l):
    return 1/(np.pi*l*np.sqrt(f_array_dise(l)))

def f_array_hybd(l,α):
    a_conv_h = np.minimum((3/l+1)/4,1)/l**2
    a_conv_k = 1/12
    a_dise_h = f_array_dise(l)
    a_dise_k = 1/np.pi**2
    return (a_conv_h + α**2 * a_dise_h) / (a_conv_k + α**2 * a_dise_k)
    

Nα=21
αmin=0
αmax=2
vα = np.linspace(αmin,αmax,Nα)
vαc = np.linspace(0.95,0.05,Nα)

fig1 = plt.figure(1,figsize=(W,H),tight_layout=False);
plt.subplots_adjust(top=0.9,right=0.925,bottom=0.3,left=0.075)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlim([lmin,lmax])
plt.ylim([0,5])
plt.xlabel(r'$l$')
plt.ylabel(r'$a_h/a_k$')
plt.grid()

for i in range(0,Nα):
    plt.plot(l,f_array_hybd(l,vα[i]),color=plt.cm.Spectral(vαc[i]))

plt.plot(l,f_array_dise(l),'k--',label=r'Differential')
plt.plot(l,f_array_conv(l),'k-.',label=r'Conventional')
#plt.plot(l,np.pi/16/l,'tab:gray',label=r'$\frac{\pi}{16l}$')
#plt.plot(l,1/3*l**2,'tab:gray',label=r'$\frac{1}{3}l^2$')
plt.legend(loc=5)

fig2 = plt.figure(2,figsize=(W,H),tight_layout=False);
plt.subplots_adjust(top=0.9,right=0.925,bottom=0.3,left=0.075)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlim([lmin,lmax])
plt.ylim([0,6])
plt.xlabel(r'$l$')
plt.ylabel(r'$\alpha(l)$')
plt.grid()

plt.plot(l,α(l),'k')