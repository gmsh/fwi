#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 13:36:20 2021

@author: xavier
"""
import numpy as np
import scipy as sp
import scipy.special
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Helvetica'],'size':10})
rc('text', usetex=True)
#Value for 1 width figure
H = 1.5;
W = 6.5;
save = False;

L = 10.
λ = 1.
k = 2*np.pi / λ
N = 2**8+1
Δxr = L/N
xr = np.linspace(-(L-Δxr)/2, (L-Δxr)/2, N)
ξr = (np.arange(0,N)-np.floor(N/2))/L
Ne = 3

Nϵ=75
ϵmax=0.2
ϵmin=0.
Δϵ=(ϵmax-ϵmin)/(Nϵ-1)
ξmin=0
ξmax=2/λ
xmin=0
xmax=L/2

Jmax=210
J = np.zeros(Nϵ)
tmp = (np.loadtxt('paper3_example4_multiscale0.csv',delimiter=';',skiprows=1))
J = tmp[:,2]

d_num = np.zeros([Ne,N,Nϵ])*1j
for i in range(0,Nϵ):
        tmp = (np.loadtxt('paper3_example4_multiscale_df0n'+str(i)+'0'+'.csv',delimiter=';',skiprows=0,max_rows=Ne)).view(np.complex128);
        d_num[:,:,i] = tmp

def d(xr,e,ϵ):
    i = (np.around(xr / Δxr)+(N-1)/2).astype(int)
    j = (np.around((ϵ+ϵmin) / Δϵ)).astype(int)
    return d_num[e,i,j]
"""
SPATIAL DOMAIN ANALYSIS
"""
def w(e,ϵ):
    return d(xr,e,ϵ) / d(xr,e,0)
def r(e,ϵ):
    return 1-w(e,ϵ)

"""
FREQUENCY DOMAIN ANALYSIS
"""
Nint = 8
ξintr = np.linspace(ξr[0],ξr[N-1],N+(N-1)*Nint)
def cfft(q,ξr):
    out = np.zeros(np.size(ξr))*1j
    for i in range(0,N):
        out += np.nan_to_num(q[i]) * np.exp(-1j*2*np.pi*xr[i]*ξr)
    return out / N

def f_d(ξr,e,ϵ):
    return cfft(d(xr,e,ϵ),ξr)

def f_r(ξr,e,ϵ):
    return cfft(r(e,ϵ),ξr)  

"""
COST FUNCTION
"""
α = 0.
s = 1.
#Numerical
def σ(ξr):
    return (1+α*L*2*np.pi*1j*ξr) / (1+s*λ*2*np.pi*1j*ξr)

def distance(ϵ):
    sume = 0.
    for e in range(0,Ne):
        sume += 0.5*(np.linalg.norm(f_r(ξr,e,ϵ)*σ(ξr)))**2
    return sume/Ne
v_distance = np.vectorize(distance)
       
"""
COST FUNCTION
"""
ϵ = np.linspace(ϵmin,ϵmax,Nϵ)
#Least squares
fig1 = plt.figure(1,figsize=(W,H),tight_layout=False);
plt.subplots_adjust(top=0.9,right=0.975,bottom=0.3,left=0.1)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlabel(r'$\epsilon$')
plt.xlim([0,ϵmax])
plt.ylim([0,Jmax])
plt.grid()
plt.plot(ϵ,J,'C0',marker='x',markersize=4.5,linewidth=0.)
plt.plot(ϵ,v_distance(ϵ),'C1',marker='.',markersize=4.5,linewidth=0.)