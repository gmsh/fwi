#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 25 14:43:28 2022

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
plt.rcParams['ytick.right'] = plt.rcParams['ytick.labelright'] = False
plt.rcParams['ytick.left'] = plt.rcParams['ytick.labelleft'] = True
plt.rcParams['xtick.bottom'] = plt.rcParams['xtick.labelbottom'] = False
plt.rcParams['xtick.top'] = plt.rcParams['xtick.labeltop'] = True
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb} \renewcommand{\familydefault}{\sfdefault} \usepackage{sansmathfonts}')
rc('font',**{'family':'sans-serif','sans-serif':['Computer Modern Serif'],'size':10})
rc('text', usetex=True)
lw = 1.5
H = 2.0;
L = 3.0;

# Marmousi
"""
path = "extended_marmousi_data/"

#prename = "timewave_synthetics_data"; s = 61
#prename = "timewave_synthetics_data_snr2"; s = 61
#prename = "paper2_extended_timewave_initial_data"; s = 0
#prename = "paper2_extended_timewave_inverse_data"; s = 0

T=8.0
Δt=0.004
Nr=242


amplitude = 0.02
Te=4.0
time_samp = 4
ΔNr = 60
ΔT = 1.
"""

"""
# Concrete 3

path = "concrete3_data/"

#prename = "timewave_synthetics_data"; s = 134
#prename = "timewave_synthetics_data_snrm3"; s = 134
#prename = "paper2_concrete3_timewave_initial_data"; s = 0
prename = "paper2_concrete3_timewave_inverse_data"; s = 0

T=200 #[ms]
Δt=0.1 #[ms]
Nr=226


amplitude = 0.1
Te=100 #[ms]
time_samp = 4
ΔNr = 50
ΔT = 25 #[ms]
"""

# Cross
"""
path = "cross_data/"

#prename = "cross_time_synthetics_data"; s = 20
#prename = "cross_time_synthetics_data_snr6"; s = 20
#prename = "cross_timewave_initial_data"; s = 0
prename = "cross_timewave_inverse_data"; s = 0

T=100.
Δt=1e-2
Nr=119


amplitude = 0.01
#amplitude = 0.0005
Te=50
time_samp = 20
ΔNr = 20
ΔT = 10
"""

# S-Cube
path = "win_data/"

prename = "scube_winsynthetics_data"; s = 0

T=20.0
Δt=0.02
Nr=1818


amplitude = 0.002
Te=6.98
time_samp = 1
ΔNr = 80
ΔT = 4.

#
# Time domain
#

N=np.int(Te/Δt)
data = np.zeros([np.int(N/time_samp)+1,Nr]);

for n in range(0,N+1,time_samp):
    tmp = np.loadtxt(path+prename+"f"+str(n)+".csv",delimiter=";",ndmin=2);
    data[np.int(n/time_samp),:] = tmp[s,0:2*Nr:2]
  
figure=plt.figure(figsize=(L,2*H),tight_layout=False)
plt.subplots_adjust(top=0.93,right=0.9,bottom=0.06,left=0.1)
ax=plt.subplot()
ax.yaxis.set_label_position("right")
ax.set_ylabel('Y', rotation=-90, labelpad=15)
ax.xaxis.set_label_position("bottom")
ax.set_xlabel('Y', rotation=0, labelpad=5)
plt.gca().invert_yaxis()
time = np.arange(0,N+time_samp+1,time_samp)*Δt #[s]
plt.pcolormesh(np.arange(0,Nr+1,1)-0.5,time-Δt*time_samp/2,data[:,:],vmin=-amplitude,vmax=amplitude,cmap='binary')
plt.xticks(np.arange(0,Nr,ΔNr));
plt.yticks(np.arange(0,Te*1.1,ΔT));
plt.xlabel(r'receiver index [-]')
plt.ylabel(r'time [s]')
#plt.ylabel(r'time [ms]')
#plt.ylabel(r'$c_{0}$ $\times$ time [m]')