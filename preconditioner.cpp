//GmshFem Library
#include "GmshFem.h"
#include "Post.h"
using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::post;

//FWI Library
#include "common/wave/updater.h"
#include "common/model/updater.h"
#include "common/sensitivity/updater.h"
#include "specific/physic.h"
#include "specific/configuration/newConfiguration.h"
#include "specific/wave/equation/newEquation.h"

#include "specific/model/parametrization/newParametrization.h"
#include "specific/data/objective/newObjective.h"

template<Physic T_Physic>
int preconditioner(const GmshFem& gmshFem)
{
    std::string name = "";
    gmshFem.userDefinedParameter(name, "prename");
    std::string suffix = "noname";
    gmshFem.userDefinedParameter(suffix, "name");
    name += suffix;

    const ParametrizationInterface* const parametrization = newParametrization<T_Physic>(gmshFem);

    auto configuration = newConfiguration(name, parametrization, gmshFem);

    unsigned int n_freq_tot = 0;
    if(!gmshFem.userDefinedParameter(n_freq_tot, "n_freq"))
    {
        throw Exception("Total frequency number could not be found.");
    }
    std::vector<double> frequency(n_freq_tot);
    const Data<T_Physic>* const d0 = new Data<T_Physic>(configuration.get());
    for (unsigned int f = 0; f < n_freq_tot; f++)
    {
        std::string suffix = std::to_string(f);
        if(!gmshFem.userDefinedParameter(frequency[f], "frequency"+suffix))
        {
            throw Exception("Frequency #"+suffix+" could not be found.");
        }
    }

    unsigned int n_group = 0;
    if(!gmshFem.userDefinedParameter(n_group, "n_group"))
    {
        throw Exception("Number of frequency groups could not be found.");
    }

    for (unsigned int g = 0; g < n_group; g++)
    {
        std::string suffix_g = std::to_string(g);
        msg::print << "--- Frequency group #" + suffix_g << " --- " << msg::endl;
        msg::indent();

        unsigned int n_freq = 1;
        if(!gmshFem.userDefinedParameter(n_freq, "n_freq"+suffix_g))
        {
            msg::warning << "Total frequency number for frequency group #" << suffix_g << " could not be found." << msg::endl;
            msg::warning << "A single frequency is used for frequency group #" << suffix_g << msg::endl;
        }
        std::vector<unsigned int> freq_idx(n_freq);
        for (unsigned int f = 0; f < n_freq; f++)
        {
            std::string suffix_f = std::to_string(f);
            if(!gmshFem.userDefinedParameter(freq_idx[f], "frequency"+suffix_g+suffix_f))
            {
                if(n_freq == 1)
                {
                    freq_idx[f] = g;
                }
                else
                {
                    throw Exception("Frequency index #"+suffix_f+" for group #" + suffix_f + " could not be found.");
                }
            }
            msg::print << "frequency #" + suffix_f << " = " << frequency[freq_idx[f]] << msg::endl;
        }

        msg::print << "Compute preconditioner" << msg::endl;

        ModelUpdater mu(configuration.get(),nullptr,nullptr);
        ModelFunction m0 = configuration->m0();
        std::array<bool,4> needUpToDate = {true,false,false,false};
        mu.get(needUpToDate,&m0).write(Type::FS,name+"_m"+suffix_g);

        std::vector<EquationInterface<T_Physic>*> equation(n_freq);
        std::vector<EquationInterface<T_Physic>*> pequation(n_freq);
        std::vector<ObjectiveInterface<T_Physic>*> objective(n_freq);
        WaveUpdater<T_Physic> *const wu( (WaveUpdater<T_Physic> *) std::malloc(n_freq * sizeof(WaveUpdater<T_Physic>)) );
        //Normalement we don't need data for a preconditioner
        DataUpdater<T_Physic> *const du( (DataUpdater<T_Physic> *) std::malloc(n_freq * sizeof(DataUpdater<T_Physic>)) );
        SensitivityUpdater* *const sue(new SensitivityUpdater*[n_freq]);
        for (unsigned int f = 0; f < n_freq; f++)
        {
            std::string suffix_f = std::to_string(freq_idx[f]);

            wave::Discretization<T_Physic> w_discret(gmshFem,suffix_f);
            equation[f] = newEquation(2.*M_PI*frequency[freq_idx[f]],configuration.get(),w_discret,gmshFem,suffix_f);
            pequation[f] = new ParametrizedEquation<T_Physic>(configuration.get(),equation[f],parametrization,gmshFem,suffix_f);

            objective[f] = newObjective<T_Physic>(d0,configuration.get(),gmshFem,suffix_f);
            objective[f]->link(&mu,&du[f]);

            new(&wu[f]) WaveUpdater<T_Physic>(configuration.get(),nullptr,pequation[f]);
            new(&du[f]) DataUpdater<T_Physic>(configuration.get(),&wu[f],objective[f]);
            sue[f] = new SensitivityUpdaterFromEquation<T_Physic>(&wu[f],&du[f],pequation[f]);
        }
        SensitivityUpdaterFromVector su(n_freq,sue);

        for (unsigned int c = 0; c < parametrization->size(); c++)
        {
            save(su.get(Order::DIAG,Support::BLK,mu.get(needUpToDate))[c], configuration->model_unknown(Support::BLK), name+"_p"+suffix_g+"c"+std::to_string(c), "pos", "");
        }

        for (unsigned int f = 0; f < n_freq; f++)
        {
            delete pequation[f];
            delete equation[f];
            delete objective[f];
            delete sue[f];
        }
        delete[] sue;
        std::free(wu);
        std::free(du);
        msg::unindent();
    }
    msg::unindent();
    delete parametrization;

    return 0;
}

int main(int argc, char **argv)
{
  GmshFem gmshFem(argc, argv);
  Physic T_Physic = to_physic(gmshFem);
  switch (T_Physic)
  {
    case Physic::acoustic: default:
      return preconditioner<Physic::acoustic>(gmshFem);
    case Physic::electromagnetic:
      return preconditioner<Physic::electromagnetic>(gmshFem);
    case Physic::elastodynamic:
      return preconditioner<Physic::elastodynamic>(gmshFem);
  }
}
