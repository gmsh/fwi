// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
#include <array>

//GmshFem Library
#include "GmshFem.h"
#include "Exception.h"
#include "Message.h"

//Gmsh Library
#include "gmsh.h"

//GmshFWI Library
#include "antenna.h"

namespace gmodel = gmsh::model;
namespace factory = gmsh::model::geo;

using namespace gmshfem;
using namespace gmshfem::common;

/*
 * AntennaFactory
*/
namespace point
{
    Antenna AntennaFactory::addAntenna(double x0, double y0, double z0, double h, typename gmshfem::MathObject< double, gmshfem::Degree::Degree1 >::Object polarization) const
    {
        Antenna out;
        out.dim = dim();
        out.tag.push_back(factory::addPoint(x0, y0, z0, h));
        return out;
    }
}

namespace dipole
{
    AntennaFactory::AntennaFactory(const gmshfem::common::GmshFem& gmshFem, const std::string suffix)
    {
        if(!(
            gmshFem.userDefinedParameter(_r0, "antenna_r0") ||
            gmshFem.userDefinedParameter(_r0, "antenna_r0"+suffix)
        ))
        {
            throw common::Exception("Antenna radius could not be found.");
        }
    }
    Antenna AntennaFactory::addAntenna(double x0, double y0, double z0, double h, typename gmshfem::MathObject< double, gmshfem::Degree::Degree1 >::Object polarization) const
    {
        unsigned int p0 = factory::addPoint(x0, y0, z0, h);
        unsigned int p1 = factory::addPoint(x0+_r0*polarization[0], y0+_r0*polarization[1], z0, h);
        unsigned int p2 = factory::addPoint(x0-_r0*polarization[0], y0-_r0*polarization[1], z0, h);

        Antenna out;
        out.dim = dim();
        out.tag.push_back(factory::addLine(p0, p1));
        out.tag.push_back(factory::addLine(p0, p2));

        return out;
    }
}

/*
 * newAntenna
*/
AntennaFactoryInterface* newAntennaFactory(const gmshfem::common::GmshFem& gmshFem, const std::string suffix)
{
    std::string antenna = "point";
    if(!(
        gmshFem.userDefinedParameter(antenna, "antenna"+suffix) ||
        gmshFem.userDefinedParameter(antenna, "antenna")
    ))
    {
        msg::warning << "Antenna type could not be found. Point antenne (default) are used." << msg::endl;
    }
    if(antenna=="point"){return new point::AntennaFactory();}
    else if(antenna=="dipole"){return new dipole::AntennaFactory(gmshFem,suffix);}
    else
    {
        throw gmshfem::common::Exception("Antenna " + antenna + " is not valid.");
        return nullptr;
    }
}
