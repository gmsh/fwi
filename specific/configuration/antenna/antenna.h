// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_CONFIGURATION_ANTENNA
#define H_CONFIGURATION_ANTENNA

//GmshFEM Library
#include "GmshFem.h"

//GmshFWI Library

struct Antenna
{
    std::vector<int> tag;
    unsigned int dim;
};

class AntennaFactoryInterface
{
public:
    virtual ~AntennaFactoryInterface() = default;
    virtual Antenna addAntenna(double x0, double y0, double z0, double h, typename gmshfem::MathObject< double, gmshfem::Degree::Degree1 >::Object polarization = {0.,0.,0.}) const = 0;
    virtual unsigned int dim() const = 0;
};

namespace point
{
    class AntennaFactory: public AntennaFactoryInterface
    {
    public:
        virtual Antenna addAntenna(double x0, double y0, double z0, double h, typename gmshfem::MathObject< double, gmshfem::Degree::Degree1 >::Object polarization = {0.,0.,0.}) const;
        virtual unsigned int dim() const {return 0;};
    };
};

namespace dipole
{
    class AntennaFactory: public AntennaFactoryInterface
    {
    private:
        double _r0;
    public:
        AntennaFactory(const gmshfem::common::GmshFem& gmshFem, const std::string suffix="");
        virtual Antenna addAntenna(double x0, double y0, double z0, double h,typename gmshfem::MathObject< double, gmshfem::Degree::Degree1 >::Object polarization = {0.,0.,0.}) const;
        virtual unsigned int dim() const {return 1;};
    };
};

AntennaFactoryInterface* newAntennaFactory(const gmshfem::common::GmshFem& gmshFem, const std::string suffix = "");

#endif // H_CONFIGURATION_ANTENNA
