// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
#include <numeric>
#include <array>

//GmshFEM Library
#include "GmshFem.h"
#include "Exception.h"
#include "Message.h"
#include "Domain.h"
#include "Function.h"
#include "CSVio.h"

//Gmsh Library
#include "gmsh.h"

//GmshFWI Library
#include "../../common/model/macro.h"
#include "../../common/data/element.h"
#include "../wave/correlation.h"
#include "circular_acquisition.h"

namespace gmodel = gmsh::model;
namespace factory = gmsh::model::geo;

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::domain;
using namespace gmshfem::function;

static const std::complex< double > im = std::complex< double >(0., 1.);

namespace circular_acquisition
{
    UnknownRegion to_unknownregion(const GmshFem& gmshFem)
    {
        std::string str;
        if(! gmshFem.userDefinedParameter(str, "unknown") )
        {
        throw common::Exception("The unknown region to consider could not be found.");
        }
        if(str=="all") {return UnknownRegion::All;}
        else if(str=="inclusion") {return UnknownRegion::Inclusion;}
        else if(str=="background") {return UnknownRegion::Background;}
        else if(str=="none") {return UnknownRegion::None;}
        else
        {
        throw common::Exception("The unknown region " + str + " is not handled.");
        }
        return UnknownRegion::None;
    }
    /*
    *  class Configuration
    */
    Configuration::Configuration(std::string name, const ParametrizationInterface* const parametrization, const GmshFem& gmshFem) : ConfigurationInterface(name, parametrization, gmshFem)
    {
        msg::print << "Initialize inclusion configuration" << msg::endl;

        /*
        * MESH
        */
        if(
          !(
            gmshFem.userDefinedParameter(_rer, "rer") &&
            gmshFem.userDefinedParameter(_rext, "rext") &&
            gmshFem.userDefinedParameter(_np, "np") &&
            gmshFem.userDefinedParameter(_emitter_offset, "emitter_offset") &&
            gmshFem.userDefinedParameter(_receiver_offset, "receiver_offset") &&
            gmshFem.userDefinedParameter(_emitter_skip, "emitter_skip") &&
            gmshFem.userDefinedParameter(_receiver_skip, "receiver_skip") &&
            gmshFem.userDefinedParameter(_h, "h")
            )
        )
        {
          throw common::Exception("A geometric parameter could not be found.");
        }
        gmshFem.userDefinedParameter(_ni, "inclusion_ni");
        if(_ni!=0)
        {
            unsigned int filled = 0;
            if(!gmshFem.userDefinedParameter(filled, "inclusion_filled"))
            {
                throw common::Exception("Inclusion filling could not be found.");
            }
            _areFilled = ((bool) filled);

            _inclusion_geo.resize(_ni);
            for (unsigned int i = 0; i < _ni; i++)
            {
                _inclusion_geo[i] = newInclusion(gmshFem,std::to_string(i));
            }
        }
        unsigned int r_on_e = 0;
        gmshFem.userDefinedParameter(r_on_e, "receiver_on_emitter");
        _receiver_on_emitter = ((bool) r_on_e);

        _ne=0;
        _nr=0;
        for (unsigned int p = 0; p < _np; p++)
        {
            if( (p+_emitter_offset) % _emitter_skip == 0)
            {
                _ne++;
            }
            if( (p+_receiver_offset) % _receiver_skip == 0)
            {
                _nr++;
            }
        }
        _ns=_ne;

        mesh();

        /*
        * DOMAIN
        */
        _data_omega = Domain(2,1);

        _background[Support::BLK] = Domain(2,21);
        _background[Support::BND] = Domain(1,12);

        _inclusion.resize(_ni);
        for (unsigned int i = 0; i < _ni; i++)
        {
            if(_areFilled)
            {
                _inclusion[i][Support::BLK] = Domain(2,1000+i);
                _inclusions[Support::BLK] |= Domain(2,1000+i);
            }
            else
            {
                _inclusion[i][Support::BND] = Domain(1,2000+i);
                _inclusions[Support::BND] |= Domain(1,2000+i);
            }
        }

        _wave_omega[Support::BLK] = _background[Support::BLK] | _inclusions[Support::BLK];
        _wave_omega[Support::BND] = _background[Support::BND] | _inclusions[Support::BND];
        _unknown_region = to_unknownregion(gmshFem);
        switch (_unknown_region)
        {
          case UnknownRegion::Inclusion:
            _model_known = _background;
            _model_unknown = _inclusions;
            break;
          case UnknownRegion::Background:
            _model_known = _inclusions;
            _model_unknown = _background;
            break;
          case UnknownRegion::All:
            _model_unknown[Support::BLK] = _background[Support::BLK] | _inclusions[Support::BLK];
            _model_unknown[Support::BND] = _background[Support::BND] | _inclusions[Support::BND];
            break;
          case UnknownRegion::None: default:
            _model_known[Support::BLK] = _background[Support::BLK] | _inclusions[Support::BLK];
            _model_known[Support::BND] = _background[Support::BND] | _inclusions[Support::BND];
            break;
        }

        for (unsigned int p = 0; p < _np; p++)
        {
            _point.push_back(Domain(0,1000+p));
            _points |= Domain(0,1000+p);
            if( (p+_emitter_offset) % _emitter_skip == 0)
            {
                _emitter_idx.push_back(p);
            }
            if( (p+_receiver_offset) % _receiver_skip == 0)
            {
                _receiver_idx.push_back(p);
            }
        }
        for (unsigned int s = 0; s < _ns; s++)
        {
            _emitter.push_back({_emitter_idx[s]});
            if(_receiver_on_emitter)
            {
                _receiver.push_back(_receiver_idx);
            }
            else
            {
                std::vector<unsigned int> buffer = _receiver_idx;
                buffer.erase(std::remove(buffer.begin(), buffer.end(), _emitter_idx[s]), buffer.end());
                _receiver.push_back(buffer);
            }
        }

        /*
        * Reference model function
        */
        _mb.resize(model_size());
        _mi.resize(model_size());
        for (unsigned int c = 0; c < model_size(); c++)
        {
            std::string suffix = "c"+std::to_string(c);
            double Remb, Immb;
            if
            (!
                (
                    gmshFem.userDefinedParameter(Remb, "Re(mb"+suffix+")") &&
                    gmshFem.userDefinedParameter(Immb, "Im(mb"+suffix+")")
                )
            )
            {
                throw common::Exception("Background (reference) model parameter (component "+suffix+" ) could not be found.");
            }
            else
            {
                _mb[c] = Remb + im * Immb;
                _mc[c] = _mb[c];
            }
            ScalarPiecewiseFunction< std::complex< double > > m0;
            std::string m0_type = "constant";
            if(!gmshFem.userDefinedParameter(m0_type, "m0_type"+suffix))
            {
                msg::warning << "Reference model type could not be found. Constant background is used (default)." << msg::endl;
            }
            if(m0_type=="file")
            {
                std::string path = "";
                if(!gmshFem.userDefinedParameter(path, "m0_path"+suffix))
                {
                    throw common::Exception("Path to circular data could not be found.");
                }
                m0.addFunction(bilinearInterpolation(path),_background[Support::BLK] | _background[Support::BND] );
            }
            else if(m0_type=="file.pos")
            {
                std::string path = "";
                if(!gmshFem.userDefinedParameter(path, "m0_path"+suffix))
                {
                    throw common::Exception("Path to model file could not be found.");
                }

                gmsh::merge(path+suffix+".pos");
                ScalarFunction<std::complex<double>> mpos = probeScalarView<std::complex<double>>(c);

                m0.addFunction(mpos,_background[Support::BLK] | _background[Support::BND]);
            }
            else if(m0_type=="constant")
            {
                m0.addFunction(_mb[c],_background[Support::BLK] | _background[Support::BND]);
            }
            else
            {
                throw common::Exception("Initial model type ("+ suffix + ") " + m0_type +" is unknown.");
            }

            _mi[c].resize(_ni);
            for (unsigned int i = 0; i < _ni; i++)
            {
                double Remi=0., Immi=0.;
                if
                (
                    !(gmshFem.userDefinedParameter(Remi, "Re(mi"+std::to_string(i)+suffix+")") &&
                    gmshFem.userDefinedParameter(Immi, "Im(mi"+std::to_string(i)+suffix+")"))
                )
                {
                    throw Exception("Inclusion model parameter could not be found.");
                }
                _mi[c][i] = Remi + im * Immi;
                m0.addFunction(_mi[c][i],_inclusion[i][Support::BLK] | _inclusion[i][Support::BND]);
            }
            _m0.push_back(m0);
        }
    }


    void Configuration::wave_mesh() const
    {
        int p0 = factory::addPoint(0., 0., 0., _h);

        int pb1 = factory::addPoint(_rext, 0., 0., _h);
        int pb2 = factory::addPoint(0., _rext, 0., _h);
        int pb3 = factory::addPoint(-_rext, 0., 0., _h);
        int pb4 = factory::addPoint(0., -_rext, 0., _h);

        int lb1 = factory::addCircleArc(pb1, p0, pb2);
        int lb2 = factory::addCircleArc(pb2, p0, pb3);
        int lb3 = factory::addCircleArc(pb3, p0, pb4);
        int lb4 = factory::addCircleArc(pb4, p0, pb1);

        int clb = factory::addCurveLoop({lb1,lb2,lb3,lb4});

        //Vertical emitters
        std::vector<int> pnt(_np);
        double DAngle = 2. * M_PI /((double) _np);
        for (unsigned int p = 0; p < _np; p++)
        {
            double angle = ((double)p) * DAngle;
            pnt[p] = factory::addPoint(_rer*std::cos(angle), _rer*std::sin(angle), 0., _h);
        }

        int sb;
        std::vector<int> si;
        std::vector<std::vector<int>> li(_ni);
        std::vector<int> li_tot;
        if(_ni!=0)
        {
            std::vector<int> cli(_ni,0);
            if(_areFilled){si.resize(_ni);}
            for (unsigned int i = 0; i < _ni; i++)
            {
              li[i] = _inclusion_geo[i]->addInclusion();
              li_tot.insert(li_tot.end(), li[i].begin(), li[i].end());
              cli[i] = factory::addCurveLoop(li[i]);
              if(_areFilled)
              {
                  si[i] = factory::addPlaneSurface({cli[i]});
              }
            }
            std::vector<int> clbcli = cli;
            clbcli.insert(clbcli.begin(), clb);
            sb = factory::addPlaneSurface(clbcli);
        }
        else
        {
            sb = factory::addPlaneSurface({clb});
        }


        factory::synchronize();
        gmodel::mesh::embed(0, pnt, 2, sb);

        for (unsigned int p = 0; p <_np; p++)
        {
            gmodel::addPhysicalGroup(0, {pnt[p]}, 1000+p);
            gmodel::setPhysicalName(0, 1000+p, "emitter_receiver_"+std::to_string(p));
        }

        gmodel::addPhysicalGroup(1, {lb1,lb2,lb3,lb4}, 12);
        gmodel::setPhysicalName(1, 12, "background_bnd");
        gmodel::addPhysicalGroup(2, {sb}, 21);
        gmodel::setPhysicalName(2, 21, "background_vol");
        for (unsigned int i = 0; i < _ni; i++)
        {
            if(_areFilled)
            {
                gmodel::addPhysicalGroup(2, {si[i]}, 1000+i);
                gmodel::setPhysicalName(2, 1000+i, "inclusion_vol"+std::to_string(i));
            }
            else
            {
                gmodel::addPhysicalGroup(1, li[i], 2000+i);
                gmodel::setPhysicalName(1, 2000+i, "inclusion_bnd"+std::to_string(i));
            }
        }
    }
    void Configuration::data_mesh() const
    {
        double DAngle = 2. * M_PI /((double) _np);
        double Ds_e = DAngle * _emitter_skip * _rer;
        double Ds_r = DAngle * _receiver_skip * _rer;
        double Le0 = DAngle * _emitter_offset * _rer;
        double Lr0 = DAngle * _receiver_offset * _rer;
        double Le = (_ne-1) * Ds_e;
        double Lr = (_nr-1) * Ds_r;

        int p0 = factory::addPoint(Le0, Lr0, 1.);
        int p1 = factory::addPoint(Le0+Le, Lr0, 1.);
        int p2 = factory::addPoint(Le0+Le, Lr0+Lr, 1.);
        int p3 = factory::addPoint(Le0, Lr0+Lr, 1.);

        int l0 = factory::addLine(p0, p1);
        int l1 = factory::addLine(p1, p2);
        int l2 = factory::addLine(p2, p3);
        int l3 = factory::addLine(p3, p0);

        int cl = factory::addCurveLoop({l0,l1,l2,l3});
        int s1 = factory::addPlaneSurface({cl});

        factory::mesh::setTransfiniteCurve(l0,_ne);
        factory::mesh::setTransfiniteCurve(l1,_nr);
        factory::mesh::setTransfiniteCurve(l2,_ne);
        factory::mesh::setTransfiniteCurve(l3,_nr);
        factory::mesh::setTransfiniteSurface(s1,"Left",{p0,p1,p2,p3});

        factory::mesh::setRecombine(2,s1);

        gmodel::addPhysicalGroup(2, {s1}, 1);
        gmodel::setPhysicalName(2, 1, "data_omega");
    }

    std::array<unsigned int,2> Configuration::data_coordinate_to_index(double xs, double xr) const
    {
        if( (!_receiver_on_emitter) )
        {
            throw Exception("Data space when an emitter is not also a receiver is not implemented yet");
        }
        std::array<unsigned int,2> index;

        double DAngle = 2. * M_PI /((double) _np);
        double Ds_e = DAngle * _emitter_skip * _rer;
        double Ds_r = DAngle * _receiver_skip * _rer;
        double Le0 = DAngle * _emitter_offset * _rer;
        double Lr0 = DAngle * _receiver_offset * _rer;
        index[0] = std::round( (xs-Le0) / Ds_e );
        index[1] = std::round( (xr-Lr0) / Ds_r );

        return index;
    }

    std::array<double,2> Configuration::index_to_data_coordinate(unsigned int s, unsigned int r) const
    {
        if( (!_receiver_on_emitter) )
        {
            throw Exception("Data space when an emitter is not also a receiver is not implemented yet");
        }

        std::array<double,2> index;

        double DAngle = 2. * M_PI /((double) _np);
        double Ds_e = DAngle * _emitter_skip * _rer;
        double Ds_r = DAngle * _receiver_skip * _rer;
        double Le0 = DAngle * _emitter_offset * _rer;
        double Lr0 = DAngle * _receiver_offset * _rer;
        index[0] = Le0 + ((double)s) * Ds_e;
        index[1] = Lr0 + ((double)r) * Ds_r;

        return index;
    }

    double Configuration::data_area() const
    {
        double DAngle = 2. * M_PI /((double) _np);
        double Ds_e = DAngle * _emitter_skip * _rer;
        double Ds_r = DAngle * _receiver_skip * _rer;
        double Le = (_ne-1) * Ds_e;
        double Lr = (_nr-1) * Ds_r;

        return Le * Lr;
    };

    double Configuration::datapoint_area() const
    {
        double DAngle = 2. * M_PI /((double) _np);
        double Ds_e = DAngle * _emitter_skip * _rer;
        double Ds_r = DAngle * _receiver_skip * _rer;

        return Ds_e * Ds_r;
    };

    bool Configuration::data_coordinate_isValid(double xs,double xr) const
    {
        double DAngle = 2. * M_PI /((double) _np);
        double Ds_e = DAngle * _emitter_skip * _rer;
        double Ds_r = DAngle * _receiver_skip * _rer;
        double Le0 = DAngle * _emitter_offset * _rer;
        double Lr0 = DAngle * _receiver_offset * _rer;
        double Le = (_ne-1) * Ds_e;
        double Lr = (_nr-1) * Ds_r;

        double eps = 1e-8;
        gmsh::option::getNumber("Geometry.MatchMeshTolerance",eps);
        if( (Le0-eps <= xs) && (xs <= Le0+Le+eps) && (Lr0-eps <= xr) && (xr <= Lr0+Lr+eps) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    template<Physic T_Physic>
    ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd, const WaveMultiField<T_Physic>& g, const Configuration* const config)
    {
        return autocorrelate<T_Physic>(g,config->emitter_idx())*autocorrelate<T_Physic>(g,config->receiver_idx());
    }

    template ModelMonoFunction green0_preconditioner<Physic::acoustic>(const Data<Physic::acoustic>&, const WaveMultiField<Physic::acoustic>&, const Configuration* const);
    template ModelMonoFunction green0_preconditioner<Physic::electromagnetic>(const Data<Physic::electromagnetic>&, const WaveMultiField<Physic::electromagnetic>&, const Configuration* const);
    template ModelMonoFunction green0_preconditioner<Physic::elastodynamic>(const Data<Physic::elastodynamic>&, const WaveMultiField<Physic::elastodynamic>&, const Configuration* const);

} // namespace circular_acquisition
