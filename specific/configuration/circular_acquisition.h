// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_CONFIGURATION_CIRCULAR_ACQUISITION
#define H_CONFIGURATION_CIRCULAR_ACQUISITION

//GmshFEM Library
#include "GmshFem.h"
//#include "Function.h"

//GmshFWI Library
#include "../../common/configuration.h"
#include "../../common/wave/element.h"
#include "inclusion/inclusion.h"

//Forward declaration
template<Physic T_Physic>
class Data;

namespace circular_acquisition
{
    enum class UnknownRegion
    {
        None,
        Inclusion,
        Background,
        All
    };
    UnknownRegion to_unknownregion(const gmshfem::common::GmshFem& gmshFem);

    class Configuration final : public ConfigurationInterface
    {
    private:
        double _rer;
        double _rext;

        unsigned int _ne;
        unsigned int _nr;
        unsigned int _emitter_offset;
        unsigned int _receiver_offset;
        unsigned int _emitter_skip;
        unsigned int _receiver_skip;
        bool _receiver_on_emitter;

        unsigned int _ni;
        bool _areFilled;
        std::vector<const InclusionInterface*> _inclusion_geo;

        double _h;

        UnknownRegion _unknown_region;
        std::array<gmshfem::domain::Domain,2> _background;
        std::array<gmshfem::domain::Domain,2> _inclusions;
        std::vector<std::array<gmshfem::domain::Domain,2>> _inclusion;

        std::vector<unsigned int> _emitter_idx;
        std::vector<unsigned int> _receiver_idx;

        std::vector<std::complex<double>> _mb;
        std::vector<std::vector<std::complex<double>>> _mi;

        virtual void wave_mesh() const;
        virtual void data_mesh() const;
    public:
        Configuration(std::string name, const ParametrizationInterface* const parametrization, const gmshfem::common::GmshFem& gmshFem);

        const std::vector<unsigned int>& emitter_idx() const {return _emitter_idx;};
        const std::vector<unsigned int>& receiver_idx() const {return _receiver_idx;};

        bool areFilled() const {return _areFilled;};
        unsigned int ner() const {return _np;};


        virtual std::array<unsigned int,2> data_coordinate_to_index(double xs, double xr) const;
        virtual std::array<double,2> index_to_data_coordinate(unsigned int s, unsigned int r) const;
        virtual bool data_coordinate_isValid(double xs,double xr) const;

        virtual double area() const {return M_PI * _rext * _rext;};
        virtual double data_area() const override;
        virtual double datapoint_area() const;
        virtual double array() const {return 2. * M_PI * _rer;};
        virtual double depth() const {return _rer;};

        virtual std::string wave_gmodel() const {return _name;};
        virtual std::string model_gmodel() const {return _name;};
        virtual std::string data_gmodel() const {return _name;};
    };

    template<Physic T_Physic>
    ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd, const WaveMultiField<T_Physic>& g, const Configuration* const config);
} // namespace soil

#endif // H_CONFIGURATION_CIRCULAR_ACQUISITION
