#ifndef H_CONFIGURATION_FLEXIBLE_ACQUISITION
#define H_CONFIGURATION_FLEXIBLE_ACQUISITION

//GmshFEM Library
#include "GmshFem.h"
//#include "Function.h"

//GmshFWI Library
#include "../../common/configuration.h"
#include "../../common/wave/element.h"
#include "inclusion/inclusion.h"

//Forward declaration
template<Physic T_Physic>
class Data;

namespace flexible_acquisition
{

    class Configuration final : public ConfigurationInterface
    {
    private:
    
        /**
         * Emetters and receivers are stored together, and for each shot, one point is set as emitter, 
         * and some others (potentially including the emitter) are enabled as receivers
        */
        std::vector<std::pair<double, double>> _er_positions;

        struct ShotConfig {
            unsigned int emetterIdx;
            std::vector<unsigned int> enabledReceivers;
        };

        std::vector<ShotConfig> _shotsConfigs;

        /* Domain is a rectangle. "x" goes from 0 to L and "y" from -H to 0. Known part is y between 0 and -Hsub*/
        double _H;
        double _L;
        double _Hsub;

        // Mesh resolution
        double _h;


        bool _areFilled;

        // Subdomains: all in the parent class
    /*
        double _rer;
        double _rext;

        unsigned int _ne;
        unsigned int _nr;
        unsigned int _emitter_offset;
        unsigned int _receiver_offset;
        unsigned int _emitter_skip;
        unsigned int _receiver_skip;
        bool _receiver_on_emitter;

        unsigned int _ni;
        
        std::vector<const InclusionInterface*> _inclusion_geo;


        UnknownRegion _unknown_region;
        std::array<gmshfem::domain::Domain,2> _background;
        std::array<gmshfem::domain::Domain,2> _inclusions;
        std::vector<std::array<gmshfem::domain::Domain,2>> _inclusion;



        std::vector<std::complex<double>> _mb;
        std::vector<std::vector<std::complex<double>>> _mi;
*/

        std::array<gmshfem::domain::Domain, 2> _subsurface;
        std::array<gmshfem::domain::Domain, 2> _supersurface;

        std::vector<std::complex<double>> _m_super;
        std::vector<std::complex<double>> _m_sub;

        virtual void wave_mesh() const;
        virtual void data_mesh() const;


        void setupInitialModel(const ParametrizationInterface* const parametrization, const gmshfem::common::GmshFem& gmshFem);
    public:
        Configuration(std::string name, const ParametrizationInterface* const parametrization, const gmshfem::common::GmshFem& gmshFem);


        bool areFilled() const {return _areFilled;};
        unsigned int ner() const {return _np;};

//        virtual void mesh() const;

        virtual std::array<unsigned int,2> data_coordinate_to_index(double xs, double xr) const;
        virtual std::array<double,2> index_to_data_coordinate(unsigned int s, unsigned int r) const;
        virtual bool data_coordinate_isValid(double xs,double xr) const;

        virtual double area() const {return _H * _L;};
        virtual double data_area() const override;
        virtual double datapoint_area() const;
        virtual double array() const {return 2. * (_L + _H);};
        virtual double depth() const {return _H;};

        virtual std::string wave_gmodel() const {return _name;};
        virtual std::string model_gmodel() const {return _name;};
        virtual std::string data_gmodel() const {return _name;};
    };

    template<Physic T_Physic>
    ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd, const WaveMultiField<T_Physic>& g, const Configuration* const config);
} // namespace soil

#endif // H_CONFIGURATION_FLEXIBLE_ACQUISITION
