// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_GREEN0_PRECONDITIONER
#define H_GREEN0_PRECONDITIONER

//Standard Library

//GmshFEM Library
#include "Exception.h"

//GmshFWI Library
#include "surface_acquisition.h"
#include "volume_acquisition.h"
#include "layeredvolume_acquisition.h"
#include "line_acquisition.h"
#include "rectangular_acquisition.h"
#include "circular_acquisition.h"
#include "flexible_acquisition.h"

using namespace gmshfem;
using namespace gmshfem::common;

template<Physic T_Physic>
ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd, const WaveMultiField<T_Physic>& g, const ConfigurationInterface* const config)
{
    if (const surface_acquisition::Configuration* const surface_acquisition_config = dynamic_cast<const surface_acquisition::Configuration* const>(config))
    {
        return surface_acquisition::green0_preconditioner(dd,g,surface_acquisition_config);
    }
    else if (const volume_acquisition::Configuration* const volume_acquisition_config = dynamic_cast<const volume_acquisition::Configuration* const>(config))
    {
        return volume_acquisition::green0_preconditioner(dd,g,volume_acquisition_config);
    }
    else if (const layeredvolume_acquisition::Configuration* const inclusion_config = dynamic_cast<const layeredvolume_acquisition::Configuration* const>(config))
    {
        return layeredvolume_acquisition::green0_preconditioner(dd,g,inclusion_config);
    }
    else if (const line_acquisition::Configuration* const inclusion_config = dynamic_cast<const line_acquisition::Configuration* const>(config))
    {
        return line_acquisition::green0_preconditioner(dd,g,inclusion_config);
    }
    else if (const rectangular_acquisition::Configuration* const rectangular_acquisition_config = dynamic_cast<const rectangular_acquisition::Configuration* const>(config))
    {
        return rectangular_acquisition::green0_preconditioner(dd,g,rectangular_acquisition_config);
    }
    else if (const circular_acquisition::Configuration* const circular_acquisition_config = dynamic_cast<const circular_acquisition::Configuration* const>(config))
    {
        return circular_acquisition::green0_preconditioner(dd,g,circular_acquisition_config);
    }
    else
    {
        throw Exception("Dynamic cast to specific configuration has failed.");
        return ModelMonoFunction();
    }
}

#endif // H_GREEN0_PRECONDITIONER
