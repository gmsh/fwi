// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
//#include <numeric>
#include <array>

//GmshFem Library
#include "GmshFem.h"
#include "Exception.h"
//#include "Message.h"
//#include "Domain.h"
//#include "Function.h"
//#include "Formulation.h"
//#include "CSVio.h"
//Gmsh Library
#include "gmsh.h"
//GmshFWI Library
#include "inclusion.h"

namespace gmodel = gmsh::model;
namespace factory = gmsh::model::geo;

using namespace gmshfem;
using namespace gmshfem::common;
//using namespace gmshfem::domain;

/*
 * InclusionInterface
*/
InclusionInterface::InclusionInterface(const gmshfem::common::GmshFem& gmshFem, const std::string suffix)
{
    if(
      !(
        gmshFem.userDefinedParameter(_x0, "inclusion_x0i"+suffix) &&
        gmshFem.userDefinedParameter(_y0, "inclusion_y0i"+suffix) &&
        gmshFem.userDefinedParameter(_r, "inclusion_ri"+suffix) &&
        (gmshFem.userDefinedParameter(_h, "h") || gmshFem.userDefinedParameter(_h, "inclusion_hi"+suffix))
        )
    )
    {
      throw common::Exception("Inclusion #"+suffix+" parameter could not be found.");
    }
}

/*
 * Inclusion
*/
namespace cross
{
    std::vector<int> Inclusion::addInclusion() const
    {
        std::array<int,12> px;
        px[0] = factory::addPoint(_x0+_r,_y0+_r/4., 0., _h);
        px[1] = factory::addPoint(_x0+_r/4,_y0+_r/4., 0., _h);
        px[2] = factory::addPoint(_x0+_r/4,_y0+_r, 0., _h);
        px[3] = factory::addPoint(_x0-_r/4,_y0+_r, 0., _h);
        px[4] = factory::addPoint(_x0-_r/4,_y0+_r/4., 0., _h);
        px[5] = factory::addPoint(_x0-_r,_y0+_r/4., 0., _h);
        px[6] = factory::addPoint(_x0-_r,_y0-_r/4., 0., _h);
        px[7] = factory::addPoint(_x0-_r/4.,_y0-_r/4., 0., _h);
        px[8] = factory::addPoint(_x0-_r/4.,_y0-_r, 0., _h);
        px[9] = factory::addPoint(_x0+_r/4.,_y0-_r, 0., _h);
        px[10] = factory::addPoint(_x0+_r/4.,_y0-_r/4., 0., _h);
        px[11] = factory::addPoint(_x0+_r,_y0-_r/4., 0., _h);

        std::vector<int> lx(12,0);
        for (unsigned int i = 0; i < 11; i++)
        {
            lx[i] = factory::addLine(px[i], px[i+1]);
        }
        lx[11] = factory::addLine(px[11], px[0]);

        return lx;
    }
}

namespace cylinder
{
    std::vector<int> Inclusion::addInclusion() const
    {
        int pc0 = factory::addPoint(_x0, _y0, 0., _h);
        int pc1 = factory::addPoint(_x0+_r, _y0, 0., _h);
        int pc2 = factory::addPoint(_x0, _y0+_r, 0., _h);
        int pc3 = factory::addPoint(_x0-_r, _y0, 0., _h);
        int pc4 = factory::addPoint(_x0, _y0-_r, 0., _h);

        int lc1 = factory::addCircleArc(pc1, pc0, pc2);
        int lc2 = factory::addCircleArc(pc2, pc0, pc3);
        int lc3 = factory::addCircleArc(pc3, pc0, pc4);
        int lc4 = factory::addCircleArc(pc4, pc0, pc1);

        std::vector<int> lc = {lc1,lc2,lc3,lc4};
        return lc;
    }
}

/*
 * newInclusion
*/
InclusionInterface* newInclusion(const gmshfem::common::GmshFem& gmshFem, const std::string suffix)
{
    std::string inclusion;
    if(!(
        gmshFem.userDefinedParameter(inclusion, "inclusion"+suffix) ||
        gmshFem.userDefinedParameter(inclusion, "inclusion")
    ))
    {
      throw gmshfem::common::Exception("Inclusion type could not be found.");
    }
    if(inclusion=="cross"){return new cross::Inclusion(gmshFem,suffix);}
    else if(inclusion=="cylinder"){return new cylinder::Inclusion(gmshFem,suffix);}
    else
    {
        throw gmshfem::common::Exception("Inclusion " + inclusion + " is not valid.");
        return NULL;
    }
}
