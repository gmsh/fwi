// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_CONFIGURATION_INCLUSION
#define H_CONFIGURATION_INCLUSION

//GmshFEM Library
#include "GmshFem.h"

//GmshFWI Library

class InclusionInterface
{
protected:
    double _x0;
    double _y0;
    double _r;
    double _h;
public:
    InclusionInterface(const gmshfem::common::GmshFem& gmshFem, const std::string = "");
    virtual ~InclusionInterface() = default;
    virtual std::vector<int> addInclusion() const = 0;
};

namespace cross
{
    class Inclusion: public InclusionInterface
    {
    public:
        Inclusion(const gmshfem::common::GmshFem& gmshFem, const std::string suffix) : InclusionInterface::InclusionInterface(gmshFem,suffix) {};
        virtual std::vector<int> addInclusion() const;
    };
};

namespace cylinder
{
    class Inclusion: public InclusionInterface
    {
    public:
        Inclusion(const gmshfem::common::GmshFem& gmshFem, const std::string suffix) : InclusionInterface::InclusionInterface(gmshFem,suffix) {};
        virtual std::vector<int> addInclusion() const;
    };
};

InclusionInterface* newInclusion(const gmshfem::common::GmshFem& gmshFem, const std::string suffix = "");

#endif // H_CONFIGURATION_INCLUSION
