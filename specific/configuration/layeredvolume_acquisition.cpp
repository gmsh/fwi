// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
#include <numeric>
//GmshFem Library
#include "GmshFem.h"
#include "Exception.h"
#include "Message.h"
#include "Domain.h"
#include "Function.h"
//#include "Formulation.h"
#include "CSVio.h"
//Gmsh Library
#include "gmsh.h"
//GmshFWI library
#include "../../common/model/macro.h"
#include "../wave/correlation.h"
#include "layeredvolume_acquisition.h"

namespace gmodel = gmsh::model;
namespace factory = gmsh::model::geo;

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::domain;
using namespace gmshfem::function;

static const std::complex< double > im = std::complex< double >(0., 1.);

namespace layeredvolume_acquisition
{
    Layer to_layer(std::string layer)
    {
        if(layer=="constant"){return Layer::Constant;}
        else if(layer=="file"){return Layer::File;}
        else
        {
            throw common::Exception("Unknown layer type.");
        }
    }
    std::vector<std::array<double,2>> to_layercoord(const GmshFem& gmshFem,unsigned int l)
    {
        std::string filename;
        if(!gmshFem.userDefinedParameter(filename, "layercoord"+std::to_string(l)) )
        {
            throw common::Exception("Layer coordinate file could not be found");
        }

        CSVio file(filename,' ',OpeningMode::Reading);
        unsigned int np=0;
        file >> np;
        if( !file.isEOL() )
        {
         throw Exception(filename+" does not have the appropriate format: first line has more than one value.");
        }
        std::vector<double> x(np,0.);
        for (unsigned int j = 0; j < np; j++)
        {
         file >> x[j];
         if( file.isEOL() ){break;}
        }

        std::vector<double> y(np,0.);
        for (unsigned int i = 0; i < np; i++)
        {
         file >> y[i];
         if( file.isEOL() ){break;}
        }

        std::vector<std::array<double,2>> layercoord(np,{0.,0.});
        for (unsigned int i = 0; i < np; i++)
        {
            layercoord[i][0] = x[i];
            layercoord[i][1] = y[i];
        }
        return layercoord;
    }

    UnknownRegion to_unknownregion(const GmshFem& gmshFem)
    {
        std::string str;
        if(! gmshFem.userDefinedParameter(str, "unknown") )
        {
        throw common::Exception("The unknown region to consider could not be found.");
        }
        if(str=="all") {return UnknownRegion::All;}
        else if(str=="none") {return UnknownRegion::None;}
        else
        {
        throw common::Exception("The unknown region " + str + " is not handled.");
        }
        return UnknownRegion::None;
    }
    /*
    *  class Configuration
    */
    Configuration::Configuration(std::string name, const ParametrizationInterface* const parametrization, const GmshFem& gmshFem) : ConfigurationInterface(name, parametrization, gmshFem)
    {
        msg::print << "Initialize layered volume acquisition configuration" << msg::endl;

        /*
        * MESH
        */
        std::string layer;
        if(
          !(
            gmshFem.userDefinedParameter(_xe, "xe") &&
            gmshFem.userDefinedParameter(_xr, "xr") &&
            gmshFem.userDefinedParameter(_ye, "ye") &&
            gmshFem.userDefinedParameter(_yr, "yr") &&
            gmshFem.userDefinedParameter(_He, "He") &&
            gmshFem.userDefinedParameter(_Hr, "Hr") &&
            gmshFem.userDefinedParameter(_Le, "Le") &&
            gmshFem.userDefinedParameter(_Lr, "Lr") &&
            gmshFem.userDefinedParameter(_nxe, "nxe") &&
            gmshFem.userDefinedParameter(_nxr, "nxr") &&
            gmshFem.userDefinedParameter(_nye, "nye") &&
            gmshFem.userDefinedParameter(_nyr, "nyr") &&
            gmshFem.userDefinedParameter(_L, "L") &&
            gmshFem.userDefinedParameter(_H, "H") &&
            gmshFem.userDefinedParameter(_h, "h") &&
            gmshFem.userDefinedParameter(_H_L1, "H_L1") &&
            gmshFem.userDefinedParameter(_H_L2, "H_L2") &&
            gmshFem.userDefinedParameter(_H_01, "H_01") &&
            gmshFem.userDefinedParameter(_H_02, "H_02") &&
            gmshFem.userDefinedParameter(layer, "layer")
            )
        )
        {
          throw common::Exception("A geometric parameter could not be found.");
        }
        gmshFem.userDefinedParameter(_ni, "inclusion_ni");
        if(_ni!=0)
        {
            unsigned int filled = 0;
            if(!gmshFem.userDefinedParameter(filled, "inclusion_filled"))
            {
                throw common::Exception("Inclusion filling could not be found.");
            }
            _areFilled = ((bool) filled);

            _inclusion_geo.resize(_ni);
            for (unsigned int i = 0; i < _ni; i++)
            {
                _inclusion_geo[i] = newInclusion(gmshFem,std::to_string(i));
            }
        }

        //Layers
        _layer = to_layer(layer);
        switch (_layer)
        {
            case Layer::Constant:
                _layercoord1.push_back({0.,_H_01});
                _layercoord1.push_back({_L,_H_L1});
                _layercoord2.push_back({0.,_H_02});
                _layercoord2.push_back({_L,_H_L2});
                break;
            case Layer::File:
                _layercoord1 = to_layercoord(gmshFem,1);
                if( (_layercoord1.front()[0] != 0.) || (_layercoord1.back()[0] != _L) )
                {
                    throw common::Exception("Layer coordinate 1 does not span [0,L].");
                }
                for (auto it = _layercoord1.begin(); it != _layercoord1.end(); it++)
                {
                    (*it)[1] += _H_01 + (_H_L1-_H_01)/_L * (*it)[0];
                }
                _layercoord2 = to_layercoord(gmshFem,2);
                if( (_layercoord2.front()[0] != 0.) || (_layercoord2.back()[0] != _L) )
                {
                    throw common::Exception("Layer coordinate 2 does not span [0,L].");
                }
                for (auto it = _layercoord2.begin(); it != _layercoord2.end(); it++)
                {
                    (*it)[1] += _H_02 + (_H_L2-_H_02)/_L * (*it)[0];
                }
                break;
        }

        //Emitters number in layers
        unsigned int p=0;
        while( _layercoord1[p][0] < _xe ){p++;};
        double _Hxe1p = _layercoord1[p][1];
        double _Hxe1p_1 = _layercoord1[p-1][1];
        _Hxe1 = _Hxe1p + (_Hxe1p_1-_Hxe1p) / (_layercoord1[p-1][0]-_layercoord1[p][0]) * (_xe-_layercoord1[p][0]);

        p=0;
        while( _layercoord2[p][0] < _xe ){p++;};
        double _Hxe2p = _layercoord2[p][1];
        double _Hxe2p_1 = _layercoord2[p-1][1];
        _Hxe2 = _Hxe2p + (_Hxe2p_1-_Hxe2p) / (_layercoord2[p-1][0]-_layercoord2[p][0]) * (_xe-_layercoord2[p][0]);

        double He0 = (_H - _He)/2.;
        double deltaHe = _He/(((double) _nxe)-1.);
        for (unsigned int e = 0; e < _nxe; e++)
        {
            double He = He0 + e*deltaHe;
            if(He>_Hxe2){_nxeb[2]++;}
            else if(He>_Hxe1){_nxeb[1]++;}
            else if(He>=He0){_nxeb[0]++;}
        }

        //Receivers number in layers
        p=0;
        while( _layercoord1[p][0] < _xr ){p++;};
        double _Hxr1p = _layercoord1[p][1];
        double _Hxr1p_1 = _layercoord1[p-1][1];
        _Hxr1 = _Hxr1p + (_Hxr1p_1-_Hxr1p) / (_layercoord1[p-1][0]-_layercoord1[p][0]) * (_xr-_layercoord1[p][0]);

        p=0;
        while( _layercoord2[p][0] < _xr ){p++;};
        double _Hxr2p = _layercoord2[p][1];
        double _Hxr2p_1 = _layercoord2[p-1][1];
        _Hxr2 = _Hxr2p + (_Hxr2p_1-_Hxr2p) / (_layercoord2[p-1][0]-_layercoord2[p][0]) * (_xr-_layercoord2[p][0]);

        double Hr0 = (_H - _Hr)/2.;
        double deltaHr = _Hr/(((double) _nxr)-1.);
        for (unsigned int r = 0; r < _nxr; r++)
        {
            double Hr = Hr0 + r*deltaHr;
            if(Hr>_Hxr2){_nxrb[2]++;}
            else if(Hr>_Hxr1){_nxrb[1]++;}
            else if(Hr>=Hr0){_nxrb[0]++;}
        }
        unsigned int r_on_e = 0;
        gmshFem.userDefinedParameter(r_on_e, "receiver_on_emitter");
        _receiver_on_emitter = ((bool) r_on_e);


        mesh();

        /*
        * DOMAIN
        */
        if(nxe() != 0){_data_omega |= Domain(2,1);}
        if(nye() != 0){_data_omega |= Domain(2,2);}

        for (unsigned int b = 0; b < 3; b++)
        {
            _background[b][Support::BLK] = Domain(2,20+b);
            _background[b][Support::BND] = Domain(1,10+b);
            _wave_omega[Support::BLK] |= _background[b][Support::BLK];
            _wave_omega[Support::BND] |= _background[b][Support::BND];
        }

        _inclusion.resize(_ni);
        for (unsigned int i = 0; i < _ni; i++)
        {
            if(_areFilled)
            {
                _inclusion[i][Support::BLK] = Domain(2,1000+i);
                _inclusions[Support::BLK] |= Domain(2,1000+i);
            }
            else
            {
                _inclusion[i][Support::BND] = Domain(1,2000+i);
                _inclusions[Support::BND] |= Domain(1,2000+i);
            }
            //_wave_omega[Support::BLK] |= _cylinder[c];
        }
        _wave_omega[Support::BLK] |= _inclusions[Support::BLK];
        _wave_omega[Support::BND] |= _inclusions[Support::BND];

        _unknown_region = to_unknownregion(gmshFem);
        switch (_unknown_region)
        {
          case UnknownRegion::All:
            _model_unknown[Support::BLK] = _wave_omega[Support::BLK];
            _model_unknown[Support::BND] = _wave_omega[Support::BND];
            break;
          case UnknownRegion::None: default:
            _model_known[Support::BLK] = _wave_omega[Support::BLK];
            _model_known[Support::BND] = _wave_omega[Support::BND];
            break;
        }

        _ns = _nxe+_nye;
        _np = _nxe+_nye;
        //Emitters X
        unsigned int e=0;
        for (unsigned int b = 0; b < 3; b++)
        {
            for (unsigned int p = 0; p < _nxeb[b]; p++)
            {
                _point.push_back(Domain(0,(b+1)*1000+e));
                _pointsb[b] |= Domain(0,(b+1)*1000+e);
                e++;
            }
        }
        _emitter_idx_X.resize(_nxe);
        std::iota(_emitter_idx_X.begin(),_emitter_idx_X.end(),0);

        //Emitters Y
        for (unsigned int e = 0; e < _nye; e++)
        {
            _point.push_back(Domain(0,7000+e));
        }
        _emitter_idx_Y.resize(_nye);
        std::iota(_emitter_idx_Y.begin(),_emitter_idx_Y.end(),_nxe);

        //Receivers X
        if(!receiversAreEmittersX())
        {
          _np+=_nxr;
          unsigned int r=0;
          for (unsigned int b = 0; b < 3; b++)
          {
              for (unsigned int p = 0; p < _nxrb[b]; p++)
              {
                  _point.push_back(Domain(0,(b+4)*1000+r));
                  _pointsb[b] |= Domain(0,(b+4)*1000+r);
                  r++;
              }
          }
          _receiver_idx_X.resize(_nxr);
          std::iota(_receiver_idx_X.begin(),_receiver_idx_X.end(),_nxe);
        }
        else
        {
            _receiver_idx_X = _emitter_idx_X;
        }

        //Receivers Y
        if(!receiversAreEmittersY())
        {
          _np+=_nyr;
          for (unsigned int r = 0; r < _nyr; r++)
          {
            _point.push_back(Domain(0,8000+r));
          }
          _receiver_idx_Y.resize(_nyr);
          std::iota(_receiver_idx_Y.begin(),_receiver_idx_Y.end(),_nxe+_nye+_nxr);
        }
        else
        {
            _receiver_idx_Y = _emitter_idx_Y;
        }

        for (unsigned int p = 0; p < _np; p++)
        {
          _points |= _point[p];
        }

        for (unsigned int s = 0; s < _nxe; s++)
        {
          _emitter.push_back({s});
          std::vector<unsigned int> rec(_nxr,0);
          if(receiversAreEmittersX())
          {
            std::iota(rec.begin(),rec.end(),0);
            if(!_receiver_on_emitter)
            {
                rec.erase(rec.begin()+s);
            }
            _receiver.push_back(rec);
          }
          else
          {
            std::iota(rec.begin(),rec.end(),_nxe+_nye);
            _receiver.push_back(rec);
          }
        }
        for (unsigned int s = _nxe; s < _nxe+_nye; s++)
        {
          _emitter.push_back({s});
          std::vector<unsigned int> rec(_nyr,0);
          if(receiversAreEmittersY())
          {
            std::iota(rec.begin(),rec.end(),_nxe);
            if(!_receiver_on_emitter)
            {
                rec.erase(rec.begin()+s-_nxe);
            }
            _receiver.push_back(rec);
          }
          else
          {
            unsigned int _n = _nxe+_nye;
            if(!receiversAreEmittersX()){_n+=_nxr;}
            std::iota(rec.begin(),rec.end(),_n);
            _receiver.push_back(rec);
          }
        }

        /*
        * Reference model function
        */
        _mi.resize(model_size());
        _mb.resize(model_size());
        for (unsigned int c = 0; c < model_size(); c++)
        {
            /* Layers */
            for (unsigned int b = 0; b < 3; b++)
            {
                double Rem=0.;
                double Imm=0.;
                std::string suffix = "b"+std::to_string(b)+"c"+std::to_string(c);
                if
                (
                    !(
                        gmshFem.userDefinedParameter(Rem, "Re(m"+suffix+")") &&
                        gmshFem.userDefinedParameter(Imm, "Im(m"+suffix+")")
                    )
                )
                {
                    throw common::Exception("Background #"+suffix+" model parameter could not be found.");
                }
                else
                {
                    _mb[c][b] = Rem + im * Imm;
                }
            }
            _mc[c] = _mb[c][1];

            _mi[c].resize(_ni);
            for (unsigned int i = 0; i < _ni; i++)
            {
                double Rem=0.;
                double Imm=0.;
                std::string suffix = "i"+std::to_string(i)+"c"+std::to_string(c);
                if
                (
                    !(
                        gmshFem.userDefinedParameter(Rem, "Re(m"+suffix+")") &&
                        gmshFem.userDefinedParameter(Imm, "Im(m"+suffix+")")
                    )
                )
                {
                    throw common::Exception("Inclusion #"+suffix+" model parameter could not be found.");
                }
                _mi[c][i] = Rem + im * Imm;
            }

            ScalarPiecewiseFunction< std::complex< double > > m0;
            for (unsigned int b = 0; b < 3; b++)
            {
                std::string DRembfile;
                std::string DImmbfile;
                std::string suffix = "b"+std::to_string(b)+"c"+std::to_string(c);
                if
                (
                    (
                        gmshFem.userDefinedParameter(DRembfile, "DRe(m"+suffix+")_file") &&
                        gmshFem.userDefinedParameter(DImmbfile, "DIm(m"+suffix+")_file")
                    )
                )
                {
                    m0.addFunction(_mb[c][b]+bilinearInterpolation(DRembfile)+im*bilinearInterpolation(DImmbfile),_background[b][Support::BLK] | _background[b][Support::BND] | _pointsb[b] );
                }
                else if
                (
                    (
                        gmshFem.userDefinedParameter(DRembfile, "DRe(m"+suffix+")_file")
                    )
                )
                {
                    m0.addFunction(_mb[c][b]+bilinearInterpolation(DRembfile),_background[b][Support::BLK] | _background[b][Support::BND] | _pointsb[b] );
                }
                else if
                (
                    (
                        gmshFem.userDefinedParameter(DImmbfile, "DIm(m"+suffix+")_file")
                    )
                )
                {
                    m0.addFunction(_mb[c][b]+im*bilinearInterpolation(DImmbfile),_background[b][Support::BLK] | _background[b][Support::BND] | _pointsb[b] );
                }
                else
                {
                    m0.addFunction(_mb[c][b],_background[b][Support::BLK] | _background[b][Support::BND] | _pointsb[b] );
                }
            }
            for (unsigned int i = 0; i < _ni; i++)
            {
                m0.addFunction(_mi[c][i],_inclusion[i][Support::BLK] | _inclusion[i][Support::BND]);
            }
            _m0.emplace_back(m0);
        }
    }

    void Configuration::wave_mesh() const
    {
        std::vector<int> pl1(_layercoord1.size());
        for (unsigned int p = 0; p < _layercoord1.size(); p++)
        {
            pl1[p] = factory::addPoint(_layercoord1[p][0], _layercoord1[p][1], 0., _h);
        }
        std::vector<int> pl2(_layercoord2.size());
        for (unsigned int p = 0; p < _layercoord2.size(); p++)
        {
            pl2[p] = factory::addPoint(_layercoord2[p][0], _layercoord2[p][1], 0., _h);
        }

        int pb1 = factory::addPoint(0., 0., 0., _h);
        int pb2 = factory::addPoint(_L, 0., 0., _h);
        int pb3 = pl1.back();
        int pb4 = pl2.back();
        int pb5 = factory::addPoint(_L, _H, 0., _h);
        int pb6 = factory::addPoint(0., _H, 0., _h);
        int pb7 = pl2.front();
        int pb8 = pl1.front();

        int lbv1 = factory::addLine(pb2, pb3);
        int lbv2 = factory::addLine(pb3, pb4);
        int lbv3 = factory::addLine(pb4, pb5);

        int lbv4 = factory::addLine(pb6, pb7);
        int lbv5 = factory::addLine(pb7, pb8);
        int lbv6 = factory::addLine(pb8, pb1);

        int lbh1 = factory::addLine(pb1, pb2);
        int lbh2 = factory::addSpline(pl1);
        int lbh3 = factory::addSpline(pl2);
        int lbh4 = factory::addLine(pb6, pb5);

        std::array<int,3> clb;
        clb[0] = factory::addCurveLoop({lbh1,lbv1,-lbh2,lbv6});
        clb[1] = factory::addCurveLoop({lbh2,lbv2,-lbh3,lbv5});
        clb[2] = factory::addCurveLoop({lbh3,lbv3,-lbh4,lbv4});

        //Vertical emitters
        std::array<std::vector<int>,3> pxeb({{std::vector<int>(0,0),std::vector<int>(0,0),std::vector<int>(0,0)}});
        double He0 = (_H - _He)/2.;
        double deltaHe = _He/(((double) _nxe)-1.);

        unsigned int b=0;
        for (unsigned int e = 0; e < _nxe; e++)
        {
            double Hee = He0 + ((double)e)*deltaHe;
            if(Hee > _Hxe1){b=1;}
            if(Hee > _Hxe2){b=2;}
            //msg::print << "b = " << b << "  Hee = " << Hee <<msg::endl;
            pxeb[b].push_back(factory::addPoint(_xe, Hee, 0., _h));
        }

        std::array<std::vector<int>,3> pxrb({{std::vector<int>(0,0),std::vector<int>(0,0),std::vector<int>(0,0)}});
        if(!receiversAreEmittersX())
        {
            //Vertical receivers
            double Hr0 = (_H - _Hr)/2.;
            double deltaHr = _Hr/(((double) _nxr)-1.);

            unsigned int b=0;
            for (unsigned int r = 0; r < _nxr; r++)
            {
                double Hrr = Hr0 + ((double)r)*deltaHr;
                if(Hrr > _Hxr1){b=1;}
                if(Hrr > _Hxr2){b=2;}
                pxrb[b].push_back(factory::addPoint(_xr, Hrr, 0., _h));
            }
        }

        //Horizontal emitters
        std::array<std::vector<int>,3> pyeb({{std::vector<int>(0,0),std::vector<int>(0,0),std::vector<int>(0,0)}});
        double Le0 = (_L - _Le)/2.;
        double deltaLe = _Le/(((double) _nye)-1.);
        if(_ye>=std::max(_H_02,_H_L2)){b=2;}
        else if(_ye<=std::min(_H_01,_H_L1)){b=0;}
        else
        {
            throw common::Exception("Horizontal emitters can only be in zone 0 or zone 2");
        }
        for (unsigned int e = _nxe; e < _nxe+_nye; e++)
        {
            double Lee = Le0 + ((double)(e-_nxe))*deltaLe;
            pyeb[b].push_back(factory::addPoint(Lee, _ye, 0., _h));
        }

        std::array<std::vector<int>,3> pyrb({{std::vector<int>(0,0),std::vector<int>(0,0),std::vector<int>(0,0)}});
        if(!receiversAreEmittersY())
        {
            //Horizontal receivers
            double Lr0 = (_L - _Lr)/2.;
            double deltaLr = _Lr/(((double) _nyr)-1.);
            if(_yr>=std::max(_H_02,_H_L2)){b=2;}
            else if(_yr<=std::min(_H_01,_H_L1)){b=0;}
            else
            {
                throw common::Exception("Horizontal receivers can only be in zone 0 or zone 2");
            }
            unsigned int _n = _nxe+_nye;
            if(!receiversAreEmittersX()){_n+=_nxr;}
            for (unsigned int r = _n; r < _n+_nyr; r++)
            {
                double Lrr = Lr0 + ((double)(r-_n))*deltaLr;
                pyrb[b].push_back(factory::addPoint(Lrr, _yr, 0., _h));
            }
        }

        std::array<int,3> sb;

        sb[0] = factory::addPlaneSurface({clb[0]});
        sb[2] = factory::addPlaneSurface({clb[2]});

        std::vector<int> si;
        std::vector<std::vector<int>> li(_ni);
        std::vector<int> li_tot;
        if(_ni!=0)
        {
            std::vector<int> cli(_ni,0);
            if(_areFilled){si.resize(_ni);}
            for (unsigned int i = 0; i < _ni; i++)
            {
              li[i] = _inclusion_geo[i]->addInclusion();
              li_tot.insert(li_tot.end(), li[i].begin(), li[i].end());
              cli[i] = factory::addCurveLoop(li[i]);
              if(_areFilled)
              {
                  si[i] = factory::addPlaneSurface({cli[i]});
              }
            }
            std::vector<int> clbcli = cli;
            clbcli.insert(clbcli.begin(), clb[1]);
            sb[1] = factory::addPlaneSurface(clbcli);
        }
        else
        {
            sb[1] = factory::addPlaneSurface({clb[1]});
        }

        factory::synchronize();
        for (unsigned int b = 0; b < 3; b++)
        {
            gmodel::mesh::embed(0, pxeb[b], 2, sb[b]);
            gmodel::mesh::embed(0, pxrb[b], 2, sb[b]);
            gmodel::mesh::embed(0, pyeb[b], 2, sb[b]);
            gmodel::mesh::embed(0, pyrb[b], 2, sb[b]);
        }

        unsigned int rx=0;
        unsigned int ex=0;
        unsigned int ry=0;
        unsigned int ey=0;
        for (unsigned int b = 0; b < 3; b++)
        {
            if(receiversAreEmittersX())
            {
                for (auto it = pxeb[b].begin(); it < pxeb[b].end(); it++)
                {
                    gmodel::addPhysicalGroup(0, {*it}, (b+1)*1000+ex);
                    gmodel::setPhysicalName(0, (b+1)*1000+ex, "emitter_receiver_x_"+std::to_string(ex));
                    ex++;
                }
            }
            else
            {
                for (auto it = pxeb[b].begin(); it < pxeb[b].end(); it++)
                {
                    gmodel::addPhysicalGroup(0, {*it}, (b+1)*1000+ex);
                    gmodel::setPhysicalName(0, (b+1)*1000+ex, "emitter_x_"+std::to_string(ex));
                    //msg::print << "e = " << ex << msg::endl;
                    //msg::print << "b = " << b << msg::endl;
                    ex++;
                }
                for (auto it = pxrb[b].begin(); it < pxrb[b].end(); it++)
                {
                    gmodel::addPhysicalGroup(0, {*it}, (b+4)*1000+rx);
                    gmodel::setPhysicalName(0, (b+4)*1000+rx, "receiver_x_"+std::to_string(rx));
                    rx++;
                }
            }

            if(receiversAreEmittersY())
            {
                for (auto it = pyeb[b].begin(); it < pyeb[b].end(); it++)
                {
                    gmodel::addPhysicalGroup(0, {*it}, 7000+ey);
                    gmodel::setPhysicalName(0, 7000, "emitter_receiver_y_"+std::to_string(ey));
                    ey++;
                }
            }
            else
            {
                for (auto it = pyeb[b].begin(); it < pyeb[b].end(); it++)
                {
                    gmodel::addPhysicalGroup(0, {*it}, 7000+ey);
                    gmodel::setPhysicalName(0, 7000+ey, "emitter_y_"+std::to_string(ey));
                    //msg::print << "e = " << ey << msg::endl;
                    //msg::print << "b = " << b << msg::endl;
                    ey++;
                }
                for (auto it = pyrb[b].begin(); it < pyrb[b].end(); it++)
                {
                    gmodel::addPhysicalGroup(0, {*it}, 8000+ry);
                    gmodel::setPhysicalName(0, 8000+ry, "receiver_y_"+std::to_string(ry));
                    ry++;
                }
            }

            gmodel::addPhysicalGroup(2, {sb[b]}, 20+b);
            gmodel::setPhysicalName(2, 20+b, "background_vol_"+std::to_string(b+1));
        }

        gmodel::addPhysicalGroup(1, {lbv6,lbh1,lbv1}, 10);
        gmodel::setPhysicalName(1, 10, "background_bnd_1");
        gmodel::addPhysicalGroup(1, {lbv2,lbv5}, 11);
        gmodel::setPhysicalName(1, 11, "background_bnd_2");
        gmodel::addPhysicalGroup(1, {lbv3,-lbh4,lbv4}, 12);
        gmodel::setPhysicalName(1, 12, "background_bnd_3");


        for (unsigned int i = 0; i < _ni; i++)
        {
            if(_areFilled)
            {
                gmodel::addPhysicalGroup(2, {si[i]}, 1000+i);
                gmodel::setPhysicalName(2, 1000+i, "inclusion_vol"+std::to_string(i));
            }
            else
            {
                gmodel::addPhysicalGroup(1, li[i], 2000+i);
                gmodel::setPhysicalName(1, 2000+i, "inclusion_bnd"+std::to_string(i));
            }
        }

        factory::removeAllDuplicates();
    }

    void Configuration::data_mesh() const
    {
        if(nxe() != 0)
        {
            double ors = (_H - _He) / 2.;
            double orr = (_H - _Hr) / 2.;
            //Vertical array
            int p00 = factory::addPoint(ors, orr, 1.);
            int pHe0 = factory::addPoint(ors+_He,orr, 1.);
            int pHeHr = factory::addPoint(ors+_He, orr+_Hr, 1.);
            int p0Hr = factory::addPoint(ors, orr+_Hr, 1.);

            int ly0 = factory::addLine(p00, pHe0);
            int lxHe = factory::addLine(pHe0, pHeHr);
            int lyHr = factory::addLine(pHeHr, p0Hr);
            int lx0 = factory::addLine(p0Hr, p00);

            int cl = factory::addCurveLoop({ly0,lxHe,lyHr,lx0});
            int s1 = factory::addPlaneSurface({cl});

            factory::mesh::setTransfiniteCurve(ly0,_nxe);
            factory::mesh::setTransfiniteCurve(lxHe,_nxr);
            factory::mesh::setTransfiniteCurve(lyHr,_nxe);
            factory::mesh::setTransfiniteCurve(lx0,_nxr);

            factory::mesh::setTransfiniteSurface(s1,"Left",{p00,pHe0,pHeHr,p0Hr});

            //msg::print << "Hellow data 1"  << msg::endl;
            factory::mesh::setRecombine(2,s1);
            gmodel::addPhysicalGroup(2, {s1}, 1);
            gmodel::setPhysicalName(2, 1, "data_omega1");
            factory::removeAllDuplicates();
        }

        if(nye() != 0)
        {
            double ors = _H + (_L - _Le) / 2.;
            double orr = _H + (_L - _Lr) / 2.;
            //Horizontal array
            int pHeHr = factory::addPoint(ors, orr, 1.);
            int pLe0 = factory::addPoint(ors+_Le, orr, 1.);
            int pLeLr = factory::addPoint(ors+_Le, orr+_Lr, 1.);
            int p0Lr = factory::addPoint(ors+0., orr+_Lr, 1.);

            int lyHr2 = factory::addLine(pHeHr, pLe0);
            int lxLe = factory::addLine(pLe0, pLeLr);
            int lyLr = factory::addLine(pLeLr, p0Lr);
            int lxHe2 = factory::addLine(p0Lr, pHeHr);

            int cl2 = factory::addCurveLoop({lyHr2,lxLe,lyLr,lxHe2});
            int s2 = factory::addPlaneSurface({cl2});

            factory::mesh::setTransfiniteCurve(lyHr2,_nxe);
            factory::mesh::setTransfiniteCurve(lxLe,_nxr);
            factory::mesh::setTransfiniteCurve(lyLr,_nxe);
            factory::mesh::setTransfiniteCurve(lxHe2,_nxr);

            factory::mesh::setTransfiniteSurface(s2,"Left",{pHeHr,pLe0,pLeLr,p0Lr});

            //msg::print << "Hellow data 2"  << msg::endl;
            factory::mesh::setRecombine(2,s2);
            gmodel::addPhysicalGroup(2, {s2}, 2);
            gmodel::setPhysicalName(2, 2, "data_omega2");
            factory::removeAllDuplicates();
        }
    }

    std::array<unsigned int,2> Configuration::data_coordinate_to_index(double xs, double xr) const
    {
        if( (!_receiver_on_emitter) && (receiversAreEmittersX() || receiversAreEmittersY()) )
        {
            throw Exception("Data space when an emitter is not also a receiver is not implemented yet");
        }

        std::array<unsigned int,2> index;
        if(xs<_H && xr < _H)
        {
            double xe0 = (_H-_He)/2.;
            double stepe = _He/(nxe()-1);
            double xr0 = (_H-_Hr)/2.;
            double stepr = _Hr/(nxr()-1);
            index[0] = std::round((xs-xe0)/stepe);
            index[1] = std::round((xr-xr0)/stepr);
        }
        else if(xs>=_H && xr>=_H)
        {
            double ye0 = _H + (_L-_Le)/2.;
            double stepe = _Le/(nye()-1);
            double yr0 = _H + (_L-_Lr)/2.;
            double stepr = _Lr/(nyr()-1);
            index[0] = std::round((xs-ye0)/stepe);
            index[1] = std::round((xr-yr0)/stepr);
        }
        else
        {
            throw Exception("Invalid data coordinate.");
        }
        return index;
    }

    std::array<double,2> Configuration::index_to_data_coordinate(unsigned int s, unsigned int r) const
    {
        if( (!_receiver_on_emitter) && (receiversAreEmittersX() || receiversAreEmittersY()))
        {
            throw Exception("Data space when an emitter is not also a receiver is not implemented yet");
        }

        std::array<double,2> index;
        if(s < nxe() && r < nxr())
        {
            double xe0 = (_H-_He)/2.;
            double stepe = _He/(nxe()-1);
            double xr0 = (_H-_Hr)/2.;
            double stepr = _Hr/(nxr()-1);
            index[0] = xe0 + s*stepe;
            index[1] = xr0 + r*stepr;
        }
        else if( s>=nxe() && r>=nxr())
        {
            double xe0 = _H + (_L-_Le)/2.;
            double stepe = _Le/(nye()-1);
            double xr0 = _H + (_L-_Lr)/2.;
            double stepr = _Lr/(nyr()-1);
            index[0] = xe0 + (s-nxe())*stepe;
            index[1] = xr0 + (r-nxr())*stepr;
        }
        else
        {
            throw Exception("Invalid index.");
        }
        return index;
    }

    double Configuration::data_area() const
    {
        double areaY = 0.;
        double areaX = 0.;
        if(_nye > 1 && _nyr > 1){areaY = _Le * _Lr;}
        if(_nxe > 1 && _nxr > 1){areaX = _He * _Hr;}
        return (areaX + areaY);
    };

    double Configuration::datapoint_area() const
    {
        double areaY = _Le * _Lr / (double(_nye-1)) / (double(_nyr-1));
        double areaX = _He * _Hr / (double(_nxe-1)) / (double(_nxr-1));
        if(_nye < 2 || _nyr < 2){return areaX;}
        if(_nxe < 2 || _nxr < 2){return areaY;}
        return 0.5 * (areaX + areaY);
    };

    bool Configuration::data_coordinate_isValid(double xs,double xr) const
    {
        double eps = 1e-8;
        gmsh::option::getNumber("Geometry.MatchMeshTolerance",eps);
        if( nxe() != 0 )
        {
            double xe0 = (_H-_He)/2.;
            double xr0 = (_H-_Hr)/2.;
            if( (xe0-eps <= xs) && (xs <= xe0+_He+eps) && (xr0-eps <= xr) && (xr <= xr0+_Hr+eps) ){return true;}
        }

        if( nye() != 0 )
        {
            double xe0 = _H + (_L-_Le)/2.;
            double xr0 = _H + (_L-_Lr)/2.;
            if( (xe0-eps <= xs) && (xs <= xe0+_Le+eps) && (xr0-eps <= xr) && (xr <= xr0+_Lr+eps) ){return true;}
        }
        return false;
    }

    template<Physic T_Physic>
    ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd,const WaveMultiField<T_Physic>& g, const Configuration* const config)
    {
        ModelMonoFunction termX;
        if( !config->receiversAreEmittersX() )
        {
            termX = autocorrelate<T_Physic>(g,config->emitter_idx_X())*autocorrelate<T_Physic>(g,config->receiver_idx_X());
        }
        else
        {
            termX = pow(autocorrelate<T_Physic>(g,config->emitter_idx_X()),2);
        }

        ModelMonoFunction termY;
        if( !config->receiversAreEmittersY() )
        {
            termY = autocorrelate<T_Physic>(g,config->emitter_idx_Y())*autocorrelate<T_Physic>(g,config->receiver_idx_Y());
        }
        else
        {
            termY = pow(autocorrelate<T_Physic>(g,config->emitter_idx_Y()),2);
        }
        return termX + termY;
    }

    template ModelMonoFunction green0_preconditioner<Physic::acoustic>(const Data<Physic::acoustic>&, const WaveMultiField<Physic::acoustic>&, const Configuration* const);
    template ModelMonoFunction green0_preconditioner<Physic::electromagnetic>(const Data<Physic::electromagnetic>&, const WaveMultiField<Physic::electromagnetic>&, const Configuration* const);
    template ModelMonoFunction green0_preconditioner<Physic::elastodynamic>(const Data<Physic::elastodynamic>&, const WaveMultiField<Physic::elastodynamic>&, const Configuration* const);

} // namespace layeredvolume_acquisition
