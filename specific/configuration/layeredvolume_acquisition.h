// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_CONFIGURATION_LAYEREDVOLUME_ACQUISITION
#define H_CONFIGURATION_LAYEREDVOLUME_ACQUISITION
//Standard library
#include <array>
//GmshFEM Library
#include "GmshFem.h"

//GmshFWI Library
#include "../../common/configuration.h"
#include "../../common/wave/element.h"
#include "inclusion/inclusion.h"

//Forward declaration
template<Physic T_Physic>
class Data;

namespace layeredvolume_acquisition
{
    enum class Layer
    {
        Constant,
        File
    };
    Layer to_layer(std::string layer);

    std::vector<std::array<double,2>> to_layercoord(const gmshfem::common::GmshFem& gmshFem,unsigned int l);

    enum class UnknownRegion
    {
        None,
        All
    };
    UnknownRegion to_unknownregion(const gmshfem::common::GmshFem& gmshFem);

    class Configuration final : public ConfigurationInterface
    {
    private:
        double _xe;
        double _xr;
        double _ye;
        double _yr;

        double _He;
        double _Hr;
        double _Le;
        double _Lr;

        bool _receiver_on_emitter;
        unsigned int _nxe;
        unsigned int _nxr;
        std::array<unsigned int,3> _nxeb;
        std::array<unsigned int,3> _nxrb;
        unsigned int _nye;
        unsigned int _nyr;

        double _L;
        double _H;
        //Layers
        Layer _layer;
        double _H_L1;
        double _H_L2;
        double _H_01;
        double _H_02;
        double _Hxe1;
        double _Hxe2;
        double _Hxr1;
        double _Hxr2;
        std::vector<std::array<double,2>> _layercoord1;
        std::vector<std::array<double,2>> _layercoord2;

        unsigned int _ni;
        bool _areFilled;
        std::vector<const InclusionInterface*> _inclusion_geo;

        double _h;

        UnknownRegion _unknown_region;
        std::array<std::array<gmshfem::domain::Domain,2>,3> _background;
        std::array<gmshfem::domain::Domain,2> _inclusions;
        std::vector<std::array<gmshfem::domain::Domain,2>> _inclusion;
        std::array<gmshfem::domain::Domain,3> _pointsb;

        std::vector<unsigned int> _emitter_idx_X;
        std::vector<unsigned int> _emitter_idx_Y;
        std::vector<unsigned int> _receiver_idx_X;
        std::vector<unsigned int> _receiver_idx_Y;

        std::vector<std::array<std::complex<double>,3>> _mb;
        std::vector<std::vector<std::complex<double>>> _mi;

        virtual void wave_mesh() const;
        virtual void data_mesh() const;
    public:
        Configuration(std::string name, const ParametrizationInterface* const parametrization, const gmshfem::common::GmshFem& gmshFem);

        const std::vector<unsigned int>& emitter_idx_X() const {return _emitter_idx_X;};
        const std::vector<unsigned int>& emitter_idx_Y() const {return _emitter_idx_Y;};
        const std::vector<unsigned int>& receiver_idx_X() const {return _receiver_idx_X;};
        const std::vector<unsigned int>& receiver_idx_Y() const {return _receiver_idx_Y;};
        bool receiversAreEmittersX () const {return (std::abs(_xe-_xr)<1e-14) && (std::abs(_He-_Hr)<1e-14) && (_nxe==_nxr) && (_nxe != 0) && (_nxr != 0);};
        bool receiversAreEmittersY () const {return (std::abs(_ye-_yr)<1e-14) && (std::abs(_Le-_Lr)<1e-14) && (_nye==_nyr) && (_nye != 0) && (_nyr != 0);};
        unsigned int nxe() const {return _nxe;};
        unsigned int nxr() const {return _nxr;};
        unsigned int nye() const {return _nye;};
        unsigned int nyr() const {return _nyr;};


        virtual std::array<unsigned int,2> data_coordinate_to_index(double xs, double xr) const;
        virtual std::array<double,2> index_to_data_coordinate(unsigned int s, unsigned int r) const;
        virtual bool data_coordinate_isValid(double xs,double xr) const;

        virtual double area() const {return _L * _H; };
        virtual double data_area() const override;
        virtual double datapoint_area() const;
        virtual double array() const {return std::max(_He,_Le);};
        virtual double depth() const {return std::max(_H,_L)/2.;};

        virtual std::string wave_gmodel() const {return _name;};
        virtual std::string model_gmodel() const {return _name;};
        virtual std::string data_gmodel() const {return _name;};
    };

    template<Physic T_Physic>
    ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd, const WaveMultiField<T_Physic>& g, const Configuration* const config);
} // namespace layeredvolume_acquisition

#endif // H_CONFIGURATION_LAYEREDVOLUME_ACQUISITION
