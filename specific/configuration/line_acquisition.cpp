// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
#include <numeric>
#include <array>

//GmshFEM Library
#include "GmshFem.h"
#include "Exception.h"
#include "Message.h"
#include "Domain.h"
#include "Function.h"
#include "CSVio.h"

//Gmsh Library
#include "gmsh.h"

//GmshFWI Library
#include "../../common/model/macro.h"
#include "../../common/data/element.h"
#include "../wave/correlation.h"
#include "line_acquisition.h"

namespace gmodel = gmsh::model;
namespace factory = gmsh::model::geo;

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::domain;
using namespace gmshfem::function;

static const std::complex< double > im = std::complex< double >(0., 1.);

namespace line_acquisition
{
    UnknownRegion to_unknownregion(const GmshFem& gmshFem)
    {
        std::string str;
        if(! gmshFem.userDefinedParameter(str, "unknown") )
        {
        throw common::Exception("The unknown region to consider could not be found.");
        }
        if(str=="all") {return UnknownRegion::All;}
        else if(str=="inclusion") {return UnknownRegion::Inclusion;}
        else if(str=="background") {return UnknownRegion::Background;}
        else if(str=="none") {return UnknownRegion::None;}
        else
        {
        throw common::Exception("The unknown region " + str + " is not handled.");
        }
        return UnknownRegion::None;
    }
    /*
    *  class Configuration
    */
    Configuration::Configuration(std::string name, const ParametrizationInterface* const parametrization, const GmshFem& gmshFem) : ConfigurationInterface(name, parametrization, gmshFem),
    _ni(0)
    {
        msg::print << "Initialize line configuration" << msg::endl;

        /*
        * MESH
        */
        if(
          !(
            gmshFem.userDefinedParameter(_xer, "xer") &&
            gmshFem.userDefinedParameter(_L, "L") &&
            gmshFem.userDefinedParameter(_h, "h")
            )
        )
        {
          throw common::Exception("A geometric parameter could not be found.");
        }

        gmshFem.userDefinedParameter(_ni, "inclusion_ni");
        _inclusion_depth.resize(_ni);
        _inclusion_position.resize(_ni);
        for (unsigned int i = 0; i < _ni; i++)
        {
            if(
                !(
                    gmshFem.userDefinedParameter(_inclusion_depth[i], "inclusion_depth"+std::to_string(i)) &&
                    gmshFem.userDefinedParameter(_inclusion_position[i], "inclusion_position"+std::to_string(i))
                )
            )
            {
                throw common::Exception("An inclusion #"+ std::to_string(i)+ "  parameter could not be found.");
            }
        }

        unsigned int reflec = 1;
        unsigned int trans = 1;
        gmshFem.userDefinedParameter(reflec, "reflection");
        gmshFem.userDefinedParameter(trans, "transmission");
        _reflection = ((bool) reflec);
        _transmission = ((bool) trans);

        mesh();

        /*
        * DOMAIN
        */
        _background[Support::BLK] = Domain(1,21);
        _background[Support::BND] = Domain(0,12);

        _inclusion.resize(_ni);
        for (unsigned int i = 0; i < _ni; i++)
        {
            _inclusion[i] = Domain(1,2000+i);
            _inclusions |= Domain(1,2000+i);
        }

        _wave_omega[Support::BLK] = _background[Support::BLK] | _inclusions;
        _wave_omega[Support::BND] = _background[Support::BND];
        _unknown_region = to_unknownregion(gmshFem);
        switch (_unknown_region)
        {
          case UnknownRegion::Inclusion:
            _model_known = _background;
            _model_unknown[Support::BLK] = _inclusions;
            break;
          case UnknownRegion::Background:
            _model_known[Support::BLK] = _inclusions;
            _model_unknown = _background;
            break;
          case UnknownRegion::All:
            _model_unknown[Support::BLK] = _background[Support::BLK] | _inclusions;
            _model_unknown[Support::BND] = _background[Support::BND];
            break;
          case UnknownRegion::None: default:
            _model_known[Support::BLK] = _background[Support::BLK] | _inclusions;
            _model_known[Support::BND] = _background[Support::BND];
            break;
        }

        _np = 2;
        _point.push_back(Domain(0,1000));
        _point.push_back(Domain(0,1001));
        _points = Domain(0,1000) | Domain(0,1001);
        _ns = 2;
        _emitter.push_back({0});
        _emitter.push_back({1});
        if(_reflection && _transmission)
        {
            _receiver.push_back({0,1});
            _receiver.push_back({1,0});
        }
        else if(_reflection)
        {
            _receiver.push_back({0});
            _receiver.push_back({1});
        }
        else if (_transmission)
        {
            _receiver.push_back({1});
            _receiver.push_back({0});
        }
        else
        {
            throw common::Exception("Acquisistion must at least be transmission or reflection");
        }

        /*
        * Reference model function
        */
        _mb.resize(model_size());
        _mi.resize(model_size());
        for (unsigned int c = 0; c < model_size(); c++)
        {
            double Remb, Immb;
            if
            (!
                (
                    gmshFem.userDefinedParameter(Remb, "Re(mbc"+std::to_string(c)+")") &&
                    gmshFem.userDefinedParameter(Immb, "Im(mbc"+std::to_string(c)+")")
                )
            )
            {
                throw common::Exception("Background (reference) model parameter (component"+std::to_string(c)+") could not be found.");
            }
            else
            {
                _mb[c] = Remb + im * Immb;
                _mc[c] = _mb[c];
            }
            ScalarPiecewiseFunction< std::complex< double > > m0;
            m0.addFunction(_mb[c],_background[Support::BLK] | _background[Support::BND] | _points);

            _mi[c].resize(_ni);
            for (unsigned int i = 0; i < _ni; i++)
            {
                double Remi=0., Immi=0.;
                if
                (
                    !(gmshFem.userDefinedParameter(Remi, "Re(mi"+std::to_string(i)+"c"+std::to_string(c)+")") &&
                    gmshFem.userDefinedParameter(Immi, "Im(mi"+std::to_string(i)+"c"+std::to_string(c)+")"))
                )
                {
                    throw Exception("Inclusion model parameter could not be found.");
                }
                _mi[c][i] = Remi + im * Immi;
                m0.addFunction(_mi[c][i],_inclusion[i]);
            }
            _m0.push_back(m0);
        }
    }
    void Configuration::mesh() const
    {
        msg::print << "Generate meshes" << msg::endl;
        gmsh::option::setNumber("General.Terminal", 1);

        gmodel::add(_name);
        if(!_remesh)
        {
          gmsh::open(_name+".msh");
          return;
        }

        wave_mesh();
        data_mesh();

        gmsh::option::setNumber("General.ExpertMode",1);
        factory::synchronize();
        gmodel::mesh::generate();

        gmsh::write(_name+".msh");
    }
    void Configuration::wave_mesh() const
    {
        double eps = 1e-8;
        gmsh::option::getNumber("Geometry.MatchMeshTolerance",eps);

        int pb1 = factory::addPoint(0., 0., 0., _h);
        int pb2 = factory::addPoint(_L, 0., 0., _h);
        int pe1 = factory::addPoint(_xer, 0., 0., _h);
        int pe2 = factory::addPoint(_L-_xer, 0., 0., _h);

        std::vector<int> lb;
        std::vector<int> li;

        lb.push_back(factory::addLine(pb1, pe1));

        int pold = pe1;
        double xold = _xer;
        for (unsigned int i = 0; i < _ni; i++)
        {
            double xia = _inclusion_position[i]-_inclusion_depth[i]/2.;
            int pia;
            if(std::abs(xold-xia)<eps)
            {
                pia = pold;
            }
            else
            {
                pia = factory::addPoint(xia, 0., 0., _h);
                lb.push_back(factory::addLine(pold, pia));
            }

            double xib = _inclusion_position[i]+_inclusion_depth[i]/2.;
            int pib = factory::addPoint(xib, 0., 0., _h);
            li.push_back(factory::addLine(pia, pib));
            xold=xib;
            pold=pib;
        }

        lb.push_back(factory::addLine(pold, pe2));
        lb.push_back(factory::addLine(pe2, pb2));

        factory::synchronize();

        gmodel::addPhysicalGroup(0, {pe1}, 1000);
        gmodel::setPhysicalName(0, 1000, "emitter_receiver0");
        gmodel::addPhysicalGroup(0, {pe2}, 1001);
        gmodel::setPhysicalName(0, 1001, "emitter_receiver1");

        gmodel::addPhysicalGroup(0, {pb1,pb2}, 12);
        gmodel::setPhysicalName(0, 12, "background_bnd");

        gmodel::addPhysicalGroup(1, lb, 21);
        gmodel::setPhysicalName(1, 21, "background_vol");
        for (unsigned int i = 0; i < _ni; i++)
        {
            gmodel::addPhysicalGroup(1, {li[i]}, 2000+i);
            gmodel::setPhysicalName(1, 2000+i, "inclusion_vol"+std::to_string(i));
        }
    }
    void Configuration::data_mesh() const {}

    std::array<unsigned int,2> Configuration::data_coordinate_to_index(double xs, double xr) const
    {
        throw Exception("data_coordinate_to_index not valid for line acquisistion.");
        return {0,0};
    }

    std::array<double,2> Configuration::index_to_data_coordinate(unsigned int s, unsigned int r) const
    {
        throw Exception("index_to_data_coordinate not valid for line acquisistion.");
        return {0,0};
    }

    double Configuration::data_area() const
    {
        return 0.;
    };

    double Configuration::datapoint_area() const
    {
        return 0.;
    };

    bool Configuration::data_coordinate_isValid(double xs,double xr) const
    {
        throw Exception("data_coordinate_isValid not valid for line acquisistion.");
        return false;
    }

    template<Physic T_Physic>
    ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd, const WaveMultiField<T_Physic>& g, const Configuration* const config)
    {
        if (config->transmission() && config->reflection())
        {
            return pow(autocorrelate<T_Physic>(g,{0,1}),2);
        }
        else if(config->reflection())
        {
            return pow(autocorrelate<T_Physic>(g,{0}),2) + pow(autocorrelate<T_Physic>(g,{1}),2);
        }
        else if(config->transmission())
        {
            return 2. * autocorrelate<T_Physic>(g,{0}) * autocorrelate<T_Physic>(g,{1});
        }
        else
        {
            throw common::Exception("Acquisistion must at least be transmission or reflection");
        }
    }

    template ModelMonoFunction green0_preconditioner<Physic::acoustic>(const Data<Physic::acoustic>&, const WaveMultiField<Physic::acoustic>&, const Configuration* const);
    template ModelMonoFunction green0_preconditioner<Physic::electromagnetic>(const Data<Physic::electromagnetic>&, const WaveMultiField<Physic::electromagnetic>&, const Configuration* const);
    template ModelMonoFunction green0_preconditioner<Physic::elastodynamic>(const Data<Physic::elastodynamic>&, const WaveMultiField<Physic::elastodynamic>&, const Configuration* const);

} // namespace soil
