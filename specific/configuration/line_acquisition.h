// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_CONFIGURATION_LINE_ACQUISITION
#define H_CONFIGURATION_LINE_ACQUISITION

//GmshFEM Library
#include "GmshFem.h"
//#include "Function.h"

//GmshFWI Library
#include "../../common/configuration.h"
#include "../../common/wave/element.h"

//Forward declaration
template<Physic T_Physic>
class Data;

namespace line_acquisition
{
    enum class UnknownRegion
    {
        None,
        Inclusion,
        Background,
        All
    };
    UnknownRegion to_unknownregion(const gmshfem::common::GmshFem& gmshFem);

    class Configuration final : public ConfigurationInterface
    {
    private:
        double _xer;
        bool _reflection;
        bool _transmission;

        double _L;

        unsigned int _ni;
        std::vector<double> _inclusion_depth;
        std::vector<double> _inclusion_position;

        double _h;

        UnknownRegion _unknown_region;
        std::array<gmshfem::domain::Domain,2> _background;
        gmshfem::domain::Domain _inclusions;
        std::vector<gmshfem::domain::Domain> _inclusion;

        std::vector<std::complex<double>> _mb;
        std::vector<std::vector<std::complex<double>>> _mi;

        virtual void wave_mesh() const;
        virtual void data_mesh() const;
    public:
        Configuration(std::string name, const ParametrizationInterface* const parametrization, const gmshfem::common::GmshFem& gmshFem);

        bool reflection() const {return _reflection;};
        bool transmission() const {return _transmission;};

        virtual void mesh() const;

        virtual std::array<unsigned int,2> data_coordinate_to_index(double xs, double xr) const;
        virtual std::array<double,2> index_to_data_coordinate(unsigned int s, unsigned int r) const;
        virtual bool data_coordinate_isValid(double xs,double xr) const;

        virtual double area() const {return _L; };
        virtual double data_area() const override;
        virtual double datapoint_area() const;
        virtual double array() const {return 0.;};
        virtual double depth() const {return std::abs(_L/2.-_xer);};

        virtual std::string wave_gmodel() const {return _name;};
        virtual std::string model_gmodel() const {return _name;};
        virtual std::string data_gmodel() const {return _name;};
    };

    template<Physic T_Physic>
    ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd, const WaveMultiField<T_Physic>& g, const Configuration* const config);
} // namespace soil

#endif // H_CONFIGURATION_VOLUME_ACQUISITION
