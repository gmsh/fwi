// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_NEW_CONFIGURATION
#define H_NEW_CONFIGURATION

//Standard Library
#include <string>
#include <memory>

//GmshFem Library
#include "GmshFem.h"
#include "Exception.h"

//FWI Library
#include "surface_acquisition.h"
#include "volume_acquisition.h"
#include "layeredvolume_acquisition.h"
#include "circular_acquisition.h"
#include "line_acquisition.h"
#include "rectangular_acquisition.h"
#include "flexible_acquisition.h"

std::unique_ptr<ConfigurationInterface> newConfiguration(std::string name, const ParametrizationInterface* const parametrization, const gmshfem::common::GmshFem& gmshFem)
{
    std::string configuration;
    if(!gmshFem.userDefinedParameter(configuration, "configuration"))
    {
      throw gmshfem::common::Exception("Configuration type could not be found.");
    }
    if(configuration=="surface_acquisition"){return std::make_unique< surface_acquisition::Configuration>(name, parametrization, gmshFem);}
    else if(configuration=="volume_acquisition"){return std::make_unique< volume_acquisition::Configuration>(name, parametrization, gmshFem);}
    else if(configuration=="layeredvolume_acquisition"){return std::make_unique< layeredvolume_acquisition::Configuration>(name, parametrization, gmshFem);}
    else if(configuration=="circular_acquisition"){return std::make_unique< circular_acquisition::Configuration>(name, parametrization, gmshFem);}
    else if(configuration=="line_acquisition"){return std::make_unique< line_acquisition::Configuration>(name, parametrization, gmshFem);}
    else if(configuration=="rectangular_acquisition"){return std::make_unique< rectangular_acquisition::Configuration>(name, parametrization, gmshFem);}
    else if(configuration=="flexible_acquisition"){return std::make_unique< flexible_acquisition::Configuration>(name, parametrization, gmshFem);}
    else
    {
        throw gmshfem::common::Exception("Configuration" + configuration + " is not valid.");
        return NULL;
    }
}

#endif // H__NEW_CONFIGURATION
