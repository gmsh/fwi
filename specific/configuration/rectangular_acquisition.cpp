// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
#include <numeric>
//GmshFem Library
#include "GmshFem.h"
#include "Exception.h"
#include "Message.h"
#include "Domain.h"
#include "Function.h"
#include "CSVio.h"
//Gmsh Library
#include "gmsh.h"
//GmshFWI library
#include "../../common/model/macro.h"
#include "../wave/correlation.h"
#include "rectangular_acquisition.h"
#include "../../common/data/element.h"

namespace gmodel = gmsh::model;
namespace factory = gmsh::model::geo;

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::domain;
using namespace gmshfem::function;

static const std::complex< double > im = std::complex< double >(0., 1.);

namespace rectangular_acquisition
{
    UnknownRegion to_unknownregion(const GmshFem& gmshFem)
    {
      std::string str;
      if(! gmshFem.userDefinedParameter(str, "unknown") )
      {
        throw common::Exception("The unknown region to consider could not be found.");
      }
      if(str=="none") {return UnknownRegion::None;}
      else if(str=="all") {return UnknownRegion::All;}
      else
      {
        throw common::Exception("The unknown region " + str + " is not handled.");
      }
      return UnknownRegion::None;
    }
    /*
    *  class Configuration
    */
    Configuration::Configuration(std::string name, const ParametrizationInterface* const parametrization, const GmshFem& gmshFem) : ConfigurationInterface(name, parametrization, gmshFem)
    {
        /*
        * MESH
        */
        if(
          !(
            gmshFem.userDefinedParameter(_L, "L") &&
            gmshFem.userDefinedParameter(_H, "H") &&
            gmshFem.userDefinedParameter(_hx, "hx") &&
            gmshFem.userDefinedParameter(_hy, "hy")  &&
            gmshFem.userDefinedParameter(_Ler, "Ler") &&
            gmshFem.userDefinedParameter(_Her, "Her")
            )
        )
        {
          throw common::Exception("A geometric parameter could not be found.");
        }

        _nx = std::round(_L / _hx)+1;
        _ny = std::round(_H / _hy)+1;
        _hx = _L / ((double) _nx-1);
        _hy = _H / ((double) _ny-1);
        msg::print << "Effective horizontal mesh size hx = " << _hx << msg::endl;
        msg::print << "Effective vertical mesh size hy = " << _hy << msg::endl;
        unsigned int nerx = std::round( _Ler / _hx)+1;
        unsigned int nery = std::round( _Her / _hy)+1;
        _Ler = ((double)nerx-1)*_hx;
        _Her = ((double)nery-1)*_hy;
        msg::print << "Effective horizontal array length Her = " << _Her << msg::endl;
        msg::print << "Effective vertical array length Ler = " << _Ler << msg::endl;

        unsigned int nerp = 2 * (nerx+nery-2);

        _re_offset = 0;
        _em_offset = 0;
        _re_step = 1;
        _em_step = 1;
        unsigned int receiverOnEmitter = 1;
        if(
            !(
                gmshFem.userDefinedParameter(_em_offset, "em_offset") &&
                gmshFem.userDefinedParameter(_re_offset, "re_offset") &&
                gmshFem.userDefinedParameter(_em_step, "em_step") &&
                gmshFem.userDefinedParameter(_re_step, "re_step") &&
                gmshFem.userDefinedParameter(receiverOnEmitter, "ReceiverOnEmitter")
            )
        )
        {
            msg::warning << "An emitter/receiver parameter could not be found." << msg::endl;
        }

        std::vector<unsigned int> p_excluded(0);
        unsigned int exclude_bottom_points = 0;
        gmshFem.userDefinedParameter(exclude_bottom_points, "exclude_bottom_points");
        bool bottom = ((bool) exclude_bottom_points);
        if(bottom)
        {
            p_excluded.resize(nerx);
            std::iota(p_excluded.begin(), p_excluded.end(), 0);
        }
        unsigned int np_excluded = 0;
        gmshFem.userDefinedParameter(np_excluded, "n_excluded_points");
        for (unsigned i = 0; i < np_excluded; i++)
        {
            unsigned int tmp;
            if(!gmshFem.userDefinedParameter(tmp, "p_ex"+std::to_string(i)))
            {
                throw common::Exception("An excluded point index could not be found.");
            }
            p_excluded.push_back(tmp);
        }

        unsigned int ncx=(_nx - nerx) / 2;
        unsigned int ncy=(_ny - nery) / 2;
        for (unsigned int p = 0; p < nerp; p++)
        {
            if(
                (((p+_em_offset) % _em_step)==0 || ((p+_re_offset) % _re_step)==0)
                &&
                (std::find(p_excluded.begin(), p_excluded.end(),p) == p_excluded.end())
            )
            {
                _point_coord.push_back({ ((double)ncx) * _hx, ((double)ncy) * _hy});
                _point_is_emitter.push_back(((p+_em_offset) % _em_step)==0);
                _point_is_receiver.push_back(((p+_re_offset) % _re_step)==0);
            }

            if(p<nerx-1)
            {
                ncx++;
            }
            else if(p < nerx+nery-2)
            {
                ncy++;
            }
            else if(p < 2*nerx+nery-3)
            {
                ncx--;
            }
            else
            {
                ncy--;
            }
        }
        _np = _point_coord.size();

        for(unsigned int p = 0; p < _np; p++)
        {
            if(_point_is_emitter[p])
            {
                _emitter.push_back({p});
                std::vector<unsigned int> rec;
                for (unsigned int q = 0; q < _np; q++)
                {
                    if(_point_is_receiver[q] && (p!=q || _receiver_on_emitter) )
                    {
                        rec.push_back({q});
                    }
                }
                _receiver.push_back(rec);
            }
        }
        _ns=_emitter.size();

        mesh();

        /*
        * DOMAIN
        */
        //_data_omega = empty
        _background[Support::BLK] = Domain(2,21);
        _background[Support::BND] = Domain(1,12);

        _wave_omega[Support::BLK] = _background[Support::BLK];
        _wave_omega[Support::BND] = _background[Support::BND];

        _unknown_region = to_unknownregion(gmshFem);
        switch (_unknown_region)
        {
          case UnknownRegion::None:
              _model_known[Support::BLK] = _background[Support::BLK];
              _model_known[Support::BND] = _background[Support::BND];
              break;
        case UnknownRegion::All:
              _model_unknown[Support::BLK] = _background[Support::BLK];
              _model_unknown[Support::BND] = _background[Support::BND];
            break;
        }

        _point.resize(_np);
        for (unsigned int p = 0; p < _np; p++)
        {
            _point[p] = Domain(0,100+p);
        }


        /*
        * Reference model function
        */
        _mb.resize(model_size());
        for (unsigned int c = 0; c < model_size(); c++)
        {
            std::string suffix = "c"+std::to_string(c);
            double Remb, Immb;
            if
            (!
                (
                    gmshFem.userDefinedParameter(Remb, "Re(mbc"+std::to_string(c)+")") &&
                    gmshFem.userDefinedParameter(Immb, "Im(mbc"+std::to_string(c)+")")
                )
            )
            {
                throw common::Exception("Background (reference) model parameter (component"+std::to_string(c)+") could not be found.");
            }
            else
            {
                _mb[c] = Remb + im * Immb;
                _mc[c] = _mb[c];
            }

            ScalarPiecewiseFunction< std::complex< double > > m0;
            std::string m0_type;
            if(!gmshFem.userDefinedParameter(m0_type, "m0_type"+suffix))
            {
                throw Exception("Reference model type could not be found.");
            }

            if(m0_type=="file")
            {
                std::string path = "";
                if(!gmshFem.userDefinedParameter(path, "m0_path"+suffix))
                {
                    throw common::Exception("Path to subsurface data could not be found.");
                }
                m0.addFunction(bilinearInterpolation(path),_background[Support::BLK] | _background[Support::BND] );
            }
            else if(m0_type=="file.pos")
            {
                std::string path = "";
                if(!gmshFem.userDefinedParameter(path, "m0_path"+suffix))
                {
                    throw common::Exception("Path to model file could not be found.");
                }

                gmsh::merge(path+suffix+".pos");
                ScalarFunction<std::complex<double>> mpos = probeScalarView<std::complex<double>>(c);

                m0.addFunction(mpos,_background[Support::BLK] | _background[Support::BND]);
            }
            else if(m0_type=="constant")
            {
                m0.addFunction(_mb[c],_background[Support::BLK] | _background[Support::BND]);
            }
            else
            {
                throw common::Exception("Initial model type ("+ suffix + ") " + m0_type +" is unknown.");
            }
            _m0.push_back(m0);
        }
    }

    void Configuration::mesh() const
    {
        msg::print << "Generate meshes" << msg::endl;
        gmsh::option::setNumber("General.Terminal", 1);
        gmodel::add(_name);
        if(!_remesh)
        {
          gmsh::open(_name+".msh");
          return;
        }
        wave_mesh();
        data_mesh();

        factory::synchronize();
        gmodel::mesh::generate();

        gmodel::mesh::removeDuplicateNodes();

        gmsh::write(_name+".msh");
    }
    void Configuration::wave_mesh() const
    {
        int pb1 = factory::addPoint(0., 0., 0.);
        int pb2 = factory::addPoint(_L, 0., 0.);
        int pb3 = factory::addPoint(_L, _H, 0.);
        int pb4 = factory::addPoint(0., _H, 0.);

        int lbh1 = factory::addLine(pb1, pb2);
        int lbv1 = factory::addLine(pb2, pb3);
        int lbh2 = factory::addLine(pb3, pb4);
        int lbv2 = factory::addLine(pb4, pb1);

        factory::mesh::setTransfiniteCurve(lbh1,_nx);
        factory::mesh::setTransfiniteCurve(lbh2,_nx);
        factory::mesh::setTransfiniteCurve(lbv1,_ny);
        factory::mesh::setTransfiniteCurve(lbv2,_ny);

        int clb = factory::addCurveLoop({lbh1,lbv1,lbh2,lbv2});
        int sb = factory::addPlaneSurface({clb});

        std::vector<int> per(_np);
        for (unsigned int p = 0; p < _np; p++)
        {
            per[p] = factory::addPoint( _point_coord[p][0], _point_coord[p][1], 0.);
        }

        factory::mesh::setTransfiniteSurface(sb,"Left",{pb1,pb2,pb3,pb4});
        factory::mesh::setRecombine(2,sb);
        factory::synchronize();

        gmodel::addPhysicalGroup(1, {lbh1,lbv1,lbh2,lbv2}, 12);
        gmodel::setPhysicalName(1, 12, "background_bnd");
        gmodel::addPhysicalGroup(2, {sb}, 21);
        gmodel::setPhysicalName(2, 21, "background_vol");

        for (unsigned int p = 0; p < _np; p++)
        {
            if(_point_is_emitter[p] && _point_is_receiver[p])
            {
                gmodel::addPhysicalGroup(0, {per[p]}, 100+p);
                gmodel::setPhysicalName(0, 100+p, "emitter_receiver_"+std::to_string(p));
            }
            else if(_point_is_emitter[p])
            {
                gmodel::addPhysicalGroup(0, {per[p]}, 100+p);
                gmodel::setPhysicalName(0, 100+p, "emitter_"+std::to_string(p));
            }
            else if(_point_is_receiver[p])
            {
                gmodel::addPhysicalGroup(0, {per[p]}, 100+p);
                gmodel::setPhysicalName(0, 100+p, "receiver_"+std::to_string(p));
            }
            else
            {
                throw common::Exception("A physical point is neither an emitter neither a receiver.");
            }
        }
    }
    void Configuration::data_mesh() const {}

    std::array<unsigned int,2> Configuration::data_coordinate_to_index(double xs, double xr) const
    {
        throw common::Exception("Data space not implemented for rectangular acquisition.");
        return {0,0};
    }

    std::array<double,2> Configuration::index_to_data_coordinate(unsigned int s, unsigned int r) const
    {
        throw common::Exception("Data space not implemented for rectangular acquisition.");
        return {0.,0.};
    }

    bool Configuration::data_coordinate_isValid(double xs,double xr) const
    {
        throw common::Exception("Data space not implemented for rectangular acquisition.");
        return false;
    }

    template<Physic T_Physic>
    ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd, const WaveMultiField<T_Physic>& g, const Configuration* const config)
    {
        if( (config->all_emitter_idx(false).size() == 0) && (config->all_receiver_idx(false).size() == 0) )
        {
            return pow(autocorrelate<T_Physic>(g,config->all_emitter_idx(true)),2);
        }
        else
        {
            return autocorrelate<T_Physic>(g,config->all_emitter_idx(true)) * autocorrelate<T_Physic>(g,config->all_receiver_idx(true));
        }
    }

    template ModelMonoFunction green0_preconditioner<Physic::acoustic>(const Data<Physic::acoustic>&, const WaveMultiField<Physic::acoustic>&, const Configuration* const);
    template ModelMonoFunction green0_preconditioner<Physic::electromagnetic>(const Data<Physic::electromagnetic>&, const WaveMultiField<Physic::electromagnetic>&, const Configuration* const);
    template ModelMonoFunction green0_preconditioner<Physic::elastodynamic>(const Data<Physic::elastodynamic>&, const WaveMultiField<Physic::elastodynamic>&, const Configuration* const);

} // namespace surface_acquisition
