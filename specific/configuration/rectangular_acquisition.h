// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_CONFIGURATION_RECTANGULAR_ACQUISITION
#define H_CONFIGURATION_RECTANGULAR_ACQUISITION

//GmshFEM Library
#include "GmshFem.h"
//#include "Function.h"

//GmshFWI Library
#include "../../common/configuration.h"
#include "../../common/wave/element.h"

//Forward declaration
template<Physic T_Physic>
class Data;

namespace rectangular_acquisition
{
  enum class UnknownRegion
  {
      None,
      All
  };
  UnknownRegion to_unknownregion(const gmshfem::common::GmshFem& gmshFem);

  class Configuration final : public ConfigurationInterface
  {
  private:
    double _L;
    double _H;
    double _hx;
    double _hy;
    unsigned int _nx;
    unsigned int _ny;
    double _Ler;
    double _Her;
    unsigned int _em_offset;
    unsigned int _re_offset;
    unsigned int _em_step;
    unsigned int _re_step;
    bool _receiver_on_emitter;
    std::vector<std::array<double,2>> _point_coord;
    std::vector<bool> _point_is_emitter;
    std::vector<bool> _point_is_receiver;

    std::array<gmshfem::domain::Domain,2> _background;

    std::vector<std::complex<double>> _mb;

    UnknownRegion _unknown_region;

    virtual void wave_mesh() const;
    virtual void data_mesh() const;
  public:
    Configuration(std::string name, const ParametrizationInterface* const parametrization, const gmshfem::common::GmshFem& gmshFem);
    virtual void mesh() const;

    virtual std::array<unsigned int,2> data_coordinate_to_index(double xs, double xr) const;
    virtual std::array<double,2> index_to_data_coordinate(unsigned int s, unsigned int r) const;
    virtual bool data_coordinate_isValid(double xs,double xr) const;

    double H() const {return _H;};
    virtual double area() const {return _L * _H;};
    virtual double data_area() const  override
    {
        throw gmshfem::common::Exception("Data space not implemented for rectangular acquisition.");
        return 0.;
    };
    virtual double datapoint_area() const
    {
        throw gmshfem::common::Exception("Data space not implemented for rectangular acquisition.");
        return 0.;
    };
    virtual double array() const {return 2. * (_Ler+_Her);};
    virtual double depth() const {return (_Ler+_Her) / 4.;};

    virtual std::string wave_gmodel() const {return _name;};
    virtual std::string model_gmodel() const {return _name;};
    virtual std::string data_gmodel() const {return _name;};
  };

  template<Physic T_Physic>
  ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd, const WaveMultiField<T_Physic>& g, const Configuration* const config);
} // namespace soil

#endif // H_CONFIGURATION_RECTANGULAR_ACQUISITION
