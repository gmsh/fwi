// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
#include <numeric>
//GmshFem Library
#include "GmshFem.h"
#include "Exception.h"
#include "Message.h"
#include "Domain.h"
#include "Function.h"
//#include "Formulation.h"
#include "CSVio.h"
//Gmsh Library
#include "gmsh.h"
//GmshFWI library
#include "../../common/model/macro.h"
#include "../wave/correlation.h"
#include "surface_acquisition.h"
#include "../../common/data/element.h"

namespace gmodel = gmsh::model;
namespace factory = gmsh::model::geo;

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::domain;
using namespace gmshfem::function;
/*
using namespace gmshfem::field;
using namespace gmshfem::problem;
using namespace gmshfem::equation;
*/
static const std::complex< double > im = std::complex< double >(0., 1.);

namespace surface_acquisition
{
    UnknownRegion to_unknownregion(const GmshFem& gmshFem)
    {
      std::string str;
      if(! gmshFem.userDefinedParameter(str, "unknown") )
      {
        throw common::Exception("The unknown region to consider could not be found.");
      }
      if(str=="subsurface") {return UnknownRegion::Subsurface;}
      else if(str=="none") {return UnknownRegion::None;}
      else if(str=="all") {return UnknownRegion::All;}
      else
      {
        throw common::Exception("The unknown region " + str + " is not handled.");
      }
      return UnknownRegion::None;
    }
    /*
    *  class Configuration
    */
    Configuration::Configuration(std::string name, const ParametrizationInterface* const parametrization, const GmshFem& gmshFem) : ConfigurationInterface(name, parametrization, gmshFem)
    {
        /*
        * MESH
        */
        if(
          !(
            gmshFem.userDefinedParameter(_xoffset, "xoffset") &&
            gmshFem.userDefinedParameter(_ymax, "ymax") &&
            gmshFem.userDefinedParameter(_ymin, "ymin") &&
            gmshFem.userDefinedParameter(_Ler, "Ler") &&
            gmshFem.userDefinedParameter(_np, "np") &&
            gmshFem.userDefinedParameter(_L, "L") &&
            gmshFem.userDefinedParameter(_H, "H") &&
            gmshFem.userDefinedParameter(_h, "h")
            )
        )
        {
          throw common::Exception("A geometric parameter could not be found.");
        }

        _re_start = 0;
        _em_start = 0;
        _re_skip = 1;
        _em_skip = 1;
        unsigned int receiverOnEmitter = 1;
        if(
          !(
            gmshFem.userDefinedParameter(_re_start, "re_start") &&
            gmshFem.userDefinedParameter(_em_start, "em_start") &&
            gmshFem.userDefinedParameter(_re_skip, "re_skip") &&
            gmshFem.userDefinedParameter(_em_skip, "em_skip") &&
            gmshFem.userDefinedParameter(receiverOnEmitter, "ReceiverOnEmitter")
            )
        )
        {
          msg::warning << "An emitter/receiver parameter could not be found." << msg::endl;
        }

        _receiverOnEmitter = ((bool) receiverOnEmitter);

        for (unsigned int p = 0; p < _np; p++)
        {
            if( ( (p+_em_start) % _em_skip ) == 0){_ne++;}
            if( ( (p+_re_start) % _re_skip ) == 0){_nr++;}
        }
        _deltaLe = _Ler / (((double) _np)-1.) * ((double) _em_skip);
        _deltaLr = _Ler / (((double) _np)-1.) * ((double) _re_skip);
        _Lr = (((double) _nr)-1.) * _deltaLr;
        _Le = (((double) _ne)-1.) * _deltaLe;

        mesh();

        /*
        * DOMAIN
        */
        _data_omega = Domain(2,1);
        _subsurface[Support::BLK] = Domain(2, 21);
        _subsurface[Support::BND] = Domain(1, 11);
        _supersurface[Support::BLK] = Domain(2, 22);
        _supersurface[Support::BND] = Domain(1, 12);

        _named_domains["top_bnd"] = Domain("top_bnd");

        _wave_omega[Support::BLK] = _subsurface[Support::BLK] | _supersurface[Support::BLK];
        _wave_omega[Support::BND] = _subsurface[Support::BND] | _supersurface[Support::BND];

        _unknown_region = to_unknownregion(gmshFem);
        switch (_unknown_region)
        {
          case UnknownRegion::None:
              _model_known[Support::BLK] = _subsurface[Support::BLK]|_supersurface[Support::BLK];
              _model_known[Support::BND] = _subsurface[Support::BND] | _supersurface[Support::BND];
              break;
          case UnknownRegion::Subsurface:
              _model_known[Support::BLK] = _supersurface[Support::BLK];
              _model_known[Support::BND] = _supersurface[Support::BND];
              _model_unknown[Support::BLK] = _subsurface[Support::BLK];
              _model_unknown[Support::BND] = _subsurface[Support::BND];
              break;
        case UnknownRegion::All:
            _model_unknown[Support::BLK] = _subsurface[Support::BLK] | _supersurface[Support::BLK];
            _model_unknown[Support::BND] = _subsurface[Support::BND] | _supersurface[Support::BND];
            break;
        }

        _ns = _ne;
        for (unsigned int p = 0; p < _np; p++)
        {
            _point.push_back(Domain(0,100+p));
            _points |= _point[p];

            if( (p+_em_start) % _em_skip == 0)
            {
                _emitter.push_back({p});
                std::vector<unsigned int> rec;
                for (unsigned int q = 0; q < _np; q++)
                {
                    if(
                        ((q+_re_start) % _re_skip == 0)
                        &&
                        (p!=q || _receiverOnEmitter)
                    )
                    {
                        rec.push_back(q);
                    }
                }
                _receiver.push_back(rec);
            }
        }

        /*
        * Reference model function
        */
        _m_super.resize(model_size());
        _m_sub.resize(model_size());
        for (unsigned int c = 0; c < model_size(); c++)
        {
            ScalarPiecewiseFunction< std::complex< double > > m0;
            std::string suffix = "c"+std::to_string(c);
            double Rem_super,Imm_super,Rem_sub,Imm_sub;
            if(!(
                gmshFem.userDefinedParameter(Rem_super, "Re(m_super"+suffix+")") && gmshFem.userDefinedParameter(Imm_super, "Im(m_super"+suffix+")") &&
                gmshFem.userDefinedParameter(Rem_sub, "Re(m_sub"+suffix+")") && gmshFem.userDefinedParameter(Imm_sub, "Im(m_sub"+suffix+")")
            ))
            {
                throw common::Exception("A model component " + suffix + " could not be found.");
            }
            _m_super[c] = Rem_super + im*Imm_super;
            _m_sub[c] = Rem_sub + im*Imm_sub;
            _mc[c] = _m_sub[c];

            m0.addFunction(_m_super[c],_supersurface[Support::BLK] | _supersurface[Support::BND] | _points);

            std::string m0_type;
            if(!gmshFem.userDefinedParameter(m0_type, "m0_type"+suffix))
            {
                throw Exception("Reference model type could not be found.");
            }

            if(m0_type=="file")
            {
                std::string path = "";
                if(!gmshFem.userDefinedParameter(path, "m0_path"+suffix))
                {
                    throw common::Exception("Path to subsurface data could not be found.");
                }
                m0.addFunction(bilinearInterpolation(path),_subsurface[Support::BLK] | _subsurface[Support::BND] );
            }
            else if(m0_type=="file.pos")
            {
                std::string path = "";
                if(!gmshFem.userDefinedParameter(path, "m0_path"+suffix))
                {
                    throw common::Exception("Path to subsurface data could not be found.");
                }

                gmsh::merge(path+suffix+".pos");
                ScalarFunction<std::complex<double>> mpos = probeScalarView<std::complex<double>>(c);

                m0.addFunction(mpos,_subsurface[Support::BLK] | _subsurface[Support::BND] );
            }
            else if(m0_type=="inverse_linear_squared")
            {
                double Rea_0,Ima_0;
                if(!(
                    gmshFem.userDefinedParameter(Rea_0, "Re(a0"+suffix+")") && gmshFem.userDefinedParameter(Ima_0, "Im(a0"+suffix+")")
                ))
                {
                    throw common::Exception("Initial model parameter (a0) could not be found.");
                }

                double Rea_H,Ima_H;
                if(!(
                    gmshFem.userDefinedParameter(Rea_H, "Re(aH"+suffix+")") && gmshFem.userDefinedParameter(Ima_H, "Im(aH"+suffix+")")
                ))
                {
                    throw common::Exception("Initial model parameter (aH) could not be found.");
                }

                ScalarFunction< std::complex<double> > num = (Rea_0+im*Ima_0) -  ( (Rea_H+im*Ima_H) - (Rea_0+im*Ima_0) ) / H() * y< std::complex<double> >();
                m0.addFunction(1. / pow(num,2),_subsurface[Support::BLK] | _subsurface[Support::BND]);
            }
            else if(m0_type=="linear")
            {
                double Rea_0,Ima_0;
                if(!(
                    gmshFem.userDefinedParameter(Rea_0, "Re(a0"+suffix+")") && gmshFem.userDefinedParameter(Ima_0, "Im(a0"+suffix+")")
                ))
                {
                    throw common::Exception("Initial model parameter (a0) could not be found.");
                }

                double Rea_H,Ima_H;
                if(!(
                    gmshFem.userDefinedParameter(Rea_H, "Re(aH"+suffix+")") && gmshFem.userDefinedParameter(Ima_H, "Im(aH"+suffix+")")
                ))
                {
                    throw common::Exception("Initial model parameter (aH) could not be found.");
                }

                ScalarFunction< std::complex<double> > lin = (Rea_0+im*Ima_0) -  ( (Rea_H+im*Ima_H) - (Rea_0+im*Ima_0) ) / H() * (y< std::complex<double> >()+_ymin);
                m0.addFunction(lin,_subsurface[Support::BLK] | _subsurface[Support::BND]);
            }
            else if(m0_type=="constant")
            {
                m0.addFunction(_m_sub[c],_subsurface[Support::BLK] | _subsurface[Support::BND]);
            }
            else
            {
                throw common::Exception("Initial model type ("+ suffix + ") " + m0_type +" is unknown.");
            }
            _m0.push_back(m0);
        }
    }

    
    void Configuration::wave_mesh() const
    {
        double Le0 = (_L - _Ler)/2.;
        double deltaLer = _Ler/(((double) _np)-1.);

        int pymaxl = factory::addPoint(_xoffset, _ymax, 0., _h);
        int p0l = factory::addPoint(_xoffset,    0., 0., _h);
        int pHl = factory::addPoint(_xoffset,   -_H, 0., _h);
        //Emitters/receivers at y
        std::vector<int> pymax(_np);
        std::vector<int> p0(_np);
        std::vector<int> pH(_np);
        for (unsigned int n = 0; n < _np; n++)
        {
          double Ln = Le0 + ((double)n)*deltaLer;
          pymax[n] = factory::addPoint(_xoffset+Ln, _ymax, 0., _h);
          p0[n] = factory::addPoint(_xoffset+Ln,    0., 0., _h);
          pH[n] = factory::addPoint(_xoffset+Ln,   -_H, 0., _h);
        }
        int pymaxr = factory::addPoint(_xoffset+_L, _ymax, 0., _h);
        int p0r = factory::addPoint(_xoffset+_L,    0., 0., _h);
        int pHr = factory::addPoint(_xoffset+_L,   -_H, 0., _h);

        std::vector<int> lymax(_np+1);
        std::vector<int> l0(_np+1);
        std::vector<int> lH(_np+1);
        lymax[0] = factory::addLine(pymaxl, pymax[0]);
        l0[0] = factory::addLine(p0l, p0[0]);
        lH[0] = factory::addLine(pHl, pH[0]);
        for (unsigned int n = 1; n < _np; n++)
        {
          lymax[n] = factory::addLine(pymax[n-1], pymax[n]);
          l0[n] = factory::addLine(p0[n-1], p0[n]);
          lH[n] = factory::addLine(pH[n-1], pH[n]);
        }
        lymax[_np] = factory::addLine(pymax[_np-1], pymaxr);
        l0[_np] = factory::addLine(p0[_np-1], p0r);
        lH[_np] = factory::addLine(pH[_np-1], pHr);

        std::vector<int> lymax_reverse(_np+1);
        std::vector<int> l0_reverse(_np+1);
        std::vector<int> lH_reverse(_np+1);
        for (unsigned int n = 0; n < _np+1; n++)
        {
          lymax_reverse[n] = -lymax[n];
          l0_reverse[n] = -l0[n];
          lH_reverse[n] = -lH[n];
        }

        if(_ymin != 0.)
        {
            int pyminl = factory::addPoint(_xoffset, -_ymin, 0., _h);
            //Emitters/receivers at y
            std::vector<int> pymin(_np);
            for (unsigned int n = 0; n < _np; n++)
            {
                double Ln = Le0 + ((double)n)*deltaLer;
                pymin[n] = factory::addPoint(_xoffset+Ln, -_ymin, 0., _h);
            }
            int pyminr = factory::addPoint(_xoffset+_L, -_ymin, 0., _h);

            std::vector<int> lymin(_np+1);
            lymin[0] = factory::addLine(pyminl, pymin[0]);
            for (unsigned int n = 1; n < _np; n++)
            {
                lymin[n] = factory::addLine(pymin[n-1], pymin[n]);
            }
            lymin[_np] = factory::addLine(pymin[_np-1], pyminr);

            std::vector<int> lymin_reverse(_np+1);
            for (unsigned int n = 0; n < _np+1; n++)
            {
                lymin_reverse[n] = -lymin[n];
            }

            // Nomenclature: line from one y to another, left or right. All lines go up
            int lHyminl = factory::addLine(pHl,pyminl);
            int lymin0l = factory::addLine(pyminl,p0l);
            int l0ymaxl = factory::addLine(p0l,pymaxl);
            int lHyminr = factory::addLine(pHr,pyminr);
            int lymin0r = factory::addLine(pyminr,p0r);
            int l0ymaxr = factory::addLine(p0r,pymaxr);

            std::vector<int> clbv1max = {l0ymaxl};
            clbv1max.insert(std::end(clbv1max), std::begin(lymax), std::end(lymax));
            clbv1max.push_back({-l0ymaxr});
            clbv1max.insert(std::end(clbv1max), std::begin(l0_reverse), std::end(l0_reverse));

            int clb1max = factory::addCurveLoop({clbv1max});

            std::vector<int> clbv1min = {lymin0l};
            clbv1min.insert(std::end(clbv1min), std::begin(l0), std::end(l0));
            clbv1min.push_back({-lymin0r});
            clbv1min.insert(std::end(clbv1min), std::begin(lymin_reverse), std::end(lymin_reverse));

            int clb1min = factory::addCurveLoop({clbv1min});

            std::vector<int> clbv2 = {lHyminl};
            clbv2.insert(std::end(clbv2), std::begin(lymin), std::end(lymin));
            clbv2.push_back({-lHyminr});
            clbv2.insert(std::end(clbv2), std::begin(lH_reverse), std::end(lH_reverse));

            int clb2 = factory::addCurveLoop({clbv2});

            int s_supermax = factory::addPlaneSurface({clb1max});
            int s_supermin = factory::addPlaneSurface({clb1min});
            int s_sub = factory::addPlaneSurface({clb2});

            unsigned int n_ymax = ((unsigned int)std::round(_ymax / _h))+1;
            unsigned int n_ymin = ((unsigned int)std::round(_ymin / _h))+1;
            unsigned int n_H = ((unsigned int)std::round( (_H-_ymin) / _h))+1;
            unsigned int n_r = ((unsigned int)std::round(_Ler / _h / (_np-1)))+1;
            unsigned int n_bord = ((unsigned int)std::round((_L-_Ler) / 2. / _h))+1;

            factory::mesh::setTransfiniteCurve(lHyminl,n_H);
            factory::mesh::setTransfiniteCurve(lHyminr,n_H);
            factory::mesh::setTransfiniteCurve(lymin0l,n_ymin);
            factory::mesh::setTransfiniteCurve(lymin0r,n_ymin);
            factory::mesh::setTransfiniteCurve(l0ymaxl,n_ymax);
            factory::mesh::setTransfiniteCurve(l0ymaxr,n_ymax);

            factory::mesh::setTransfiniteCurve(lymax[0],n_bord);
            factory::mesh::setTransfiniteCurve(l0[0],n_bord);
            factory::mesh::setTransfiniteCurve(lymin[0],n_bord);
            factory::mesh::setTransfiniteCurve(lH[0],n_bord);
            factory::mesh::setTransfiniteCurve(lymax[_np],n_bord);
            factory::mesh::setTransfiniteCurve(l0[_np],n_bord);
            factory::mesh::setTransfiniteCurve(lymin[_np],n_bord);
            factory::mesh::setTransfiniteCurve(lH[_np],n_bord);
            for (unsigned int n = 1; n < _np; n++)
            {
                factory::mesh::setTransfiniteCurve(lymax[n],n_r);
                factory::mesh::setTransfiniteCurve(l0[n],n_r);
                factory::mesh::setTransfiniteCurve(lymin[n],n_r);
                factory::mesh::setTransfiniteCurve(lH[n],n_r);
            }

            factory::mesh::setTransfiniteSurface(s_supermax,"Left",{p0l,pymaxl,pymaxr,p0r});
            factory::mesh::setTransfiniteSurface(s_supermin,"Left",{pyminl,p0l,p0r,pyminr});
            factory::mesh::setTransfiniteSurface(s_sub,"Left",{pHl,pyminl,pyminr,pHr});

            factory::mesh::setRecombine(2,s_supermax);
            factory::mesh::setRecombine(2,s_supermin);
            factory::mesh::setRecombine(2,s_sub);

            factory::synchronize();
            for (unsigned int n = 0; n <_np; n++)
            {
                gmodel::addPhysicalGroup(0, {p0[n]}, 100+n);
                gmodel::setPhysicalName(0, 100+n, "emitter_receiver_"+std::to_string(n));
            }

            std::vector<int> lsub = {-lHyminr};
            lsub.insert(std::end(lsub), std::begin(lH_reverse), std::end(lH_reverse));
            lsub.push_back({lHyminl});
            gmodel::addPhysicalGroup(1, lsub, 11);
            gmodel::setPhysicalName(1, 11, "subsurface_bnd");

            std::vector<int> lsuper = {lymin0l,l0ymaxl};
            lsuper.insert(std::end(lsuper), std::begin(lymax), std::end(lymax));
            lsuper.push_back({-l0ymaxr});
            lsuper.push_back({-lymin0r});
            gmodel::addPhysicalGroup(1, lsuper, 12);
            gmodel::setPhysicalName(1, 12, "supersurface_bnd");

            // Add another physical groupe for the top boundary, to represent e.g. reflection at the air-water interface
            gmodel::addPhysicalGroup(1, lymax, 13);
            gmodel::setPhysicalName(1, 13, "top_bnd");

            gmodel::addPhysicalGroup(2, {s_sub}, 21);
            gmodel::setPhysicalName(2, 21, "subsurface_vol");
            gmodel::addPhysicalGroup(2, {s_supermin,s_supermax}, 22);
            gmodel::setPhysicalName(2, 22, "supersurface_vol");
        }
        else
        {
            int lH0l = factory::addLine(pHl,p0l);
            int l0ymaxl = factory::addLine(p0l,pymaxl);
            int lH0r = factory::addLine(pHr,p0r);
            int l0ymaxr = factory::addLine(p0r,pymaxr);

            std::vector<int> clbv1max = {l0ymaxl};
            clbv1max.insert(std::end(clbv1max), std::begin(lymax), std::end(lymax));
            clbv1max.push_back({-l0ymaxr});
            clbv1max.insert(std::end(clbv1max), std::begin(l0_reverse), std::end(l0_reverse));

            int clb1max = factory::addCurveLoop({clbv1max});

            std::vector<int> clbv2 = {lH0l};
            clbv2.insert(std::end(clbv2), std::begin(l0), std::end(l0));
            clbv2.push_back({-lH0r});
            clbv2.insert(std::end(clbv2), std::begin(lH_reverse), std::end(lH_reverse));

            int clb2 = factory::addCurveLoop({clbv2});

            int s_supermax = factory::addPlaneSurface({clb1max});
            int s_sub = factory::addPlaneSurface({clb2});

            unsigned int n_ymax = ((unsigned int)std::round(_ymax / _h))+1;
            unsigned int n_H = ((unsigned int)std::round( (_H) / _h))+1;
            unsigned int n_r = ((unsigned int)std::round(_Ler / _h / (_np-1)))+1;
            unsigned int n_bord = ((unsigned int)std::round((_L-_Ler) / 2. / _h))+1;

            factory::mesh::setTransfiniteCurve(lH0l,n_H);
            factory::mesh::setTransfiniteCurve(lH0r,n_H);
            factory::mesh::setTransfiniteCurve(l0ymaxl,n_ymax);
            factory::mesh::setTransfiniteCurve(l0ymaxr,n_ymax);

            factory::mesh::setTransfiniteCurve(lymax[0],n_bord);
            factory::mesh::setTransfiniteCurve(l0[0],n_bord);
            factory::mesh::setTransfiniteCurve(lH[0],n_bord);
            factory::mesh::setTransfiniteCurve(lymax[_np],n_bord);
            factory::mesh::setTransfiniteCurve(l0[_np],n_bord);
            factory::mesh::setTransfiniteCurve(lH[_np],n_bord);
            for (unsigned int n = 1; n < _np; n++)
            {
                factory::mesh::setTransfiniteCurve(lymax[n],n_r);
                factory::mesh::setTransfiniteCurve(l0[n],n_r);
                factory::mesh::setTransfiniteCurve(lH[n],n_r);
            }

            factory::mesh::setTransfiniteSurface(s_supermax,"Left",{p0l,pymaxl,pymaxr,p0r});
            factory::mesh::setTransfiniteSurface(s_sub,"Left",{pHl,p0l,p0r,pHr});

            factory::mesh::setRecombine(2,s_supermax);
            factory::mesh::setRecombine(2,s_sub);

            factory::synchronize();
            for (unsigned int n = 0; n <_np; n++)
            {
                gmodel::addPhysicalGroup(0, {p0[n]}, 100+n);
                gmodel::setPhysicalName(0, 100+n, "emitter_receiver_"+std::to_string(n));
            }

            std::vector<int> lsub = {-lH0r};
            lsub.insert(std::end(lsub), std::begin(lH_reverse), std::end(lH_reverse));
            lsub.push_back({lH0l});
            gmodel::addPhysicalGroup(1, lsub, 11);
            gmodel::setPhysicalName(1, 11, "subsurface_bnd");

            std::vector<int> lsuper = {l0ymaxl};
            lsuper.insert(std::end(lsuper), std::begin(lymax), std::end(lymax));
            lsuper.push_back({-l0ymaxr});
            gmodel::addPhysicalGroup(1, lsuper, 12);
            gmodel::setPhysicalName(1, 12, "supersurface_bnd");

            // Add another physical groupe for the top boundary, to represent e.g. reflection at the air-water interface
            gmodel::addPhysicalGroup(1, lymax, 13);
            gmodel::setPhysicalName(1, 13, "top_bnd");

            gmodel::addPhysicalGroup(2, {s_sub}, 21);
            gmodel::setPhysicalName(2, 21, "subsurface_vol");
            gmodel::addPhysicalGroup(2, {s_supermax}, 22);
            gmodel::setPhysicalName(2, 22, "supersurface_vol");
        }
    }
    void Configuration::data_mesh() const
    {
        int p00 = factory::addPoint(0., 0.,1.,_deltaLe);
        int pL0 = factory::addPoint(_Le, 0.,1.,_deltaLe);
        int pLL = factory::addPoint(_Le, _Lr,1.,_deltaLr);
        int p0L = factory::addPoint(0., _Lr,1.,_deltaLr);

        int ly0 = factory::addLine(p00, pL0);
        int lxL = factory::addLine(pL0, pLL);
        int lyL = factory::addLine(pLL, p0L);
        int lx0 = factory::addLine(p0L, p00);

        int cl = factory::addCurveLoop({ly0,lxL,lyL,lx0});
        int s1 = factory::addPlaneSurface({cl});

        factory::mesh::setTransfiniteCurve(ly0,_ne);
        factory::mesh::setTransfiniteCurve(lxL,_nr);
        factory::mesh::setTransfiniteCurve(lyL,_ne);
        factory::mesh::setTransfiniteCurve(lx0,_nr);

        factory::mesh::setTransfiniteSurface(s1,"Left",{p00,pL0,pLL,p0L});

        factory::mesh::setRecombine(2,s1);

        factory::synchronize();
        gmodel::addPhysicalGroup(2, {s1}, 1);
        gmodel::setPhysicalName(2, 1, "data_omega");
    }

    std::array<unsigned int,2> Configuration::data_coordinate_to_index(double xs, double xr) const
    {
        if(!data_coordinate_isValid(xs,xr))
        {
            throw common::Exception("Data coordinate: ("+std::to_string(xs)+","+std::to_string(xr)+")"+" is not valid.");
        }
        unsigned int s = std::round(xs/_deltaLe);
        unsigned int r = std::round(xr/_deltaLr);
        std::array<unsigned int,2> index;
        index[0] = s;
        if(r<s || _receiverOnEmitter)
        {
            index[1] = r;
        }
        else if(r>0)
        {
            index[1] = r-1;
        }
        else
        {
            index[1] = 0;
        }
        return index;
    }

    std::array<double,2> Configuration::index_to_data_coordinate(unsigned int s, unsigned int r) const
    {
        std::array<double,2> coord;
        coord[0] = ((double) s) * _deltaLe;
        if(r<s || _receiverOnEmitter){coord[1] = ((double) r)*_deltaLr;}
        else{coord[1] = ((double) r+1)*_deltaLr;}
        return coord;
    }

    bool Configuration::data_coordinate_isValid(double xs,double xr) const
    {
        double eps = 1e-8;
        gmsh::option::getNumber("Geometry.MatchMeshTolerance",eps);
        if( (!_receiverOnEmitter) && (std::abs(xr-xs) < eps) ){return false;}

        return (-eps <= xs) && (xs <= _Le+eps) && (-eps <= xr) && (xr <= _Lr+eps);
    }

    template<Physic T_Physic>
    ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd, const WaveMultiField<T_Physic>& g, const Configuration* const config)
    {
        if( (config->all_emitter_idx(false).size() == 0) && (config->all_receiver_idx(false).size() == 0) )
        {
            return pow(autocorrelate<T_Physic>(g,config->all_emitter_idx(true)),2);
        }
        else
        {
            return autocorrelate<T_Physic>(g,config->all_emitter_idx(true)) * autocorrelate<T_Physic>(g,config->all_receiver_idx(true));
        }
    }

    template ModelMonoFunction green0_preconditioner<Physic::acoustic>(const Data<Physic::acoustic>&, const WaveMultiField<Physic::acoustic>&, const Configuration* const);
    template ModelMonoFunction green0_preconditioner<Physic::electromagnetic>(const Data<Physic::electromagnetic>&, const WaveMultiField<Physic::electromagnetic>&, const Configuration* const);
    template ModelMonoFunction green0_preconditioner<Physic::elastodynamic>(const Data<Physic::elastodynamic>&, const WaveMultiField<Physic::elastodynamic>&, const Configuration* const);

} // namespace surface_acquisition
