// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_CONFIGURATION_SURFACE_ACQUISITION
#define H_CONFIGURATION_SURFACE_ACQUISITION

//GmshFEM Library
#include "GmshFem.h"
//#include "Function.h"

//GmshFWI Library
#include "../../common/configuration.h"
#include "../../common/wave/element.h"

//Forward declaration
template<Physic T_Physic>
class Data;

namespace surface_acquisition
{
  enum class UnknownRegion
  {
      None,
      Subsurface,
      All
  };
  UnknownRegion to_unknownregion(const gmshfem::common::GmshFem& gmshFem);

  class Configuration final : public ConfigurationInterface
  {
  private:
    double _Ler;
    unsigned int _ne;
    unsigned int _nr;
    unsigned int _re_start;
    unsigned int _em_start;
    unsigned int _re_skip;
    unsigned int _em_skip;
    bool _receiverOnEmitter;
    double _deltaLe;
    double _deltaLr;
    double _Le;
    double _Lr;
    double _xoffset;
    double _ymax;
    double _ymin;
    double _L;
    double _H;
    double _h;

    std::array<gmshfem::domain::Domain,2> _subsurface;    std::array<gmshfem::domain::Domain,2> _supersurface;

    std::vector<std::complex<double>> _m_super;
    std::vector<std::complex<double>> _m_sub;

    UnknownRegion _unknown_region;

    virtual void wave_mesh() const;
    virtual void data_mesh() const;
  public:
    Configuration(std::string name, const ParametrizationInterface* const parametrization, const gmshfem::common::GmshFem& gmshFem);
    unsigned int ne() const {return _ne;};
    unsigned int nr() const {return _nr;};

    virtual std::array<unsigned int,2> data_coordinate_to_index(double xs, double xr) const;
    virtual std::array<double,2> index_to_data_coordinate(unsigned int s, unsigned int r) const;
    virtual bool data_coordinate_isValid(double xs,double xr) const;

    double H() const {return _H;};
    virtual double area() const {return _L * _H;};
    virtual double data_area() const  override {return _Ler * _Ler;};
    virtual double datapoint_area() const {return _Ler * _Ler / (double(_ne-1)) / (double(_nr-1));};
    virtual double array() const {return _Ler;};
    virtual double depth() const {return _H;};

    gmshfem::domain::Domain subsurface(Support support) const {return _subsurface[support];};
    gmshfem::domain::Domain supersurface(Support support) const {return _supersurface[support];};

    virtual std::string wave_gmodel() const {return _name;};
    virtual std::string model_gmodel() const {return _name;};
    virtual std::string data_gmodel() const {return _name;};
  };

  template<Physic T_Physic>
  ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd, const WaveMultiField<T_Physic>& g, const Configuration* const config);
} // namespace soil

#endif // H_CONFIGURATION_SURFACE_ACQUISITION
