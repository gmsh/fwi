// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
#include <numeric>
#include <array>

//GmshFEM Library
#include "GmshFem.h"
#include "Exception.h"
#include "Message.h"
#include "Domain.h"
#include "Function.h"
#include "CSVio.h"

//Gmsh Library
#include "gmsh.h"

//GmshFWI Library
#include "../../common/model/macro.h"
#include "../../common/data/element.h"
#include "../wave/correlation.h"
#include "volume_acquisition.h"

namespace gmodel = gmsh::model;
namespace factory = gmsh::model::geo;

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::domain;
using namespace gmshfem::function;

static const std::complex< double > im = std::complex< double >(0., 1.);

namespace volume_acquisition
{
    SpatialDistributionInclusion to_spatialdistributioninclusion(const GmshFem& gmshFem)
    {
      std::string buffer;
      if(gmshFem.userDefinedParameter(buffer, "inclusion_spatialdistribution"))
      {
        if(buffer=="constant"){return SpatialDistributionInclusion::Constant;}
        else if(buffer=="heavisidey0"){return SpatialDistributionInclusion::HeavisideY0;}
      }
      return SpatialDistributionInclusion::Constant;
    }
    UnknownRegion to_unknownregion(const GmshFem& gmshFem)
    {
        std::string str;
        if(! gmshFem.userDefinedParameter(str, "unknown") )
        {
        throw common::Exception("The unknown region to consider could not be found.");
        }
        if(str=="all") {return UnknownRegion::All;}
        else if(str=="inclusion") {return UnknownRegion::Inclusion;}
        else if(str=="background") {return UnknownRegion::Background;}
        else if(str=="none") {return UnknownRegion::None;}
        else
        {
        throw common::Exception("The unknown region " + str + " is not handled.");
        }
        return UnknownRegion::None;
    }
    /*
    *  class Configuration
    */
    Configuration::Configuration(std::string name, const ParametrizationInterface* const parametrization, const GmshFem& gmshFem) : ConfigurationInterface(name, parametrization, gmshFem), _antenna_factory(newAntennaFactory(gmshFem))
    {
        msg::print << "Initialize inclusion configuration" << msg::endl;

        /*
        * MESH
        */
        if(
          !(
            gmshFem.userDefinedParameter(_xe, "xe") &&
            gmshFem.userDefinedParameter(_xr, "xr") &&
            gmshFem.userDefinedParameter(_ye, "ye") &&
            gmshFem.userDefinedParameter(_yr, "yr") &&
            gmshFem.userDefinedParameter(_He, "He") &&
            gmshFem.userDefinedParameter(_Hr, "Hr") &&
            gmshFem.userDefinedParameter(_Le, "Le") &&
            gmshFem.userDefinedParameter(_Lr, "Lr") &&
            gmshFem.userDefinedParameter(_nxe, "nxe") &&
            gmshFem.userDefinedParameter(_nxr, "nxr") &&
            gmshFem.userDefinedParameter(_nye, "nye") &&
            gmshFem.userDefinedParameter(_nyr, "nyr") &&
            gmshFem.userDefinedParameter(_L, "L") &&
            gmshFem.userDefinedParameter(_H, "H") &&
            gmshFem.userDefinedParameter(_h, "h")
            )
        )
        {
          throw common::Exception("A geometric parameter could not be found.");
        }
        gmshFem.userDefinedParameter(_ni, "inclusion_ni");
        if(_ni!=0)
        {
            unsigned int filled = 0;
            if(!gmshFem.userDefinedParameter(filled, "inclusion_filled"))
            {
                throw common::Exception("Inclusion filling could not be found.");
            }
            _areFilled = ((bool) filled);

            _inclusion_geo.resize(_ni);
            for (unsigned int i = 0; i < _ni; i++)
            {
                _inclusion_geo[i] = newInclusion(gmshFem,std::to_string(i));
            }
        }
        unsigned int r_on_e = 0;
        gmshFem.userDefinedParameter(r_on_e, "receiver_on_emitter");
        _receiver_on_emitter = ((bool) r_on_e);

        mesh();

        /*
        * DOMAIN
        */
        if(nxe() != 0){_data_omega |= Domain(2,1);}
        if(nye() != 0){_data_omega |= Domain(2,2);}

        _background[Support::BLK] = Domain(2,21);
        _background[Support::BND] = Domain(1,12);

        _inclusion.resize(_ni);
        for (unsigned int i = 0; i < _ni; i++)
        {
            if(_areFilled)
            {
                _inclusion[i][Support::BLK] = Domain(2,10000+i);
                _inclusions[Support::BLK] |= Domain(2,10000+i);
            }
            else
            {
                _inclusion[i][Support::BND] = Domain(1,20000+i);
                _inclusions[Support::BND] |= Domain(1,20000+i);
            }
        }

        _wave_omega[Support::BLK] = _background[Support::BLK] | _inclusions[Support::BLK];
        _wave_omega[Support::BND] = _background[Support::BND] | _inclusions[Support::BND];
        _unknown_region = to_unknownregion(gmshFem);
        switch (_unknown_region)
        {
          case UnknownRegion::Inclusion:
            _model_known = _background;
            _model_unknown = _inclusions;
            break;
          case UnknownRegion::Background:
            _model_known = _inclusions;
            _model_unknown = _background;
            break;
          case UnknownRegion::All:
            _model_unknown[Support::BLK] = _background[Support::BLK] | _inclusions[Support::BLK];
            _model_unknown[Support::BND] = _background[Support::BND] | _inclusions[Support::BND];
            break;
          case UnknownRegion::None: default:
            _model_known[Support::BLK] = _background[Support::BLK] | _inclusions[Support::BLK];
            _model_known[Support::BND] = _background[Support::BND] | _inclusions[Support::BND];
            break;
        }

        _ns = _nxe+_nye;
        _np = _nxe+_nye;
        //Emitters X
        for (unsigned int e = 0; e < _nxe; e++)
        {
          _point.push_back(Domain(_antenna_factory->dim(),1000+e));
        }
        _emitter_idx_X.resize(_nxe);
        std::iota(_emitter_idx_X.begin(),_emitter_idx_X.end(),0);

        //Emitters Y
        for (unsigned int e = 0; e < _nye; e++)
        {
          _point.push_back(Domain(_antenna_factory->dim(),3000+e));
        }
        _emitter_idx_Y.resize(_nye);
        std::iota(_emitter_idx_Y.begin(),_emitter_idx_Y.end(),_nxe);

        //Receivers X
        if(!receiversAreEmittersX())
        {
          _np+=_nxr;
          for (unsigned int r = 0; r < _nxr; r++)
          {
            _point.push_back(Domain(_antenna_factory->dim(),2000+r));
          }
          _receiver_idx_X.resize(_nxr);
          std::iota(_receiver_idx_X.begin(),_receiver_idx_X.end(),_nxe+_nye);
        }
        else
        {
            _receiver_idx_X = _emitter_idx_X;
        }

        //Receivers Y
        if(!receiversAreEmittersY())
        {
          _np+=_nyr;
          for (unsigned int r = 0; r < _nyr; r++)
          {
            _point.push_back(Domain(_antenna_factory->dim(),4000+r));
          }
          _receiver_idx_Y.resize(_nyr);
          std::iota(_receiver_idx_Y.begin(),_receiver_idx_Y.end(),_nxe+_nye+_nxr);
        }
        else
        {
            _receiver_idx_Y = _emitter_idx_Y;
        }

        for (unsigned int p = 0; p < _np; p++)
        {
          _points |= _point[p];
        }

        typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree1 >::Object ey(0.,1.,0.);
        for (unsigned int s = 0; s < _nxe; s++)
        {
          _emitter.push_back({s});
          _emitter_polarization.emplace_back(1,ey);
          std::vector<unsigned int> rec(_nxr,0);
          if(receiversAreEmittersX())
          {
            std::iota(rec.begin(),rec.end(),0);
            if(!_receiver_on_emitter)
            {
                rec.erase(rec.begin()+s);
            }
            _receiver.push_back(rec);
          }
          else
          {
            std::iota(rec.begin(),rec.end(),_nxe+_nye);
            _receiver.push_back(rec);
          }
          _receiver_polarization.emplace_back(rec.size(),ey);
        }

        typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree1 >::Object ex(1.,0.,0.);
        for (unsigned int s = _nxe; s < _nxe+_nye; s++)
        {
          _emitter.push_back({s});
          _emitter_polarization.emplace_back(1,ex);
          std::vector<unsigned int> rec(_nyr,0);
          if(receiversAreEmittersY())
          {
            std::iota(rec.begin(),rec.end(),_nxe);
            if(!_receiver_on_emitter)
            {
                rec.erase(rec.begin()+s-_nxe);
            }
            _receiver.push_back(rec);
          }
          else
          {
            unsigned int _n = _nxe+_nye;
            if(!receiversAreEmittersX()){_n+=_nxr;}
            std::iota(rec.begin(),rec.end(),_n);
            _receiver.push_back(rec);
          }
          _receiver_polarization.emplace_back(rec.size(),ex);
        }

        /*
        * Reference model function
        */
        _mb.resize(model_size());
        _mi.resize(model_size());
        for (unsigned int c = 0; c < model_size(); c++)
        {
            double Remb, Immb;
            if
            (!
                (
                    gmshFem.userDefinedParameter(Remb, "Re(mbc"+std::to_string(c)+")") &&
                    gmshFem.userDefinedParameter(Immb, "Im(mbc"+std::to_string(c)+")")
                )
            )
            {
                throw common::Exception("Background (reference) model parameter (component"+std::to_string(c)+") could not be found.");
            }
            else
            {
                _mb[c] = Remb + im * Immb;
                _mc[c] = _mb[c];
            }
            ScalarPiecewiseFunction< std::complex< double > > m0;
            std::string path;
            if(gmshFem.userDefinedParameter(path, "mb_path"))
            {
                m0.addFunction(bilinearInterpolation(path,true),_background[Support::BLK] | _background[Support::BND] | _points);
            }
            else
            {
                m0.addFunction(_mb[c],_background[Support::BLK] | _background[Support::BND] | _points);
            }

            _mi[c].resize(_ni);
            _inclusion_spatialdistribution = to_spatialdistributioninclusion(gmshFem);
            for (unsigned int i = 0; i < _ni; i++)
            {
                double Remi=0., Immi=0.;
                if
                (
                    !(gmshFem.userDefinedParameter(Remi, "Re(mi"+std::to_string(i)+"c"+std::to_string(c)+")") &&
                    gmshFem.userDefinedParameter(Immi, "Im(mi"+std::to_string(i)+"c"+std::to_string(c)+")"))
                )
                {
                    throw Exception("Inclusion model parameter could not be found.");
                }
                _mi[c][i] = Remi + im * Immi;

                switch (_inclusion_spatialdistribution)
                {
                    case SpatialDistributionInclusion::Constant:
                    m0.addFunction(_mi[c][i],_inclusion[i][Support::BLK] | _inclusion[i][Support::BND]);
                    break;
                    case SpatialDistributionInclusion::HeavisideY0:
                    double Remjp = 0.;
                    double Immjp = 0.;
                    double yjp = 0.;
                    std::complex<double> mjp = 0.;
                    if(!(
                        gmshFem.userDefinedParameter(Remjp, "Re(mjump)") &&
                        gmshFem.userDefinedParameter(Immjp, "Im(mjump)") &&
                        gmshFem.userDefinedParameter(yjp, "yjump")
                    ))
                    {
                        throw Exception("A spatial distribution parameter could not be found.");
                    }
                    mjp = Remjp + im * Immjp;
                    m0.addFunction(
                        _mi[c][i] + (2. * heaviside( y<std::complex<double>>() - yjp ) -1. ) * mjp,
                        _inclusion[i][Support::BLK] | _inclusion[i][Support::BND]
                    );
                    break;
                }
            }
            _m0.push_back(m0);
        }
    }

    void Configuration::wave_mesh() const
    {
        int pb1 = factory::addPoint(0., 0., 0., _h);
        int pb2 = factory::addPoint(_L, 0., 0., _h);
        int pb3 = factory::addPoint(_L, _H, 0., _h);
        int pb4 = factory::addPoint(0., _H, 0., _h);

        int lbh1 = factory::addLine(pb1, pb2);
        int lbv1 = factory::addLine(pb2, pb3);
        int lbh2 = factory::addLine(pb3, pb4);
        int lbv2 = factory::addLine(pb4, pb1);

        int clb = factory::addCurveLoop({lbh1,lbv1,lbh2,lbv2});

        typename gmshfem::MathObject< double, gmshfem::Degree::Degree1 >::Object ey(0.,1.,0.);
        //Vertical emitters
        std::vector<Antenna> px(_nxe);
        double He0 = (_H - _He)/2.;
        double deltaHe = 0.;
        if(_nxe>1){deltaHe=_He/(((double) _nxe)-1.);}
        for (unsigned int e = 0; e < _nxe; e++)
        {
            double Hee = He0 + ((double)e)*deltaHe;
            px[e] = _antenna_factory->addAntenna(_xe, Hee, 0., _h, ey);
        }

        if(!receiversAreEmittersX())
        {
        //Vertical receivers
        px.resize(_nxe+_nxr);
        double Hr0 = (_H - _Hr)/2.;
        double deltaHr = _Hr/(((double) _nxr)-1.);
        for (unsigned int r = 0; r < _nxr; r++)
        {
            double Hrr = Hr0 + ((double)r)*deltaHr;
            px[_nxe+r] = _antenna_factory->addAntenna(_xr, Hrr, 0., _h, ey);
        }
        }

        typename gmshfem::MathObject< double, gmshfem::Degree::Degree1 >::Object ex(1.,0.,0.);
        //Horizontal emitters
        std::vector<Antenna> py(_nye);
        double Le0 = (_L - _Le)/2.;
        double deltaLe = _Le/(((double) _nye)-1.);
        for (unsigned int e = 0; e < _nye; e++)
        {
            double Lee = Le0 + ((double)e)*deltaLe;
            py[e] = _antenna_factory->addAntenna(Lee, _ye, 0., _h, ex);
        }
        if(!receiversAreEmittersY())
        {
            //Horizontal receivers
            py.resize(_nye+_nyr);
            double Lr0 = (_L - _Lr)/2.;
            double deltaLr = _Lr/(((double) _nyr)-1.);
            for (unsigned int r = 0; r < _nyr; r++)
            {
                double Lrr = Lr0 + ((double)r)*deltaLr;
                py[_nye+r] = _antenna_factory->addAntenna(Lrr,_yr, 0., _h, ex);
            }
        }

        int sb;
        std::vector<int> si;
        std::vector<std::vector<int>> li(_ni);
        std::vector<int> li_tot;
        if(_ni!=0)
        {
            std::vector<int> cli(_ni,0);
            if(_areFilled){si.resize(_ni);}
            for (unsigned int i = 0; i < _ni; i++)
            {
              li[i] = _inclusion_geo[i]->addInclusion();
              li_tot.insert(li_tot.end(), li[i].begin(), li[i].end());
              cli[i] = factory::addCurveLoop(li[i]);
              if(_areFilled)
              {
                  si[i] = factory::addPlaneSurface({cli[i]});
              }
            }
            std::vector<int> clbcli = cli;
            clbcli.insert(clbcli.begin(), clb);
            sb = factory::addPlaneSurface(clbcli);
        }
        else
        {
            sb = factory::addPlaneSurface({clb});
        }


        factory::synchronize();
        for (unsigned int p = 0; p < px.size(); p++)
        {
            gmodel::mesh::embed(px[p].dim, px[p].tag, 2, sb);
        }
        for (unsigned int p = 0; p < py.size(); p++)
        {
            gmodel::mesh::embed(py[p].dim, py[p].tag, 2, sb);
        }

        if(receiversAreEmittersX())
        {
            for (unsigned int e = 0; e <_nxe; e++)
            {
                gmodel::addPhysicalGroup(px[e].dim, px[e].tag, 1000+e);
                gmodel::setPhysicalName(px[e].dim, 1000+e, "emitter_receiver_x_"+std::to_string(e));
            }
        }
        else
        {
            for (unsigned int e = 0; e <_nxe; e++)
            {
                gmodel::addPhysicalGroup(px[e].dim, px[e].tag, 1000+e);
                gmodel::setPhysicalName(px[e].dim, 1000+e,"emitter_x_"+std::to_string(e));
            }
            for (unsigned int r = 0; r < _nxr; r++)
            {
                gmodel::addPhysicalGroup(px[_nxe+r].dim, px[_nxe+r].tag, 2000+r);
                gmodel::setPhysicalName(px[_nxe+r].dim, 2000+r, "receiver_x_"+std::to_string(r) );
            }
        }
        if(receiversAreEmittersY())
        {
            for (unsigned int e = 0; e <_nye; e++)
            {
                gmodel::addPhysicalGroup(py[e].dim, py[e].tag, 3000+e);
                gmodel::setPhysicalName(py[e].dim, 3000+e,"emitter_receiver_y_"+std::to_string(e));
            }
        }
        else
        {
            for (unsigned int e = 0; e <_nye; e++)
            {
                gmodel::addPhysicalGroup(py[e].dim, py[e].tag, 3000+e);
                gmodel::setPhysicalName(py[e].dim, 3000+e,"emitter_y_"+std::to_string(e));
            }
            for (unsigned int r = 0; r < _nyr; r++)
            {
                gmodel::addPhysicalGroup(py[_nye+r].dim, py[_nye+r].tag, 4000+r);
                gmodel::setPhysicalName(py[_nye+r].dim, 4000+r, "receiver_y_"+std::to_string(r) );
            }
        }

        gmodel::addPhysicalGroup(1, {lbh1,lbv1,lbh2,lbv2}, 12);
        gmodel::setPhysicalName(1, 12, "background_bnd");
        gmodel::addPhysicalGroup(2, {sb}, 21);
        gmodel::setPhysicalName(2, 21, "background_vol");
        for (unsigned int i = 0; i < _ni; i++)
        {
            if(_areFilled)
            {
                gmodel::addPhysicalGroup(2, {si[i]}, 10000+i);
                gmodel::setPhysicalName(2, 10000+i, "inclusion_vol"+std::to_string(i));
            }
            else
            {
                gmodel::addPhysicalGroup(1, li[i], 20000+i);
                gmodel::setPhysicalName(1, 20000+i, "inclusion_bnd"+std::to_string(i));
            }
        }
    }
    void Configuration::data_mesh() const
    {
        if(nxe() != 0)
        {
            double ors = (_H - _He) / 2.;
            double orr = (_H - _Hr) / 2.;
            //Vertical array
            int p00 = factory::addPoint(ors, orr, 1.);
            int pHe0 = factory::addPoint(ors+_He,orr, 1.);
            int pHeHr = factory::addPoint(ors+_He, orr+_Hr, 1.);
            int p0Hr = factory::addPoint(ors, orr+_Hr, 1.);

            int ly0 = factory::addLine(p00, pHe0);
            int lxHe = factory::addLine(pHe0, pHeHr);
            int lyHr = factory::addLine(pHeHr, p0Hr);
            int lx0 = factory::addLine(p0Hr, p00);

            int cl = factory::addCurveLoop({ly0,lxHe,lyHr,lx0});
            int s1 = factory::addPlaneSurface({cl});

            factory::mesh::setTransfiniteCurve(ly0,_nxe);
            factory::mesh::setTransfiniteCurve(lxHe,_nxr);
            factory::mesh::setTransfiniteCurve(lyHr,_nxe);
            factory::mesh::setTransfiniteCurve(lx0,_nxr);

            factory::mesh::setTransfiniteSurface(s1,"Left",{p00,pHe0,pHeHr,p0Hr});

            factory::mesh::setRecombine(2,s1);
            factory::synchronize();
            gmodel::addPhysicalGroup(2, {s1}, 1);
            gmodel::setPhysicalName(2, 1, "data_omega1");
        }

        if(nye() != 0)
        {
            double ors = _H + (_L - _Le) / 2.;
            double orr = _H + (_L - _Lr) / 2.;
            //Horizontal array
            int pHeHr = factory::addPoint(ors, orr, 1.);
            int pLe0 = factory::addPoint(ors+_Le, orr, 1.);
            int pLeLr = factory::addPoint(ors+_Le, orr+_Lr, 1.);
            int p0Lr = factory::addPoint(ors, orr+_Lr, 1.);

            int lyHr2 = factory::addLine(pHeHr, pLe0);
            int lxLe = factory::addLine(pLe0, pLeLr);
            int lyLr = factory::addLine(pLeLr, p0Lr);
            int lxHe2 = factory::addLine(p0Lr, pHeHr);

            int cl2 = factory::addCurveLoop({lyHr2,lxLe,lyLr,lxHe2});
            int s2 = factory::addPlaneSurface({cl2});

            factory::mesh::setTransfiniteCurve(lyHr2,_nye);
            factory::mesh::setTransfiniteCurve(lxLe,_nyr);
            factory::mesh::setTransfiniteCurve(lyLr,_nye);
            factory::mesh::setTransfiniteCurve(lxHe2,_nyr);

            factory::mesh::setTransfiniteSurface(s2,"Left",{pHeHr,pLe0,pLeLr,p0Lr});

            factory::mesh::setRecombine(2,s2);
            gmodel::addPhysicalGroup(2, {s2}, 2);
            gmodel::setPhysicalName(2, 2, "data_omega2");
        }
    }

    std::array<unsigned int,2> Configuration::data_coordinate_to_index(double xs, double xr) const
    {
        if( (!_receiver_on_emitter) && (receiversAreEmittersX() || receiversAreEmittersY()))
        {
            throw Exception("Data space when an emitter is not also a receiver is not implemented yet");
        }

        std::array<unsigned int,2> index;
        if(xs<_H && xr < _H)
        {
            double xe0 = (_H-_He)/2.;
            double stepe = _He/(nxe()-1);
            double xr0 = (_H-_Hr)/2.;
            double stepr = _Hr/(nxr()-1);
            index[0] = std::round((xs-xe0)/stepe);
            index[1] = std::round((xr-xr0)/stepr);
        }
        else if(xs>=_H && xr>=_H)
        {
            double ye0 = _H + (_L-_Le)/2.;
            double stepe = _Le/(nye()-1);
            double yr0 = _H + (_L-_Lr)/2.;
            double stepr = _Lr/(nyr()-1);
            index[0] = std::round((xs-ye0)/stepe);
            index[1] = std::round((xr-yr0)/stepr);
        }
        else
        {
            throw Exception("Invalid data coordinate.");
        }
        return index;
    }

    std::array<double,2> Configuration::index_to_data_coordinate(unsigned int s, unsigned int r) const
    {
        if( (!_receiver_on_emitter) && (receiversAreEmittersX() || receiversAreEmittersY()))
        {
            throw Exception("Data space when an emitter is not also a receiver is not implemented yet");
        }

        std::array<double,2> index;
        if(s < nxe() && r < nxr())
        {
            double xe0 = (_H-_He)/2.;
            double stepe = _He/(nxe()-1);
            double xr0 = (_H-_Hr)/2.;
            double stepr = _Hr/(nxr()-1);
            index[0] = xe0 + s*stepe;
            index[1] = xr0 + r*stepr;
        }
        else if( s>=nxe() && r>=nxr())
        {
            double xe0 = _H + (_L-_Le)/2.;
            double stepe = _Le/(nye()-1);
            double xr0 = _H + (_L-_Lr)/2.;
            double stepr = _Lr/(nyr()-1);
            index[0] = xe0 + (s-nxe())*stepe;
            index[1] = xr0 + (r-nxr())*stepr;
        }
        else
        {
            throw Exception("Invalid index.");
        }
        return index;
    }

    double Configuration::data_area() const
    {
        double areaY = 0.;
        double areaX = 0.;
        if(_nye > 1 && _nyr > 1){areaY = _Le * _Lr;}
        if(_nxe > 1 && _nxr > 1){areaX = _He * _Hr;}
        return (areaX + areaY);
    };

    double Configuration::datapoint_area() const
    {
        double areaY = _Le * _Lr / (double(_nye-1)) / (double(_nyr-1));
        double areaX = _He * _Hr / (double(_nxe-1)) / (double(_nxr-1));
        if(_nye < 2 || _nyr < 2){return areaX;}
        if(_nxe < 2 || _nxr < 2){return areaY;}
        return 0.5 * (areaX + areaY);
    };

    bool Configuration::data_coordinate_isValid(double xs,double xr) const
    {
        double eps = 1e-8;
        gmsh::option::getNumber("Geometry.MatchMeshTolerance",eps);
        if( nxe() != 0 )
        {
            double xe0 = (_H-_He)/2.;
            double xr0 = (_H-_Hr)/2.;
            if( (xe0-eps <= xs) && (xs <= xe0+_He+eps) && (xr0-eps <= xr) && (xr <= xr0+_Hr+eps) ){return true;}
        }

        if( nye() != 0 )
        {
            double xe0 = _H + (_L-_Le)/2.;
            double xr0 = _H + (_L-_Lr)/2.;
            if( (xe0-eps <= xs) && (xs <= xe0+_Le+eps) && (xr0-eps <= xr) && (xr <= xr0+_Lr+eps) ){return true;}
        }
        return false;
    }

    template<Physic T_Physic>
    ModelMonoFunction green0_preconditioner(const Data<T_Physic>& dd, const WaveMultiField<T_Physic>& g, const Configuration* const config)
    {
        ModelMonoFunction termX;
        if( !config->receiversAreEmittersX() )
        {
            termX = autocorrelate<T_Physic>(g,config->emitter_idx_X())*autocorrelate<T_Physic>(g,config->receiver_idx_X());
        }
        else
        {
            termX = pow(autocorrelate<T_Physic>(g,config->emitter_idx_X()),2);
        }

        ModelMonoFunction termY;
        if( !config->receiversAreEmittersY() )
        {
            termY = autocorrelate<T_Physic>(g,config->emitter_idx_Y())*autocorrelate<T_Physic>(g,config->receiver_idx_Y());
        }
        else
        {
            termY = pow(autocorrelate<T_Physic>(g,config->emitter_idx_Y()),2);
        }
        return termX + termY;
    }

    template ModelMonoFunction green0_preconditioner<Physic::acoustic>(const Data<Physic::acoustic>&, const WaveMultiField<Physic::acoustic>&, const Configuration* const);
    template ModelMonoFunction green0_preconditioner<Physic::electromagnetic>(const Data<Physic::electromagnetic>&, const WaveMultiField<Physic::electromagnetic>&, const Configuration* const);
    template ModelMonoFunction green0_preconditioner<Physic::elastodynamic>(const Data<Physic::elastodynamic>&, const WaveMultiField<Physic::elastodynamic>&, const Configuration* const);

} // namespace soil
