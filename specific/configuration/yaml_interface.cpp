#include "GmshFem.h"
#include "yaml_interface.h"

ShotsConfigurationYAML::ShotsConfigurationYAML(const std::string &path)
{
#ifndef ENABLE_YAML_CPP
    throw gmshfem::common::Exception("Cannot use Flexible Acquisition without YAML-CPP.");
#else
    YAML::Node config = YAML::LoadFile(path);

    for (const auto &coord : config["coordinates"])
    {
        double x = coord[0].as<double>();
        double y = coord[1].as<double>();
        _points.emplace_back(x, y);
    }

    for (const auto &shot : config["shots"])
    {
        std::vector<unsigned> emitters, receivers;
        for (const auto &emitter : shot["emitters"])
        {
            emitters.push_back(emitter.as<int>());
        }
        for (const auto &receiver : shot["receivers"])
        {
            receivers.push_back(receiver.as<int>());
        }

        _emitters.push_back(std::move(emitters));
        _receivers.push_back(std::move(receivers));
    }
#endif
}