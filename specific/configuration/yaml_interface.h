#ifndef H_YAML_INTERFACE_SHOTS
#define H_YAML_INTERFACE_SHOTS

#include <vector>
#include <string>
#include <utility>
#include "yaml-cpp/yaml.h"
// TODO: refactor (abstract + impl)

class ShotsConfigurationInterface //(abstract)
{
    public:
        using Coordinates = std::pair<double, double>;

        /**
         * Describe the number of shots
         */
        virtual unsigned numShots() const = 0;

        /**
         * Describe the number of points
         */
        virtual unsigned numPoints() const = 0;

        /**
         * emitters(s) is the list of enabled emitters in shot "s".
         * Array must be of size numShots()
         */
        virtual const std::vector<unsigned> &emitters(unsigned shot) const = 0;

        /**
         * receivers(s) is the list of enabled receivers in shot "s".
         * Array must be of size numShots()
         */
        virtual const std::vector<unsigned> &receivers(unsigned shot) const = 0;

        /**
         * List of all points
         * Array must be of size numPoints()
         */
        virtual const std::vector<Coordinates> &points() const = 0;
};

class ShotsConfigurationYAML : public ShotsConfigurationInterface {
    private:
        std::vector<std::vector<unsigned>> _emitters, _receivers;
        std::vector<ShotsConfigurationInterface::Coordinates> _points;

    public:

        ShotsConfigurationYAML(const std::string& path);

        virtual unsigned numShots() const override {
            return _emitters.size();
        }

        virtual unsigned numPoints() const override {
            return _points.size();
        }

        virtual const std::vector<unsigned> &emitters(unsigned shot) const override {
            return _emitters.at(shot);
        }

        virtual const std::vector<unsigned> &receivers(unsigned shot) const override {
            return _receivers.at(shot);
        }

        virtual const std::vector<Coordinates> &points() const override {
            return _points;
        }

        

};

#endif