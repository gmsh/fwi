// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFWI Library
#include "cift.h"

//GmshFEM Library
#include "Message.h"
#include "Exception.h"

using namespace gmshfem;
using namespace gmshfem::common;

static const std::complex< double > im = std::complex< double >(0., 1.);

const std::vector< std::vector<std::complex<double>> > inverse_fourier_matrix(const std::vector< double >& time, const std::vector< double >& freq, const gmshfem::common::GmshFem& gmshFem)
{
    unsigned int n_freq = freq.size();
    double fmin = freq[0];
    double fmax = freq[n_freq-1];
    double B = fmax-fmin;
    double fmed = (fmax+fmin)/2.;
    double df = B / ((double)(n_freq-1));
    double T = 1. / df;

    //double Beff = B+2*df;
    //double Tseff = 2. / Beff;

    unsigned int nt = time.size();
    std::vector< std::vector<std::complex<double>> > F(nt, std::vector<std::complex<double>>(n_freq,0.));

    msg::warning << "Inverse Fourier matrix only ok for DFT pairs with zero padding." << msg::endl;

    for (unsigned int f = 0, fo = std::round(fmin/df); f < n_freq; f++, fo++)
    {
        //std::complex<double> raisedcos = Tseff * std::min(0.5 * ( 1. + std::cos(M_PI*Tseff*(freq[f]-fmed)) ),1.);
        //std::complex<double> window = -2. * im * 2. * M_PI * freq[f] * raisedcos;

        for (unsigned int t = 0; t < nt; t++)
        {
            double x = ((double)t*(fo)) / ((double)nt);
            F[t][f] = std::exp(im*2.*M_PI*x) / T * ((double) n_freq-1) / ((double) n_freq); //* window;
        }
    }
    return F;
}

const std::vector< std::complex<double> > window(const std::vector< double >& freq)
{
    unsigned int n_freq = freq.size();
    double fmin = freq[0];
    double fmax = freq[n_freq-1];
    double B = fmax-fmin;
    double fmed = (fmax+fmin)/2.;
    double df = B / ((double)(n_freq-1));

    double Beff = B+2.*df;
    double Tseff = 2. / Beff;

    std::vector< std::complex<double> > w(n_freq,0.);

    for (unsigned int f = 0; f < n_freq; f++)
    {
        std::complex<double> raisedcos = Tseff * std::min(0.5 * ( 1. + std::cos(M_PI*Tseff*(freq[f]-fmed)) ),1.);
        w[f] = -2. * im * 2. * M_PI * freq[f] * raisedcos;
    }

    return w;
}

const double pseudo_fourier_normalization(const std::vector< double >& time, const std::vector< double >& freq)
{
    unsigned int n_freq = freq.size();
    unsigned int n_time = time.size();
    double fmin = freq[0];
    double fmax = freq[n_freq-1];
    double B = fmax-fmin;
    double df = B / ((double)(n_freq-1));
    double T = 1. / df;
    /*
    unsigned int n_f = std::round(2.*(fmax/df+1.)-1.);
    msg::print << " fmax =" << fmax << msg::endl;
    msg::print << " df =" << df << msg::endl;
    msg::print << " n_f =" << n_f << msg::endl;
    msg::print << " ((double)(n_f-1)/n_f) =" << ((double)(n_f-1)/n_f) << msg::endl;
    */
    return 1. / ((double)n_time) / std::pow(1. / T * ((double)(n_freq-1)/n_freq),2);
}
