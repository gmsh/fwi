// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_DATA_CIFT
#define H_SPECIFIC_DATA_CIFT

//Standard Library
#include <complex>

//GmshFWI Library

//GmshFEM Library
#include "GmshFem.h"

const std::vector< std::vector<std::complex<double>> > inverse_fourier_matrix(const std::vector< double >& time, const std::vector< double >& freq, const gmshfem::common::GmshFem& gmshFem);

const std::vector< std::complex<double> > window(const std::vector< double >& freq);

const double pseudo_fourier_normalization(const std::vector< double >& time, const std::vector< double >& freq);

#endif // H_SPECIFIC_DATA_CiFT
