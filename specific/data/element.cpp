// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM
#include "Exception.h"
#include "Formulation.h"

//GmshFWI Library
#include "element.h"
#include "../../common/data/element.h"

using namespace gmshfem;
using namespace gmshfem::common;

template<>
typename gmshfem::MathObject< std::complex< double >, propertiesOf<Physic::acoustic>::degree >::Object s_ones<Physic::acoustic>(){return 1.;}
template<>
typename gmshfem::MathObject< std::complex< double >, propertiesOf<Physic::electromagnetic>::degree >::Object s_ones<Physic::electromagnetic>(){return PointData<Physic::electromagnetic>::Object(1.,1.,1.);}
template<>
typename gmshfem::MathObject< std::complex< double >, propertiesOf<Physic::elastodynamic>::degree >::Object s_ones<Physic::elastodynamic>(){return PointData<Physic::elastodynamic>::Object(1.,1.,1.);}

/* acoustic */
template<>
double norm2<Physic::acoustic>(const typename PointData<Physic::acoustic>::Object& d){return std::norm(d);}
template<>
typename PointData<Physic::acoustic>::Object conj<Physic::acoustic>(const typename PointData<Physic::acoustic>::Object& d){return std::conj(d);}
template<>
typename PointData<Physic::acoustic>::Object abs<Physic::acoustic>(const typename PointData<Physic::acoustic>::Object& d){return std::abs(d);}
template<>
typename PointData<Physic::acoustic>::Object real<Physic::acoustic>(const typename PointData<Physic::acoustic>::Object& d){return std::real(d);}
template<>
typename PointData<Physic::acoustic>::Object arg<Physic::acoustic>(const typename PointData<Physic::acoustic>::Object& d){return std::arg(d);}
template<>
typename PointData<Physic::acoustic>::Object wise_divide<Physic::acoustic>(const typename PointData<Physic::acoustic>::Object& d1,const typename PointData<Physic::acoustic>::Object& d2){return d1/d2;}
template<>
typename PointData<Physic::acoustic>::Object wise_multiply<Physic::acoustic>(const typename PointData<Physic::acoustic>::Object& d1,const typename PointData<Physic::acoustic>::Object& d2){return d1*d2;}

/* electromagnetic */
template<>
double norm2<Physic::electromagnetic>(const typename PointData<Physic::electromagnetic>::Object& d){return d.squaredNorm();}
template<>
typename PointData<Physic::electromagnetic>::Object conj<Physic::electromagnetic>(const typename PointData<Physic::electromagnetic>::Object& d){return d.conjugate();}
template<>
typename PointData<Physic::electromagnetic>::Object real<Physic::electromagnetic>(const typename PointData<Physic::electromagnetic>::Object& d){return d.real();}
template<>
typename PointData<Physic::electromagnetic>::Object arg<Physic::electromagnetic>(const typename PointData<Physic::electromagnetic>::Object& d){return d.cwiseArg();}
template<>
typename PointData<Physic::electromagnetic>::Object abs<Physic::electromagnetic>(const typename PointData<Physic::electromagnetic>::Object& d)
{
    return PointData<Physic::electromagnetic>::ones() * d.norm();
}
template<>
typename PointData<Physic::electromagnetic>::Object wise_divide<Physic::electromagnetic>(const typename PointData<Physic::electromagnetic>::Object& d1,const typename PointData<Physic::electromagnetic>::Object& d2){return d1.array() / d2.array() ;}
template<>
typename PointData<Physic::electromagnetic>::Object wise_multiply<Physic::electromagnetic>(const typename PointData<Physic::electromagnetic>::Object& d1,const typename PointData<Physic::electromagnetic>::Object& d2){return d1.array() * d2.array() ;}

/* elastodynamic */
template<>
double norm2<Physic::elastodynamic>(const typename PointData<Physic::elastodynamic>::Object& d){return d.squaredNorm();}
template<>
typename PointData<Physic::elastodynamic>::Object conj<Physic::elastodynamic>(const typename PointData<Physic::elastodynamic>::Object& d){return d.conjugate();}
template<>
typename PointData<Physic::elastodynamic>::Object real<Physic::elastodynamic>(const typename PointData<Physic::elastodynamic>::Object& d){return d.real();}
template<>
typename PointData<Physic::elastodynamic>::Object arg<Physic::elastodynamic>(const typename PointData<Physic::elastodynamic>::Object& d){return d.cwiseArg();}
template<>
typename PointData<Physic::elastodynamic>::Object abs<Physic::elastodynamic>(const typename PointData<Physic::elastodynamic>::Object& d)
{
    return PointData<Physic::elastodynamic>::ones() * d.norm();
}
template<>
typename PointData<Physic::elastodynamic>::Object wise_divide<Physic::elastodynamic>(const typename PointData<Physic::elastodynamic>::Object& d1,const typename PointData<Physic::elastodynamic>::Object& d2){return d1.array() / d2.array() ;}
template<>
typename PointData<Physic::elastodynamic>::Object wise_multiply<Physic::elastodynamic>(const typename PointData<Physic::elastodynamic>::Object& d1,const typename PointData<Physic::elastodynamic>::Object& d2){return d1.array() * d2.array() ;}
