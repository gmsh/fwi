// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_DATA_ELEMENT
#define H_SPECIFIC_DATA_ELEMENT

//GmshFEM Library
#include "MathObject.h"
#include "FieldInterface.h"
#include "Function.h"
#include "Post.h"

//GmshFWI Library
#include "../physic.h"

template<Physic T_Physic>
typename gmshfem::MathObject< std::complex< double >, propertiesOf<T_Physic>::degree >::Object s_ones();

/*
* PointData
*/
template<Physic T_Physic>
class PointData final
{
public:
    typedef typename gmshfem::MathObject< std::complex< double >, propertiesOf<T_Physic>::degree >::Object Object;
    static constexpr Object ones(){return s_ones<T_Physic>();};
};

template<Physic T_Physic>
double norm2(const typename PointData<T_Physic>::Object& d);
template<Physic T_Physic>
typename PointData<T_Physic>::Object conj(const typename PointData<T_Physic>::Object& d);
template<Physic T_Physic>
typename PointData<T_Physic>::Object real(const typename PointData<T_Physic>::Object& d);
template<Physic T_Physic>
typename PointData<T_Physic>::Object arg(const typename PointData<T_Physic>::Object& d);
template<Physic T_Physic>
typename PointData<T_Physic>::Object abs(const typename PointData<T_Physic>::Object& d);
template<Physic T_Physic>
typename PointData<T_Physic>::Object wise_divide(const typename PointData<T_Physic>::Object& d1,const typename PointData<T_Physic>::Object& d2);
template<Physic T_Physic>
typename PointData<T_Physic>::Object wise_multiply(const typename PointData<T_Physic>::Object& d1,const typename PointData<T_Physic>::Object& d2);

#endif // H_SPECIFIC_DATA_ELEMENT
