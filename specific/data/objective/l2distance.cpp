// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFem Library
#include "Exception.h"

//GmshFEM Library
#include "l2distance.h"

#define d0 _d0
#define d ds.state(Type::FS)
#define rho ds.state(Type::AS)
#define dd ds.state(Type::PFS)
#define drho ds.state(Type::PAS)

using namespace gmshfem::common;
/*
* L2NORM
*/
namespace l2distance
{
    template<Physic T_Physic>
    double Objective<T_Physic>::performance(const Data<T_Physic>& d1)
    {
        Data<T_Physic> r = d1-d0;
        return 0.5 * l2norm2(r) / _norm;
    }

    template<Physic T_Physic>
    const Data<T_Physic>& Objective<T_Physic>::update(Type type, const DataStateEvaluator<T_Physic>& ds)
    {
        switch (type)
        {
            case Type::FS: case Type::PFS: default:
                throw Exception("Objective can not update (perturbed) forward data.");
                _v = ds.state(type);
                break;
            case Type::AS:
                _v = conj(d-d0) / _norm;
                break;
            case Type::PAS:
                _v = conj(dd) / _norm;
                break;
            case Type::DS:
                /* Does not take into account user sparsity input */
                _v.value( PointData<T_Physic>::ones() / std::complex<double>(_norm) );
                break;
        }
        return _v;
    }
    template class Objective<Physic::acoustic>;
    template class Objective<Physic::electromagnetic>;
    template class Objective<Physic::elastodynamic>;
}
