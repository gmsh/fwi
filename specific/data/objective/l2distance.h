// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_DATA_OBJECTIVE_L2NORM
#define H_SPECIFIC_DATA_OBJECTIVE_L2NORM

//GmshFEM Library
#include "GmshFem.h"

//GmshFWI Library
#include "../../../common/data/objective/objective.h"

/*
* L2NORM
*/
namespace l2distance
{
    template<Physic T_Physic>
    class Objective final: public ObjectiveInterface<T_Physic>
    {
    private:
        using ObjectiveInterface<T_Physic>::_d0;
        using ObjectiveInterface<T_Physic>::_v;
        double _norm;
        bool _scaled;
    public:
        Objective(const Data<T_Physic>& d0, const gmshfem::common::GmshFem& gmshFem) : ObjectiveInterface<T_Physic>(d0), _norm(1.), _scaled(false)
        {
            gmshFem.userDefinedParameter(_norm, "data_norm");

            unsigned int scaled = false;
            gmshFem.userDefinedParameter(scaled, "data_scaled");
            _scaled = ((bool) scaled);
        };

        virtual double performance(const Data<T_Physic>& d);
        virtual const Data<T_Physic>& update(Type type, const DataStateEvaluator<T_Physic>& ds);
    };
};

#endif //H_SPECIFIC_WAVE_INNERPRODUCT_SOBOLEV
