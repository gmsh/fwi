// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_DATA_NEWOBJECTIVE
#define H_SPECIFIC_DATA_NEWOBJECTIVE

//Standard Library
#include <string>
#include <memory>

//GmshFEM Library
#include "Exception.h"

//GmshFWI Library
#include "l2distance.h"
#include "phase.h"

template<Physic T_Physic>
std::unique_ptr<ObjectiveInterface<T_Physic>> newObjective(const Data<T_Physic>& d0, const ConfigurationInterface* const config, const gmshfem::common::GmshFem& gmshFem,std::string suffix = "")
{
    std::string objective;
    if(!gmshFem.userDefinedParameter(objective, "objective"))
    {
      throw gmshfem::common::Exception("Objective type could not be found.");
    }
    if(objective=="l2distance"){return std::make_unique< l2distance::Objective<T_Physic> >(d0,gmshFem);}
    else if(objective=="conventional_phase"){return std::make_unique< conventional_phase::Objective<T_Physic> >(d0,gmshFem);}
    else if(objective=="logarithmic_phase"){return std::make_unique< logarithmic_phase::Objective<T_Physic> >(d0,gmshFem);}
    else
    {
        throw gmshfem::common::Exception("Objective " + objective + " is not valid.");
        return nullptr;
    }
};

#endif //H_SPECIFIC_MODEL_NEWOBJECTIVE
