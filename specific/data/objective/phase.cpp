// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFem Library
#include "Exception.h"

//GmshFEM Library
#include "phase.h"

#define d0 _d0
#define d ds.state(Type::FS)
#define rho ds.state(Type::AS)
#define dd ds.state(Type::PFS)
#define drho ds.state(Type::PAS)

using namespace gmshfem::common;

static const std::complex< double > im = std::complex< double >(0., 1.);

/*
* CONVENTIONAL PHASE
*/
namespace conventional_phase
{
    template<Physic T_Physic>
    double Objective<T_Physic>::performance(const Data<T_Physic>& d1)
    {
        Data<T_Physic> r = d1/abs(d1)-d0/abs(d0);
        return 0.5 * l2norm2(r);
    }

    template<Physic T_Physic>
    const Data<T_Physic>& Objective<T_Physic>::update(Type type, const DataStateEvaluator<T_Physic>& ds)
    {
        switch (type)
        {
            case Type::FS: case Type::PFS: default:
                throw Exception("Objective can not update (perturbed) forward data.");
                break;
            case Type::AS:
            {
                Data<T_Physic> absd = abs(d);
                Data<T_Physic> cnjr = conj(d/absd-d0/abs(d0));
                _v = cnjr / absd - real(cnjr*d) * conj(d) / (absd*absd*absd);
                break;
            }
            case Type::PAS:
                throw Exception("Objective perturbed adjoint state for phase is not defined. Linear/Anti-linear problem.");
                break;
            case Type::DS:
            /* Does not take into account user sparsity input */
            _v.value( PointData<T_Physic>::ones() );
                break;
        }
        return _v;
    }
    template class Objective<Physic::acoustic>;
    template class Objective<Physic::electromagnetic>;
    template class Objective<Physic::elastodynamic>;
}

/*
* LOGARITHMIC PHASE
*/
namespace logarithmic_phase
{
    template<Physic T_Physic>
    double Objective<T_Physic>::performance(const Data<T_Physic>& d1)
    {
        Data<T_Physic> r = arg( d1 * conj(d0) ) / M_PI * im;
        return 0.5 * l2norm2(r);
    }

    template<Physic T_Physic>
    const Data<T_Physic>& Objective<T_Physic>::update(Type type, const DataStateEvaluator<T_Physic>& ds)
    {
        switch (type)
        {
            case Type::FS: case Type::PFS: default:
                throw Exception("Objective can not update (perturbed) forward data.");
                break;
            case Type::AS:
            {
                Data<T_Physic> r = arg( d * conj(d0) ) / M_PI * -im;
                _v = r / d;
                break;
            }
            case Type::PAS:
                throw Exception("Objective perturbed adjoint state for phase is not defined. Linear/Anti-linear problem.");
                break;
            case Type::DS:
            /* Does not take into account user sparsity input */
            _v.value( PointData<T_Physic>::ones() );
                break;
        }
        return _v;
    }
    template class Objective<Physic::acoustic>;
    template class Objective<Physic::electromagnetic>;
    template class Objective<Physic::elastodynamic>;
}
