// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
#include <string>

//GmshFEM Library
#include "Exception.h"

//GmshFWI Library
#include "sobolev.h"

template<class M>
std::unique_ptr<InnerProductInterface> newInnerProduct(const ConfigurationInterface* const config,  const M& m_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix)
{
    std::string innerproduct;
    if(!gmshFem.userDefinedParameter(innerproduct, "innerproduct"))
    {
      throw gmshfem::common::Exception("Inner product type could not be found.");
    }
    if(innerproduct=="sobolev_l2"){return std::make_unique< sobolev_l2::InnerProduct>(config,m_discret,gmshFem, suffix);}
    else if(innerproduct=="sobolev_df"){return std::make_unique< sobolev_df::InnerProduct>(config,m_discret,gmshFem, suffix);}
    else
    {
        throw gmshfem::common::Exception("Inner product " + innerproduct + " is not valid.");
        return nullptr;
    }
}

template std::unique_ptr<InnerProductInterface> newInnerProduct<model::Discretization>(const ConfigurationInterface* const,  const model::Discretization&, const gmshfem::common::GmshFem&, std::string);
template std::unique_ptr<InnerProductInterface> newInnerProduct<ModelField>(const ConfigurationInterface* const,  const ModelField&, const gmshfem::common::GmshFem&, std::string);
