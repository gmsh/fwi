// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_MODEL_NEWINNERPRODUCT
#define H_SPECIFIC_MODEL_NEWINNERPRODUCT

//Standard Library
#include <memory>
//GmshFEM Library
#include "GmshFem.h"
//GmshFWI Library
#include "../../../common/model/innerproduct/innerproduct.h"

template<class M>
std::unique_ptr<InnerProductInterface> newInnerProduct(const ConfigurationInterface* const config,  const M& m_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix);

#endif //H_SPECIFIC_MODEL_NEWINNERPRODUCT
