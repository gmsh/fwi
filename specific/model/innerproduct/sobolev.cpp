// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "Function.h"

//GmshFWI Library
#include "sobolev.h"
#include "../../../common/model/updater.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::equation;
using namespace gmshfem::function;

static const std::complex< double > im = std::complex< double >(0., 1.);

/*
* Sobolev
*/
SobolevInnerProductInterface::SobolevInnerProductInterface(const ConfigurationInterface* const config, const model::Discretization& m_discret, const GmshFem& gmshFem, std::string suffix) : DifferentialInnerProductInterface(config,m_discret,gmshFem,suffix), _mu(nullptr), _su(nullptr), _diag_preconditioner_scale(false), _diag_preconditioner_smooth(false), _update_interval(1), _p0(_m_size,1.),  _weight(_m_size,1.), _stabilization(_m_size,0.), _alpha(0.), _bulkWeight(1.0), _boundaryWeight(1.)
{
    double scale = 0.;
    gmshFem.userDefinedParameter(scale, "scale");
    gmshFem.userDefinedParameter(scale, "scale"+suffix);
    _alpha = (scale / 2. / M_PI) * (scale / 2. / M_PI);

    unsigned int diag_preconditioner_scale = 0;
    gmshFem.userDefinedParameter(diag_preconditioner_scale, "innerproduct_diag_preconditioner_scale");
    _diag_preconditioner_scale = ((bool) diag_preconditioner_scale);
    unsigned int diag_preconditioner_smooth = 0;
    gmshFem.userDefinedParameter(diag_preconditioner_smooth, "innerproduct_diag_preconditioner_smooth");
    _diag_preconditioner_smooth = ((bool) diag_preconditioner_smooth);

    if(!gmshFem.userDefinedParameter(_update_interval, "innerproduct_update_interval"))
    {
        msg::warning << "Inner product update interval could not be found. Default (1) is used." << msg::endl;
    }

    for (unsigned int c = 0; c < _m_size; c++)
    {
        gmshFem.userDefinedParameter(_p0[c], "innerproduct_preconditioner_ref");
        gmshFem.userDefinedParameter(_p0[c], "innerproduct_preconditioner_ref"+suffix);
        gmshFem.userDefinedParameter(_p0[c], "innerproduct_preconditioner_ref"+std::to_string(c));
        gmshFem.userDefinedParameter(_p0[c], "innerproduct_preconditioner_ref"+std::to_string(c)+suffix);

        gmshFem.userDefinedParameter(_weight[c], "innerproduct_weight");
        gmshFem.userDefinedParameter(_weight[c], "innerproduct_weight"+suffix);
        gmshFem.userDefinedParameter(_weight[c], "innerproduct_weight"+std::to_string(c));
        gmshFem.userDefinedParameter(_weight[c], "innerproduct_weight"+std::to_string(c)+suffix);
        _weight[c] *= _weight[c];

        gmshFem.userDefinedParameter(_stabilization[c], "innerproduct_stabilization");
        gmshFem.userDefinedParameter(_stabilization[c], "innerproduct_stabilization"+suffix);
        gmshFem.userDefinedParameter(_stabilization[c], "innerproduct_stabilization"+std::to_string(c));
        gmshFem.userDefinedParameter(_stabilization[c], "innerproduct_stabilization"+std::to_string(c)+suffix);

        msg::print << "inner_product_weight" << c << " = " << _weight[c] << msg::endl;
    }


    gmshFem.userDefinedParameter(_bulkWeight, "innerproduct_weight_blk");
    gmshFem.userDefinedParameter(_bulkWeight, "innerproduct_weight_blk"+suffix);
    gmshFem.userDefinedParameter(_boundaryWeight, "innerproduct_weight_bnd");
    gmshFem.userDefinedParameter(_boundaryWeight, "innerproduct_weight_bnd"+suffix);
};

SobolevInnerProductInterface::SobolevInnerProductInterface(const ConfigurationInterface* const config, const ModelField& m_discret, const GmshFem& gmshFem, std::string suffix) : DifferentialInnerProductInterface(config,m_discret,gmshFem,suffix), _mu(nullptr), _su(nullptr), _diag_preconditioner_scale(false), _diag_preconditioner_smooth(false), _update_interval(1), _p0(_m_size,1.),  _weight(_m_size,1.), _stabilization(_m_size,0.), _alpha(0.), _bulkWeight(1.0), _boundaryWeight(1.)
{
    double scale = 0.;
    gmshFem.userDefinedParameter(scale, "scale");
    gmshFem.userDefinedParameter(scale, "scale"+suffix);
    _alpha = (scale / 2. / M_PI) * (scale / 2. / M_PI);

    unsigned int diag_preconditioner_scale = 0;
    gmshFem.userDefinedParameter(diag_preconditioner_scale, "innerproduct_diag_preconditioner_scale");
    _diag_preconditioner_scale = ((bool) diag_preconditioner_scale);
    unsigned int diag_preconditioner_smooth = 0;
    gmshFem.userDefinedParameter(diag_preconditioner_smooth, "innerproduct_diag_preconditioner_smooth");
    _diag_preconditioner_smooth = ((bool) diag_preconditioner_smooth);

    if(!gmshFem.userDefinedParameter(_update_interval, "innerproduct_update_interval"))
    {
        msg::warning << "Inner product update interval could not be found. Default (1) is used." << msg::endl;
    }

    for (unsigned int c = 0; c < _m_size; c++)
    {
        gmshFem.userDefinedParameter(_p0[c], "innerproduct_preconditioner_ref");
        gmshFem.userDefinedParameter(_p0[c], "innerproduct_preconditioner_ref"+suffix);
        gmshFem.userDefinedParameter(_p0[c], "innerproduct_preconditioner_ref"+std::to_string(c));
        gmshFem.userDefinedParameter(_p0[c], "innerproduct_preconditioner_ref"+std::to_string(c)+suffix);

        gmshFem.userDefinedParameter(_weight[c], "innerproduct_weight");
        gmshFem.userDefinedParameter(_weight[c], "innerproduct_weight"+suffix);
        gmshFem.userDefinedParameter(_weight[c], "innerproduct_weight"+std::to_string(c));
        gmshFem.userDefinedParameter(_weight[c], "innerproduct_weight"+std::to_string(c)+suffix);
        _weight[c] *= _weight[c];

        gmshFem.userDefinedParameter(_stabilization[c], "innerproduct_stabilization");
        gmshFem.userDefinedParameter(_stabilization[c], "innerproduct_stabilization"+suffix);
        gmshFem.userDefinedParameter(_stabilization[c], "innerproduct_stabilization"+std::to_string(c));
        gmshFem.userDefinedParameter(_stabilization[c], "innerproduct_stabilization"+std::to_string(c)+suffix);

        msg::print << "inner_product_weight" << c << " = " << _weight[c] << msg::endl;
    }


    gmshFem.userDefinedParameter(_bulkWeight, "innerproduct_weight_blk");
    gmshFem.userDefinedParameter(_bulkWeight, "innerproduct_weight_blk"+suffix);
    gmshFem.userDefinedParameter(_boundaryWeight, "innerproduct_weight_bnd");
    gmshFem.userDefinedParameter(_boundaryWeight, "innerproduct_weight_bnd"+suffix);
};

void SobolevInnerProductInterface::link(ModelUpdater* const mu, SensitivityUpdater* const su)
{
    if(_mu!=nullptr || _su!=nullptr)
    {
        throw Exception("Model and sensitivity updaters are already linked for this Sobolev inner product.");
    }
    else
    {
        _mu=mu;
        _su=su;
    }
}
void SobolevInnerProductInterface::unlink()
{
    _mu=nullptr;
    _su=nullptr;
}
void SobolevInnerProductInterface::modelIsObsolete()
{
    if( (_diag_preconditioner_scale || _diag_preconditioner_smooth) && (_update_interval==1) )
    {
        _systemAreUpToDate=false;
        _systemAreFactorized=false;
    }
}
bool SobolevInnerProductInterface::iteration(double iteration)
{
    _iteration = iteration;
    if( (_iteration % _update_interval == 0) && (_update_interval!=1) )
    {
        _systemAreUpToDate=false;
        _systemAreFactorized=false;
        return true;
    }
    return false;
}

void SobolevInnerProductInterface::setRHS(const Sensitivity& bulk_sensi,const Sensitivity& boundary_sensi)
{
    for (unsigned int c = 0; c < _m_size; c++)
    {
        if(_bulkWeight!=0.)
        {
            _formulation[c].integral(-_bulkWeight * conj(bulk_sensi[c]),tf(_j[c]), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
        }
        if(_boundaryWeight!=0.)
        {
            _formulation[c].integral(-_boundaryWeight * conj(boundary_sensi[c]),tf(_j[c]), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd));
        }
    }
}

void SobolevInnerProductInterface::alpha(double alpha)
{
    _alpha=alpha;
    _systemAreUpToDate=false;
    _systemAreFactorized=false;
};
void SobolevInnerProductInterface::bulkWeight(double bulkWeight)
{
    _bulkWeight=bulkWeight;
};
void SobolevInnerProductInterface::boundaryWeight(double boundaryWeight)
{
    _boundaryWeight=boundaryWeight;
};

namespace sobolev_l2
{
    void InnerProduct::setLHS()
    {
        msg::print << "Update inner product" << msg::endl;
        for (unsigned int c = 0; c < _m_size; c++)
        {
            unsigned int degree = _j[c].degree();
            if(_diag_preconditioner_scale)
            {
                std::array<bool,4> needToBeUpToDate = {true,false,false,false};
                _formulation[c].integral(_weight[c] * (_su->get(Order::DIAG,Support::BLK,_mu->get(needToBeUpToDate))[c] + _stabilization[c])* dof(_j[c]), tf(_j[c]), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
            }
            else
            {
                _formulation[c].integral(_weight[c] * _p0[c] * dof(_j[c]), tf(_j[c]), _config->model_unknown(Support::BLK), integrationType(2*degree));
            }

            if(_alpha>0.)
            {
                if(_diag_preconditioner_smooth)
                {
                    std::array<bool,4> needToBeUpToDate = {true,false,false,false};
                    _formulation[c].integral(_weight[c] * (_su->get(Order::DIAG,Support::BLK,_mu->get(needToBeUpToDate))[c] + _stabilization[c]) * _alpha*grad(dof(_j[c])), grad(tf(_j[c])), _config->model_unknown(Support::BLK),integrationType(_integrationDegreeBlk));
                }
                else
                {
                    _formulation[c].integral(_weight[c] * _p0[c] * _alpha*grad(dof(_j[c])), grad(tf(_j[c])), _config->model_unknown(Support::BLK),integrationType(2*degree));
                }
            }
        }
    }

    std::complex<double> InnerProduct::penalization(const ModelField& m1, const ModelField& m2) const
    {
        unsigned int degree = _j[0].degree();
        std::complex<double> sum = 0.;
        for (unsigned int c = 0; c < _m_size; c++)
        {
            sum += _weight[c] * gmshfem::post::integrate( grad(m1[c]) * grad(m2[c]), _config->model_unknown(Support::BLK), integrationType(8*degree) );
        }
        return sum;
    };
};

namespace sobolev_df
{
    void InnerProduct::modelIsObsolete()
    {
        if(_update_interval==1)
        {
            _systemAreUpToDate=false;
            _systemAreFactorized=false;

            _structureIsUpToDate=false;
        }
    }
    bool InnerProduct::iteration(double iteration)
    {
        _iteration = iteration;
        if( (_iteration % _update_interval == 0) && (_update_interval!=1) )
        {
            _systemAreUpToDate=false;
            _systemAreFactorized=false;

            _structureIsUpToDate=false;
            return true;
        }
        return false;
    }

    void InnerProduct::setLHS()
    {
        msg::print << "Update inner product" << msg::endl;
        for (unsigned int c = 0; c < _m_size; c++)
        {
            unsigned int degree = _j[c].degree();
            if(_diag_preconditioner_scale)
            {
                std::array<bool,4> needToBeUpToDate = {true,false,false,false};
                _formulation[c].integral(_weight[c] * (_su->get(Order::DIAG,Support::BLK,_mu->get(needToBeUpToDate))[c] + _stabilization[c])* dof(_j[c]), tf(_j[c]), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
            }
            else
            {
                _formulation[c].integral(_weight[c] * _p0[c] * dof(_j[c]), tf(_j[c]), _config->model_unknown(Support::BLK), integrationType(2*degree));
            }
            if(_alpha>0.)
            {
                std::array<bool,4> needToBeUpToDate = {true,false,false,false};
                if(!_structureIsUpToDate){update_filter(_mu->get(needToBeUpToDate).field(Type::FS));}

                if(_diag_preconditioner_smooth)
                {
                    _formulation[c].integral( _weight[c] * _alpha * (_su->get(Order::DIAG,Support::BLK,_mu->get(needToBeUpToDate))[c] + _stabilization[c]) * _D * grad(dof(_j[c])), grad(tf(_j[c])), _config->model_unknown(Support::BLK),integrationType(_integrationDegreeBlk));
                }
                else
                {
                    _formulation[c].integral( _weight[c] * _alpha * _p0[c] * _D * grad(dof(_j[c])), grad(tf(_j[c])), _config->model_unknown(Support::BLK),integrationType(6*degree-2));
                }
            }
        }
    }

    void InnerProduct::update_filter(const ModelField& m)
    {
        unsigned int degree = _j[0].degree();
        if(!_filterIsFactorized)
        {
            _filter_formulation.removeSystem();
            _filter_formulation.initSystem();

            _filter_formulation.integral(dof(_q), tf(_q), _config->model_unknown(Support::BLK), integrationType(2*degree));
            _filter_formulation.integral( _alpha_structure * grad(dof(_q)), grad(tf(_q)), _config->model_unknown(Support::BLK),integrationType(2*degree-2));

            _filter_formulation.pre();
            _filter_formulation.assemble();
            _filter_formulation.removeTerms();
        }

        _D =
        tensor< std::complex< double > >
        (
            1.,0.,0.,
            0.,1.,0.,
            0.,0.,0.
        );

        for (unsigned int c = 0; c < _m_size; c++)
        {
            //XX
            _filter_formulation.integral(-pow(xComp(grad(m[c])),2), tf(_q), _config->model_unknown(Support::BLK), integrationType(2*degree-1));
            _filter_formulation.assemble();
            _filter_formulation.removeTerms();
            _filter_formulation.solve(_filterIsFactorized);
            _filter_formulation.setRHSToZero();
            _filterIsFactorized=true;
            _m2x[c] = _q;

            //YY
            _filter_formulation.integral(-pow(yComp(grad(m[c])),2), tf(_q), _config->model_unknown(Support::BLK), integrationType(2*degree-1));
            _filter_formulation.assemble();
            _filter_formulation.removeTerms();
            _filter_formulation.solve(_filterIsFactorized);
            _filter_formulation.setRHSToZero();
            _filterIsFactorized=true;
            _m2y[c] = _q;

            //XY
            _filter_formulation.integral(-xComp(grad(m[c]))*yComp(grad(m[c])), tf(_q), _config->model_unknown(Support::BLK), integrationType(2*degree-1));
            _filter_formulation.assemble();
            _filter_formulation.removeTerms();
            _filter_formulation.solve(_filterIsFactorized);
            _filter_formulation.setRHSToZero();
            _filterIsFactorized=true;
            _mxmy[c] = _q;


            ScalarFunction< std::complex< double > > gradm2 = ScalarFunction< std::complex< double > >(_m2x[c])+ScalarFunction< std::complex< double > >(_m2y[c])+_beta[c]*_beta[c];

            _D = _D
            -
            _weight_structure[c] *
            tensor< std::complex< double > >
            (
                _m2x[c],_mxmy[c],0.,
                _mxmy[c],_m2y[c],0.,
                0.,0.,0.
            ) / gradm2;
        }

        /*
        bool _flooding = true;
        if(_flooding)
        {
            ScalarFunction< std::complex< double > > partition = heaviside< std::complex< double > >( yComp(grad(m[0]))+1e-7 );
            _D = partition * _D
            +
            (1-partition)*tensor< std::complex< double > >
            (
                0.,0.,0.,
                0.,1.,0.,
                0.,0.,0.
            );
        }
        */
        /*
        ScalarFunction< std::complex< double > > gradm2 = ScalarFunction< std::complex< double > >(_m2x[0])+ScalarFunction< std::complex< double > >(_m2y[0]);
        gmshfem::post::save(sqrt(gradm2), _config->model_unknown(Support::BLK), "absgradm0", "pos", "");

        gradm2 = ScalarFunction< std::complex< double > >(_m2x[1])+ScalarFunction< std::complex< double > >(_m2y[1]);
        gmshfem::post::save(sqrt(gradm2), _config->model_unknown(Support::BLK), "absgradm1", "pos", "");

        throw gmshfem::common::Exception("STOP");
        */

        _structureIsUpToDate=true;
    }

    std::complex<double> InnerProduct::penalization(const ModelField& m1, const ModelField& m2) const
    {
        unsigned int degree = _j[0].degree();
        std::complex<double> sum = 0.;
        for (unsigned int c = 0; c < _m_size; c++)
        {
            if(_structureIsUpToDate)
            {
                sum += _weight[c] * gmshfem::post::integrate( grad(m1[c]) * _D * grad(m2[c]), _config->model_unknown(Support::BLK), integrationType(6*degree-2) ) / _beta[c] / _beta[c];
            }
            else
            {
                msg::warning << "Penalization requested while structure is not up to date." << msg::endl;
            }
        }
        return sum;
    };
};
