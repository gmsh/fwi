// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_WAVE_INNERPRODUCT_SOBOLEV
#define H_SPECIFIC_WAVE_INNERPRODUCT_SOBOLEV

//GmshFEM Library
#include "GmshFem.h"
#include "Function.h"

//GmshFWI Library
#include "../../../common/model/innerproduct/innerproduct.h"

/*
* Sobolev
*/
class SobolevInnerProductInterface: public DifferentialInnerProductInterface
{
protected:
    ModelUpdater* _mu;
    SensitivityUpdater* _su;

    bool _diag_preconditioner_scale;
    bool _diag_preconditioner_smooth;
    unsigned int _update_interval;

    std::vector<double> _p0;
    std::vector<double> _weight;
    std::vector<double> _stabilization;

    double _alpha;
    double _bulkWeight;
    double _boundaryWeight;
public:
    SobolevInnerProductInterface(const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix="");
    SobolevInnerProductInterface(const ConfigurationInterface* const config, const ModelField& m_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix="");
    virtual void link(ModelUpdater* const mu, SensitivityUpdater* const mpu) override;
    virtual void unlink() override;
    virtual void modelIsObsolete() override;

    virtual void scale(double scale) override {alpha((scale / 2. / M_PI) * (scale / 2. / M_PI));};
    virtual bool iteration(double iteration) override; //return: is obsolete at iteration ?
    void bulkWeight(double bulkWeight);
    void boundaryWeight(double boundaryWeight);
private:
    void alpha(double alpha);
    virtual void setLHS() = 0;
    virtual void setRHS(const Sensitivity& bulk_sensi,const Sensitivity& boundary_sensi);
};

namespace sobolev_l2
{
    class InnerProduct final: public SobolevInnerProductInterface
    {
    public:
        InnerProduct(const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix="") : SobolevInnerProductInterface(config,m_discret,gmshFem,suffix) {};
        InnerProduct(const ConfigurationInterface* const config, const ModelField& m_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix="") : SobolevInnerProductInterface(config,m_discret,gmshFem,suffix) {};

        virtual std::complex<double> penalization(const ModelField& m1,const ModelField& m2) const override;
    private:
        virtual void setLHS();
    };
};

namespace sobolev_df
{
    class InnerProduct final: public SobolevInnerProductInterface
    {
    private:
        std::vector<double> _beta;
        std::vector<double> _weight_structure;
        double _alpha_structure;

        ModelMonoField _q;
        ModelField _m2x;
        ModelField _m2y;
        ModelField _mxmy;
        gmshfem::problem::Formulation< std::complex< double > > _filter_formulation;

        gmshfem::function::TensorFunction< std::complex< double > > _D;
        bool _structureIsUpToDate;
        bool _filterIsFactorized;
    public:
        InnerProduct(const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix="") : SobolevInnerProductInterface(config,m_discret,gmshFem,suffix), _beta(_m_size,0.), _weight_structure(_m_size,1./((double)_m_size)), _q("_q",_config->model_evaldomain(),_config->model_gmodel(),m_discret), _filter_formulation("_filter_formulation"), _structureIsUpToDate(false), _filterIsFactorized(false)
        {
            _m2x = ModelField(_m_size,ModelMonoField("_m2x",_config->model_evaldomain(),_config->model_gmodel(),m_discret));
            _m2y = ModelField(_m_size,ModelMonoField("_m2y",_config->model_evaldomain(),_config->model_gmodel(),m_discret));
            _mxmy = ModelField(_m_size,ModelMonoField("_mxmy",_config->model_evaldomain(),_config->model_gmodel(),m_discret));

            for (unsigned int c = 0; c < _m_size; c++)
            {
                std::string suffix1 = "c"+std::to_string(c);
                if(!(
                    gmshFem.userDefinedParameter(_beta[c], "innerproduct_beta") ||
                    gmshFem.userDefinedParameter(_beta[c], "innerproduct_beta"+suffix) ||
                    gmshFem.userDefinedParameter(_beta[c], "innerproduct_beta"+suffix1) ||
                    gmshFem.userDefinedParameter(_beta[c], "innerproduct_beta"+suffix+suffix1)

                ))
                {
                    throw gmshfem::common::Exception("A directional filter sobolev inner product parameter (beta) could not be found.");
                }
                if(!(
                    gmshFem.userDefinedParameter(_weight_structure[c], "innerproduct_structure_weight"+suffix1) ||
                    gmshFem.userDefinedParameter(_weight_structure[c], "innerproduct_structure_weight"+suffix+suffix1)
                ))
                {
                    gmshfem::msg::warning << "A directional filter sobolev inner product parameter (weight) could not be found. Equal weights are used." << gmshfem::msg::endl;
                }
            }

            double scale_structure;
            if(!(
                gmshFem.userDefinedParameter(scale_structure, "innerproduct_scale_structure") ||
                gmshFem.userDefinedParameter(scale_structure, "innerproduct_scale_structure"+suffix)
            ))
            {
                throw gmshfem::common::Exception("A directional filter sobolev inner product parameter (scale_structure) could not be found.");
            }
            _alpha_structure = (scale_structure / 2. / M_PI) * (scale_structure / 2. / M_PI);
        };
        InnerProduct(const ConfigurationInterface* const config, const ModelField& m, const gmshfem::common::GmshFem& gmshFem, std::string suffix="") : SobolevInnerProductInterface(config,m,gmshFem,suffix), _beta(_m_size,0.), _weight_structure(_m_size,1./((double)_m_size)), _q(m[0]), _filter_formulation("_filter_formulation"), _structureIsUpToDate(false), _filterIsFactorized(false)
        {
            _m2x = m;
            _m2y = m;
            _mxmy = m;

            for (unsigned int c = 0; c < _m_size; c++)
            {
                std::string suffix1 = "c"+std::to_string(c);
                if(!(
                    gmshFem.userDefinedParameter(_beta[c], "innerproduct_beta") ||
                    gmshFem.userDefinedParameter(_beta[c], "innerproduct_beta"+suffix) ||
                    gmshFem.userDefinedParameter(_beta[c], "innerproduct_beta"+suffix1) ||
                    gmshFem.userDefinedParameter(_beta[c], "innerproduct_beta"+suffix+suffix1)
                ))
                {
                    throw gmshfem::common::Exception("A directional filter sobolev inner product parameter (beta) could not be found.");
                }

                if(!(
                    gmshFem.userDefinedParameter(_weight_structure[c], "innerproduct_structure_weight"+suffix1) ||
                    gmshFem.userDefinedParameter(_weight_structure[c], "innerproduct_structure_weight"+suffix+suffix1)
                ))
                {
                    gmshfem::msg::warning << "A directional filter sobolev inner product parameter (weight) could not be found. Equal weights are used." << gmshfem::msg::endl;
                }
            }

            double scale_structure;
            if(!(
                gmshFem.userDefinedParameter(scale_structure, "innerproduct_scale_structure") ||
                gmshFem.userDefinedParameter(scale_structure, "innerproduct_scale_structure"+suffix)
            ))
            {
                throw gmshfem::common::Exception("A directional filter sobolev inner product parameter (scale_structure) could not be found.");
            }
            _alpha_structure = (scale_structure / 2. / M_PI) * (scale_structure / 2. / M_PI);
        };

        virtual void modelIsObsolete() override;
        virtual bool iteration(double iteration) override; //return: is obsolete at iteration ?
        virtual std::complex<double> penalization(const ModelField& m1,const ModelField& m2) const override;
    private:
        virtual void setLHS();

        void update_filter(const ModelField& m);
    };
};

#endif //H_SPECIFIC_WAVE_INNERPRODUCT_SOBOLEV
