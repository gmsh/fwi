// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_MODEL_PARAMETRIZATION_HELMHOLTZ
#define H_SPECIFIC_MODEL_PARAMETRIZATION_HELMHOLTZ

//GmshFEM Library

//GmshFWI Library
#include "../../../common/model/parametrization.h"
#include "../../../common/model/state.h"

namespace helmholtz
{
    namespace natural
    {
        class Parametrization : public ParametrizationInterface
        {
            public:
                virtual bool isModelIndependant() const override {return true;};
                /* clone */
                virtual ParametrizationInterface* clone() const
                {
                    return new Parametrization();
                }
                /* operator() */
                virtual ModelFunction operator()(const ModelFunction& m) const
                {
                    return m;
                }
                virtual ModelFunction operator()(const ModelFunction& fct, const ModelFunction& ms) const
                {
                    return fct;
                };
                virtual Sensitivity operator()(const SensitivityStateEvaluator& fct, Order order, Support support, const ModelStateEvaluator& ms) const
                {
                    return fct.state(order,support);
                }

                /* frequency_shift */
                virtual ModelFunction frequency_shift(double pulsation, const ModelFunction& m) const
                {
                    return m;
                };

                virtual unsigned int size() const {return 2;};
        };
    };
    /*
    namespace density_bulkmodulus
    {
        class Parametrization : public ParametrizationInterface
        {
            public:
                virtual bool isModelIndependant() const override {return true;};
                // clone
                virtual ParametrizationInterface* clone() const
                {
                    return new Parametrization();
                }
                // operator()
                virtual ModelFunction operator()(const ModelFunction& m) const
                {
                    ModelFunction out;
                    out.emplace_back(1. / m[0]);
                    out.emplace_back(1. / m[1]);
                    return out;
                }
                virtual ModelFunction operator()(const ModelFunction& fct, Type type, const ModelStateEvaluator& ms) const
                {
                    ModelFunction out;
                    switch (type)
                    {
                        case Type::FS:
                            //nu = 1 / rho; K = 1 / B
                            out.emplace_back(1. / fct[0]);
                            out.emplace_back(1. / fct[1]);
                            break;
                        case Type::PFS:
                            //dnu = -1 / rho^2 drho; dK = -1 / B^2 dB;
                            out.emplace_back(-1. / pow(ms.state(Type::FS)[0],2) * fct[0]);
                            out.emplace_back(-1. / pow(ms.state(Type::FS)[1],2) * fct[1]);
                            break;
                        case Type::DS:
                            out.emplace_back(1. / pow(ms.state(Type::FS)[0],4) * fct[0]);
                            out.emplace_back(1. / pow(ms.state(Type::FS)[1],4) * fct[1]);
                            break;
                        case Type::AS: case Type::PAS:
                            out.emplace_back(-1. / pow(ms.state(Type::FS)[0],2) * fct[0]);
                            out.emplace_back(-1. / pow(ms.state(Type::FS)[1],2) * fct[1]);
                            break;
                    }
                    return out;
                };

                // frequency_shift
                virtual ModelFunction frequency_shift(double pulsation, const ModelFunction& m) const
                {
                    return m;
                };

                virtual unsigned int size() const {return 2;};
        };
    };
    */
};


#endif //H_SPECIFIC_MODEL_PARAMETRIZATION_HELMHOLTZ
