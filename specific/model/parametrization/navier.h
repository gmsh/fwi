// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_MODEL_PARAMETRIZATION_NAVIER
#define H_SPECIFIC_MODEL_PARAMETRIZATION_NAVIER

//GmshFEM Library

//GmshFWI Library
#include "../../../common/model/parametrization.h"
#include "../../../common/model/state.h"

namespace navier
{
    namespace natural
    {
        class Parametrization : public ParametrizationInterface
        {
            public:
                virtual bool isModelIndependant() const override {return true;};
                /* clone */
                virtual ParametrizationInterface* clone() const
                {
                    return new Parametrization();
                }
                /* operator() */
                virtual ModelFunction operator()(const ModelFunction& m) const
                {
                    return m;
                }
                virtual ModelFunction operator()(const ModelFunction& fct, const ModelFunction& m) const
                {
                    return fct;
                };
                virtual Sensitivity operator()(const SensitivityStateEvaluator& fct, Order order, Support support, const ModelStateEvaluator& ms) const
                {
                    return fct.state(order,support);
                }

                virtual unsigned int size() const {return 3;};
        };
    };
};

#endif //H_SPECIFIC_MODEL_PARAMETRIZATION_NAVIER
