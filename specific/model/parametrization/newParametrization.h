// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_MODEL_PARAMETRIZATION_NEWPARAMETRIZATION
#define H_SPECIFIC_MODEL_PARAMETRIZATION_NEWPARAMETRIZATION

//GmshFEM Library

//GmshFWI Library
#include "simplewave.h"
#include "helmholtz.h"
#include "navier.h"

template<Physic T_Physic>
ParametrizationInterface* newParametrization(const gmshfem::common::GmshFem& gmshFem);

/* acoustic */
template<>
ParametrizationInterface* newParametrization<Physic::acoustic>(const gmshfem::common::GmshFem& gmshFem)
{
    std::string equation;
    if(!gmshFem.userDefinedParameter(equation, "equation"))
    {
      throw gmshfem::common::Exception("Equation type could not be found.");
    }
    if(equation=="simplewave")
    {
        std::string parametrization = "natural";
        if(!gmshFem.userDefinedParameter(parametrization, "parametrization"))
        {
            gmshfem::msg::warning << "Parametrization type could not be found. Default parametrization is natural." << gmshfem::msg::endl;
        }
        if(parametrization=="natural")
        {
            return new simplewave::natural::Parametrization();
        }
        else if(parametrization=="slowness2_real")
        {
            return new simplewave::slowness2_real::Parametrization();
        }
        else if(parametrization=="ln_slowness2_real")
        {
            return new simplewave::ln_slowness2_real::Parametrization(gmshFem);
        }
        else if(parametrization=="permcond_bireal")
        {
            return new simplewave::permcond_bireal::Parametrization();
        }
        else if(parametrization=="ln_permcond_bireal")
        {
            return new simplewave::ln_permcond_bireal::Parametrization(gmshFem);
        }
        else
        {
            throw gmshfem::common::Exception("Parametrization " + parametrization + " is not valid in acoustics for the simple wave equation.");
            return nullptr;
        }
    }
    else if(equation=="helmholtz")
    {
        std::string parametrization = "natural";
        if(!gmshFem.userDefinedParameter(parametrization, "parametrization"))
        {
            gmshfem::msg::warning << "Parametrization type could not be found. Default parametrization is natural." << gmshfem::msg::endl;
        }
        if(parametrization=="natural")
        {
            return new helmholtz::natural::Parametrization();
        }
        /*
        else if(parametrization=="density_bulkmodulus")
        {
            return new helmholtz::density_bulkmodulus::Parametrization();
        }
        */
        else
        {
            throw gmshfem::common::Exception("Parametrization " + parametrization + " is not valid in acoustics for Helmholtz equation.");
            return nullptr;
        }
    }
    else
    {
        throw gmshfem::common::Exception("Equation " + equation + " is not valid in acoustics.");
        return nullptr;
    }
}

/* electromagnetic */
template<>
ParametrizationInterface* newParametrization<Physic::electromagnetic>(const gmshfem::common::GmshFem& gmshFem)
{
    std::string equation;
    if(!gmshFem.userDefinedParameter(equation, "equation"))
    {
      throw gmshfem::common::Exception("Equation type could not be found.");
    }
    if(equation=="maxwell")
    {
        std::string parametrization = "natural";
        if(!gmshFem.userDefinedParameter(parametrization, "parametrization"))
        {
            gmshfem::msg::warning << "Parametrization type could not be found. Default parametrization is natural." << gmshfem::msg::endl;
        }
        if(parametrization=="natural")
        {
            return new simplewave::natural::Parametrization();
        }
        else if(parametrization=="slowness2_real")
        {
            return new simplewave::slowness2_real::Parametrization();
        }
        else if(parametrization=="ln_slowness2_real")
        {
            return new simplewave::ln_slowness2_real::Parametrization(gmshFem);
        }
        else if(parametrization=="permcond_bireal")
        {
            return new simplewave::permcond_bireal::Parametrization();
        }
        else
        {
            throw gmshfem::common::Exception("Parametrization " + parametrization + " is not valid in electromagnetics for Helmholtz equation.");
            return nullptr;
        }
    }
    else
    {
        throw gmshfem::common::Exception("No available parametrization for equation " + equation + " in electromagnetics.");
        return nullptr;
    }
}

/* elastodynamic */
template<>
ParametrizationInterface* newParametrization<Physic::elastodynamic>(const gmshfem::common::GmshFem& gmshFem)
{
    std::string equation;
    if(!gmshFem.userDefinedParameter(equation, "equation"))
    {
      throw gmshfem::common::Exception("Equation type could not be found.");
    }
    if(equation=="navier")
    {
        std::string parametrization = "natural";
        if(!gmshFem.userDefinedParameter(parametrization, "parametrization"))
        {
            gmshfem::msg::warning << "Parametrization type could not be found. Default parametrization is natural." << gmshfem::msg::endl;
        }
        if(parametrization=="natural")
        {
            return new navier::natural::Parametrization();
        }
        else
        {
            throw gmshfem::common::Exception("Parametrization " + parametrization + " is not valid in elastodynamics for Helmholtz equation.");
            return nullptr;
        }
    }
    else
    {
        throw gmshfem::common::Exception("Equation " + equation + " is not valid in elastodynamics.");
        return nullptr;
    }
}

#endif //H_COMMON_MODEL_NEWPARAMETRIZATION
