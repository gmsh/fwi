// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_MODEL_PARAMETRIZATION_SIMPLEWAVE
#define H_SPECIFIC_MODEL_PARAMETRIZATION_SIMPLEWAVE

//GmshFEM Library

//GmshFWI Library
#include "../../../common/model/parametrization.h"
#include "../../../common/model/state.h"

namespace simplewave
{
    namespace natural
    {
        class Parametrization : public ParametrizationInterface
        {
            public:
                virtual bool isModelIndependant() const override {return true;};
                /* clone */
                virtual ParametrizationInterface* clone() const
                {
                    return new Parametrization();
                }
                /* operator() */
                virtual ModelFunction operator()(const ModelFunction& m) const
                {
                    return m;
                }
                virtual ModelFunction operator()(const ModelFunction& fct, const ModelFunction& m) const
                {
                    return fct;
                };
                virtual Sensitivity operator()(const SensitivityStateEvaluator& fct, Order order, Support support, const ModelStateEvaluator& ms) const
                {
                    return fct.state(order,support);
                };

                virtual unsigned int size() const {return 1;};
        };
    };
    namespace slowness2_real
    {
        class Parametrization : public ParametrizationInterface
        {
            public:
                virtual bool isModelIndependant() const override {return true;};
                /* clone */
                virtual ParametrizationInterface* clone() const
                {
                    return new Parametrization();
                }
                /* operator() */
                virtual ModelFunction operator()(const ModelFunction& m) const
                {
                    return m;
                }
                virtual ModelFunction operator()(const ModelFunction& fct, const ModelFunction& m) const
                {
                    return fct;
                };
                virtual Sensitivity operator()(const SensitivityStateEvaluator& fct, Order order, Support support, const ModelStateEvaluator& ms) const
                {
                    Sensitivity out;
                    out.emplace_back( real(fct.state(order,support)[0]) );
                    return out;
                };

                virtual unsigned int size() const {return 1;};
        };
    };
    namespace ln_slowness2_real
    {
        class Parametrization : public ParametrizationInterface
        {
            private:
                bool _gaussNewton;
                Parametrization(bool gaussNewton) : _gaussNewton(gaussNewton) {};

            public:
                Parametrization(const gmshfem::common::GmshFem& gmshFem)
                {
                    unsigned int gn = 0;
                    if(!gmshFem.userDefinedParameter(gn, "gaussNewton"))
                    {
                        gmshfem::msg::warning << "Gauss-Newton approximation (equation) flag could not be found. Approximation is not used (default)." << gmshfem::msg::endl;
                    }
                    _gaussNewton = ((bool) gn);
                }

                /* clone */
                virtual ParametrizationInterface* clone() const
                {
                    return new Parametrization(_gaussNewton);
                }
                /* operator() */
                virtual ModelFunction operator()(const ModelFunction& m) const
                {
                    ModelFunction out;
                    //s^2 = exp(ln(s^2))
                    out.emplace_back( exp(m[0]) );
                    return out;
                }
                virtual ModelFunction operator()(const ModelFunction& fct, const ModelFunction& m) const
                {
                    ModelFunction out;
                    //ds^2 =  exp(ln(s^2)) dln(s^2)
                    out.emplace_back( exp(m[0]) * fct[0] );
                    return out;
                };
                virtual Sensitivity operator()(const SensitivityStateEvaluator& fct, Order order, Support support, const ModelStateEvaluator& ms) const
                {
                    Sensitivity out;
                    switch (order)
                    {
                        case Order::FST:
                            //j = j0 s^2
                            out.emplace_back( real(fct.state(Order::FST,support)[0]) * ms.state(Type::FS)[0] );
                            break;
                        case Order::SCD:
                            if(!_gaussNewton)
                            {
                                //dj = dj0 s^2 + j0 ds^2
                                out.emplace_back( real(fct.state(Order::SCD,support)[0]) * ms.state(Type::FS)[0] + real(fct.state(Order::FST,support)[0]) * ms.state(Type::PFS)[0] );
                            }
                            else
                            {
                                //dj = dj0 s^2
                                out.emplace_back( real(fct.state(Order::SCD,support)[0]) * ms.state(Type::FS)[0]
                                );
                            }
                            break;
                        case Order::DIAG:
                            //h = h0 (s^2)^2
                            out.emplace_back( fct.state(Order::DIAG,support)[0] * pow(ms.state(Type::FS)[0],2) );
                            break;
                    }
                    return out;
                };

                virtual unsigned int size() const {return 1;};
        };
    };

    namespace permcond_bireal
    {
        class Parametrization : public ParametrizationInterface
        {
            public:
                virtual bool isModelIndependant() const override {return true;};
                /* clone */
                virtual ParametrizationInterface* clone() const
                {
                    return new Parametrization();
                }
                /* operator() */
                virtual ModelFunction operator()(const ModelFunction& m) const
                {
                    //s^2 = epsilon -j sigma / omega
                    //out = m[0] - j m[1] / _pulsation
                    std::complex< double > im = std::complex< double >(0., 1.);
                    ModelFunction out;
                    out.emplace_back(m[0] - m[1] * (im  / _pulsation));
                    return out;
                }
                virtual ModelFunction operator()(const ModelFunction& fct, const ModelFunction& m) const
                {
                    //ds^2 = depsilon -j dsigma / omega
                    //out = m[0] - j m[1] / _pulsation
                    //(linear)
                    return (*this)(fct);
                };
                virtual Sensitivity operator()(const SensitivityStateEvaluator& fct, Order order, Support support, const ModelStateEvaluator& ms) const
                {
                    Sensitivity out;
                    switch (order)
                    {
                        case Order::FST: case Order::SCD:
                            out.emplace_back(real(fct.state(order,support)[0]));
                            out.emplace_back(imag(fct.state(order,support)[0]) * (1./_pulsation));
                            break;
                        case Order::DIAG:
                            out.emplace_back(fct.state(Order::DIAG,support)[0]);
                            out.emplace_back(fct.state(Order::DIAG,support)[0]*(1./_pulsation/_pulsation));
                            break;
                    }
                    return out;
                };

                virtual unsigned int size() const {return 2;};
        };
    };
    namespace ln_permcond_bireal
    {
        class Parametrization : public ParametrizationInterface
        {
            private:
                double _pulsation0;
                bool _gaussNewton;
                Parametrization(double pulsation0,bool gaussNewton) : _pulsation0(pulsation0), _gaussNewton(gaussNewton) {};

            public:
                Parametrization(const gmshfem::common::GmshFem& gmshFem)
                {
                    if(!gmshFem.userDefinedParameter(_pulsation0, "parametrization_frequency0"))
                    {
                        throw gmshfem::common::Exception("Reference pulsation for parametrization not found");
                    }
                    _pulsation0 *= 2. * M_PI;
                    unsigned int gn = 0;
                    if(!gmshFem.userDefinedParameter(gn, "gaussNewton"))
                    {
                        gmshfem::msg::warning << "Gauss-Newton approximation (equation) flag could not be found. Approximation is not used (default)." << gmshfem::msg::endl;
                    }
                    _gaussNewton = ((bool) gn);
                }

                /* clone */
                virtual ParametrizationInterface* clone() const
                {
                    return new Parametrization(_pulsation0,_gaussNewton);
                }
                /* operator() */
                virtual ModelFunction operator()(const ModelFunction& m) const
                {
                    //s^2 = exp(ln(epsilon)) -j exp(ln(sigma)) / (omega/omega0)
                    //out = exp(m[0]) - j exp(m[1]) / (_pulsation/_pulsation0)
                    std::complex< double > im = std::complex< double >(0., 1.);
                    ModelFunction out;
                    out.emplace_back(exp(m[0]) - exp(m[1]) * (im  / _pulsation * _pulsation0) );
                    return out;
                }
                virtual ModelFunction operator()(const ModelFunction& fct, const ModelFunction& m) const
                {
                    const std::complex< double > im = std::complex< double >(0., 1.);
                    ModelFunction out;
                    //ds^2 = dln(epsilon) exp(ln(epsilon)) -j dln(sigma) exp(ln(sigma)) / (omega/omega0)
                    //out = fct[0] * exp(m[0]) - j fct[1] * exp(m[1]) / (_pulsation/_pulsation0)
                    out.emplace_back(fct[0] * exp(m[0]) - fct[1] * exp(m[1]) * (im  / _pulsation * _pulsation0) );
                    return out;
                };
                virtual Sensitivity operator()(const SensitivityStateEvaluator& fct, Order order, Support support, const ModelStateEvaluator& ms) const
                {
                    ModelFunction out;
                    switch (order)
                    {
                        case Order::FST:
                            //j =  real(j0) epsilon = real(j0) real(s^2)
                            out.emplace_back( real(fct.state(Order::FST,support)[0]) * real(ms.state(Type::FS)[0])  );
                            //j =  imag(j0) * sigma * k0 / k = imag(j0) * -imag(s^2)
                            out.emplace_back( -imag(fct.state(Order::FST,support)[0]) * imag(ms.state(Type::FS)[0]) );
                            break;
                        case Order::SCD:
                            if(!_gaussNewton)
                            {
                                //dj =  real(dj0) epsilon + real(j0) depsilon = real(dj0) real(s^2) + real(j0) real(ds^2)
                                out.emplace_back(
                                    real(fct.state(Order::SCD,support)[0]) * real(ms.state(Type::FS)[0]) + real(fct.state(Order::FST,support)[0]) * real(ms.state(Type::PFS)[0])
                                );
                                //dj =  imag(dj0) * sigma * k0 / k + imag(j0) * dsigma * k0 / k = imag(dj0) * -imag(s^2) + imag(j0) * -imag(ds^2)
                                out.emplace_back(
                                    -imag(fct.state(Order::SCD,support)[0]) * imag(ms.state(Type::FS)[0])
                                    -imag(fct.state(Order::FST,support)[0]) * imag(ms.state(Type::PFS)[0])
                                );
                            }
                            else
                            {
                                //dj =  real(dj0) epsilon = real(dj0) real(s^2)
                                out.emplace_back(
                                    real(fct.state(Order::SCD,support)[0]) * real(ms.state(Type::FS)[0])
                                );
                                //dj =  imag(dj0) * sigma * k0 / k = imag(dj0) * -imag(s^2)
                                out.emplace_back(
                                    -imag(fct.state(Order::SCD,support)[0]) * imag(ms.state(Type::FS)[0])
                                );
                            }
                            break;
                        case Order::DIAG:
                            //dj =  real(dj0) epsilon = real(dj0) real(s^2)
                            out.emplace_back( fct.state(Order::DIAG,support)[0] * pow(real(ms.state(Type::FS)[0]),2) );
                            //dj =  imag(dj0) * sigma * k0 / k = imag(dj0) * -imag(s^2)
                            out.emplace_back( fct.state(Order::DIAG,support)[0] * pow(imag(ms.state(Type::FS)[0]),2) );
                            break;
                    }
                    return out;
                };

                virtual unsigned int size() const {return 2;};
        };
    };
};


#endif //H_SPECIFIC_MODEL_PARAMETRIZATION_SIMPLEWAVE
