// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "Function.h"

//GmshFWI Library
#include "flooding.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::equation;
using namespace gmshfem::function;
using namespace gmshfem::post;

/*
* Flooding
*/
namespace flooding
{
    Regularization::Regularization(const ConfigurationInterface* const config, const ModelField& m, const gmshfem::common::GmshFem& gmshFem, std::string suffix) : DifferentialRegularizationInterface(config,m,gmshFem,suffix)
    {
        if(!(
            gmshFem.userDefinedParameter(_p, "regularization_p") ||
            gmshFem.userDefinedParameter(_p, "regularization_p"+suffix)
        ))
        {
            throw Exception("Regularization order could not be found.");
        }
        if(_p<0.5)
        {
            throw Exception("Flooding regularization parameter p could not be smaller than 0.5.");
        }

    };

    double Regularization::performance(const ModelField& m)
    {
        std::complex<double> out = 0.;
        for (unsigned int c = 0; c < _m_size; c++)
        {
            ScalarFunction<std::complex<double>> mz = yComp(grad(m[c]));
            ScalarFunction<std::complex<double>> f = (abs(mz) - mz) / 2.;
            out +=  _lambda[c] * integrate(  pow(f,2.*_p) / 2. / _p, _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk) );
        }
        return std::real(out);
    }

    void Regularization::setRHS(Order order, Support support, const ModelState& ms)
    {
        for (unsigned int c = 0; c < _m_size; c++)
        {
            ScalarFunction<std::complex<double>> mz = yComp(grad(ms.field(Type::FS)[c]));
            ScalarFunction<std::complex<double>> f = (abs(mz) - mz) / 2.;

            ScalarFunction<std::complex<double>> partition = heaviside< std::complex< double > >( -mz );
            VectorFunction< std::complex< double > > integrand;
            if(_p==0.5)
            {
                integrand = vector< std::complex< double > >(0.,-partition,0.);
            }
            else
            {
                integrand = vector< std::complex< double > >(0.,-partition * pow(f,2.*_p-1.),0.);
            }

            switch (support)
            {
                case Support::BND:default:break;
                case Support::BLK:
                    switch (order)
                    {
                        case Order::FST:
                            _formulation[c].integral(-1. * _lambda[c] * integrand, grad(tf(_k[c])),_config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                            break;
                        case Order::SCD:
                            break;
                        default:break;
                    }
                break;
            }
        }
    }
};
