// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
#include <string>

//GmshFEM Library
#include "Exception.h"

//GmshFWI Library
#include "tikhonov.h"
#include "totalvariation.h"
#include "flooding.h"

RegularizationInterface* newRegularization(const ConfigurationInterface* const config,  const ModelField& m, const gmshfem::common::GmshFem& gmshFem, std::string suffix)
{
    std::string regularization;
    if(!gmshFem.userDefinedParameter(regularization, "regularization"))
    {
      gmshfem::msg::warning << "Regularization type could not be found." << gmshfem::msg::endl;
      return nullptr;
    }
    if(regularization=="tikhonov"){return new tikhonov::Regularization(config,m,gmshFem, suffix);}
    else if(regularization=="totalvariation"){return new totalvariation::Regularization(config,m,gmshFem, suffix);}
    else if(regularization=="flooding"){return new flooding::Regularization(config,m,gmshFem, suffix);}
    else
    {
        throw gmshfem::common::Exception("Regularization " + regularization + " is not valid.");
        return nullptr;
    }
}
