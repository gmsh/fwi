// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_MODEL_NEWREGULARIZATION
#define H_SPECIFIC_MODEL_NEWREGULARIZATION

//Standard Library
//GmshFEM Library
#include "GmshFem.h"
//GmshFWI Library
#include "../../../common/model/regularization/regularization.h"

RegularizationInterface* newRegularization(const ConfigurationInterface* const config,  const ModelField& m, const gmshfem::common::GmshFem& gmshFem, std::string suffix = "");

#endif //H_SPECIFIC_MODEL_NEWREGULARIZATION
