// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library

//GmshFWI Library
#include "tikhonov.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::equation;
using namespace gmshfem::function;
using namespace gmshfem::post;

/*
* Tikhonov
*/
namespace tikhonov
{
    Regularization::Regularization(const ConfigurationInterface* const config, const ModelField& m, const gmshfem::common::GmshFem& gmshFem, std::string suffix) : DifferentialRegularizationInterface(config,m,gmshFem,suffix) {};

    double Regularization::performance(const ModelField& m)
    {
        std::complex<double> out = 0.;
        for (unsigned int c = 0; c < _m_size; c++)
        {
            out += _lambda[c] * integrate( conj(grad(m[c])) * grad(m[c]), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk) );
        }
        return std::real(out);
    }
    void Regularization::setRHS(Order order, Support support, const ModelState& ms)
    {
        for (unsigned int c = 0; c < _m_size; c++)
        {
            switch (support)
            {
                case Support::BND:default:break;
                case Support::BLK:
                    switch (order)
                    {
                        case Order::FST:
                            _formulation[c].integral(-2. * _lambda[c] * conj(grad(ms.field(Type::FS)[c])),grad(tf(_k[c])), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                            break;
                        case Order::SCD:
                            _formulation[c].integral(-2. * _lambda[c] * conj(grad(ms.field(Type::PFS)[c])),grad(tf(_k[c])), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                            break;
                        default:break;
                    }
                break;
            }
        }
    }
};
