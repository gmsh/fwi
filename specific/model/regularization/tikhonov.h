// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_WAVE_REGULARIZATION_TIKHONOV
#define H_SPECIFIC_WAVE_REGULARIZATION_TIKHONOV

//GmshFEM Library
#include "GmshFem.h"

//GmshFWI Library
#include "../../../common/model/regularization/regularization.h"

/*
* Tikhonov
*/
namespace tikhonov
{
    class Regularization final: public DifferentialRegularizationInterface
    {
    public:
        Regularization(const ConfigurationInterface* const config, const ModelField& m, const gmshfem::common::GmshFem& gmshFem, std::string suffix="");

        virtual double performance(const ModelField& m);

        virtual void setRHS(Order order, Support support, const ModelState& ms);
    };
};

#endif //H_SPECIFIC_WAVE_REGULARIZATION_TIKHONOV
