// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "Function.h"

//GmshFWI Library
#include "totalvariation.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::equation;
using namespace gmshfem::function;
using namespace gmshfem::post;

/*
* Total variation
*/
namespace totalvariation
{
    Regularization::Regularization(const ConfigurationInterface* const config, const ModelField& m, const gmshfem::common::GmshFem& gmshFem, std::string suffix) : DifferentialRegularizationInterface(config,m,gmshFem,suffix), _beta(_m_size,0.)
    {
        for (unsigned int c = 0; c < _m_size; c++)
        {
            std::string suffix1 = "c"+std::to_string(c);
            if(!(
                gmshFem.userDefinedParameter(_beta[c], "regularization_beta") ||
                gmshFem.userDefinedParameter(_beta[c], "regularization_beta"+suffix)
                ||
                gmshFem.userDefinedParameter(_beta[c], "regularization_beta"+suffix1) ||
                gmshFem.userDefinedParameter(_beta[c], "regularization_beta"+suffix1+suffix)

            ))
            {
                throw Exception("Regularization stabilization (beta) could not be found.");
            }
        }
    };

    double Regularization::performance(const ModelField& m)
    {
        std::complex<double> out = 0.;
        for (unsigned int c = 0; c < _m_size; c++)
        {
            out +=  _lambda[c] * integrate( norm(grad(m[c])), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk) );
        }
        return std::real(out);
    }

    void Regularization::setRHS(Order order, Support support, const ModelState& ms)
    {
        for (unsigned int c = 0; c < _m_size; c++)
        {
            ScalarFunction<std::complex<double>> norm = sqrt(conj(grad(ms.field(Type::FS)[c]))*grad(ms.field(Type::FS)[c])+_beta[c]*_beta[c]);
            switch (support)
            {
                case Support::BND:default:break;
                case Support::BLK:
                    switch (order)
                    {
                        case Order::FST:
                            _formulation[c].integral(-1. * _lambda[c] * conj(grad(ms.field(Type::FS)[c]))/norm,grad(tf(_k[c])), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                            break;
                        case Order::SCD:
                            _formulation[c].integral(-1. * _lambda[c] * conj(grad(ms.field(Type::PFS)[c]))/norm,grad(tf(_k[c])), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                            _formulation[c].integral(_lambda[c] / pow(norm,3) * conj(grad(ms.field(Type::PFS)[c]))*grad(ms.field(Type::FS)[c]),conj(grad(ms.field(Type::FS)[c]))*grad(tf(_k[c])), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                            break;
                        default:break;
                    }
                break;
            }
        }
    }
};
