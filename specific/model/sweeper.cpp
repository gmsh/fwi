// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//gmshFWI Library
#include "sweeper.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::equation;
using namespace gmshfem::function;

/*
* ModelFunctionSweeper
*/

ModelFunctionSweeperInterface* newModelFunctionSweeper(const gmshfem::common::GmshFem& gmshFem)
{
    std::string sweeper;
    if(!gmshFem.userDefinedParameter(sweeper, "sweep_type"))
    {
      throw gmshfem::common::Exception("Sweeping type could not be found.");
    }
    if(sweeper=="constant"){return new constant::ModelFunctionSweeper(gmshFem);}
    else if(sweeper=="linear-y"){return new linear_y::ModelFunctionSweeper(gmshFem);}
    else
    {
        throw gmshfem::common::Exception("Sweeping type " + sweeper + " is not valid.");
        return nullptr;
    }
}

namespace constant
{
    ModelFunctionSweeper::ModelFunctionSweeper(const gmshfem::common::GmshFem& gmshFem)
    {
        if(!(
            gmshFem.userDefinedParameter(_a0, "sweep_c00") &&
            gmshFem.userDefinedParameter(_aN, "sweep_c0N") &&
            gmshFem.userDefinedParameter(_Na, "sweep_N1") &&
            gmshFem.userDefinedParameter(_b0, "sweep_c10") &&
            gmshFem.userDefinedParameter(_bN, "sweep_c1N") &&
            gmshFem.userDefinedParameter(_Nb, "sweep_N2")
        ))
        {
            throw Exception("A constant sweeper parameter could not be found");
        }
        if(_Na!=0){_asteps = (_aN-_a0) / ((double)_Na);}
        if(_Nb!=0){_bsteps = (_bN-_b0) / ((double)_Nb);}
    }
    ModelFunction ModelFunctionSweeper::operator()(unsigned int index1, unsigned int index2) const
    {
        ModelFunction out(2);
        double a = _a0+((double)index1)*_asteps;
        double b = _b0+((double)index2)*_bsteps;
        out[0] = a;
        out[1] = b;
        msg::print << "a = " << a << "; b = " << b << msg::endl;
        return out;
    }
}

namespace linear_y
{
    ModelFunctionSweeper::ModelFunctionSweeper(const gmshfem::common::GmshFem& gmshFem)
    {
        if(!gmshFem.userDefinedParameter(_a0, "sweep_a0"))
        {
            throw Exception("Sweep parameter (a00) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_aH0, "sweep_aH0"))
        {
            throw Exception("Sweep parameter (aH0) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_aHN, "sweep_aHN"))
        {
            throw Exception("Sweep parameter (aHN) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_H, "sweep_H"))
        {
            throw Exception("Sweep parameter (H) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_offset, "sweep_offset"))
        {
            throw Exception("Sweep parameter (offset) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_N, "sweep_N1"))
        {
            throw Exception("Sweep number (N) could not be found");
        }
        if(_N!=0){_steps = (_aHN-_aH0) / ((double)_N);}
    }
    ModelFunction ModelFunctionSweeper::operator()(unsigned int index1, unsigned int index2) const
    {
        double aH = _aH0+((double)index1)*_steps;
        ScalarFunction< std::complex<double> > m = _a0 - (aH - _a0) / _H * (y< std::complex<double> >()+_offset);
        return ModelFunction(1,m);
    }
}

namespace linear
{
    ModelFunctionSweeper::ModelFunctionSweeper(const gmshfem::common::GmshFem& gmshFem)
    {
        if(!gmshFem.userDefinedParameter(_a0, "sweep_a0"))
        {
            throw Exception("Sweep parameter (a00) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_aH0, "sweep_aH0"))
        {
            throw Exception("Sweep parameter (aH0) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_aHN, "sweep_aHN"))
        {
            throw Exception("Sweep parameter (aHN) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_H, "sweep_H"))
        {
            throw Exception("Sweep parameter (H) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_offset, "sweep_offset"))
        {
            throw Exception("Sweep parameter (offset) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_N, "sweep_N1"))
        {
            throw Exception("Sweep number (N) could not be found");
        }
        if(_N!=0){_steps = (_aHN-_aH0) / ((double)_N);}
    }
    ModelFunction ModelFunctionSweeper::operator()(unsigned int index1, unsigned int index2) const
    {
        double aH = _aH0+((double)index1)*_steps;
        ScalarFunction< std::complex<double> > m = _a0 - (aH - _a0) / _H * (y< std::complex<double> >()+_offset);
        return ModelFunction(1,m);
    }
}

/*
* ModelFieldSweeper
*/
ModelFieldSweeperInterface* newModelFieldSweeper(std::string name, const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem)
{
    std::string sweeper;
    if(!gmshFem.userDefinedParameter(sweeper, "sweeper_"+name+"_type"))
    {
      throw gmshfem::common::Exception("Sweeping type for "+name+" could not be found.");
    }
    if(sweeper=="constant"){return new constant::ModelFieldSweeper(name,config,m_discret,gmshFem);}
    else if(sweeper=="gaussian"){return new gaussian::ModelFieldSweeper(name,config,m_discret,gmshFem,false);}
    else if(sweeper=="derivative_gaussian"){return new gaussian::ModelFieldSweeper(name,config,m_discret,gmshFem,true);}
    else
    {
        throw gmshfem::common::Exception("Sweeping type " + sweeper + " is not valid.");
        return nullptr;
    }
}

namespace constant
{
    ModelFieldSweeper::ModelFieldSweeper(std::string name, const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem) : ModelFieldSweeperInterface(name,config,m_discret,gmshFem), _N(std::numeric_limits<unsigned int>::max())
    {
        if(!gmshFem.userDefinedParameter(_m0, "sweeper_"+name+"_m0"))
        {
            throw Exception("Sweeper parameter (m0) could not be found");
        }
        _formulation.integral(-_m0, tf(_k[0]),_config->model_unknown(Support::BLK), _integrationType+"10");
        _formulation.assemble();
        _formulation.removeTerms();
        _formulation.solve(true);
    }

    ModelField ModelFieldSweeper::operator()(unsigned int index1, unsigned int index2) const
    {
        return _k;
    }
    std::array<double,2> ModelFieldSweeper::value(unsigned int index1, unsigned int index2) const
    {
        return {_m0, 0.};
    }
    std::array<unsigned int,2> ModelFieldSweeper::max_idx() const
    {
        return {_N,0};
    }
}

namespace gaussian
{
    ModelFieldSweeper::ModelFieldSweeper(std::string name, const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem, bool derivative) : ModelFieldSweeperInterface(name,config,m_discret,gmshFem), _derivative(derivative)
    {
        if(!gmshFem.userDefinedParameter(_a0, "sweeper_"+name+"_a0"))
        {
            throw Exception("Sweeper parameter (a0) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_aN, "sweeper_"+name+"_aN"))
        {
            throw Exception("Sweeper parameter (aN) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_N, "sweeper_"+name+"_N"))
        {
            throw Exception("Sweeper parameter (N) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_x0, "sweeper_"+name+"_x0"))
        {
            throw Exception("Sweeper parameter (x0) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_y0, "sweeper_"+name+"_y0"))
        {
            throw Exception("Sweeper parameter (y0) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_minf, "sweeper_"+name+"_minf"))
        {
            throw Exception("Sweeper parameter (minf) could not be found");
        }
        if(!gmshFem.userDefinedParameter(_mg, "sweeper_"+name+"_mg"))
        {
            throw Exception("Sweeper parameter (mg) could not be found");
        }

        if(_N!=0){_step = (_aN-_a0) / ((double)_N);}
    }

    ModelField ModelFieldSweeper::operator()(unsigned int index1, unsigned int index2) const
    {
        double a = value(index1,index2)[0];
        ScalarFunction<std::complex<double>> r2 = pow(x<std::complex<double>>()-_x0,2) + pow(y<std::complex<double>>()-_y0,2);
        ModelMonoFunction m;
        if(!_derivative)
        {
            m = _minf + _mg * exp( - a * r2 );
        }
        else
        {
            m = - _mg * r2 * exp( - a * r2 );
        }

        _formulation.integral(-m, tf(_k[0]),_config->model_unknown(Support::BLK), _integrationType+"10");
        _formulation.assemble();
        _formulation.removeTerms();
        _formulation.solve(true);
        _formulation.setRHSToZero();
        
        return _k;
    }
    std::array<double,2> ModelFieldSweeper::value(unsigned int index1, unsigned int index2) const
    {
        return {_a0 + ((double) index1) * _step, 0.};
    }
    std::array<unsigned int,2> ModelFieldSweeper::max_idx() const
    {
        return {_N,0};
    }
}
