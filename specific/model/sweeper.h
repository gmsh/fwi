// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_MODEL_SWEEPER
#define H_SPECIFIC_MODEL_SWEEPER

//gmshFWI Library
#include "../../common/model/sweeper.h"

ModelFunctionSweeperInterface* newModelFunctionSweeper(const gmshfem::common::GmshFem& gmshFem);

namespace constant
{
    /*
    * class ModelFunctionSweeper
    */
    class ModelFunctionSweeper : public ModelFunctionSweeperInterface
    {
    private:
        double _a0;
        double _aN;
        double _b0;
        double _bN;
        double _asteps;
        double _bsteps;
        unsigned int _Na;
        unsigned int _Nb;
    public:
        ModelFunctionSweeper(const gmshfem::common::GmshFem& gmshFem);
        virtual ModelFunction operator()(unsigned int index1, unsigned int index2) const override;
    };
};

namespace linear_y
{
    /*
    * class ModelFunctionSweeper
    */
    class ModelFunctionSweeper : public ModelFunctionSweeperInterface
    {
    private:
        double _a0;
        double _aH0;
        double _aHN;
        double _H;
        double _offset;
        unsigned int _N;

        double _steps;
    public:
        ModelFunctionSweeper(const gmshfem::common::GmshFem& gmshFem);
        virtual ModelFunction operator()(unsigned int index1, unsigned int index2) const override;
    };
};

namespace linear
{
    /*
    * class ModelFunctionSweeper
    */
    class ModelFunctionSweeper : public ModelFunctionSweeperInterface
    {
    private:
        double _a0;
        double _aH0;
        double _aHN;
        double _H;
        double _offset;
        unsigned int _N;

        double _steps;
    public:
        ModelFunctionSweeper(const gmshfem::common::GmshFem& gmshFem);
        virtual ModelFunction operator()(unsigned int index1, unsigned int index2) const override;
    };
};

ModelFieldSweeperInterface* newModelFieldSweeper(std::string name, const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem);

namespace constant
{
    class ModelFieldSweeper : public ModelFieldSweeperInterface
    {
    private:
        double _m0;
        unsigned int _N;
    public:
        ModelFieldSweeper(std::string name, const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem);
        virtual ModelField operator()(unsigned int index1, unsigned int index2) const;
        virtual std::array<double,2> value(unsigned int index1, unsigned int index2) const;
        virtual std::array<unsigned int,2> max_idx() const;
    };
}

namespace gaussian
{
    class ModelFieldSweeper : public ModelFieldSweeperInterface
    {
    private:
        bool _derivative;

        double _a0;
        double _aN;
        unsigned int _N;
        double _x0;
        double _y0;
        double _minf;
        double _mg;

        double _step;
    public:
        ModelFieldSweeper(std::string name, const ConfigurationInterface* const config, const model::Discretization& m_discret, const gmshfem::common::GmshFem& gmshFem, bool derivative = false);
        virtual ModelField operator()(unsigned int index1, unsigned int index2) const;
        virtual std::array<double,2> value(unsigned int index1, unsigned int index2) const;
        virtual std::array<unsigned int,2> max_idx() const;
    };
}
#endif // H_COMMON_MODEL_SWEEPER
