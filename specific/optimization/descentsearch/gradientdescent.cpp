// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFWI Library
#include "gradientdescent.h"

using namespace gmshfem;
using namespace gmshfem::common;

namespace gradientdescent
{
    /*
    * class DescentSearch
    */
    void DescentSearch::operator()(FunctionalInterface* const functional) const
    {
        msg::print <<"Search descent direction (steepest descent)" << msg::endl;
        msg::indent();
        ModelField p = functional->gradient();
        p.oppose();
        functional->setModelPerturbation(p);
        msg::unindent();
    }
}

namespace gradientdescent_tr
{
    /*
    * class DescentSearch
    */
    TrustRegionReturn DescentSearch::operator()(FunctionalInterface* const functional, double trustRegionRadius) const
    {
        //Cauchy point
        msg::print <<"Search descent direction (trust-region steepest descent)" << msg::endl;
        msg::indent();
        double jj = std::real( functional->innerproduct()->product(functional->gradient(),functional->gradient()) );
        ModelField p = functional->gradient() * (trustRegionRadius / std::sqrt(jj));
        p.oppose();
        functional->setModelPerturbation(p);
        double pred_decrease = -trustRegionRadius * std::sqrt(jj);
        msg::print  << "pred_decrease = " << pred_decrease << msg::endl;
        TrustRegionReturn out = {pred_decrease,trustRegionRadius,true};
        msg::unindent();
        return out;

    }

    double DescentSearch::predicted_performance(const ModelField& p, FunctionalInterface* const functional) const
    {
        return functional->performance() + functional->directional1(p);
    }
};
