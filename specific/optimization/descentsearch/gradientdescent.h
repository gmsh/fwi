// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_DESCENTSEARCH_GRADIENTSDESCENT
#define H_SPECIFIC_OPTIMIZATION_DESCENTSEARCH_GRADIENTSDESCENT

//GmshFEM Library
#include "GmshFem.h"
#include "CSVio.h"

//GmshFWI Library
#include "../../../common/optimization/descentsearch.h"

namespace gradientdescent
{

    /*
    * class DescentSearchHistory
    */
    class DescentSearchHistory final: public DescentSearchHistoryInterface
    {
    public:
        virtual void write(std::string filename) const {};
    };

    /*
    * class DescentSearch
    */
    class DescentSearch final: public DescentSearchInterface
    {
    private:
        DescentSearchHistory* const _history;
    public:
        DescentSearch(const gmshfem::common::GmshFem& gmshFem) : _history( new DescentSearchHistory() ) {};
        ~DescentSearch() {delete _history;};

        virtual void operator()(FunctionalInterface* const functional) const;

        virtual const DescentSearchHistoryInterface* const history() const {return _history;};
    };
};

namespace gradientdescent_tr
{
    /*
    * class DescentSearchHistory
    */
    class DescentSearchHistory final: public DescentSearchHistoryInterface
    {
    public:
        virtual void write(std::string filename) const {};
    };

    /*
    * class DescentSearch
    */
    class DescentSearch final: public TrustRegionDescentSearchInterface
    {
    private:
        DescentSearchHistory* const _history;
    public:
        DescentSearch(const gmshfem::common::GmshFem& gmshFem) : _history( new DescentSearchHistory() ) {};
        ~DescentSearch() {delete _history;};

        virtual TrustRegionReturn operator()(FunctionalInterface* const functional, double trustRegionRadius) const;
        virtual double predicted_performance(const ModelField& p, FunctionalInterface* const functional) const;

        virtual const DescentSearchHistoryInterface* const history() const {return _history;};
    };
};

#endif // H_SPECIFIC_OPTIMIZATION_DESCENTSEARCH_GRADIENTSDESCENT
