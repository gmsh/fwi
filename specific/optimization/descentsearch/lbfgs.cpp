// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
//GmshFWI Library
#include "lbfgs.h"

using namespace gmshfem;
using namespace gmshfem::common;

static const std::complex< double > im = std::complex< double >(0., 1.);

namespace lbfgs
{
    /*
    * class DescentSearchHistoryLine
    */
    void DescentSearchHistoryLine::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Rho" << "Gamma"<< csv::endl;
        }
        else
        {
            file << rho << gamma << csv::endl;
        }
    }

    /*
    * class DescentSearchHistory
    */
    void DescentSearchHistory::write(std::string filename) const
    {
        CSVio file(filename, ';', common::OpeningMode::NewFile);
        if(bfgs_hist.empty())
        {
            file << "Descent search limited history is empty." << csv::endl;
        }
        else
        {
            bfgs_hist.begin()->write(file,true);
            for (auto it = bfgs_hist.begin(); it != bfgs_hist.end(); it++)
            {
                it->write(file);
            }
        }
    }


    /*
    * class DescentSearchInterface2
    */
    DescentSearchInterface2::DescentSearchInterface2(DescentSearchHistory* const history, const GmshFem& gmshFem) : _init(false), _memory_size(0), _history(history)
    {
        if(!gmshFem.userDefinedParameter(_memory_size, "descent_memory"))
        {
            throw Exception("l-BFGS memory size could not be found.");
        }
    }

    ModelField DescentSearchInterface2::inverse_product(const ModelField& p, const InnerProductInterface* const innerproduct) const
    {
        msg::print <<"l-BFGS two-loop recursion" << msg::endl;
        msg::indent();

        unsigned int size = _history->bfgs_hist.size();
        ModelField q = p;
        std::vector<double> alpha(size);
        for (int m = size-1; m >= 0; m--)
        {
            std::complex<double> alpha_c = (_history->bfgs_hist)[m].rho * innerproduct->product((_history->bfgs_hist)[m].s,q);
            alpha[m] = std::real(alpha_c);
            q = q - (_history->bfgs_hist)[m].y * alpha[m];
        }

        if(size>0)
        {
            q = q * _history->bfgs_hist.back().gamma;
        }

        for (unsigned int m = 0; m < size; m++)
        {
            std::complex<double> beta_c = (_history->bfgs_hist)[m].rho * innerproduct->product((_history->bfgs_hist)[m].y,q);
            double beta = std::real(beta_c);
            q = q + (_history->bfgs_hist)[m].s * (alpha[m]-beta);
        }

        msg::unindent();
        return q;
    }

    void DescentSearchInterface2::update_memory(FunctionalInterface* const functional) const
    {
        msg::print <<"update limited memory BFGS" << msg::endl;
        msg::indent();

        if(_init)
        {
            ModelField s = functional->m()-_history->m;
            ModelField y = functional->gradient()-_history->g;
            std::complex<double> rho_c = 1. / functional->innerproduct()->product(s,y);
            double rho  = std::real(rho_c);
            std::complex<double> gamma_c = 1. / rho / functional->innerproduct()->product(y,y);
            double gamma = std::real(gamma_c);
            msg::print <<"rho = " << rho << " and gamma = " << gamma << msg::endl;
            if(_history->bfgs_hist.size()==_memory_size){_history->bfgs_hist.pop_front();}
            _history->bfgs_hist.emplace_back(rho,gamma,s,y);
        }
        else
        {
            throw Exception("Impossible to update before initialization");
        }

        _history->m = functional->m();
        _history->g = functional->gradient();

        msg::unindent();
    }

    void DescentSearchInterface2::initialize_memory(FunctionalInterface* const functional) const
    {
        msg::print <<"initialize limited memory BFGS" << msg::endl;
        msg::indent();

        _history->m = functional->m();
        _history->g = functional->gradient();
        _init=true;

        msg::unindent();
    }

    /*
    * class DescentSearch
    */
    void DescentSearch::operator()(FunctionalInterface* const functional) const
    {
        msg::print <<"Search descent direction (limited memory BFGS)" << msg::endl;
        msg::indent();

        ModelField q = inverse_product(functional->gradient(),functional->innerproduct());
        q.oppose();
        functional->setModelPerturbation(q);

        msg::unindent();
    }

}

namespace lbfgs_tr
{
    /*
    * class DescentSearchHistoryLine
    */
    void DescentSearchHistoryLine::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Success" << "Gradient norm squared" << "Direction norm" << "Radius squared" << "Boundary reached" << csv::endl;
        }
        else
        {
            file << success << gradientNorm2 << xx << radius2 << onBoundary << csv::endl;
        }
    }

    /*
    * class DescentSearchHistory
    */
    void DescentSearchHistory::write(std::string filename) const
    {
        CSVio file(filename, ';', common::OpeningMode::NewFile);
        if(tr_hist.empty() || bfgs_hist.empty())
        {
            file << "Trust region descent search history is empty." << csv::endl;
        }
        else
        {
            tr_hist.begin()->write(file,true);
            bfgs_hist.begin()->write(file,true);
            int tot_size = tr_hist.size();
            int mem_size = bfgs_hist.size();
            int offset = tot_size - mem_size;
            for (int i = 0; i < tot_size; i++)
            {
                tr_hist[i].write(file);
                if(i >= offset)
                {
                    bfgs_hist[i-offset].write(file);
                }
                else
                {
                    file << "--" << csv::endl;
                }
            }
        }
    }

    /*
    * class DescentSearch
    */

    ModelField DescentSearch::direct_product(const ModelField& p, const InnerProductInterface* const innerproduct) const
    {
        if(!_aIsUpToDate){unroll(innerproduct);}

        unsigned size = _a.size();
        if(size == 0){return p;}
        else
        {
            ModelField q = p / _history->bfgs_hist.back().gamma;
            for (unsigned int i = 0; i < size; i++)
            {
                q += _history->bfgs_hist[i].y * innerproduct->product(_history->bfgs_hist[i].y , p) * _history->bfgs_hist[i].rho;
                q -= _a[i] * innerproduct->product(_a[i] , p);
            }
            return q;
        }
    }

    void DescentSearch::unroll(const InnerProductInterface* const innerproduct) const
    {
        msg::print <<"unrolling the l-BFGS formula" << msg::endl;
        unsigned int size = _history->bfgs_hist.size();
        _a.resize(size);
        for (unsigned int i = 0; i < size; i++)
        {
            _a[i] = _history->bfgs_hist[i].s / _history->bfgs_hist.back().gamma;
            for (size_t j = 0; j < i; j++)
            {
                _a[i] += _history->bfgs_hist[j].y * innerproduct->product(_history->bfgs_hist[j].y , _history->bfgs_hist[i].s) * _history->bfgs_hist[j].rho;
                _a[i] -= _a[j] * innerproduct->product(_a[j] , _history->bfgs_hist[i].s);
            }
            _a[i] = _a[i] / std::sqrt( innerproduct->product(_history->bfgs_hist[i].s,_a[i]) );
        }
        _aIsUpToDate=true;
    }
    double to_tau(double pp,double xp,double xx,double radius2)
    {
        double a = pp;
        double b = xp;
        double c = xx-radius2;
        double rho = b*b - a*c;
        return (-b + std::sqrt(rho))/a;
    }
    TrustRegionReturn DescentSearch::operator()(FunctionalInterface* const functional, double trustRegionRadius) const
    {
        msg::print <<"Search descent direction (trust region limited memory BFGS)" << msg::endl;
        msg::indent();

        double jj = 0.;
        double pp = 0.;
        double pred_decrease = 0.;
        bool onBoundary = false;
        double radius2 = trustRegionRadius * trustRegionRadius;
        msg::print  << "radius2 = " << radius2 << msg::endl;

        const ModelField& j = functional->gradient();

        ModelField pB = inverse_product(j,functional->innerproduct());
        pB.oppose();
        double pBpB = std::real(functional->innerproduct()->product(pB,pB));
        msg::print  << "pBpB = " << pBpB << msg::endl;

        if(pBpB<=radius2)
        {
            onBoundary=false;
            pred_decrease = functional->directional1(pB) / 2.;
            functional->setModelPerturbation(pB);
            pp = pBpB;
        }
        else
        {
            onBoundary=true;
            jj = std::real( functional->innerproduct()->product(j,j) );

            double jBj = std::real( functional->innerproduct()->product(direct_product(j,functional->innerproduct()),j) );
            msg::print  << "jBj = " << jBj << msg::endl;
            double alpha = jj / jBj;
            msg::print  << "alpha = " << alpha << msg::endl;
            double mu = std::sqrt(radius2 / jj);
            msg::print  << "mu = " << mu << msg::endl;
            double tau = mu / alpha;
            msg::print  << "tau = " << tau << msg::endl;
            if(tau <= 1.)
            {
                pred_decrease = -mu * jj + mu*mu/2. * jBj;
                functional->setModelPerturbation(j * (-mu));
                pp = mu*mu*jj;
            }
            else
            {
                ModelField pC = j * (-alpha);
                ModelField q = pB - pC;
                double qq =std::real( functional->innerproduct()->product(q,q) );
                double pCq = std::real( functional->innerproduct()->product(pC,q) );
                double taup = to_tau(qq,pCq,alpha*alpha*jj,radius2); //solve ||pC+taup * q||^2=radius2
                msg::print  << "taup = " << taup << msg::endl;

                ModelField x = pC + q * taup;
                pred_decrease = functional->directional1(x) + 0.5 * std::real( functional->innerproduct()->product(direct_product(x,functional->innerproduct()),x) );

                functional->setModelPerturbation(x);
                pp = std::real( functional->innerproduct()->product(x,x) );
            }
        }
        msg::print  << "pp = " << pp << msg::endl;
        msg::print  << "predicted decrease = " << pred_decrease << msg::endl;

        static_cast<DescentSearchHistory*>(_history)->tr_hist.emplace_back(!onBoundary, jj, pp, radius2, onBoundary);

        msg::unindent();
        TrustRegionReturn out = {pred_decrease,std::sqrt(pp),onBoundary};
        return out;
    }

    double DescentSearch::predicted_performance(const ModelField& p, FunctionalInterface* const functional) const
    {
        double pBp = std::real( functional->innerproduct()->product(direct_product(p,functional->innerproduct()),p) );
        return functional->performance() + functional->directional1(p) + 0.5 * pBp;
    }
}
