// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_DESCENTSEARCH_LBFGS
#define H_SPECIFIC_OPTIMIZATION_DESCENTSEARCH_LBFGS

//Standard Library
#include <deque>

//GmshFEM Library
#include "GmshFem.h"
#include "CSVio.h"

//GmshFWI Library
#include "../../../common/optimization/descentsearch.h"

/* Two loops implementation, see Nocedal. See Sorber for the complex case.*/
namespace lbfgs
{
    /*
    * class DescentSearchHistoryLine
    */
    class DescentSearchHistoryLine
    {
    public:
        const double rho;
        const double gamma;
        const ModelField s; //we could only store dofs values instead
        const ModelField y; //we could only store dofs values instead

        DescentSearchHistoryLine(double rho_init, double gamma_init, const ModelField& s_init, const ModelField& y_init) : rho(rho_init), gamma(gamma_init), s(s_init), y(y_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class DescentSearchHistory
    */
    class DescentSearchHistory: public DescentSearchHistoryInterface
    {
    public:
        std::deque<DescentSearchHistoryLine> bfgs_hist;
        ModelField m;
        ModelField g;
        virtual void write(std::string filename) const;
        virtual ~DescentSearchHistory() = default;
    };

    /*
    * class DescentSearchInterface2
    */
    class DescentSearchInterface2
    {
    protected:
        mutable bool _init;
        unsigned int _memory_size;
        DescentSearchHistory* const _history;
    public:
        DescentSearchInterface2(DescentSearchHistory* const history, const gmshfem::common::GmshFem& gmshFem);
        ~DescentSearchInterface2() {delete _history;};

        ModelField inverse_product(const ModelField& p, const InnerProductInterface* const innerproduct) const;
        void update_memory(FunctionalInterface* const functional) const;
        void initialize_memory(FunctionalInterface* const functional) const;
    };

    /*
    * class DescentSearch
    */
    class DescentSearch final: public DescentSearchInterface, public DescentSearchInterface2
    {
    public:
        DescentSearch(const gmshfem::common::GmshFem& gmshFem) : DescentSearchInterface2(new DescentSearchHistory(), gmshFem) {};

        virtual void operator()(FunctionalInterface* const functional) const;

        virtual const DescentSearchHistoryInterface* const history() const {return _history;};

        virtual void update(FunctionalInterface* const functional) const override {update_memory(functional);};
        virtual void initialize(FunctionalInterface* const functional) const override {initialize_memory(functional);};

    };
};

namespace lbfgs_tr
{
    /*
    * class DescentSearchHistoryLine
    */
    class DescentSearchHistoryLine
    {
    public:
        const bool success;
        const double gradientNorm2;
        const double xx;
        const double radius2;
        const bool onBoundary;

        DescentSearchHistoryLine(bool success_init, double gradientNorm2_init, double xx_init, double radius2_init, bool onBoundary_init) : success(success_init), gradientNorm2(gradientNorm2_init), xx(xx_init), radius2(radius2_init), onBoundary(onBoundary_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class DescentSearchHistory
    */
    class DescentSearchHistory final: public lbfgs::DescentSearchHistory
    {
    public:
        std::vector<DescentSearchHistoryLine> tr_hist;
        virtual void write(std::string filename) const override;
    };

    /*
    * class DescentSearch
    */
    class DescentSearch final: public TrustRegionDescentSearchInterface, public lbfgs::DescentSearchInterface2
    {
    private:
        mutable std::vector<ModelField> _a;
        mutable bool _aIsUpToDate;
    public:
        DescentSearch(const gmshfem::common::GmshFem& gmshFem) : DescentSearchInterface2(new DescentSearchHistory(),gmshFem), _a(0), _aIsUpToDate(false) {};

        ModelField direct_product(const ModelField& p, const InnerProductInterface* const innerproduct) const;
        void unroll(const InnerProductInterface* const innerproduct) const;

        virtual TrustRegionReturn operator()(FunctionalInterface* const functional, double trustRegionRadius) const;
        virtual void update(FunctionalInterface* const functional) const override {update_memory(functional);_aIsUpToDate=false;};
        virtual void initialize(FunctionalInterface* const functional) const override {initialize_memory(functional);_aIsUpToDate=false;};
        virtual double predicted_performance(const ModelField& p, FunctionalInterface* const functional) const override;

        virtual const DescentSearchHistoryInterface* const history() const {return _history;};
    };
};

#endif // H_SPECIFIC_OPTIMIZATION_DESCENTSEARCH_LBFGS
