// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
#include <string>

//GmshFEM Library
#include "Exception.h"

//GmshFWI Library
#include "newDescentSearch.h"
#include "gradientdescent.h"
#include "newton_conjugategradient.h"
#include "newton_steihaug_conjugategradient.h"
#include "lbfgs.h"

DescentSearchInterface* newDescentSearch(const gmshfem::common::GmshFem& gmshFem)
{
    std::string descentsearch;
    if(!gmshFem.userDefinedParameter(descentsearch, "descentsearch"))
    {
      throw gmshfem::common::Exception("Descent search type could not be found.");
    }
    if(descentsearch=="gradientdescent"){return new gradientdescent::DescentSearch(gmshFem);}
    else if(descentsearch=="lbfgs"){return new lbfgs::DescentSearch(gmshFem);}
    else if(descentsearch=="lbfgs_tr"){return new lbfgs_tr::DescentSearch(gmshFem);}
    else if(descentsearch=="newton_conjugategradient"){return new newton_conjugategradient::DescentSearch(gmshFem);}
    else if(descentsearch=="newton_steihaug_conjugategradient"){return new newton_steihaug_conjugategradient::DescentSearch(gmshFem);}
    else if(descentsearch=="gradientdescent_tr"){return new gradientdescent_tr::DescentSearch(gmshFem);}
    else
    {
        throw gmshfem::common::Exception("Descent search " + descentsearch + " is not valid.");
        return nullptr;
    }
}
