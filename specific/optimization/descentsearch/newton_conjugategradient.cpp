// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
//GmshFWI Library
#include "newton_conjugategradient.h"

using namespace gmshfem;
using namespace gmshfem::common;

static const std::complex< double > im = std::complex< double >(0., 1.);

namespace newton_conjugategradient
{
    /*
    * class DescentSearchHistoryLine2
    */
    void DescentSearchHistoryLine2::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Residual" << "Alpha" << "Beta" << csv::endl;
        }
        else
        {
            file << residual << alpha << beta << csv::endl;
        }
    }

    /*
    * class DescentSearchHistoryLine
    */
    void DescentSearchHistoryLine::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Success" << "Eta squared" << "Gradient norm squared" << "Iteration number" << csv::endl;
            file << "History" << csv::endl;
        }
        else
        {
            file << success << eta2 << gradientNorm2 << iterations << csv::endl;
            if(!history.empty())
            {
                history.begin()->write(file,true);
                for (auto it = history.begin(); it != history.end(); it++)
                {
                    it->write(file);
                }
            }
        }
    }

    /*
    * class DescentSearchHistory
    */
    void DescentSearchHistory::write(std::string filename) const
    {
        CSVio file(filename, ';', common::OpeningMode::NewFile);
        if(this->empty())
        {
            file << "Descent search history is empty." << csv::endl;
        }
        else
        {
            this->begin()->write(file,true);
            for (auto it = this->begin(); it != this->end(); it++)
            {
                file << "--" << csv::endl;
                it->write(file);
            }
        }
    }

    /*
    * class DescentSearch
    */
    DescentSearch::DescentSearch(const GmshFem& gmshFem) : _history( new DescentSearchHistory() )
    {
        if(!gmshFem.userDefinedParameter(_eta2, "descent_eta0"))
        {
            throw Exception("Initial value of forcing sequence could not be found.");
        }
        _eta2 *= _eta2;
        if(!gmshFem.userDefinedParameter(_maxIteration, "descent_maxIteration"))
        {
            throw Exception("Maximum number of iterations for descent search (descent_maxIteration) could not be found.");
        }
    }

    void DescentSearch::operator()(FunctionalInterface* const functional) const
    {
        msg::print <<"Search descent direction (Newton conjugate gradient)" << msg::endl;
        msg::indent();

        std::vector<DescentSearchHistoryLine2> historyline;

        //double j = functional->performance();
        double jj = std::real( functional->innerproduct()->product(functional->gradient(),functional->gradient()) );
        msg::print << "gradient norm 2 = " <<  jj  << msg::endl;
        msg::print << "eta2 = " << _eta2 << msg::endl;
        msg::print << "residual threshold = " << _eta2 * jj << msg::endl;

        //Initialisation
        ModelField x = functional->gradient();
        x.setValuesToZero();
        ModelField r = functional->gradient();
        double rr = jj;
        ModelField p = functional->gradient();
        p.oppose();
        unsigned int n=0;

        double pHp = std::real( functional->innerproduct()->product(p,functional->hessian(p)) );
        msg::print  <<  "pHp = " << pHp  << msg::endl;
        double alpha =  rr / pHp;
        msg::print  << "alpha = " << alpha << msg::endl;
        //0.5*alpha*alpha*pHp + alpha * functional->directional1(p) = -0.5*alpha*alpha*pHp;
        double pred_decrease = -0.5*alpha*alpha*pHp;
        msg::print  << "pred_decrease = " << pred_decrease << msg::endl;
        if(pHp <= 0. /*|| j+pred_decrease <= 0.*/)
        {
            msg::print << "--- Iteration #0 ----" << msg::endl;
            msg::indent();
            msg::print  << "pHp <= 0" /*|| j+pred_decrease <= 0 => alpha = 1"*/ << msg::endl;
            x=p;
            r = r+functional->hessian();
            rr = std::real( functional->innerproduct()->product(r,r) );
            msg::print << " rr = "<< rr << msg::endl;
            n++;
            historyline.emplace_back(0.,-1.,0.);
            msg::unindent();
        }
        else
        {
            while (true)
            {
                msg::print << "--- Iteration #" << n << " ----" << msg::endl;
                msg::indent();

                x=x+p*alpha;
                double rr_1=rr;
                r=r+functional->hessian()*alpha;
                rr = std::real( functional->innerproduct()->product(r,r) );
                msg::print << "rr = "<< rr << msg::endl;
                if(n>=_maxIteration || rr < _eta2 * jj)
                {
                    n++;
                    historyline.emplace_back(rr,alpha,0.);
                    msg::unindent();
                    break;
                }
                else
                {
                    double beta = rr / rr_1;
                    msg::print  << "beta = " << beta << msg::endl;
                    p = p*beta - r;
                    n++;
                    historyline.emplace_back(rr,alpha,beta);

                    pHp = std::real( functional->innerproduct()->product(p,functional->hessian(p)) );
                    msg::print  <<  "pHp = " << pHp  << msg::endl;
                    if(pHp<=0.)
                    {
                        n++;
                        historyline.emplace_back(0.,-1.,0.);
                        msg::unindent();
                        break;
                    }
                    alpha =  rr / pHp;
                    msg::print  << "alpha = " << alpha << msg::endl;
                    pred_decrease += 0.5*alpha*alpha*pHp + alpha * functional->directional1(p);
                    msg::print  << "pred_decrease = " << pred_decrease << msg::endl;
                    msg::unindent();
                    //if(j+pred_decrease<=0.){break;}
                }
            }
        }
        functional->setModelPerturbation(x);
        _history->emplace_back(rr < _eta2 * jj, _eta2, jj, n, historyline);
        msg::unindent();
    }
}
