// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_DESCENTSEARCH_NEWTONCONJUGATEGRADIENT
#define H_SPECIFIC_OPTIMIZATION_DESCENTSEARCH_NEWTONCONJUGATEGRADIENT

//GmshFEM Library
#include "GmshFem.h"
#include "CSVio.h"

//GmshFWI Library
#include "../../../common/optimization/descentsearch.h"

namespace newton_conjugategradient
{
    /*
    * class DescentSearchHistoryLine2
    */
    class DescentSearchHistoryLine2
    {
    public:
        const double residual;
        const double alpha;
        const double beta;

        DescentSearchHistoryLine2(double residual_init, double alpha_init, double beta_init) : residual(residual_init), alpha(alpha_init), beta(beta_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class DescentSearchHistoryLine
    */
    class DescentSearchHistoryLine
    {
    public:
        const bool success;
        const double eta2;
        const double gradientNorm2;
        const unsigned int iterations;
        const std::vector<DescentSearchHistoryLine2> history;

        DescentSearchHistoryLine(bool success_init, double eta2_init, double gradientNorm2_init, unsigned int iterations_init, const std::vector<DescentSearchHistoryLine2>& history_init) : success(success_init), eta2(eta2_init), gradientNorm2(gradientNorm2_init), iterations(iterations_init), history(history_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class DescentSearchHistory
    */
    class DescentSearchHistory final: public DescentSearchHistoryInterface, public std::vector<DescentSearchHistoryLine>
    {
    public:
        virtual void write(std::string filename) const;
    };

    /*
    * class DescentSearch
    */
    class DescentSearch final: public DescentSearchInterface
    {
    private:
        mutable double _eta2;
        unsigned int _maxIteration;
        DescentSearchHistory* const _history;
    public:
        DescentSearch(const gmshfem::common::GmshFem& gmshFem);
        ~DescentSearch() {delete _history;};

        virtual void operator()(FunctionalInterface* const functional) const;

        virtual const DescentSearchHistoryInterface* const history() const {return _history;};

        void eta(double eta) const {_eta2 = eta * eta;};
    };
};

#endif // H_SPECIFIC_OPTIMIZATION_DESCENTSEARCH_NEWTONCONJUGATEGRADIENT
