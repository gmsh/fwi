// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
//GmshFWI Library
#include "newton_steihaug_conjugategradient.h"

using namespace gmshfem;
using namespace gmshfem::common;

static const std::complex< double > im = std::complex< double >(0., 1.);

namespace newton_steihaug_conjugategradient
{
    /*
    * class DescentSearchHistoryLine2
    */
    void DescentSearchHistoryLine2::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Residual" << "Alpha" << "Beta" << "Direction norm" << csv::endl;
        }
        else
        {
            file << residual << alpha << beta << xx << csv::endl;
        }
    }

    /*
    * class DescentSearchHistoryLine
    */
    void DescentSearchHistoryLine::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Success" << "Eta squared" << "Gradient norm squared" << "Iteration number" << "Direction norm" << "Radius squared" << "Boundary reached" << csv::endl;
            file << "History" << csv::endl;
        }
        else
        {
            file << success << eta2 << gradientNorm2 << iterations << xx << radius2 << onBoundary << csv::endl;
            if(!history.empty())
            {
                history.begin()->write(file,true);
                for (auto it = history.begin(); it != history.end(); it++)
                {
                    it->write(file);
                }
            }
        }
    }

    /*
    * class DescentSearchHistory
    */
    void DescentSearchHistory::write(std::string filename) const
    {
        CSVio file(filename, ';', common::OpeningMode::NewFile);
        if(this->empty())
        {
            file << "Descent search history is empty." << csv::endl;
        }
        else
        {
            this->begin()->write(file,true);
            for (auto it = this->begin(); it != this->end(); it++)
            {
                file << "--" << csv::endl;
                it->write(file);
            }
        }
    }

    /*
    * class DescentSearch
    */
    DescentSearch::DescentSearch(const GmshFem& gmshFem) : _history( new DescentSearchHistory() )
    {
        if(!gmshFem.userDefinedParameter(_eta2, "descent_eta0"))
        {
            throw Exception("Initial value of forcing sequence (eta0) could not be found.");
        }
        _eta2 *= _eta2;

        if(!gmshFem.userDefinedParameter(_maxIteration, "descent_maxIteration"))
        {
            throw Exception("Maximum number of iterations for descent search (descent_maxIteration) could not be found.");
        }
    }
    double to_tau(double pp,double xp,double xx,double radius2)
    {
        double a = pp;
        double b = xp;
        double c = xx-radius2;
        double rho = b*b - a*c;
        return (-b + std::sqrt(rho))/a;
    }
    TrustRegionReturn DescentSearch::operator()(FunctionalInterface* const functional, double trustRegionRadius) const
    {
        double radius2 = trustRegionRadius * trustRegionRadius;
        msg::print <<"Search descent direction (Newton-Steihaug conjugate gradient)" << msg::endl;
        msg::indent();

        std::vector<DescentSearchHistoryLine2> historyline;

        double jj = std::real( functional->innerproduct()->product(functional->gradient(),functional->gradient()) );
        msg::print << "gradient norm 2 = " <<  jj  << msg::endl;

        msg::print << "eta2 = " << _eta2 << msg::endl;
        msg::print << "residual threshold = " << _eta2 * jj << msg::endl;
        msg::print << "radius2 = " << radius2 << msg::endl;

        //Initialisation
        ModelField x = functional->gradient();
        x.setValuesToZero();
        double xx = 0;
        ModelField r = functional->gradient();
        double rr = jj;
        ModelField p = functional->gradient();
        p.oppose();

        double pred_decrease = 0;
        bool onBoundary = false;
        unsigned int n=0;
        while (true)
        {
            msg::print << "--- Iteration #" << n << " ----" << msg::endl;
            msg::indent();

            double pHp = std::real( functional->innerproduct()->product(p,functional->hessian(p)) );
            //msg::print  <<  "pHp = " << pHp  << msg::endl;
            double xp = std::real( functional->innerproduct()->product(x,p) );
            //msg::print  <<  "xp = " << xp  << msg::endl;
            double pp = std::real( functional->innerproduct()->product(p,p) );
            //msg::print  <<  "pp = " << pp  << msg::endl;

            double alpha =  rr / pHp;
            msg::print  << "alpha = " << alpha << msg::endl;
            double x1x1 = xx + 2 * alpha * xp + alpha * alpha * pp;
            msg::print  << "xx (prediction) = " << x1x1 << msg::endl;
            if(
                (pHp<=0.) ||
                (x1x1 >= radius2)
            )
            {
                msg::print  << "Trust region contraint activated: xx (prediction) = " << x1x1 << " > radius2 or << alpha = " << alpha << " < 0" << msg::endl;
                double tau = to_tau(pp,xp,xx,radius2); //solve x1x1=radius2
                msg::print  << "tau = " << tau << msg::endl;
                pred_decrease += 0.5*tau*tau*pHp + tau*functional->directional1(p);
                msg::print  << "predicted decrease = " << pred_decrease << msg::endl;
                x = x+p*tau;
                xx = radius2;//=std::real( functional->innerproduct()->product(x,x) );
                onBoundary=true;
                //msg::print  << "xx = " << xx << msg::endl;
                n++;
                historyline.emplace_back(0.,tau,0.,xx);
                msg::unindent();
                break;
            }
            //0.5*alpha*alpha*pHp + alpha * functional->directional1(p) = -0.5*alpha*alpha*pHp;
            pred_decrease += -0.5*alpha*alpha*pHp;
            msg::print  << "pred_decrease = " << pred_decrease << msg::endl;

            x=x+p*alpha;
            xx = x1x1;
            //msg::print  << "xx = " << xx << msg::endl;
            double rr_1=rr;
            r=r+functional->hessian()*alpha;
            rr = std::real( functional->innerproduct()->product(r,r) );
            msg::print << "rr = "<< rr << msg::endl;

            if(n>=_maxIteration || rr < _eta2 * jj)
            {
                n++;
                historyline.emplace_back(rr,alpha,0.,xx);
                msg::unindent();
                break;
            }
            else
            {
                double beta = rr / rr_1;
                msg::print  << "beta = " << beta << msg::endl;
                p = p*beta - r;
                n++;
                historyline.emplace_back(rr,alpha,beta,xx);
                msg::unindent();
            }
        }

        functional->setModelPerturbation(x);
        _history->emplace_back(rr < _eta2 * jj, _eta2, jj, n, xx, radius2, onBoundary, historyline);
        msg::unindent();
        TrustRegionReturn out = {pred_decrease,std::sqrt(xx),onBoundary};
        return out;
    }

    double DescentSearch::predicted_performance(const ModelField& p, FunctionalInterface* const functional) const
    {
        return functional->performance() + functional->directional1(p) + 0.5 * functional->directional2(p,p);
    }
}
