// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_DESCENTSEARCH_NEWTONSTEIHAUGCONJUGATEGRADIENT
#define H_SPECIFIC_OPTIMIZATION_DESCENTSEARCH_NEWTONSTEIHAUGCONJUGATEGRADIENT

//GmshFEM Library
#include "GmshFem.h"
#include "CSVio.h"

//GmshFWI Library
#include "../../../common/optimization/descentsearch.h"

namespace newton_steihaug_conjugategradient
{
    /*
    * class DescentSearchHistoryLine2
    */
    class DescentSearchHistoryLine2
    {
    public:
        const double residual;
        const double alpha;
        const double beta;
        const double xx;


        DescentSearchHistoryLine2(double residual_init, double alpha_init, double beta_init, double xx_init) : residual(residual_init), alpha(alpha_init), beta(beta_init), xx(xx_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class DescentSearchHistoryLine
    */
    class DescentSearchHistoryLine
    {
    public:
        const bool success;
        const double eta2;
        const double gradientNorm2;
        const unsigned int iterations;

        const double xx;
        const double radius2;
        const bool onBoundary;

        const std::vector<DescentSearchHistoryLine2> history;

        DescentSearchHistoryLine(bool success_init, double eta2_init, double gradientNorm2_init, unsigned int iterations_init, double xx_init, double radius2_init, bool onBoundary_init, const std::vector<DescentSearchHistoryLine2>& history_init) : success(success_init), eta2(eta2_init), gradientNorm2(gradientNorm2_init), iterations(iterations_init), xx(xx_init), radius2(radius2_init), onBoundary(onBoundary_init), history(history_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class DescentSearchHistory
    */
    class DescentSearchHistory final: public DescentSearchHistoryInterface, public std::vector<DescentSearchHistoryLine>
    {
    public:
        virtual void write(std::string filename) const;
    };

    /*
    * class DescentSearch
    */
    class DescentSearch final: public TrustRegionDescentSearchInterface
    {
    private:
        double _eta2;
        unsigned int _maxIteration;
        DescentSearchHistory* const _history;
    public:
        DescentSearch(const gmshfem::common::GmshFem& gmshFem);
        ~DescentSearch() {delete _history;};

        virtual TrustRegionReturn operator()(FunctionalInterface* const functional, double trustRegionRadius) const;
        virtual double predicted_performance(const ModelField& p, FunctionalInterface* const functional) const override;

        virtual const DescentSearchHistoryInterface* const history() const {return _history;};
    };
};

#endif // H_SPECIFIC_OPTIMIZATION_DESCENTSEARCH_NEWTONSTEIHAUGCONJUGATEGRADIENT
