// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_WAVE_NEWGLOBALMINIMUMSEARCH
#define H_SPECIFIC_WAVE_NEWGLOBALMINIMUMSEARCH

//Standard Library
#include <string>

//GmshFem Library
#include "GmshFem.h"
#include "Exception.h"

//FWI Library
#include "sequential.h"

template<Physic T_Physic>
GlobalMinimumSearchInterface<T_Physic>* newGlobalMinimumSearch(const std::string& name, const ConfigurationInterface* const config, const gmshfem::common::GmshFem& gmshFem, const std::string& suffix = "")
{
    std::string minimumsearch;
    if(!gmshFem.userDefinedParameter(minimumsearch, "globalminimumsearch"))
    {
      throw gmshfem::common::Exception("Global minimum search type could not be found.");
    }
    if(minimumsearch=="sequential" || minimumsearch=="mono"){return new sequential::GlobalMinimumSearch<T_Physic>(name, config, gmshFem, suffix);}
    else
    {
        throw gmshfem::common::Exception("Global minimum search " + minimumsearch + " is not valid.");
        return nullptr;
    }
};
#endif //H_SPECIFIC_WAVE_NEWGLOBALMINIMUMSEARCH
