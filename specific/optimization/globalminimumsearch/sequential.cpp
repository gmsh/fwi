// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "Exception.h"
#include "Message.h"

//GmshFWI Library
#include "sequential.h"
#include "../../data/objective/newObjective.h"
#include "../../model/innerproduct/newInnerProduct.h"
#include "../../model/regularization/newRegularization.h"
#include "../../../common/statefunctional.h"
#include "../../optimization/descentsearch/newDescentSearch.h"
#include "../../optimization/linesearch/newLineSearch.h"
#include "../../optimization/localminimumsearch/newLocalMinimumSearch.h"

// STL
#include <memory>

using namespace gmshfem;
using namespace gmshfem::common;
using std::unique_ptr;
using std::make_unique;

namespace sequential
{
    /*
    * class GlobalMinimumSearchHistoryLine2
    */
    void GlobalMinimumSearchHistoryLine2::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Performance" << "Relative decrease" << "Sequential parameter" << "Objective" << "Regularization" << csv::endl;
        }
        else
        {
            file << performance << relDecrease << sequential_parameter << perf_obj << perf_reg << csv::endl;
        }
    }

    /*
    * class GlobalMinimumSearchHistoryLine
    */
    void GlobalMinimumSearchHistoryLine::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Success" << "Scale number" << "Parameter type" << csv::endl;
            file << "History" << csv::endl;
        }
        else
        {
            file << success << nseq << type << csv::endl;
            if(!history.empty())
            {
                history.begin()->write(file,true);
                for (auto it = history.begin(); it != history.end(); it++)
                {
                    it->write(file);
                }
            }
        }
    }

    /*
    * class GlobalMinimumSearchHistory
    */
    void GlobalMinimumSearchHistory::write(std::string filename) const
    {
        CSVio file(filename, ';', common::OpeningMode::NewFile);
        if(this->empty())
        {
            file << "Global minimum search history is empty." << csv::endl;
        }
        else
        {
            this->begin()->write(file,true);
            for (auto it = this->begin(); it != this->end(); it++)
            {
                file << "--" << csv::endl;
                it->write(file);
            }
        }
    }

    /*
    * class GlobalMinimumSearch
    */
    template<Physic T_Physic>
    GlobalMinimumSearch<T_Physic>::GlobalMinimumSearch(const std::string& name, const ConfigurationInterface* const config, const GmshFem& gmshFem, const std::string& suffix) : GlobalMinimumSearchInterface<T_Physic>::GlobalMinimumSearchInterface(name,config), _history( new GlobalMinimumSearchHistory() ), _gmshFem(gmshFem), _suffix(suffix)
    {
        std::string minimumsearch;
        gmshFem.userDefinedParameter(minimumsearch, "globalminimumsearch");
        if(minimumsearch=="mono")
        {
            _type = "none";
            _n_seq = 1;
            _seq_parameters.resize(_n_seq);
            _seq_parameters[0]=0.;
        }
        else
        {
            _type = "innerproduct_strength";
            if(!(
                gmshFem.userDefinedParameter(_type, "globalminimum_type") ||
                gmshFem.userDefinedParameter(_type, "globalminimum_type"+suffix)
            )
            )
            {
                msg::warning << "Sequential globalminimum parameter type could not be found. Default is used (innerproduct_strength)." << msg::endl;
            }
            if(!(
                gmshFem.userDefinedParameter(_n_seq, "globalminimum_n_seq") ||
                gmshFem.userDefinedParameter(_n_seq, "globalminimum_n_scale"+suffix)
            )
            )
            {
                throw Exception("Number of sequence for group "+suffix+" could not be found.");
            }
            _seq_parameters.resize(_n_seq);
            for (unsigned int sq = 0; sq < _n_seq; sq++)
            {
                std::string suffix1 = "s"+std::to_string(sq);
                std::string root = "globalminimum_sequential_parameter";
                if(!(
                    _gmshFem.userDefinedParameter(_seq_parameters[sq],root+suffix1)
                    ||
                    _gmshFem.userDefinedParameter(_seq_parameters[sq],root+suffix+suffix1)
                ))
                {
                    throw Exception("Sequential parameter "+suffix1+" could not be found.");
                }
            }
        }
    }

    template<Physic T_Physic>
    void GlobalMinimumSearch<T_Physic>::operator()(ModelField* const m, const Data<T_Physic>& d0, const std::vector<EquationInterface<T_Physic>*>& equation) const
    {
        msg::print << "Search global minimum (sequential)" << msg::endl;
        msg::indent();

        unsigned int n_freq = d0.nf();
        if(equation.size() != n_freq)
        {
            throw Exception("Data and equations do not have the same size.");
        }

        auto objective = newObjective<T_Physic>(d0,_config,_gmshFem,_suffix);
        auto const innerproduct = newInnerProduct(_config,*m,_gmshFem,_suffix);
        RegularizationInterface* const regularization = newRegularization(_config,*m,_gmshFem,_suffix);
        auto statefunctional = make_unique<StateFunctional<T_Physic>>(_config,innerproduct.get(),regularization,d0.frequency(),equation,objective.get());
        statefunctional->setModel(*m);

        LocalMinimumSearchInterface* const minimumsearch = newLocalMinimumSearch(_gmshFem);
        DescentSearchInterface* const descentsearch = newDescentSearch(_gmshFem);
        LineSearchInterface* const linesearch = (descentsearch->need_steplength() ? newLineSearch(_gmshFem) :  nullptr );


        std::vector<GlobalMinimumSearchHistoryLine2> historyline;
        double js_1 =0.;
        double js = statefunctional->performance();
        historyline.emplace_back(js,0.,0.,statefunctional->performance_objective(),statefunctional->performance_regularization());
        double rel_djs = 0.;
        bool success = false;
        unsigned int sq = 0;
        for (sq = 0; sq < _n_seq; sq++)
        {
            std::string suffix1 = std::to_string(sq);
            if (_type != "none")
            {
                msg::print << "--- Sequence #" << suffix1 << ": " << _seq_parameters[sq] << " --- " << msg::endl;
                msg::indent();
            }

            if (_type == "none"){}
            else if(_type=="innerproduct_strength")
            {
                innerproduct->scale(_seq_parameters[sq]);
                statefunctional->innerproductIsObsolete();
            }
            else if(_type=="regularization_strength")
            {
                regularization->lambda(_seq_parameters[sq]);
                statefunctional->regularizationIsObsolete();
            }
            minimumsearch->name(_name+"g"+_suffix+"s"+suffix1);
            js_1=js;
            (*minimumsearch)(m, statefunctional.get(), descentsearch, linesearch);
            js = statefunctional->performance();
            m->write(_name+"_mg"+_suffix+"s"+suffix1);
            //statefunctional->write_data(Type::FS,_name+"_dg"+_suffix+"s"+suffix1,"pos");
            rel_djs = 2. * (js_1-js) / (js_1+js);
            msg::print << "relative decrease = " <<  rel_djs  << msg::endl;
            historyline.emplace_back(js,rel_djs,_seq_parameters[sq],statefunctional->performance_objective(),statefunctional->performance_regularization());
            if (_type != "none"){msg::unindent();}
        }
        success=true;
        _history->emplace_back(success,sq,historyline);

        minimumsearch->history()->write(_name+"_localminimum_historyg" + _suffix);
        descentsearch->history()->write(_name+"_descent_historyg" + _suffix);
        if(linesearch != nullptr)
        {
            linesearch->history()->write(_name+"_linesearch_historyg" + _suffix);
        }

        delete minimumsearch;
        delete descentsearch;
        delete linesearch;

        delete regularization;

        msg::unindent();
    }
    template class GlobalMinimumSearch<Physic::acoustic>;
    template class GlobalMinimumSearch<Physic::electromagnetic>;
    template class GlobalMinimumSearch<Physic::elastodynamic>;
}
