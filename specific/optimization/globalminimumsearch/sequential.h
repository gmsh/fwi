// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_GLOBALMINIMUMSEARCH_SEQUENTIAL
#define H_SPECIFIC_OPTIMIZATION_GLOBALMINIMUMSEARCH_SEQUENTIAL

//GmshFEM Library
#include "GmshFem.h"
#include "CSVio.h"

//GmshFWI Library
#include "../../../common/optimization/globalminimumsearch.h"

namespace sequential
{
    /*
    * class GlobalMinimumSearchHistoryLine2
    */
    class GlobalMinimumSearchHistoryLine2
    {
    public:
        const double performance;
        const double relDecrease;
        const double sequential_parameter;
        const double perf_obj;
        const double perf_reg;

        GlobalMinimumSearchHistoryLine2(double performance_init, double relDecrease_init, double sequential_parameter_init, double perf_obj_init, double perf_reg_init) : performance(performance_init), relDecrease(relDecrease_init), sequential_parameter(sequential_parameter_init), perf_obj(perf_obj_init), perf_reg(perf_reg_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class GlobalMinimumSearchHistoryLine
    */
    class GlobalMinimumSearchHistoryLine
    {
    public:
        const bool success;
        const std::string type;
        const unsigned int nseq;
        const std::vector<GlobalMinimumSearchHistoryLine2> history;

        GlobalMinimumSearchHistoryLine(bool success_init, unsigned int nseq_init, const std::vector<GlobalMinimumSearchHistoryLine2>& history_init) : success(success_init), nseq(nseq_init), history(history_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class GlobalMinimumSearchHistory
    */
    class GlobalMinimumSearchHistory final: public GlobalMinimumSearchHistoryInterface, public std::vector<GlobalMinimumSearchHistoryLine>
    {
    public:
        virtual void write(std::string filename) const;
    };

    /*
    * class GlobalMinimumSearch
    */
    template<Physic T_Physic>
    class GlobalMinimumSearch final: public GlobalMinimumSearchInterface<T_Physic>
    {
    private:
        using GlobalMinimumSearchInterface<T_Physic>::_name;
        using GlobalMinimumSearchInterface<T_Physic>::_config;

        unsigned int _n_seq;
        std::vector<double> _seq_parameters;
        GlobalMinimumSearchHistory* const _history;

        const gmshfem::common::GmshFem& _gmshFem;
        std::string _type;
        std::string _suffix;
    public:
        GlobalMinimumSearch(const std::string& name, const ConfigurationInterface* const config, const gmshfem::common::GmshFem& gmshFem, const std::string& suffix);
        ~GlobalMinimumSearch() {delete _history;};

        virtual void operator()(ModelField* const m, const Data<T_Physic>& d0, const std::vector<EquationInterface<T_Physic>*>& equation) const;

        virtual const GlobalMinimumSearchHistoryInterface* const history() const {return _history;};
    };
};

#endif // H_SPECIFIC_OPTIMIZATION_GLOBALMINIMUMSEARCH_SEQUENTIAL
