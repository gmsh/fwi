// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_LINESEARCH_CONSTANT
#define H_SPECIFIC_OPTIMIZATION_LINESEARCH_CONSTANT

//GmshFEM Library
#include "GmshFem.h"
#include "CSVio.h"

//GmshFWI Library
#include "../../../common/optimization/linesearch.h"

namespace constant
{
    /*
    * class LineSearchHistoryLine
    */
    class LineSearchHistoryLine
    {
    public:
        const double performance;

        LineSearchHistoryLine(double performance_init) : performance(performance_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class LineSearchHistory
    */
    class LineSearchHistory final: public LineSearchHistoryInterface, public std::vector<LineSearchHistoryLine>
    {
    public:
        virtual void write(std::string filename) const;
    };

    /*
    * class LineSearch
    */
    class LineSearch final: public LineSearchInterface
    {
    private:
        double _a;
        LineSearchHistory* const _history;
    public:
        LineSearch(const gmshfem::common::GmshFem& gmshFem, std::string suffix="");
        ~LineSearch() {delete _history;};

        virtual double operator()(FunctionalInterface* const functional) const;

        virtual const LineSearchHistoryInterface* const history() const {return _history;};
    };
};

#endif // H_SPECIFIC_OPTIMIZATION_LINESEARCH_CONSTANT
