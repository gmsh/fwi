// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
//GmshFWI Library
#include "newLineSearch.h"
#include "independantComponent.h"

using namespace gmshfem;
using namespace gmshfem::common;

namespace independantcomponent
{
    /*
    * class LineSearchHistory
    */
    LineSearchHistory::LineSearchHistory(const std::vector<const LineSearchInterface*>& lsv) : std::vector<const LineSearchHistoryInterface*>(lsv.size())
    {
        for (unsigned int c = 0; c < lsv.size(); c++)
        {
            (*this)[c] = lsv[c]->history();
        }
    }
    void LineSearchHistory::write(std::string filename) const
    {
        for (unsigned int c = 0; c < size(); c++)
        {
            (*this)[c]->write(filename+"c"+std::to_string(c));
        }
    }


    /*
    * class LineSearch
    */
    LineSearch::LineSearch(const GmshFem& gmshFem, std::string suffix)
    {
        msg::warning << "Suffix is ignored for linesearch_component." << msg::endl;
        if(!gmshFem.userDefinedParameter(_n_dir, "linesearch_n_dir"))
        {
          throw gmshfem::common::Exception("Line search number of directions could not be found.");
        }
        _components.resize(_n_dir);
        for (unsigned int c = 0; c < _n_dir; c++)
        {
            _components[c].resize(1);
            if(!(
                gmshFem.userDefinedParameter(_components[c][0], "linesearch_component"+std::to_string(c))
            ))
            {
              throw gmshfem::common::Exception("Line search components for direction" + std::to_string(c) + "could not be found.");
            }
            _lsv.emplace_back(newLineSearch(to_linesearch(gmshFem,std::to_string(c)),gmshFem,std::to_string(c)));
        }
        _lsh = new LineSearchHistory(_lsv);
    }
    
    LineSearch::~LineSearch()
    {
        for (auto it = _lsv.begin(); it != _lsv.end(); it++)
        {
            delete *it;
        }
        delete _lsh;
    }

    double LineSearch::operator()(FunctionalInterface* const functional) const
    {
        msg::print << "Linesearch (independant component)" << msg::endl;
        msg::indent();

        ModelField m0 = functional->m();
        ModelField p0 = functional->dm();
        ModelField ma = m0;

        for (unsigned int c = 0; c < _n_dir; c++)
        {
            msg::print << "Linesearch component #" << c << msg::endl;
            msg::indent();

            ModelField p = p0;
            p.projection(_components[c]);
            functional->setModelPerturbation(p);
            double a = (*(_lsv[c]))(functional);

            ma = ma + p*a;
            functional->setModel(m0);

            msg::unindent();
        }
        functional->setModel(ma);

        msg::unindent();

        return 0.;
    }
}
