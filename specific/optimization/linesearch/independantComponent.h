// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_LINESEARCH_INDEPENDANTCOMPONENT
#define H_SPECIFIC_OPTIMIZATION_LINESEARCH_INDEPENDANTCOMPONENT

//GmshFEM Library
#include "GmshFem.h"

//GmshFWI Library
#include "../../../common/optimization/linesearch.h"

namespace independantcomponent
{
    /*
    * class LineSearchHistory
    */
    class LineSearchHistory final: public LineSearchHistoryInterface, public std::vector<const LineSearchHistoryInterface*>
    {
    public:
        LineSearchHistory(const std::vector<const LineSearchInterface*>& lsv);
        virtual void write(std::string filename) const;
    };
    /*
    * class LineSearch
    */
    class LineSearch final: public LineSearchInterface
    {
    private:
        unsigned int _n_dir;
        std::vector<std::vector<unsigned int>> _components;
        std::vector<const LineSearchInterface*> _lsv;

        const LineSearchHistory* _lsh;
    public:
        LineSearch(const gmshfem::common::GmshFem& gmshFem, std::string suffix="");
        ~LineSearch();

        virtual double operator()(FunctionalInterface* const functional) const;

        virtual const LineSearchHistoryInterface* const history() const {return _lsh;};
    };
};

#endif // H_SPECIFIC_OPTIMIZATION_LINESEARCH_INDEPENDANTCOMPONENT
