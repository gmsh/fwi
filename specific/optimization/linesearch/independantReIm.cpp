// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
//#include "FieldRoutines.h"
//#include "Exception.h"

//GmshFWI Library
#include "newLineSearch.h"
#include "independantReIm.h"

using namespace gmshfem;
using namespace gmshfem::common;

//static const std::complex< double > im = std::complex< double >(0., 1.);

namespace independantreim
{
    /*
    * class LineSearchHistory
    */
    void LineSearchHistory::write(std::string filename) const
    {
        _lshRe->write(filename+"_re");
        _lshIm->write(filename+"_im");
    }

    /*
    * class LineSearch
    */
    LineSearch::LineSearch(const GmshFem& gmshFem, std::string suffix) : _lsRe(newLineSearch(to_linesearch(gmshFem,"_re"+suffix),gmshFem,"_re"+suffix)), _lsIm(newLineSearch(to_linesearch(gmshFem,"_im"+suffix),gmshFem,"_im"+suffix)),
    _lsh(new LineSearchHistory(_lsRe->history(),_lsIm->history()))
    {}

    double LineSearch::operator()(FunctionalInterface* const functional) const
    {
        msg::print << "Linesearch (independant real/imaginary)" << msg::endl;
        msg::indent();

        ModelField m = functional->m();

        ModelField pre = functional->dm();
        pre.realify();
        ModelField pim = functional->dm();
        pim.imaginarify();

        functional->setModelPerturbation(pre);
        double are = (*_lsRe)(functional);

        functional->setModel(m);

        functional->setModelPerturbation(pim);
        double aim = (*_lsIm)(functional);

        functional->setModel(m+pre*are+pim*aim);

        msg::unindent();

        return 0.;
    }
}
