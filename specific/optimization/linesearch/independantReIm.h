// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_LINESEARCH_INDEPENDANTREIM
#define H_SPECIFIC_OPTIMIZATION_LINESEARCH_INDEPENDANTREIM

//GmshFEM Library
#include "GmshFem.h"

//GmshFWI Library
#include "../../../common/optimization/linesearch.h"

namespace independantreim
{
    /*
    * class LineSearchHistory
    */
    class LineSearchHistory final: public LineSearchHistoryInterface
    {
    private:
        const LineSearchHistoryInterface* const _lshRe;
        const LineSearchHistoryInterface* const _lshIm;
    public:
        LineSearchHistory(const LineSearchHistoryInterface* const lshRe, const LineSearchHistoryInterface* const lshIm) : _lshRe(lshRe), _lshIm(lshIm) {};
        virtual void write(std::string filename) const;
    };
    /*
    * class LineSearch
    */
    class LineSearch final: public LineSearchInterface
    {
    private:
        LineSearchInterface* const _lsRe;
        LineSearchInterface* const _lsIm;

        const LineSearchHistory* const _lsh;
    public:
        LineSearch(const gmshfem::common::GmshFem& gmshFem, std::string suffix="");
        ~LineSearch() {delete _lsRe;delete _lsIm;delete _lsh;};

        virtual double operator()(FunctionalInterface* const functional) const;

        virtual const LineSearchHistoryInterface* const history() const {return _lsh;};
    };
};

#endif // H_SPECIFIC_OPTIMIZATION_LINESEARCH_INDEPENDANTREIM
