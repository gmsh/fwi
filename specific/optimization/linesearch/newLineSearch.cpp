// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFem Library
#include "Exception.h"

//GmshFWI Library
#include "newLineSearch.h"
#include "wolfelike.h"
#include "secondorder.h"
#include "constant.h"
#include "independantReIm.h"
#include "independantComponent.h"

LineSearchInterface* newLineSearch(const std::string& linesearch,const gmshfem::common::GmshFem& gmshFem,std::string suffix)
{
    if(linesearch=="secondorder"){return new secondorder::LineSearch(gmshFem,suffix);}
    else if(linesearch=="constant"){return new constant::LineSearch(gmshFem,suffix);}
    else if(linesearch=="independantReIm"){return new independantreim::LineSearch(gmshFem,suffix);}
    else if(linesearch=="independantComponent"){return new independantcomponent::LineSearch(gmshFem,suffix);}
    else if(linesearch=="goldstein"){return new wolfelike::goldstein::LineSearch(gmshFem,suffix);}
    else if(linesearch=="armijo"){return new wolfelike::armijo::LineSearch(gmshFem,suffix);}
    else if(linesearch=="strongwolfe"){return new wolfelike::strongwolfe::LineSearch(gmshFem,suffix);}
    else
    {
        throw gmshfem::common::Exception("Line search " + linesearch + " is not valid.");
        return nullptr;
    }
}

std::string to_linesearch(const gmshfem::common::GmshFem& gmshFem,std::string suffix)
{
    std::string linesearch;
    if(!gmshFem.userDefinedParameter(linesearch, "linesearch"+suffix))
    {
      throw gmshfem::common::Exception("Line search type could not be found.");
    }
    return linesearch;
}

LineSearchInterface* newLineSearch(const gmshfem::common::GmshFem& gmshFem)
{
    return newLineSearch(to_linesearch(gmshFem),gmshFem);
}
