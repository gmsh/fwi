// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_NEWLINESEARCH
#define H_SPECIFIC_OPTIMIZATION_NEWLINESEARCH

//Standard Library
#include <string>

//GmshFem Library
#include "GmshFem.h"

//GmshFWI Library
#include "../../../common/optimization/linesearch.h"
LineSearchInterface* newLineSearch(const std::string& linesearch,const gmshfem::common::GmshFem& gmshFem,std::string suffix="");

std::string to_linesearch(const gmshfem::common::GmshFem& gmshFem,std::string suffix="");

LineSearchInterface* newLineSearch(const gmshfem::common::GmshFem& gmshFem);

#endif //H_SPECIFIC_OPTIMIZATION_NEWLINESEARCH
