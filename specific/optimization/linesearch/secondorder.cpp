// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "FieldRoutines.h"
#include "Exception.h"

//GmshFWI Library
#include "secondorder.h"

using namespace gmshfem;
using namespace gmshfem::common;

static const std::complex< double > im = std::complex< double >(0., 1.);

namespace secondorder
{
    /*
    * class LineSearchHistoryLine
    */
    void LineSearchHistoryLine::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Performance" << "a" << csv::endl;
        }
        else
        {
            file << performance << a << csv::endl;
        }
    }

    /*
    * class LineSearchHistory
    */
    void LineSearchHistory::write(std::string filename) const
    {
        CSVio file(filename, ';', common::OpeningMode::NewFile);
        if(this->empty())
        {
            file << "Line search history is empty." << csv::endl;
        }
        else
        {
            this->begin()->write(file,true);
            for (auto it = this->begin(); it != this->end(); it++)
            {
                it->write(file);
            }
        }
    }

    /*
    * class LineSearch
    */
    LineSearch::LineSearch(const GmshFem& gmshFem, std::string suffix) : _history( new LineSearchHistory() )
    {
        if(!(
            gmshFem.userDefinedParameter(_aMax, "aMax"+suffix)
        ))
        {
            throw Exception("A seconed order linesearch parameter could not be found (aMax).");
        }
    }

    double LineSearch::operator()(FunctionalInterface* const functional) const
    {
        msg::print << "Linesearch (second order)" << msg::endl;
        msg::indent();

        ModelField m = functional->m();
        ModelField p = functional->dm();

        double j = functional->performance();
        double dj = functional->directional1(p);
        double d2j = functional->directional2(p,p);
        double a = std::min( - dj / d2j, _aMax );

        msg::print << "a = " << a << msg::endl;
        functional->setModel( m+p*a );
        _history->emplace_back(j,a);

        msg::unindent();

        return a;
    }
}
