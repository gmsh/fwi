// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_LINESEARCH_SECONDORDER
#define H_SPECIFIC_OPTIMIZATION_LINESEARCH_SECONDORDER

//GmshFEM Library
#include "GmshFem.h"
#include "CSVio.h"

//GmshFWI Library
#include "../../../common/optimization/linesearch.h"

namespace secondorder
{
    /*
    * class LineSearchHistoryLine
    */
    class LineSearchHistoryLine
    {
    public:
        const double performance;
        const double a;

        LineSearchHistoryLine(double performance_init, double a_init) : performance(performance_init), a(a_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class LineSearchHistory
    */
    class LineSearchHistory final: public LineSearchHistoryInterface, public std::vector<LineSearchHistoryLine>
    {
    public:
        virtual void write(std::string filename) const;
    };

    /*
    * class LineSearch
    */
    class LineSearch final: public LineSearchInterface
    {
    private:
        double _aMax;
        LineSearchHistory* const _history;
    public:
        LineSearch(const gmshfem::common::GmshFem& gmshFem, std::string suffix="");
        ~LineSearch() {delete _history;};

        virtual double operator()(FunctionalInterface* const functional) const;

        virtual const LineSearchHistoryInterface* const history() const {return _history;};
    };
};

#endif // H_SPECIFIC_OPTIMIZATION_LINESEARCH_SECONDORDER
