// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
#include <limits>

//GmshFEM Library
#include "Exception.h"
#include "FieldRoutines.h"

//GmshFWI Library
#include "wolfelike.h"

using namespace gmshfem;
using namespace gmshfem::common;

static const std::complex< double > im = std::complex< double >(0., 1.);

namespace wolfelike
{
    /*
    * class LineSearchHistoryLine
    */
    void LineSearchHistoryLine::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Performance (initial)" << "Performance (final)" << "Decrease" << "a0" << "aN" << "Call" << csv::endl;
        }
        else
        {
            file << performance0 << performanceA << decrease << a0 << aN << N << csv::endl;
        }
    }

    /*
    * class LineSearchHistory
    */
    void LineSearchHistory::write(std::string filename) const
    {
        CSVio file(filename, ';', common::OpeningMode::NewFile);
        if(this->empty())
        {
            file << "Line search history is empty." << csv::endl;
        }
        else
        {
            this->begin()->write(file,true);
            for (auto it = this->begin(); it != this->end(); it++)
            {
                it->write(file);
            }
        }
    }

    namespace goldstein
    {
        /*
        * class LineSearch
        */
        LineSearch::LineSearch(const GmshFem& gmshFem, std::string suffix) : _a0(-1.), _history( new LineSearchHistory() )
        {
            if(!(
                gmshFem.userDefinedParameter(_rho, "rho"+suffix) &&
                gmshFem.userDefinedParameter(_c1, "c1"+suffix) &&
                gmshFem.userDefinedParameter(_c2, "c2"+suffix)
            ))
            {
                throw Exception("A goldstein linesearch parameter could not be found.");
            }
            gmshFem.userDefinedParameter(_a0, "a0"+suffix);
        }

        double LineSearch::operator()(FunctionalInterface* const functional) const
        {
            msg::print << "Linesearch (goldstein)" << msg::endl;
            msg::indent();

            ModelField m = functional->m();
            ModelField p = functional->dm();

            double j = functional->performance();
            double dj = functional->directional1(p);
            msg::print << "dj = " << dj << msg::endl;

            double a0;
            if(_a0<=0.)
            {
                if (_history->size()>0)
                {
                    double decrease_1 = _history->back().decrease;
                    a0= 2.*decrease_1/dj; //Fletcher's initialization
                }
                else
                {
                    a0=1.;
                }
            }
            else
            {
                a0=_a0;
            }

            unsigned int i=0;
            double ja = 0.;
            double a=a0;
            double a_min=0.;
            double a_max=0.;
            while (true)
            {
                msg::print << "a = " << a << "\t" << "a_min = " << a_min << "\t" << "a_max = " << a_max <<  msg::endl;
                ja = functional->performance<ModelField>(m+p*a);
                msg::print << "ja = " << ja << msg::endl;
                double ared = j-ja;
                msg::print << "actual reduction:  j-ja = " << ared << msg::endl;
                double pred = -a*dj;
                msg::print << "predicted reduction:  -adj = " << pred << msg::endl;
                msg::print << "weighted predicted reduction: _c1*pred = " << _c1*pred << "\t" << "_c2*pred = " << _c2*pred << msg::endl;
                if( (ared > _c1*pred) && (ared < _c2*pred) ){break;}
                else if ( ared <= _c1*pred )
                {
                    a_max = a;
                    a = a_min + _rho * (a_max-a_min);
                }
                else if( ared >= _c2*pred )
                {
                    a_min = a;
                    if(a_max>0.){a = a_min + _rho * (a_max-a_min);}
                    else{a/=_rho;}
                }
                else
                {
                    throw Exception("Goldstein linesearch is broken.");
                }
                i++;
            }
            _history->emplace_back(j,ja,ja-j,a0,a,i);

            msg::unindent();

            return a;
        }
    }

    namespace armijo
    {
        /*
        * class LineSearch
        */
        LineSearch::LineSearch(const GmshFem& gmshFem, std::string suffix) : _a0(-1.), _history( new LineSearchHistory() )
        {
            if(!(
                gmshFem.userDefinedParameter(_rho, "rho"+suffix) &&
                gmshFem.userDefinedParameter(_c1, "c1"+suffix)
            ))
            {
                throw Exception("A armijo linesearch parameter could not be found.");
            }
            gmshFem.userDefinedParameter(_a0, "a0"+suffix);
        }

        double LineSearch::operator()(FunctionalInterface* const functional) const
        {
            msg::print << "Linesearch (armijo)" << msg::endl;
            msg::indent();

            ModelField m = functional->m();
            ModelField p = functional->dm();

            double j = functional->performance();
            double dj = functional->directional1(p);
            msg::print << "dj = " << dj << msg::endl;

            double a0;
            if(_a0<=0.)
            {
                if (_history->size()>0)
                {
                    double decrease_1 = _history->back().decrease;
                    a0= 2.*decrease_1/dj; //Fletcher's initialization
                }
                else
                {
                    a0=1.;
                }
            }
            else
            {
                a0=_a0;
            }

            unsigned int i=0;
            double ja = 0.;
            double a=a0;
            while (true)
            {
                msg::print << "a = " << a <<  msg::endl;
                ja = functional->performance<ModelField>(m+p*a);
                msg::print << "ja = " << ja << msg::endl;
                double ared = j-ja;
                msg::print << "actual reduction:  j-ja = " << ared << msg::endl;
                double pred = -a*dj;
                msg::print << "predicted reduction:  -adj = " << pred << msg::endl;
                msg::print << "weighted predicted reduction: _c1*pred = " << _c1*pred << msg::endl;
                if( (ared > _c1*pred) ){break;}
                else{ a *= _rho; }
                i++;
            }
            _history->emplace_back(j,ja,ja-j,a0,a,i);

            msg::unindent();

            return a;
        }
    }

    namespace strongwolfe
    {
        /*
        * class LineSearch
        */
        LineSearch::LineSearch(const GmshFem& gmshFem, std::string suffix) : _a0(-1.), _maxIt(8), _history( new LineSearchHistory() )
        {
            if(!(
                gmshFem.userDefinedParameter(_rho, "rho"+suffix) &&
                gmshFem.userDefinedParameter(_c1, "c1"+suffix) &&
                gmshFem.userDefinedParameter(_c2, "c2"+suffix)
            ))
            {
                throw Exception("A goldstein linesearch parameter could not be found.");
            }
            gmshFem.userDefinedParameter(_a0, "a0"+suffix);
            if(!gmshFem.userDefinedParameter(_maxIt, "linesearch_maxIteration"+suffix))
            {
                msg::warning << "A strongwolfe linesearch parameter (maxIteration) could not be found. Default value (8) is used." << msg::endl;
            }
        }

        double LineSearch::operator()(FunctionalInterface* const functional) const
        {
            msg::print << "Linesearch (strong wolfe)" << msg::endl;
            msg::indent();

            ModelField m = functional->m();
            ModelField p = functional->dm();

            double j = functional->performance();
            double dj = functional->directional1(p);
            msg::print << "dj = " << dj << msg::endl;


            //Initialization
            double a0;
            if(_a0<=0.)
            {
                if (_history->size()>0)
                {
                    double decrease_1 = _history->back().decrease;
                    a0= 2.*decrease_1/dj; //Fletcher's initialization
                }
                else
                {
                    a0=1.;
                }
            }
            else
            {
                a0=_a0;
            }


            unsigned int call = 0;

            //Bracketing
            msg::print << "Bracketing" << msg::endl;
            msg::indent();

            double ja = 0.;
            double ja_1 = j;
            double ja_lo = 0.;
            double a=a0;
            double a_1=0.;
            double a_lo=0.;
            double a_hi=0.;
            for (unsigned int i = 0; true; i++)
            {
                msg::print << "a = " << a <<  msg::endl;
                ja = functional->performance<ModelField>(m+p*a);
                call++;
                msg::print << "ja = " << ja << msg::endl;
                double ared = j-ja;
                msg::print << "actual reduction:  j-ja = " << ared << msg::endl;
                double pred = -a*dj;
                msg::print << "predicted reduction:  -adj = " << pred << msg::endl;
                msg::print << "weighted predicted reduction: _c1*pred = " << _c1*pred << msg::endl;

                if( (ared < _c1*pred) || (ja >= ja_1) )
                {
                    a_lo = a_1;
                    ja_lo = ja_1;
                    a_hi = a;
                    break;
                }

                double dja = functional->directional1(p);
                call++;
                msg::print << "dja = " << dja << msg::endl;
                msg::print << "weighted required derivative: _c2*dj = " << _c2*dj << msg::endl;
                if( std::abs(dja) <= -_c2*dj)
                {
                    //-2 because cost function and derivative are reused at the next outer iteration
                    _history->emplace_back(j,ja,ja-j,a0,a,call-2);
                    msg::unindent();
                    msg::unindent();
                    return a;
                }

                if( dja >= 0)
                {
                    a_lo = a;
                    ja_lo = ja;
                    a_hi = a_1;
                    break;
                }

                a_1 = a;
                a /= _rho;

                if(i>=_maxIt)
                {
                    msg::print << "Linsesearch failure" << msg::endl;
                    functional->setModel(m);
                    _history->emplace_back(j,j,0.,a0,0.,call-2);
                    msg::unindent();
                    msg::unindent();
                    return 0.;
                }
            }
            msg::unindent();

            //Zooming
            msg::print << "Zooming" << msg::endl;
            msg::indent();

            for (unsigned int i = 0; true; i++)
            {
                a = (a_lo+a_hi) / 2.;

                msg::print << "a = " << a <<  msg::endl;
                ja = functional->performance<ModelField>(m+p*a);
                call++;
                msg::print << "ja = " << ja << msg::endl;
                double ared = j-ja;
                msg::print << "actual reduction:  j-ja = " << ared << msg::endl;
                double pred = -a*dj;
                msg::print << "predicted reduction:  -adj = " << pred << msg::endl;
                msg::print << "weighted predicted reduction: _c1*pred = " << _c1*pred << msg::endl;

                if( (ared < _c1*pred) || (ja >= ja_lo) )
                {
                    a_hi = a;
                }
                else
                {
                    double dja = functional->directional1(p);
                    call++;
                    msg::print << "dja = " << dja << msg::endl;
                    msg::print << "weighted required derivative: _c2*dj = " << _c2*dj << msg::endl;
                    if( std::abs(dja) <= -_c2*dj)
                    {
                        //-2 because cost function and derivative are reused at the next outer iteration
                        _history->emplace_back(j,ja,ja-j,a0,a,call-2);
                        msg::unindent();
                        msg::unindent();
                        return a;
                    }

                    if(dja * (a_hi-a_lo) >= 0)
                    {
                        a_hi = a_lo;
                    }

                    a_lo = a;
                }

                if(i>=_maxIt)
                {
                    msg::print << "Linsesearch failure" << msg::endl;
                    functional->setModel(m);
                    _history->emplace_back(j,j,0.,a0,0.,call-2);
                    msg::unindent();
                    msg::unindent();
                    return 0.;
                }
            }
        }
    }
}
