// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_LINESEARCH_WOLFELIKE
#define H_SPECIFIC_OPTIMIZATION_LINESEARCH_WOLFELIKE

//GmshFEM Library
#include "GmshFem.h"
#include "CSVio.h"

//GmshFWI Library
#include "../../../common/optimization/linesearch.h"

namespace wolfelike
{
    /*
    * class LineSearchHistoryLine
    */
    class LineSearchHistoryLine
    {
    public:
        const double performance0;
        const double performanceA;
        const double decrease;
        const double a0;
        const double aN;
        const unsigned int N;

        LineSearchHistoryLine(double performance0_init, double performanceA_init, double decrease_init, double a0_init, double aN_init, unsigned int N_init) : performance0(performance0_init), performanceA(performanceA_init), decrease(decrease_init), a0(a0_init), aN(aN_init), N(N_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class LineSearchHistory
    */
    class LineSearchHistory final: public LineSearchHistoryInterface, public std::vector<LineSearchHistoryLine>
    {
    public:
        virtual void write(std::string filename) const;
    };

    namespace goldstein
    {
        /*
        * class LineSearch
        */
        class LineSearch final: public LineSearchInterface
        {
        private:
            double _rho;
            double _c1;
            double _c2;
            double _a0;
            LineSearchHistory* const _history;
        public:
            LineSearch(const gmshfem::common::GmshFem& gmshFem, std::string suffix="");
            ~LineSearch() {delete _history;};

            virtual double operator()(FunctionalInterface* const functional) const;

            virtual const LineSearchHistoryInterface* const history() const {return _history;};
        };
    };
    namespace armijo
    {
        /*
        * class LineSearch
        */
        class LineSearch final: public LineSearchInterface
        {
        private:
            std::string _suffix;
            double _rho;
            double _c1;
            double _a0;
            LineSearchHistory* const _history;
        public:
            LineSearch(const gmshfem::common::GmshFem& gmshFem,std::string suffix = "");
            ~LineSearch() {delete _history;};

            virtual double operator()(FunctionalInterface* const functional) const;

            virtual const LineSearchHistoryInterface* const history() const {return _history;};
        };
    };
    namespace strongwolfe
    {
        /*
        * class LineSearch
        */
        class LineSearch final: public LineSearchInterface
        {
        private:
            std::string _suffix;
            double _rho;
            double _c1;
            double _c2;
            double _a0;
            unsigned int _maxIt;
            LineSearchHistory* const _history;
        public:
            LineSearch(const gmshfem::common::GmshFem& gmshFem,std::string suffix = "");
            ~LineSearch() {delete _history;};

            virtual double operator()(FunctionalInterface* const functional) const;

            virtual const LineSearchHistoryInterface* const history() const {return _history;};
        };
    };
};

#endif // H_SPECIFIC_OPTIMIZATION_LINESEARCH_WOLFELIKE
