// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "Exception.h"

//GmshFWI Library
#include "classic.h"

using namespace gmshfem;
using namespace gmshfem::common;

namespace classic
{
    /*
    * class LocalMinimumSearchHistoryLine2
    */
    void LocalMinimumSearchHistoryLine2::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Performance" << "Gradient norm squared" << "Relative decrease" << "Model update penalization" << "Model penalization" << csv::endl;
        }
        else
        {
            file << performance << gradientNorm2 << relDecrease << modelUpdateNorm << modelNorm << csv::endl;
        }
    }

    /*
    * class LocalMinimumSearchHistoryLine
    */
    void LocalMinimumSearchHistoryLine::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Success" << "Iteration number" << "Overall decrease" << csv::endl;
            file << "History" << csv::endl;
        }
        else
        {
            file << success << iterations << decrease << csv::endl;
            if(!history.empty())
            {
                history.begin()->write(file,true);
                for (auto it = history.begin(); it != history.end(); it++)
                {
                    it->write(file);
                }
            }
        }
    }

    /*
    * class LocalMinimumSearchHistory
    */
    void LocalMinimumSearchHistory::write(std::string filename) const
    {
        CSVio file(filename, ';', common::OpeningMode::NewFile);
        if(this->empty())
        {
            file << "Local minimum search history is empty." << csv::endl;
        }
        else
        {
            this->begin()->write(file,true);
            for (auto it = this->begin(); it != this->end(); it++)
            {
                file << "--" << csv::endl;
                it->write(file);
            }
        }
    }

    /*
    * class LocalMinimumSearch
    */
    LocalMinimumSearch::LocalMinimumSearch(const GmshFem& gmshFem) : LocalMinimumSearchInterface(gmshFem), _history( new LocalMinimumSearchHistory() ) {}

    void LocalMinimumSearch::operator()(ModelField* const m, FunctionalInterface* const functional, const DescentSearchInterface* const descentsearch, const LineSearchInterface* const linesearch) const
    {
        msg::print << "Search minimum (classic)" << msg::endl;
        msg::indent();

        if (!linesearch) {
            throw Exception("No linesearch provided.");
        }
        if (!descentsearch) {
            throw Exception("No descentsearch provided.");
        }

        descentsearch->initialize(functional);

        std::vector<LocalMinimumSearchHistoryLine2> historyline;

        const ModelField m0(*m);
        double m0mn = 0.;
        double mmn = 0.;

        double j0 = functional->performance();

        double jn = 0.;
        double jn_1 = 0.;
        double jjn = 0.;
        double rel_djn = 0.;
        double mean_rel_djn = 0.;
        unsigned int n = 0;
        bool success = false;
        while(true)
        {
            msg::print << "--- Iteration #"+ std::to_string(n)+" --- " << msg::endl;
            msg::indent();
            jn = functional->performance();
            msg::print << "performance = " <<  jn  << msg::endl;
            jjn = std::real( functional->innerproduct()->product(functional->gradient(),functional->gradient()) );
            msg::print << "gradient norm 2 = " <<  jjn  << msg::endl;
            mmn = std::real(functional->innerproduct()->penalization(*m,*m));
            msg::print << "model penalization = " <<  mmn  << msg::endl;
            if(n>0)
            {
                m0mn = std::real(functional->innerproduct()->penalization(*m-m0,*m-m0));
                msg::print << "model overall update penalization = " <<  m0mn  << msg::endl;
                rel_djn = 2. * (jn_1-jn) / (jn_1+jn);
                msg::print << "relative decrease = " <<  rel_djn  << msg::endl;
                historyline.emplace_back(jn,jjn,rel_djn,m0mn,mmn);
                if(n>=_meanRelDecreaseMemory)
                {
                    mean_rel_djn = 0.;
                    for (int mem = 1; mem <= ((int)_meanRelDecreaseMemory)+1; mem++)
                    {
                        mean_rel_djn += historyline.end()[-mem].relDecrease / ((double)_meanRelDecreaseMemory+1);
                    }
                    msg::print << "mean relative decrease = " <<  mean_rel_djn  << msg::endl;
                    if(mean_rel_djn < _meanRelDecreaseThreshold){msg::unindent();break;}
                }
                if( ( jn / j0 ) < _overallRelDecreaseThreshold ){msg::unindent();break;}
            }
            else
            {
                historyline.emplace_back(jn,jjn,0.,m0mn,mmn);
            }

            if(n >= _maxIteration || jn < _absDecreaseThreshold || jjn < _absGradient2NormThreshold){msg::unindent();break;}
            else
            {
                (*descentsearch)(functional);
                (*linesearch)(functional);
                *m = functional->m();
                jn_1=jn;
                n++;
                descentsearch->update(functional);
                functional->iteration(n);
                if((n % _writeInterval) == 0){m->write(_name+"m"+std::to_string(n));}
                msg::unindent();
            }
        }
        double decrease = (j0 - jn) / j0;
        msg::print << "overall relative decrease = " <<  decrease  << msg::endl;
        _history->emplace_back(success,n,decrease,historyline);
        msg::unindent();
    }
}
