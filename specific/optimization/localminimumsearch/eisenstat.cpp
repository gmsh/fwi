// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "Exception.h"
//#include "Function.h" // to be removed

//GmshFWI Library
#include "eisenstat.h"

using namespace gmshfem;
using namespace gmshfem::common;

namespace eisenstat
{
    /*
    * class LocalMinimumSearch
    */
    LocalMinimumSearch::LocalMinimumSearch(const GmshFem& gmshFem) : LocalMinimumSearchInterface(gmshFem), _a1(1.), _a2(1.), _gamma(1.), _alpha((1.+std::sqrt(5))/2.), _choice(Choice::C), _history( new classic::LocalMinimumSearchHistory() )
    {
        if(!gmshFem.userDefinedParameter(_eta0, "descent_eta0"))
        {
            throw Exception("Initial value of forcing sequence could not be found.");
        }

        unsigned int bufffer;
        gmshFem.userDefinedParameter(bufffer, "localminimum_forcing");
        _choice = ((Choice) bufffer );
        switch (_choice)
        {
            case Choice::C:
                if(!(
                    gmshFem.userDefinedParameter(_a1, "localminimum_a1") &&
                    gmshFem.userDefinedParameter(_a2, "localminimum_a2") &&
                    gmshFem.userDefinedParameter(_gamma, "localminimum_gamma") &&
                    gmshFem.userDefinedParameter(_alpha, "localminimum_alpha")
                ))
                {
                    msg::warning << "An Eisenstat forcing sequence parameter could not be found." << msg::endl;
                }
                break;
            default: break;
        }
    }

    void LocalMinimumSearch::operator()(ModelField* const m, FunctionalInterface* const functional, const newton_conjugategradient::DescentSearch* const ncg_descentsearch, const LineSearchInterface* const linesearch) const
    {
        msg::print << "Search minimum (eisenstat)" << msg::endl;
        msg::indent();

        ncg_descentsearch->initialize(functional);

        std::vector<classic::LocalMinimumSearchHistoryLine2> historyline;

        double j0 = functional->performance();
        ModelField r(*m);
        r.setValuesToZero();
        double eta = _eta0;
        double jn = 0.;
        double jn_1 = 0.;
        double jjn = 0.;
        double jjn_1 = 0.;
        double rel_djn = 0.;
        double mean_rel_djn = 0.;
        unsigned int n = 0;
        bool success = false;
        while(true)
        {
            msg::print << "--- Iteration #"+ std::to_string(n)+" --- " << msg::endl;
            msg::indent();
            jn = functional->performance();
            msg::print << "performance = " <<  jn  << msg::endl;
            jjn_1=jjn;
            jjn = std::real( functional->innerproduct()->product(functional->gradient(),functional->gradient()) );
            msg::print << "gradient norm 2 = " <<  jjn  << msg::endl;
            if(n>0)
            {
                rel_djn = 2. * (jn_1-jn) / (jn_1+jn);
                msg::print << "relative decrease = " <<  rel_djn  << msg::endl;
                historyline.emplace_back(jn,jjn,rel_djn);
                if(n>=_meanRelDecreaseMemory)
                {
                    mean_rel_djn = 0.;
                    for (int mem = 1; mem <= ((int)_meanRelDecreaseMemory)+1; mem++)
                    {
                        mean_rel_djn += historyline.end()[-mem].relDecrease / ((double)_meanRelDecreaseMemory+1);
                    }
                    msg::print << "mean relative decrease = " <<  mean_rel_djn  << msg::endl;
                    if(mean_rel_djn < _meanRelDecreaseThreshold){msg::unindent();break;}
                }
                if( ( jn / j0 ) < _overallRelDecreaseThreshold ){msg::unindent();break;}

                double num = 0.;
                double den = std::sqrt(jjn_1);
                switch (_choice)
                {
                    case Choice::A:
                    {
                        msg::print << "Forcing choice (a)" << msg::endl;
                        ModelField rho = functional->gradient() - r;
                        num = std::sqrt( std::real( functional->innerproduct()->product(rho,rho) ) );
                        break;
                    }
                    case Choice::B:
                    {
                        msg::print << "Forcing choice (b)" << msg::endl;
                        double rr = std::real( functional->innerproduct()->product(r,r) );
                        num = std::abs( std::sqrt(jjn) - std::sqrt(rr) );
                        break;
                    }
                    case Choice::C:
                    {
                        msg::print << "Forcing choice (c)" << msg::endl;
                        num = std::sqrt(jjn);
                        break;
                    }
                }
                msg::print << "num = " <<  num  << msg::endl;
                double eta_next = _a1 * std::pow(num / den, _a2);
                msg::print << "eta_next = " <<  eta_next  << msg::endl;
                if(_gamma * std::pow(eta,_alpha) > 0.1)
                {
                    eta = std::max(eta_next,_gamma*std::pow(eta,_alpha));
                }
                else
                {
                    eta = eta_next;
                }
                if(eta > 1.)
                {
                    eta = 0.9;
                }
            }
            else
            {
                historyline.emplace_back(jn,jjn,0.);
            }

            if(n >= _maxIteration || jn < _absDecreaseThreshold || jjn < _absGradient2NormThreshold){msg::unindent();break;}
            else
            {
                ncg_descentsearch->eta(eta);
                (*ncg_descentsearch)(functional);
                switch (_choice)
                {
                    case Choice::A: case Choice::B:
                    {
                        ModelField Hp = functional->hessian();
                        ModelField g = functional->gradient();
                        double a = (*linesearch)(functional);
                        r = g + Hp * a;
                        break;
                    }
                    case Choice::C:
                    {
                        (*linesearch)(functional);
                        break;
                    }
                }
                *m = functional->m();
                jn_1=jn;
                n++;
                ncg_descentsearch->update(functional);
                functional->iteration(n);
                if( (n % _writeInterval) == 0){m->write(_name+"m"+std::to_string(n));}
                msg::unindent();
            }
        }
        double decrease = (j0 - jn) / j0;
        msg::print << "overall relative decrease = " <<  decrease  << msg::endl;
        _history->emplace_back(success,n,decrease,historyline);
        msg::unindent();
    }
}
