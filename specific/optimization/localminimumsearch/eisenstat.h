// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_LOCALMINIMUMSEARCH_EISENSTAT
#define H_SPECIFIC_OPTIMIZATION_LOCALMINIMUMSEARCH_EISENSTAT

//GmshFEM Library
#include "GmshFem.h"
#include "CSVio.h"

//GmshFWI Library
#include "../descentsearch/newton_conjugategradient.h"
#include "classic.h"
#include "../../../common/optimization/localminimumsearch.h"

namespace eisenstat
{
    enum class Choice : unsigned int {A=0,B=1,C=2};

    /*
    * class LocalMinimumSearch
    */
    class LocalMinimumSearch final: public LocalMinimumSearchInterface
    {
    private:
        double _eta0;
        double _a1;
        double _a2;
        double _gamma;
        double _alpha;
        Choice _choice;
        classic::LocalMinimumSearchHistory* const _history;
    public:
        LocalMinimumSearch(const gmshfem::common::GmshFem& gmshFem);
        ~LocalMinimumSearch() {delete _history;};

        virtual void operator()(ModelField* const m, FunctionalInterface* const functional, const DescentSearchInterface* const descentsearch, const LineSearchInterface* const linesearch) const
        {
            const newton_conjugategradient::DescentSearch* const ncg_descentsearch = dynamic_cast<const newton_conjugategradient::DescentSearch*>(descentsearch);
            if(ncg_descentsearch == nullptr)
            {
                throw gmshfem::common::Exception("Eisenstat local minimum search only works with newton cg descent search.");
            }
            else
            {
                (*this)(m,functional,ncg_descentsearch,linesearch);
            }
        };

        virtual void operator()(ModelField* const m, FunctionalInterface* const functional, const newton_conjugategradient::DescentSearch* const ncg_descentsearch, const LineSearchInterface* const linesearch) const;

        virtual const LocalMinimumSearchHistoryInterface* const history() const {return _history;};
    };
};

#endif // H_SPECIFIC_OPTIMIZATION_LOCALMINIMUMSEARCH_EISENSTAT
