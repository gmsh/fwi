// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library
#include <string>

//GmshFem Library
#include "Exception.h"

//FWI Library
#include "newLocalMinimumSearch.h"
#include "classic.h"
#include "eisenstat.h"
#include "steihaug.h"
#include "yuanfan.h"

LocalMinimumSearchInterface* newLocalMinimumSearch(const gmshfem::common::GmshFem& gmshFem)
{
    std::string minimumsearch;
    if(!gmshFem.userDefinedParameter(minimumsearch, "localminimumsearch"))
    {
      throw gmshfem::common::Exception("Local minimum search type could not be found.");
    }
    if(minimumsearch=="classic"){return new classic::LocalMinimumSearch(gmshFem);}
    else if(minimumsearch=="eisenstat"){return new eisenstat::LocalMinimumSearch(gmshFem);}
    else if(minimumsearch=="steihaug"){return new steihaug::LocalMinimumSearch(gmshFem);}
    else if(minimumsearch=="yuanfan"){return new yuanfan::LocalMinimumSearch(gmshFem);}
    else
    {
        throw gmshfem::common::Exception("Local minimum search " + minimumsearch + " is not valid.");
        return nullptr;
    }
}
