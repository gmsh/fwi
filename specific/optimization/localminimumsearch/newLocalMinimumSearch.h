// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_WAVE_NEWLOCALMINIMUMSEARCH
#define H_SPECIFIC_WAVE_NEWLOCALMINIMUMSEARCH

//Standard Library

//GmshFem Library
#include "GmshFem.h"

//GmshFWI Library
#include "../../../common/optimization/localminimumsearch.h"

LocalMinimumSearchInterface* newLocalMinimumSearch(const gmshfem::common::GmshFem& gmshFem);

#endif //H_SPECIFIC_WAVE_NEWLOCALMINIMUMSEARCH
