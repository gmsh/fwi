// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "Exception.h"

//GmshFWI Library
#include "steihaug.h"

using namespace gmshfem;
using namespace gmshfem::common;

namespace steihaug
{
    /*
    * class LocalMinimumSearchHistoryLine2
    */
    void LocalMinimumSearchHistoryLine2::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Performance" << "Gradient norm squared" << "Relative decrease" << "Trust region radius" << "Actual/predicted decrease ratio" << csv::endl;
        }
        else
        {
            file << performance << gradientNorm2 << relDecrease << radius << rho << csv::endl;
        }
    }

    /*
    * class LocalMinimumSearchHistoryLine
    */
    void LocalMinimumSearchHistoryLine::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Success" << "Iteration number" << "Overall decrease" << csv::endl;
            file << "History" << csv::endl;
        }
        else
        {
            file << success << iterations << decrease << csv::endl;
            if(!history.empty())
            {
                history.begin()->write(file,true);
                for (auto it = history.begin(); it != history.end(); it++)
                {
                    it->write(file);
                }
            }
        }
    }

    /*
    * class LocalMinimumSearchHistory
    */
    void LocalMinimumSearchHistory::write(std::string filename) const
    {
        CSVio file(filename, ';', common::OpeningMode::NewFile);
        if(this->empty())
        {
            file << "Local minimum search history is empty." << csv::endl;
        }
        else
        {
            this->begin()->write(file,true);
            for (auto it = this->begin(); it != this->end(); it++)
            {
                file << "--" << csv::endl;
                it->write(file);
            }
        }
    }

    /*
    * class LocalMinimumSearch
    */
    LocalMinimumSearch::LocalMinimumSearch(const GmshFem& gmshFem) : TrustRegionLocalMinimumSearchInterface(gmshFem), _history( new LocalMinimumSearchHistory() )
    {
        if(!gmshFem.userDefinedParameter(_initRadius, "localminimum_initRadius"))
        {
            throw Exception("Initial trust region radius could not be found.");
        }
        if(!gmshFem.userDefinedParameter(_maxRadius, "localminimum_maxRadius"))
        {
            throw Exception("Maximum trust region radius could not be found.");
        }
        if(!gmshFem.userDefinedParameter(_eta, "localminimum_eta"))
        {
            throw Exception("A Steihaug trust region parameter (eta) could not be found.");
        }
    }

    void LocalMinimumSearch::operator()(ModelField* const m, FunctionalInterface* const functional, const TrustRegionDescentSearchInterface* const descentsearch) const
    {
        msg::print << "Search minimum (trust region)" << msg::endl;
        msg::indent();

        descentsearch->initialize(functional);

        std::vector<LocalMinimumSearchHistoryLine2> historyline;

        double radius = _initRadius;
        double rho = 0.;

        double j0 = functional->performance();

        double jn = 0.;
        double jn_1 = 0.;
        double jjn = 0.;
        double rel_djn = 0.;
        double mean_rel_djn = 0.;
        unsigned int n = 0;
        bool success = false;
        double update = true;
        while(true)
        {
            msg::print << "--- Iteration #"+ std::to_string(n)+" --- " << msg::endl;
            msg::indent();
            msg::print << "radius = " <<  radius  << msg::endl;
            jn = functional->performance();
            msg::print << "performance = " <<  jn  << msg::endl;
            jjn = std::real( functional->innerproduct()->product(functional->gradient(),functional->gradient()) );
            msg::print << "gradient norm 2 = " <<  jjn  << msg::endl;
            if(n>0)
            {
                rel_djn = 2. * (jn_1-jn) / (jn_1+jn);
                msg::print << "relative decrease = " <<  rel_djn  << msg::endl;
                historyline.emplace_back(jn,jjn,rel_djn,radius,rho);
                if(n>=_meanRelDecreaseMemory)
                {
                    mean_rel_djn = 0.;
                    for (int mem = 1; mem <= ((int)_meanRelDecreaseMemory)+1; mem++)
                    {
                        mean_rel_djn += historyline.end()[-mem].relDecrease / ((double)_meanRelDecreaseMemory+1);
                    }
                    msg::print << "mean relative decrease = " <<  mean_rel_djn  << msg::endl;
                    if(mean_rel_djn < _meanRelDecreaseThreshold){msg::unindent();break;}
                }
                if( ( jn / j0 ) < _overallRelDecreaseThreshold ){msg::unindent();break;}
            }
            else
            {
                historyline.emplace_back(jn,jjn,0.,radius,0.);
            }

            if(n >= _maxIteration || jn < _absDecreaseThreshold || jjn < _absGradient2NormThreshold){msg::unindent();break;}
            else
            {
                TrustRegionReturn out = (*descentsearch)(functional,radius);
                //Update model
                if(jn+_eta*out.predicted_decrease<=0)
                {
                    msg::print << "rho < " << jn/-out.predicted_decrease << msg::endl;
                    rho=-1;
                    //No update of m
                    msg::print << "No model update" << msg::endl;
                    update = false;
                }
                else
                {
                    double jmp = functional->performance( ((ModelField) *m+functional->dm()) );
                    rho = (jmp - jn) / out.predicted_decrease;
                    msg::print << "rho = " << rho << msg::endl;
                    if(rho>_eta)
                    {
                        *m = functional->m();
                        update=true;
                    }
                    else
                    {
                        functional->setModel(*m);
                        msg::print << "No model update" << msg::endl;
                        update=false;
                    }
                }

                if(update){descentsearch->update(functional);}

                //update trust region radius
                if(rho<0.25)
                {
                    radius = 0.25 * out.radius;
                }
                else
                {
                    if( (rho>0.75) && out.boundary )
                    {
                        radius = std::min(2*radius,_maxRadius);
                    }
                    //else radius is unchanged
                }

                jn_1=jn;
                n++;
                functional->iteration(n);
                if( (n % _writeInterval) == 0){m->write(_name+"m"+std::to_string(n));}
                msg::unindent();
            }
        }
        double decrease = (j0 - jn) / j0;
        msg::print << "overall relative decrease = " <<  decrease  << msg::endl;
        _history->emplace_back(success,n,decrease,historyline);
        msg::unindent();
    }
}
