// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_OPTIMIZATION_LOCALMINIMUMSEARCH_STEIHAUG
#define H_SPECIFIC_OPTIMIZATION_LOCALMINIMUMSEARCH_STEIHAUG

//GmshFEM Library
#include "GmshFem.h"
#include "CSVio.h"

//GmshFWI Library
#include "../../../common/optimization/localminimumsearch.h"

namespace steihaug
{
    /*
    * class LocalMinimumSearchHistoryLine2
    */
    class LocalMinimumSearchHistoryLine2
    {
    public:
        const double performance;
        const double gradientNorm2;
        const double relDecrease;

        const double radius;
        const double rho;

        LocalMinimumSearchHistoryLine2(double performance_init, double gradientNorm2_init, double relDecrease_init, double radius_init, double rho_init) : performance(performance_init), gradientNorm2(gradientNorm2_init), relDecrease(relDecrease_init), radius(radius_init), rho(rho_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class LocalMinimumSearchHistoryLine
    */
    class LocalMinimumSearchHistoryLine
    {
    public:
        const bool success;
        const unsigned int iterations;
        const double decrease;
        const std::vector<LocalMinimumSearchHistoryLine2> history;

        LocalMinimumSearchHistoryLine(bool success_init, unsigned int iterations_init, double decrease_init, const std::vector<LocalMinimumSearchHistoryLine2>& history_init) : success(success_init), iterations(iterations_init), decrease(decrease_init), history(history_init) {};

        void write(gmshfem::common::CSVio& file, bool write_header=false) const;
    };

    /*
    * class LocalMinimumSearchHistory
    */
    class LocalMinimumSearchHistory final: public LocalMinimumSearchHistoryInterface, public std::vector<LocalMinimumSearchHistoryLine>
    {
    public:
        virtual void write(std::string filename) const;
    };

    /*
    * class LocalMinimumSearch
    */
    class LocalMinimumSearch final: public TrustRegionLocalMinimumSearchInterface
    {
    private:
        LocalMinimumSearchHistory* const _history;

        double _initRadius;
        double _maxRadius;
        double _eta;
    public:
        LocalMinimumSearch(const gmshfem::common::GmshFem& gmshFem);
        ~LocalMinimumSearch() {delete _history;};

        virtual void operator()(ModelField* const m, FunctionalInterface* const functional, const TrustRegionDescentSearchInterface* const descentsearch) const;

        virtual const LocalMinimumSearchHistoryInterface* const history() const {return _history;};
    };
};

#endif // H_SPECIFIC_OPTIMIZATION_LOCALMINIMUMSEARCH_STEIHAUG
