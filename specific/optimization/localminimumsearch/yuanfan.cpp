// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFEM Library
#include "Exception.h"

//GmshFWI Library
#include "yuanfan.h"

using namespace gmshfem;
using namespace gmshfem::common;

namespace yuanfan
{
    /*
    * class LocalMinimumSearchHistoryLine2
    */
    void LocalMinimumSearchHistoryLine2::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Performance" << "Gradient norm squared" << "Relative decrease" << "Model update penalization" << "Model penalization" << "Fraction of gradient limit (mu)" <<"Trust region radius" << "Actual/predicted decrease ratio (prospective)" << "Actual/predicted decrease ratio (retrospective)" << csv::endl;
        }
        else
        {
            file << performance << gradientNorm2 << relDecrease << modelUpdateNorm << modelNorm << mu << radius << rho_pro << rho_rto << csv::endl;
        }
    }

    /*
    * class LocalMinimumSearchHistoryLine
    */
    void LocalMinimumSearchHistoryLine::write(CSVio& file, bool write_header) const
    {
        if(write_header)
        {
            file << "Success" << "Iteration number" << "Overall decrease" << csv::endl;
            file << "History" << csv::endl;
        }
        else
        {
            file << success << iterations << decrease << csv::endl;
            if(!history.empty())
            {
                history.begin()->write(file,true);
                for (auto it = history.begin(); it != history.end(); it++)
                {
                    it->write(file);
                }
            }
        }
    }

    /*
    * class LocalMinimumSearchHistory
    */
    void LocalMinimumSearchHistory::write(std::string filename) const
    {
        CSVio file(filename, ';', common::OpeningMode::NewFile);
        if(this->empty())
        {
            file << "Local minimum search history is empty." << csv::endl;
        }
        else
        {
            this->begin()->write(file,true);
            for (auto it = this->begin(); it != this->end(); it++)
            {
                file << "--" << csv::endl;
                it->write(file);
            }
        }
    }

    /*
    * class LocalMinimumSearch
    */
    LocalMinimumSearch::LocalMinimumSearch(const GmshFem& gmshFem) : TrustRegionLocalMinimumSearchInterface(gmshFem), _history( new LocalMinimumSearchHistory() )
    {
        unsigned int retrospective = 0;
        if(!gmshFem.userDefinedParameter(retrospective, "localminimum_retrospective"))
        {
            throw Exception("Retrospective/Prospective (localminimum_retrospective) flag could not be found.");
        }
        _retrospective = ((bool) retrospective);
        if(!gmshFem.userDefinedParameter(_maxMu, "localminimum_maxMu"))
        {
            msg::warning << "Maximum gradient fraction limit (localminimum_maxMu) could not be found. No limit set (default)." << msg::endl;
            _maxMu = std::numeric_limits<double>::max();
        }
        if(!gmshFem.userDefinedParameter(_initMu, "localminimum_initMu"))
        {
            throw Exception("Initial gradient fraction limit (localminimum_initMu) radius could not be found.");
        }
        for (unsigned int i = 0; i < 5; i++)
        {
            if(!gmshFem.userDefinedParameter(c[i], "localminimum_c"+std::to_string(i)))
            {
                throw Exception("A Yuan-Fan trust region parameter (c"+std::to_string(i)+") could not be found.");
            }
        }
        c[5] = std::numeric_limits<double>::max();
        if(!gmshFem.userDefinedParameter(c[5], "localminimum_c5"))
        {
            msg::warning << "A Yuan-Fan trust region parameter (c5) could not be found. No limit set (c5=infty)(default)." << msg::endl;
        }
    }

    void LocalMinimumSearch::operator()(ModelField* const m, FunctionalInterface* const functional, const TrustRegionDescentSearchInterface* const descentsearch) const
    {
        msg::print << "Search minimum (Yuan Fan trust-region)" << msg::endl;
        msg::indent();

        descentsearch->initialize(functional);

        std::vector<LocalMinimumSearchHistoryLine2> historyline;

        double mu = _initMu;
        if(_history->size()>0)
        {
            //Do not take last mu because it has never been tested!
            mu = std::min( (_history->back().history.end()[-2]).mu, (_history->back().history.end()[-1]).mu );
        }
        double rho_pro = 0.;
        double rho_rto = 0.;

        const ModelField m0(*m);
        double m0mn = 0.;
        double mmn = 0.;

        double j0 = functional->performance();

        double jn = 0.;
        double jn_1 = 0.;
        double jjn = 0.;
        double rel_djn = 0.;
        double mean_rel_djn = 0.;
        unsigned int n = 0;
        bool success = false;
        bool update = true;
        while(true)
        {
            msg::print << "--- Iteration #"+ std::to_string(n)+" --- " << msg::endl;
            msg::indent();
            jn = functional->performance();
            msg::print << "performance = " <<  jn  << msg::endl;
            jjn = std::real( functional->innerproduct()->product(functional->gradient(),functional->gradient()) );
            msg::print << "gradient norm 2 = " <<  jjn  << msg::endl;
            double radius = mu * std::sqrt(jjn);
            msg::print << "mu = " <<  mu  << msg::endl;
            msg::print << "radius = " <<  radius  << msg::endl;
            mmn = std::real(functional->innerproduct()->penalization(*m,*m));
            msg::print << "model penalization = " <<  mmn  << msg::endl;
            if(n>0)
            {
                m0mn = std::real(functional->innerproduct()->penalization(*m-m0,*m-m0));
                msg::print << "model overall update penalization = " <<  m0mn  << msg::endl;
                rel_djn = 2. * (jn_1-jn) / (jn_1+jn);
                msg::print << "relative decrease = " <<  rel_djn  << msg::endl;
                historyline.emplace_back(jn,jjn,rel_djn,m0mn,mmn,mu,radius,rho_pro,rho_rto);
                if(n>=_meanRelDecreaseMemory)
                {
                    mean_rel_djn = 0.;
                    for (int mem = 1; mem <= ((int)_meanRelDecreaseMemory)+1; mem++)
                    {
                        mean_rel_djn += historyline.end()[-mem].relDecrease / ((double)_meanRelDecreaseMemory+1);
                    }
                    msg::print << "mean relative decrease = " <<  mean_rel_djn  << msg::endl;
                    if(mean_rel_djn < _meanRelDecreaseThreshold){msg::unindent();break;}
                }
                if( ( jn / j0 ) < _overallRelDecreaseThreshold ){msg::unindent();break;}
            }
            else
            {
                historyline.emplace_back(jn,jjn,0.,m0mn,mmn,mu,radius,0.,0.);
            }

            if(n >= _maxIteration || jn < _absDecreaseThreshold || jjn < _absGradient2NormThreshold){msg::unindent();break;}
            else
            {
                TrustRegionReturn out = (*descentsearch)(functional,radius);
                double jmp = 0.;
                //Update model
                if(jn+c[0]*out.predicted_decrease<=0)
                {
                    msg::print << "rho_pro < " << jn + c[0] * out.predicted_decrease << msg::endl;
                    rho_pro=-1;
                    //No update of m
                    msg::print << "No model update" << msg::endl;
                    update=false;
                }
                else
                {
                    jmp = functional->performance( ((ModelField) *m+functional->dm()) );
                    rho_pro = (jmp - jn) / out.predicted_decrease;
                    msg::print << "rho_pro = " << rho_pro << msg::endl;
                    if(rho_pro>c[0])
                    {
                        *m = functional->m();
                        update=true;
                    }
                    else
                    {
                        functional->setModel(*m);
                        msg::print << "No model update" << msg::endl;
                        update=false;
                    }
                }

                if(update){descentsearch->update(functional);}

                //update trust region radius
                double rho_radius = 0.;
                if(_retrospective && update)
                {
                    ModelField p = functional->dm();
                    p.oppose();
                    double predicted_decrease_rto = descentsearch->predicted_performance(p,functional) - jmp;
                    rho_rto = (jn-jmp) / predicted_decrease_rto;
                    rho_radius = rho_rto;
                }
                else
                {
                    rho_rto = 0.;
                    rho_radius = rho_pro;
                }
                msg::print << "rho_radius = " << rho_radius << msg::endl;

                if(rho_radius<c[1])
                {
                    mu = c[3] * mu;
                }
                else
                {
                    if( (rho_radius>=c[2]) && (rho_radius<=c[5]) && (out.radius > radius/2.) )
                    {
                        msg::print << "out.radius = "<< out.radius << msg::endl;
                        mu = std::min(c[4] * mu,_maxMu);
                    }
                    //else radius is unchanged
                }

                jn_1=jn;
                n++;
                functional->iteration(n);
                if( (n % _writeInterval) == 0){m->write(_name+"m"+std::to_string(n));}
                msg::unindent();
            }
        }
        double decrease = (j0 - jn) / j0;
        msg::print << "overall relative decrease = " <<  decrease  << msg::endl;
        _history->emplace_back(success,n,decrease,historyline);
        msg::unindent();
    }
}
