// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//GmshFem Library
#include "Exception.h"

//FWI Library
#include "physic.h"

using namespace gmshfem;
using namespace gmshfem::common;

Physic to_physic(const std::string& str)
{
  Physic physic;
  if (str=="acoustic") {physic = Physic::acoustic;}
  else if (str=="electromagnetic") {physic = Physic::electromagnetic;}
  else if (str=="elastodynamic") {physic = Physic::elastodynamic;}
  else
  {
    throw Exception(str+" is not a valid physic type.");
  }
  return physic;
}
Physic to_physic(const GmshFem& gmshFem)
{
  std::string str;
  if( !gmshFem.userDefinedParameter(str, "physic") )
  {
    throw Exception("Physic type could not be found.");
  }
  return to_physic(str);
}
