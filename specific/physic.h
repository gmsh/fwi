// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_PHYSIC
#define H_PHYSIC

//Standard Library
#include <string>

//GmshFem Library
#include "GmshFem.h"
#include "FieldObject.h"

enum class Physic {acoustic,electromagnetic,elastodynamic};

Physic to_physic(const std::string& str);
Physic to_physic(const gmshfem::common::GmshFem& gmshFem);

template< Physic T_Physic >
struct propertiesOf {};

/* acoustic */
template<>
struct propertiesOf<Physic::acoustic>
{
    constexpr static gmshfem::field::Form form = gmshfem::field::Form::Form0;
    constexpr static gmshfem::Degree degree = gmshfem::Degree::Degree0;
};

/* electromagnetic */
template<>
struct propertiesOf<Physic::electromagnetic>
{
    constexpr static gmshfem::field::Form form = gmshfem::field::Form::Form1;
    constexpr static gmshfem::Degree degree = gmshfem::Degree::Degree1;
};

/* elastodynamic */
template<>
struct propertiesOf<Physic::elastodynamic>
{
    constexpr static gmshfem::field::Form form = gmshfem::field::Form::Form0;
    constexpr static gmshfem::Degree degree = gmshfem::Degree::Degree1;
};

#endif // H_PHYSIC
