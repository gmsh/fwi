// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_TODATA
#define H_SPECIFIC_TODATA

//GmshFEM Library
#include "Integrate.h"
#include "Function.h"

//GmshFWI Library
#include "../common/data/element.h"
#include "../common/wave/element.h"

using namespace gmshfem::post;
using namespace gmshfem::function;

template<Physic T_Physic>
typename PointData<T_Physic>::Object wave_to_data(unsigned int s, unsigned int r, const ConfigurationInterface* const config, const WaveMultiField<T_Physic>& w);

/* electromagnetic */
template<>
typename PointData<Physic::electromagnetic>::Object wave_to_data(unsigned int s, unsigned int r, const ConfigurationInterface* const config, const WaveMultiField<Physic::electromagnetic>& w)
{
    //Exact integration should work. It does not. Derivatives are less accurate.
    typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree1 >::Object p = config->receiver_polarization(s,r);
    return p * integrate(w[s]*p,config->receiver(s,r),"Gauss0");
};

/* elastodynamic */
template<>
typename PointData<Physic::elastodynamic>::Object wave_to_data(unsigned int s, unsigned int r, const ConfigurationInterface* const config, const WaveMultiField<Physic::elastodynamic>& w)
{
    typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree1 >::Object p = config->receiver_polarization(s,r);
    return p * integrate(w[s]*p,config->receiver(s,r),"Gauss0");
};

/* acoustic */
template<>
typename PointData<Physic::acoustic>::Object wave_to_data(unsigned int s, unsigned int r, const ConfigurationInterface* const config, const WaveMultiField<Physic::acoustic>& w)
{
    return integrate(w[s],config->receiver(s,r),"Gauss0");
};

#endif // H_SPECIFIC_TODATA
