// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard Library

//Gmsh Library

//GmshFEM Library
#include "nullaryOperations.h"

//GmshFWI Library
#include "correlation.h"

using namespace gmshfem::common;

#define nX (normal_values[i * nbrGaussPoints + j])(0)
#define nY (normal_values[i * nbrGaussPoints + j])(1)
#define nZ (normal_values[i * nbrGaussPoints + j])(2)

/*
* (Optimized) Correlation function
*/

std::string trace_to_string(CorrelationTrace trace)
{
    switch (trace)
    {
        case CorrelationTrace::IDENTITY:default: return "";
        case CorrelationTrace::NORMAL: return "normal trace";
        case CorrelationTrace::TANGENT_t: return "tangential trace (t)";
        case CorrelationTrace::TANGENT_T: return "tangential trace (T)";
    }
}

namespace gmshfem
{
    namespace function
    {
        /* acoustic */
        void Correlate<Physic::acoustic>::operator()(const WaveMultiField<Physic::acoustic> *fieldA, const WaveMultiField<Physic::acoustic> *fieldB, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const
        {
            const unsigned int nbrOfElements = points.size() / gaussPoints.size();
            const unsigned int numThreads = omp::getNumThreads();
            const unsigned int myThreadID = omp::getThreadNum();

#pragma omp single
            _nbrDofsByElements = (*fieldA)[0].getFunctionSpace()->getBasisFunctions(false, _functions, _fsIndex, gaussPoints, elementType, entity);

            std::vector< scalar::Precision< std::complex<double> > > coordinates;
            //omp::setNested(true);
#pragma omp single
            ((*fieldA)[0].getFunctionSpace())->getKeys(false, _typeKeys, _entityKeys, coordinates, elementType, entity);
            //omp::setNested(false);
            const unsigned int begin = (myThreadID * _typeKeys.size()) / numThreads;
            const unsigned int end = ((myThreadID + 1) * _typeKeys.size()) / numThreads;

            const bool needOffset = !(*fieldA)[0].getFunctionSpace()->isConstantByElements();

#pragma omp single
            _dofsValuesA.resize(_typeKeys.size());
#pragma omp single
            _dofsValuesB.resize(_typeKeys.size());

            const unsigned int nbrGaussPoints = gaussPoints.size() / 3;

#pragma omp single
            scalar::copy(_gmshGaussPoints, gaussPoints);

            if(_fieldEvaluator.needJacobians())
            {
#pragma omp single
                gmsh::model::mesh::preallocateJacobians(elementType, nbrGaussPoints, true, true, false, _gmshJacobians, _gmshDeterminants, _gmshPoints, entity.second);
                gmsh::model::mesh::getJacobians(elementType, _gmshGaussPoints, _gmshJacobians, _gmshDeterminants, _gmshPoints, entity.second, myThreadID, numThreads);
#pragma omp barrier
#pragma omp single
                scalar::move(_jacobians, _gmshJacobians);
#pragma omp single
                scalar::move(_determinants, _gmshDeterminants);
            }

            for(auto it = _idx.begin(); it != _idx.end(); it++)
            {
                (*fieldA)[*it].getValues(_typeKeys, _entityKeys, _dofsValuesA, begin, end);
                (*fieldB)[*it].getValues(_typeKeys, _entityKeys, _dofsValuesB, begin, end);
#pragma omp barrier

                Eigen::MatrixX< scalar::Precision< std::complex<double> > > fieldExpression(_fieldEvaluator.size(), _nbrDofsByElements);
#pragma omp for
                for(unsigned int i = 0; i < nbrOfElements; ++i)
                {
                    Eigen::VectorX< std::complex<double> > vecDofA = Eigen::Map< const Eigen::VectorX< std::complex<double> > >(&(_dofsValuesA[i * _nbrDofsByElements]), _nbrDofsByElements);
                    Eigen::VectorX< std::complex<double> > vecDofB = Eigen::Map< const Eigen::VectorX< std::complex<double> > >(&(_dofsValuesB[i * _nbrDofsByElements]), _nbrDofsByElements);
                    for(unsigned int j = 0; j < nbrGaussPoints; ++j)
                    {
                        _fieldEvaluator(fieldExpression,
                            &_functions[((needOffset ? _fsIndex[i] * nbrGaussPoints : 0) + j) * field::NumberOfComponentsOfForm< propertiesOf<Physic::acoustic>::form >::value * _nbrDofsByElements],
                             &_jacobians[i * 9 * nbrGaussPoints + j * 9], &_determinants[i * nbrGaussPoints + j], _nbrDofsByElements);

                        typename MathObject< std::complex<double>, Degree::Degree0 >::Object result;
                        MathObject< std::complex<double>, Degree::Degree0 >::copy(result, (fieldExpression * vecDofA).transpose() * (fieldExpression * vecDofB));

                        values[i * nbrGaussPoints + j] += result;
                    }
                }
            }
        }

        /* electromagnetic */
        void Correlate<Physic::electromagnetic>::operator()(const WaveMultiField<Physic::electromagnetic> *fieldA, const WaveMultiField<Physic::electromagnetic> *fieldB, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const
        {
            const unsigned int nbrOfElements = points.size() / gaussPoints.size();
            const unsigned int numThreads = omp::getNumThreads();
            const unsigned int myThreadID = omp::getThreadNum();

#pragma omp single
            _nbrDofsByElements = (*fieldA)[0].getFunctionSpace()->getBasisFunctions(false, _functions, _fsIndex, gaussPoints, elementType, entity);

            std::vector< scalar::Precision< std::complex<double> > > coordinates;
            //omp::setNested(true);
#pragma omp single
            ((*fieldA)[0].getFunctionSpace())->getKeys(false, _typeKeys, _entityKeys, coordinates, elementType, entity);
            //omp::setNested(false);
            const unsigned int begin = (myThreadID * _typeKeys.size()) / numThreads;
            const unsigned int end = ((myThreadID + 1) * _typeKeys.size()) / numThreads;

            const bool needOffset = !(*fieldA)[0].getFunctionSpace()->isConstantByElements();

#pragma omp single
            _dofsValuesA.resize(_typeKeys.size());
#pragma omp single
            _dofsValuesB.resize(_typeKeys.size());

            const unsigned int nbrGaussPoints = gaussPoints.size() / 3;

#pragma omp single
            scalar::copy(_gmshGaussPoints, gaussPoints);

            if(_fieldEvaluator.needJacobians())
            {
#pragma omp single
                gmsh::model::mesh::preallocateJacobians(elementType, nbrGaussPoints, true, true, false, _gmshJacobians, _gmshDeterminants, _gmshPoints, entity.second);
                gmsh::model::mesh::getJacobians(elementType, _gmshGaussPoints, _gmshJacobians, _gmshDeterminants, _gmshPoints, entity.second, myThreadID, numThreads);
#pragma omp barrier
#pragma omp single
                scalar::move(_jacobians, _gmshJacobians);
#pragma omp single
                scalar::move(_determinants, _gmshDeterminants);
            }

            OutputVector< std::complex<double>, Degree::Degree1 > normal_values;
            if(_trace!=CorrelationTrace::IDENTITY)
            {
                normal_values.resize(values.size());
                Normal< std::complex<double> > normal_op;
                normal_op(normal_values, points, gaussPoints, elementType, entity);
            }


            for(auto it = _idx.begin(); it != _idx.end(); it++)
            {
                (*fieldA)[*it].getValues(_typeKeys, _entityKeys, _dofsValuesA, begin, end);
                (*fieldB)[*it].getValues(_typeKeys, _entityKeys, _dofsValuesB, begin, end);
#pragma omp barrier

                Eigen::MatrixX< scalar::Precision< std::complex<double> > > fieldExpression(_fieldEvaluator.size(), _nbrDofsByElements);
#pragma omp for
                for(unsigned int i = 0; i < nbrOfElements; ++i)
                {
                    Eigen::VectorX< std::complex<double> > vecDofA = Eigen::Map< const Eigen::VectorX< std::complex<double> > >(&(_dofsValuesA[i * _nbrDofsByElements]), _nbrDofsByElements);
                    Eigen::VectorX< std::complex<double> > vecDofB = Eigen::Map< const Eigen::VectorX< std::complex<double> > >(&(_dofsValuesB[i * _nbrDofsByElements]), _nbrDofsByElements);

                    for(unsigned int j = 0; j < nbrGaussPoints; ++j)
                    {
                        _fieldEvaluator(fieldExpression,
                            &_functions[((needOffset ? _fsIndex[i] * nbrGaussPoints : 0) + j) * field::NumberOfComponentsOfForm< propertiesOf<Physic::electromagnetic>::form >::value * _nbrDofsByElements],
                            &_jacobians[i * 9 * nbrGaussPoints + j * 9], &_determinants[i * nbrGaussPoints + j], _nbrDofsByElements);
                        switch (_trace)
                        {
                            case CorrelationTrace::IDENTITY:default:
                            {
                                typename MathObject< std::complex<double>, Degree::Degree0 >::Object result;
                                MathObject< std::complex<double>, Degree::Degree0 >::copy(result, (fieldExpression * vecDofA).transpose() * (fieldExpression * vecDofB));

                                values[i * nbrGaussPoints + j] += result;
                                break;
                            }
                            case CorrelationTrace::NORMAL:
                            case CorrelationTrace::TANGENT_t:
                            case CorrelationTrace::TANGENT_T:
                            {
                                typename MathObject< std::complex<double>, Degree::Degree2 >::Object projection;

                                switch (_trace)
                                {
                                    case CorrelationTrace::IDENTITY:default:break;
                                    case CorrelationTrace::NORMAL:
                                    {
                                        projection(0,0) = nX*nX;
                                        projection(0,1) = nX*nY;
                                        projection(0,2) = nX*nZ;
                                        projection(1,0) = nY*nX;
                                        projection(1,1) = nY*nY;
                                        projection(1,2) = nY*nZ;
                                        projection(2,0) = nZ*nX;
                                        projection(2,1) = nZ*nY;
                                        projection(2,2) = nZ*nZ;
                                        break;
                                    }
                                    case CorrelationTrace::TANGENT_t:
                                    {
                                        projection(0,0) = 0.;
                                        projection(0,1) = -nZ;
                                        projection(0,2) = nY;
                                        projection(1,0) = nZ;
                                        projection(1,1) = 0.;
                                        projection(1,2) = -nX;
                                        projection(2,0) = -nY;
                                        projection(2,1) = nX;
                                        projection(2,2) = 0.;
                                        break;
                                    }
                                    case CorrelationTrace::TANGENT_T:
                                    {
                                        projection(0,0) = nY*nY+nZ*nZ;
                                        projection(0,1) = -nX*nY;
                                        projection(0,2) = -nX*nZ;
                                        projection(1,0) = -nY*nX;
                                        projection(1,1) = nX*nX+nZ*nZ;
                                        projection(1,2) = -nY*nZ;
                                        projection(2,0) = -nZ*nX;
                                        projection(2,1) = -nZ*nY;
                                        projection(2,2) = nX*nX*nY*nY;
                                        break;
                                    }
                                }

                                typename MathObject< std::complex<double>, Degree::Degree1 >::Object vecA = fieldExpression * vecDofA;
                                typename MathObject< std::complex<double>, Degree::Degree1 >::Object vecB = fieldExpression * vecDofB;
                                typename MathObject< std::complex<double>, Degree::Degree0 >::Object result;
                                MathObject< std::complex<double>, Degree::Degree0 >::copy(result, (projection*vecA).transpose() * (projection*vecB));

                                values[i * nbrGaussPoints + j] += result;
                                break;
                            }
                        }
                    }
                }
            }
        }

        /* elastodynamic */
        void Correlate<Physic::elastodynamic>::operator()(const WaveMultiField<Physic::elastodynamic> *fieldA, const WaveMultiField<Physic::elastodynamic> *fieldB, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const
        {
            const unsigned int nbrOfElements = points.size() / gaussPoints.size();
            const unsigned int numThreads = omp::getNumThreads();
            const unsigned int myThreadID = omp::getThreadNum();

#pragma omp single
            _nbrDofsByElements = (*fieldA)[0].getFunctionSpace()->getBasisFunctions(false, _functions, _fsIndex, gaussPoints, elementType, entity);

            std::vector< scalar::Precision< std::complex<double> > > coordinates;
            //omp::setNested(true);
#pragma omp single
            ((*fieldA)[0].getFunctionSpace())->getKeys(false, _typeKeys, _entityKeys, coordinates, elementType, entity);
            //omp::setNested(false);
            const unsigned int begin = (myThreadID * _typeKeys.size()) / numThreads;
            const unsigned int end = ((myThreadID + 1) * _typeKeys.size()) / numThreads;

            const bool needOffset = !(*fieldA)[0].getFunctionSpace()->isConstantByElements();

#pragma omp single
            _dofsValuesA.resize(T_NumFields_Elastodynamic * _typeKeys.size());
#pragma omp single
            _dofsValuesB.resize(T_NumFields_Elastodynamic * _typeKeys.size());

            const unsigned int nbrGaussPoints = gaussPoints.size() / 3;

#pragma omp single
            scalar::copy(_gmshGaussPoints, gaussPoints);

            if(_fieldEvaluator.needJacobians())
            {
#pragma omp single
                gmsh::model::mesh::preallocateJacobians(elementType, nbrGaussPoints, true, true, false, _gmshJacobians, _gmshDeterminants, _gmshPoints, entity.second);
                gmsh::model::mesh::getJacobians(elementType, _gmshGaussPoints, _gmshJacobians, _gmshDeterminants, _gmshPoints, entity.second, myThreadID, numThreads);
#pragma omp barrier
#pragma omp single
                scalar::move(_jacobians, _gmshJacobians);
#pragma omp single
                scalar::move(_determinants, _gmshDeterminants);
            }

            OutputVector< std::complex<double>, Degree::Degree1 > normal_values;
            if(_trace!=CorrelationTrace::IDENTITY)
            {
                normal_values.resize(values.size());
                Normal< std::complex<double> > normal_op;
                normal_op(normal_values, points, gaussPoints, elementType, entity);
            }

            for(auto it = _idx.begin(); it != _idx.end(); it++)
            {
                (*fieldA)[*it].getValues(_typeKeys, _entityKeys, _dofsValuesA, begin, end);
                (*fieldB)[*it].getValues(_typeKeys, _entityKeys, _dofsValuesB, begin, end);
#pragma omp barrier

                Eigen::MatrixX< scalar::Precision< std::complex<double> > > fieldExpression(_fieldEvaluator.size(), T_NumFields_Elastodynamic * _nbrDofsByElements);
#pragma omp for
                for(unsigned int i = 0; i < nbrOfElements; ++i)
                {
                    Eigen::VectorX< std::complex<double> > vecDofA = Eigen::Map< const Eigen::VectorX< std::complex<double> > >(&(_dofsValuesA[i * T_NumFields_Elastodynamic * _nbrDofsByElements]), T_NumFields_Elastodynamic * _nbrDofsByElements);
                    Eigen::VectorX< std::complex<double> > vecDofB = Eigen::Map< const Eigen::VectorX< std::complex<double> > >(&(_dofsValuesB[i * T_NumFields_Elastodynamic *_nbrDofsByElements]), T_NumFields_Elastodynamic * _nbrDofsByElements);

                    for(unsigned int j = 0; j < nbrGaussPoints; ++j)
                    {
                        _fieldEvaluator(fieldExpression,
                            &_functions[((needOffset ? _fsIndex[i] * nbrGaussPoints : 0) + j) * field::NumberOfComponentsOfForm< propertiesOf<Physic::elastodynamic>::form >::value * _nbrDofsByElements],
                            &_jacobians[i * 9 * nbrGaussPoints + j * 9], &_determinants[i * nbrGaussPoints + j], _nbrDofsByElements);
                        switch (_trace)
                        {
                            case CorrelationTrace::IDENTITY:default:
                            {
                                typename MathObject< std::complex<double>, Degree::Degree0 >::Object result;
                                MathObject< std::complex<double>, Degree::Degree0 >::copy(result, (fieldExpression * vecDofA).transpose() * (fieldExpression * vecDofB));

                                values[i * nbrGaussPoints + j] += result;
                                break;
                            }
                            case CorrelationTrace::NORMAL:
                            case CorrelationTrace::TANGENT_t:
                            case CorrelationTrace::TANGENT_T:
                            {
                                typename MathObject< std::complex<double>, Degree::Degree2 >::Object projection;

                                switch (_trace)
                                {
                                    case CorrelationTrace::IDENTITY:default:break;
                                    case CorrelationTrace::NORMAL:
                                    {
                                        projection(0,0) = nX*nX;
                                        projection(0,1) = nX*nY;
                                        projection(0,2) = nX*nZ;
                                        projection(1,0) = nY*nX;
                                        projection(1,1) = nY*nY;
                                        projection(1,2) = nY*nZ;
                                        projection(2,0) = nZ*nX;
                                        projection(2,1) = nZ*nY;
                                        projection(2,2) = nZ*nZ;
                                        break;
                                    }
                                    case CorrelationTrace::TANGENT_t:
                                    {
                                        projection(0,0) = 0.;
                                        projection(0,1) = -nZ;
                                        projection(0,2) = nY;
                                        projection(1,0) = nZ;
                                        projection(1,1) = 0.;
                                        projection(1,2) = -nX;
                                        projection(2,0) = -nY;
                                        projection(2,1) = nX;
                                        projection(2,2) = 0.;
                                        break;
                                    }
                                    case CorrelationTrace::TANGENT_T:
                                    {
                                        projection(0,0) = nY*nY+nZ*nZ;
                                        projection(0,1) = -nX*nY;
                                        projection(0,2) = -nX*nZ;
                                        projection(1,0) = -nY*nX;
                                        projection(1,1) = nX*nX+nZ*nZ;
                                        projection(1,2) = -nY*nZ;
                                        projection(2,0) = -nZ*nX;
                                        projection(2,1) = -nZ*nY;
                                        projection(2,2) = nX*nX*nY*nY;
                                        break;
                                    }
                                }

                                typename MathObject< std::complex<double>, Degree::Degree1 >::Object vecA = fieldExpression * vecDofA;
                                typename MathObject< std::complex<double>, Degree::Degree1 >::Object vecB = fieldExpression * vecDofB;
                                typename MathObject< std::complex<double>, Degree::Degree0 >::Object result;
                                MathObject< std::complex<double>, Degree::Degree0 >::copy(result, (projection*vecA).transpose() * (projection*vecB));

                                values[i * nbrGaussPoints + j] += result;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
};

/*
* (Optimized) Derivative Correlation function
*/

std::string trace_to_string(DerivativeCorrelationTrace trace)
{
    switch (trace)
    {
        case DerivativeCorrelationTrace::DIV: return "divergence";
        case DerivativeCorrelationTrace::SYM_GRAD: return "symmetrize gradient";
        case DerivativeCorrelationTrace::GRAD: return "gradient";
        default: return "";
    }
}

namespace gmshfem
{
    namespace function
    {
        /* acoustic */
        void DerivativeCorrelate<Physic::acoustic>::operator()(const WaveMultiField<Physic::acoustic> *fieldA, const WaveMultiField<Physic::acoustic> *fieldB, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const
        {
            const unsigned int nbrOfElements = points.size() / gaussPoints.size();
            const unsigned int numThreads = omp::getNumThreads();
            const unsigned int myThreadID = omp::getThreadNum();

#pragma omp single
            _nbrDofsByElements = (*fieldA)[0].getFunctionSpace()->getBasisFunctions(true, _functions, _fsIndex, gaussPoints, elementType, entity);
            std::vector< scalar::Precision< std::complex<double> > > coordinates;
            //omp::setNested(true);
#pragma omp single
            ((*fieldA)[0].getFunctionSpace())->getKeys(false, _typeKeys, _entityKeys, coordinates, elementType, entity);
            //omp::setNested(false);
            const unsigned int begin = (myThreadID * _typeKeys.size()) / numThreads;
            const unsigned int end = ((myThreadID + 1) * _typeKeys.size()) / numThreads;

            const bool needOffset = !(*fieldA)[0].getFunctionSpace()->isConstantByElements();

#pragma omp single
            _dofsValuesA.resize(_typeKeys.size());
#pragma omp single
            _dofsValuesB.resize(_typeKeys.size());
            const unsigned int nbrGaussPoints = gaussPoints.size() / 3;

#pragma omp single
            scalar::copy(_gmshGaussPoints, gaussPoints);

            if(_fieldEvaluator.needJacobians())
            {
#pragma omp single
                gmsh::model::mesh::preallocateJacobians(elementType, nbrGaussPoints, true, true, false, _gmshJacobians, _gmshDeterminants, _gmshPoints, entity.second);
                gmsh::model::mesh::getJacobians(elementType, _gmshGaussPoints, _gmshJacobians, _gmshDeterminants, _gmshPoints, entity.second, myThreadID, numThreads);
#pragma omp barrier
#pragma omp single
                scalar::move(_jacobians, _gmshJacobians);
#pragma omp single
                scalar::move(_determinants, _gmshDeterminants);
            }

            for(auto it = _idx.begin(); it != _idx.end(); it++)
            {
                (*fieldA)[*it].getValues(_typeKeys, _entityKeys, _dofsValuesA, begin, end);
                (*fieldB)[*it].getValues(_typeKeys, _entityKeys, _dofsValuesB, begin, end);
#pragma omp barrier

                Eigen::MatrixX< scalar::Precision< std::complex<double> > > fieldExpression(_fieldEvaluator.size(), _nbrDofsByElements);
#pragma omp for
                for(unsigned int i = 0; i < nbrOfElements; ++i)
                {
                    Eigen::VectorX< std::complex<double> > vecDofA = Eigen::Map< const Eigen::VectorX< std::complex<double> > >(&(_dofsValuesA[i * _nbrDofsByElements]), _nbrDofsByElements);
                    Eigen::VectorX< std::complex<double> > vecDofB = Eigen::Map< const Eigen::VectorX< std::complex<double> > >(&(_dofsValuesB[i *_nbrDofsByElements]), _nbrDofsByElements);

                    for(unsigned int j = 0; j < nbrGaussPoints; ++j)
                    {
                        _fieldEvaluator(fieldExpression,
                            &_functions[((needOffset ? _fsIndex[i] * nbrGaussPoints : 0) + j) * field::NumberOfComponentsOfForm< field::NextForm< propertiesOf<Physic::acoustic>::form >::value >::value * _nbrDofsByElements],
                            &_jacobians[i * 9 * nbrGaussPoints + j * 9], &_determinants[i * nbrGaussPoints + j], _nbrDofsByElements);

                        switch (_trace)
                        {
                            case DerivativeCorrelationTrace::GRAD:
                            {
                                typename MathObject< std::complex<double>, Degree::Degree0 >::Object result;
                                MathObject< std::complex<double>, Degree::Degree0 >::copy(result, (fieldExpression * vecDofA).transpose() * (fieldExpression * vecDofB));
                                values[i * nbrGaussPoints + j] += result;
                                break;
                            }
                            default:
                                throw gmshfem::common::Exception("Only gradient of acoustic wavefield is defined.");
                        }
                    }
                }
            }
        }

        /* elastodynamic */
        void DerivativeCorrelate<Physic::elastodynamic>::operator()(const WaveMultiField<Physic::elastodynamic> *fieldA, const WaveMultiField<Physic::elastodynamic> *fieldB, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const
        {
            const unsigned int nbrOfElements = points.size() / gaussPoints.size();
            const unsigned int numThreads = omp::getNumThreads();
            const unsigned int myThreadID = omp::getThreadNum();

#pragma omp single
            _nbrDofsByElements = (*fieldA)[0].getFunctionSpace()->getBasisFunctions(true, _functions, _fsIndex, gaussPoints, elementType, entity);
            std::vector< scalar::Precision< std::complex<double> > > coordinates;
            //omp::setNested(true);
#pragma omp single
            ((*fieldA)[0].getFunctionSpace())->getKeys(false, _typeKeys, _entityKeys, coordinates, elementType, entity);
            //omp::setNested(false);
            const unsigned int begin = (myThreadID * _typeKeys.size()) / numThreads;
            const unsigned int end = ((myThreadID + 1) * _typeKeys.size()) / numThreads;

            const bool needOffset = !(*fieldA)[0].getFunctionSpace()->isConstantByElements();

#pragma omp single
            _dofsValuesA.resize(T_NumFields_Elastodynamic * _typeKeys.size());
#pragma omp single
            _dofsValuesB.resize(T_NumFields_Elastodynamic * _typeKeys.size());
            const unsigned int nbrGaussPoints = gaussPoints.size() / 3;

#pragma omp single
            scalar::copy(_gmshGaussPoints, gaussPoints);

            if(_fieldEvaluator.needJacobians())
            {
#pragma omp single
                gmsh::model::mesh::preallocateJacobians(elementType, nbrGaussPoints, true, true, false, _gmshJacobians, _gmshDeterminants, _gmshPoints, entity.second);
                gmsh::model::mesh::getJacobians(elementType, _gmshGaussPoints, _gmshJacobians, _gmshDeterminants, _gmshPoints, entity.second, myThreadID, numThreads);
#pragma omp barrier
#pragma omp single
                scalar::move(_jacobians, _gmshJacobians);
#pragma omp single
                scalar::move(_determinants, _gmshDeterminants);
            }

            for(auto it = _idx.begin(); it != _idx.end(); it++)
            {
                (*fieldA)[*it].getValues(_typeKeys, _entityKeys, _dofsValuesA, begin, end);
                (*fieldB)[*it].getValues(_typeKeys, _entityKeys, _dofsValuesB, begin, end);
#pragma omp barrier

                Eigen::MatrixX< scalar::Precision< std::complex<double> > > fieldExpression(_fieldEvaluator.size(), T_NumFields_Elastodynamic * _nbrDofsByElements);
#pragma omp for
                for(unsigned int i = 0; i < nbrOfElements; ++i)
                {
                    Eigen::VectorX< std::complex<double> > vecDofA = Eigen::Map< const Eigen::VectorX< std::complex<double> > >(&(_dofsValuesA[i * T_NumFields_Elastodynamic  * _nbrDofsByElements]), T_NumFields_Elastodynamic  * _nbrDofsByElements);
                    Eigen::VectorX< std::complex<double> > vecDofB = Eigen::Map< const Eigen::VectorX< std::complex<double> > >(&(_dofsValuesB[i * T_NumFields_Elastodynamic  *_nbrDofsByElements]), T_NumFields_Elastodynamic * _nbrDofsByElements);

                    for(unsigned int j = 0; j < nbrGaussPoints; ++j)
                    {
                        _fieldEvaluator(fieldExpression,
                            &_functions[((needOffset ? _fsIndex[i] * nbrGaussPoints : 0) + j) * field::NumberOfComponentsOfForm< field::NextForm< propertiesOf<Physic::elastodynamic>::form >::value >::value * _nbrDofsByElements],
                            &_jacobians[i * 9 * nbrGaussPoints + j * 9], &_determinants[i * nbrGaussPoints + j], _nbrDofsByElements);

                        //field::DegreeOfCompoundForm< field::NextForm< propertiesOf<Physic::elastodynamic>::form >::value >::value
                        Eigen::MatrixX< std::complex<double> > tmpA = fieldExpression * vecDofA;
                        Eigen::MatrixX< std::complex<double> > tmpB = fieldExpression * vecDofB;
                        typename MathObject< std::complex<double>, Degree::Degree2 >::Object matA, matB;
                        MathObject< std::complex<double>, field::DegreeOfCompoundForm< field::NextForm< propertiesOf<Physic::elastodynamic>::form >::value >::value >::copy(matA, tmpA);
                        MathObject< std::complex<double>, field::DegreeOfCompoundForm< field::NextForm< propertiesOf<Physic::elastodynamic>::form >::value >::value >::copy(matB, tmpB);

                        switch (_trace)
                        {
                            case DerivativeCorrelationTrace::DIV:
                            {
                                values[i * nbrGaussPoints + j] += matA.trace() * matB.trace();
                                break;
                            }
                            case DerivativeCorrelationTrace::SYM_GRAD:
                            {
                                values[i * nbrGaussPoints + j] += 0.25 * ( (matA+matA.transpose()) * (matB.transpose()+matB) ).trace();
                                break;
                            }
                            case DerivativeCorrelationTrace::GRAD:
                            {
                                values[i * nbrGaussPoints + j] += (matA * matB).trace();
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
};
