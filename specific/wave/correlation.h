// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_WAVE_CORRELATION
#define H_SPECIFIC_WAVE_CORRELATION

//Standard Library

//Gmsh Library

//GmshFEM Library

//GmshFWI Library
#include "../../common/wave/correlation.h"

/*
* (Optimized) Correlation function
*/

enum CorrelationTrace : unsigned int {IDENTITY=0, NORMAL=1, TANGENT_t=2, TANGENT_T=3};
std::string trace_to_string(CorrelationTrace trace);

namespace gmshfem
{
    namespace function
    {
        template<Physic T_Physic>
        class Correlate {};

        /* acoustic */
        template<>
        class Correlate<Physic::acoustic> final : public BiWaveMultiFieldOperation<Physic::acoustic>
        {
        private:
            std::vector<unsigned int> _idx;

            mutable std::vector< int > _typeKeys;
            mutable std::vector< unsigned long long > _entityKeys;
            mutable std::vector< std::complex<double> > _dofsValuesA;
            mutable std::vector< std::complex<double> > _dofsValuesB;
            mutable std::vector< scalar::Precision< std::complex<double> > > _functions;
            mutable std::vector< int > _fsIndex;
            mutable unsigned int _nbrDofsByElements;
            mutable std::vector< double > _gmshJacobians, _gmshDeterminants, _gmshPoints, _gmshGaussPoints;
            mutable std::vector< scalar::Precision< std::complex<double> > > _jacobians, _determinants;
            mutable term::evaluator::FieldEvaluator< std::complex<double>, propertiesOf<Physic::acoustic>::form > _fieldEvaluator;
        public:
            Correlate<Physic::acoustic>(const std::vector<unsigned int>& idx, CorrelationTrace trace = CorrelationTrace::IDENTITY): _idx(idx)
            {
                if(trace != CorrelationTrace::IDENTITY)
                {
                    throw gmshfem::common::Exception("Acoustic wavefield has no trace defined.");
                }
            }
            Correlate<Physic::acoustic>(const Correlate &other) : BiWaveMultiFieldOperation< Physic::acoustic >(other), _idx(other._idx) {}

            virtual std::string name() const override
            {
                return "Correlate";
            }

            virtual void operator()(const WaveMultiField<Physic::acoustic> *fieldA, const WaveMultiField<Physic::acoustic> *fieldB, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const override;
        };

        /* electromagnetic */
        template<>
        class Correlate<Physic::electromagnetic> final : public BiWaveMultiFieldOperation<Physic::electromagnetic>
        {
        private:
            std::vector<unsigned int> _idx;
            CorrelationTrace _trace;

            mutable std::vector< int > _typeKeys;
            mutable std::vector< unsigned long long > _entityKeys;
            mutable std::vector< std::complex<double> > _dofsValuesA;
            mutable std::vector< std::complex<double> > _dofsValuesB;
            mutable std::vector< scalar::Precision< std::complex<double> > > _functions;
            mutable std::vector< int > _fsIndex;
            mutable unsigned int _nbrDofsByElements;
            mutable std::vector< double > _gmshJacobians, _gmshDeterminants, _gmshPoints, _gmshGaussPoints;
            mutable std::vector< scalar::Precision< std::complex<double> > > _jacobians, _determinants;
            mutable term::evaluator::FieldEvaluator< std::complex<double>, propertiesOf<Physic::electromagnetic>::form > _fieldEvaluator;
        public:
            Correlate<Physic::electromagnetic>(const std::vector<unsigned int>& idx, CorrelationTrace trace = CorrelationTrace::IDENTITY): _idx(idx), _trace(trace) {}
            Correlate<Physic::electromagnetic>(const Correlate &other) : BiWaveMultiFieldOperation< Physic::electromagnetic >(other), _idx(other._idx), _trace(other._trace) {}

            virtual std::string name() const override
            {
                return "Correlate "+trace_to_string(_trace);
            }

            void operator()(const WaveMultiField<Physic::electromagnetic> *fieldA, const WaveMultiField<Physic::electromagnetic> *fieldB, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const override;
        };

        /* elastodynamic */
        template<>
        class Correlate<Physic::elastodynamic> final : public BiWaveMultiFieldOperation<Physic::elastodynamic>
        {
        private:
            std::vector<unsigned int> _idx;
            CorrelationTrace _trace;

            mutable std::vector< int > _typeKeys;
            mutable std::vector< unsigned long long > _entityKeys;
            mutable std::vector< std::complex<double> > _dofsValuesA;
            mutable std::vector< std::complex<double> > _dofsValuesB;
            mutable std::vector< scalar::Precision< std::complex<double> > > _functions;
            mutable std::vector< int > _fsIndex;
            mutable unsigned int _nbrDofsByElements;
            mutable std::vector< double > _gmshJacobians, _gmshDeterminants, _gmshPoints, _gmshGaussPoints;
            mutable std::vector< scalar::Precision< std::complex<double> > > _jacobians, _determinants;
            mutable term::evaluator::CompoundFieldEvaluator< std::complex<double>, propertiesOf<Physic::elastodynamic>::form, T_NumFields_Elastodynamic > _fieldEvaluator;

        public:
            Correlate<Physic::elastodynamic>(const std::vector<unsigned int>& idx, CorrelationTrace trace = CorrelationTrace::IDENTITY): _idx(idx), _trace(trace) {}

            Correlate<Physic::elastodynamic>(const Correlate &other) :
            BiWaveMultiFieldOperation< Physic::elastodynamic >(other), _idx(other._idx), _trace(other._trace) {}

            virtual std::string name() const override
            {
                return "Correlate "+trace_to_string(_trace);
            }

            void operator()(const WaveMultiField<Physic::elastodynamic> *fieldA, const WaveMultiField<Physic::elastodynamic> *fieldB, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const override;
        };
    }
};

template<Physic T_Physic>
ModelMonoFunction correlate(const WaveMultiField<T_Physic>& u, const WaveMultiField<T_Physic>& v, const std::vector<unsigned int>& index, CorrelationTrace trace = CorrelationTrace::IDENTITY)
{
    return gmshfem::function::Function< std::complex<double>, gmshfem::Degree::Degree0 >( new gmshfem::function::BiWaveMultiFieldNode< gmshfem::function::Correlate< T_Physic >,T_Physic >( gmshfem::function::Correlate< T_Physic >(index,trace), &u, &v) );
};

template<Physic T_Physic>
ModelMonoFunction correlate(const WaveMultiField<T_Physic>& u, const WaveMultiField<T_Physic>& v, CorrelationTrace trace = CorrelationTrace::IDENTITY)
{
    std::vector<unsigned int> idx(u.size());
    std::iota(idx.begin(), idx.end(), 0);
    return correlate(u,v,idx,trace);
};

/*
* (Optimized) Derivative Correlation function
*/

enum DerivativeCorrelationTrace : unsigned int {DIV=0, SYM_GRAD=1, GRAD=2};
std::string trace_to_string(DerivativeCorrelationTrace trace);

namespace gmshfem
{
    namespace function
    {
        template<Physic T_Physic>
        class DerivativeCorrelate {};

        /* acoustic */
        template<>
        class DerivativeCorrelate<Physic::acoustic> final : public BiWaveMultiFieldOperation<Physic::acoustic>
        {
        private:
            std::vector<unsigned int> _idx;
            DerivativeCorrelationTrace _trace;

            mutable std::vector< int > _typeKeys;
            mutable std::vector< unsigned long long > _entityKeys;
            mutable std::vector< std::complex<double> > _dofsValuesA;
            mutable std::vector< std::complex<double> > _dofsValuesB;
            mutable std::vector< scalar::Precision< std::complex<double> > > _functions;
            mutable std::vector< int > _fsIndex;
            mutable unsigned int _nbrDofsByElements;
            mutable std::vector< double > _gmshJacobians, _gmshDeterminants, _gmshPoints, _gmshGaussPoints;
            mutable std::vector< scalar::Precision< std::complex<double> > > _jacobians, _determinants;
            mutable term::evaluator::FieldEvaluator< std::complex<double>, field::NextForm< propertiesOf<Physic::acoustic>::form >::value > _fieldEvaluator;

        public:
            DerivativeCorrelate<Physic::acoustic>(const std::vector<unsigned int>& idx, DerivativeCorrelationTrace trace): _idx(idx), _trace(trace)
            {
                if(trace != DerivativeCorrelationTrace::GRAD)
                {
                    throw gmshfem::common::Exception("Only gradient of acoustic wavefield is defined.");
                }
            }


            DerivativeCorrelate<Physic::acoustic>(const DerivativeCorrelate &other) :
            BiWaveMultiFieldOperation<Physic::acoustic>(other), _idx(other._idx), _trace(other._trace) {}

            virtual std::string name() const override
            {
                return "Correlate "+trace_to_string(_trace);
            }

            virtual void operator()(const WaveMultiField<Physic::acoustic> *fieldA, const WaveMultiField<Physic::acoustic> *fieldB, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const override;
        };

        /* elastodynamic */
        template<>
        class DerivativeCorrelate<Physic::elastodynamic> final : public BiWaveMultiFieldOperation<Physic::elastodynamic>
        {
        private:
            std::vector<unsigned int> _idx;
            DerivativeCorrelationTrace _trace;

            mutable std::vector< int > _typeKeys;
            mutable std::vector< unsigned long long > _entityKeys;
            mutable std::vector< std::complex<double> > _dofsValuesA;
            mutable std::vector< std::complex<double> > _dofsValuesB;
            mutable std::vector< scalar::Precision< std::complex<double> > > _functions;
            mutable std::vector< int > _fsIndex;
            mutable unsigned int _nbrDofsByElements;
            mutable std::vector< double > _gmshJacobians, _gmshDeterminants, _gmshPoints, _gmshGaussPoints;
            mutable std::vector< scalar::Precision< std::complex<double> > > _jacobians, _determinants;
            mutable term::evaluator::CompoundFieldEvaluator< std::complex<double>, field::NextForm< propertiesOf<Physic::elastodynamic>::form >::value, T_NumFields_Elastodynamic> _fieldEvaluator;

        public:
            DerivativeCorrelate<Physic::elastodynamic>(const std::vector<unsigned int>& idx, DerivativeCorrelationTrace trace): _idx(idx), _trace(trace) {}

            DerivativeCorrelate<Physic::elastodynamic>(const DerivativeCorrelate &other) :
            BiWaveMultiFieldOperation< Physic::elastodynamic >(other), _idx(other._idx), _trace(other._trace) {}

            virtual std::string name() const override
            {
                return "Correlate "+trace_to_string(_trace);
            }

            virtual void operator()(const WaveMultiField<Physic::elastodynamic> *fieldA, const WaveMultiField<Physic::elastodynamic> *fieldB, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const override;
        };
    }
};

template<Physic T_Physic>
ModelMonoFunction correlate_derivative(const WaveMultiField<T_Physic>& u, const WaveMultiField<T_Physic>& v, const std::vector<unsigned int>& index, DerivativeCorrelationTrace trace)
{
    return gmshfem::function::Function< std::complex<double>, gmshfem::Degree::Degree0 >( new gmshfem::function::BiWaveMultiFieldNode< gmshfem::function::DerivativeCorrelate< T_Physic >,T_Physic >( gmshfem::function::DerivativeCorrelate< T_Physic >(index,trace), &u, &v) );
};

template<Physic T_Physic>
ModelMonoFunction correlate_derivative(const WaveMultiField<T_Physic>& u, const WaveMultiField<T_Physic>& v, DerivativeCorrelationTrace trace)
{
    std::vector<unsigned int> idx(u.size());
    std::iota(idx.begin(), idx.end(), 0);
    return correlate_derivative(u,v,idx,trace);
};


/*
* (Optimized) Autocorrelate function
*/

namespace gmshfem
{
    namespace function
    {
        template<Physic T_Physic>
        class Autocorrelate final : public WaveMultiFieldOperation<T_Physic>
        {
        private:
            std::vector<unsigned int> _idx;

            mutable std::vector< int > _typeKeys;
            mutable std::vector< unsigned long long > _entityKeys;
            mutable std::vector< std::complex<double> > _dofsValues;
            mutable std::vector< scalar::Precision< std::complex<double> > > _functions;
            mutable std::vector< int > _fsIndex;
            mutable unsigned int _nbrDofsByElements;
            mutable std::vector< double > _gmshJacobians, _gmshDeterminants, _gmshPoints, _gmshGaussPoints;
            mutable std::vector< scalar::Precision< std::complex<double> > > _jacobians, _determinants;
            mutable term::evaluator::FieldEvaluator< std::complex<double>, propertiesOf<T_Physic>::form > _fieldEvaluator;

         public:
            Autocorrelate<T_Physic>(const std::vector<unsigned int>& idx): _idx(idx) {}

            Autocorrelate<T_Physic>(const Autocorrelate &other) :
            WaveMultiFieldOperation< T_Physic >(other), _idx(other._idx) {}

            virtual std::string name() const override
            {
                return "Auto-correlate";
            }

            void operator()(const WaveMultiField<T_Physic>* field, OutputVector< std::complex<double>, Degree::Degree0 > &values, const std::vector< scalar::Precision< std::complex<double> >, numa::allocator< scalar::Precision< std::complex<double> > > > &points, const std::vector< scalar::Precision< std::complex<double> > > &gaussPoints, const int elementType, const std::pair< int, int > &entity) const override
            {
                const unsigned int nbrOfElements = points.size() / gaussPoints.size();
                const unsigned int numThreads = omp::getNumThreads();
                const unsigned int myThreadID = omp::getThreadNum();

#pragma omp single
                _nbrDofsByElements = (*field)[0].getFunctionSpace()->getBasisFunctions(false, _functions, _fsIndex, gaussPoints, elementType, entity);

                std::vector< scalar::Precision< std::complex<double> > > coordinates;
                //omp::setNested(true);
#pragma omp single
                (*field)[0].getFunctionSpace()->getKeys(false, _typeKeys, _entityKeys, coordinates, elementType, entity);;
                //omp::setNested(false);
                const unsigned int begin = (myThreadID * _typeKeys.size()) / numThreads;
                const unsigned int end = ((myThreadID + 1) * _typeKeys.size()) / numThreads;
                const bool needOffset = !(*field)[0].getFunctionSpace()->isConstantByElements();

#pragma omp single
                _dofsValues.resize(_typeKeys.size());

                const unsigned int nbrGaussPoints = gaussPoints.size() / 3;

#pragma omp single
                scalar::copy(_gmshGaussPoints, gaussPoints);

                if(_fieldEvaluator.needJacobians())
                {
#pragma omp single
                    gmsh::model::mesh::preallocateJacobians(elementType, nbrGaussPoints, true, true, false, _gmshJacobians, _gmshDeterminants, _gmshPoints, entity.second);
                    gmsh::model::mesh::getJacobians(elementType, _gmshGaussPoints, _gmshJacobians, _gmshDeterminants, _gmshPoints, entity.second, myThreadID, numThreads);
#pragma omp barrier
#pragma omp single
                    scalar::move(_jacobians, _gmshJacobians);
#pragma omp single
                    scalar::move(_determinants, _gmshDeterminants);
                }

                for(auto it = _idx.begin(); it != _idx.end(); it++)
                {
                    (*field)[*it].getValues(_typeKeys, _entityKeys, _dofsValues, begin, end);
#pragma omp barrier

                    Eigen::MatrixX< scalar::Precision< std::complex<double> > > valueBasis(_fieldEvaluator.size(), _nbrDofsByElements);
#pragma omp for
                    for(unsigned int i = 0; i < nbrOfElements; ++i)
                    {
                        Eigen::VectorX< std::complex<double> > vecDof = Eigen::Map< const Eigen::VectorX< std::complex<double> > >(&(_dofsValues[i * _nbrDofsByElements]), _nbrDofsByElements);

                        for(unsigned int j = 0; j < nbrGaussPoints; ++j)
                        {
                            _fieldEvaluator(valueBasis, &_functions[((needOffset ? _fsIndex[i] * nbrGaussPoints : 0) + j) * field::NumberOfComponentsOfForm< propertiesOf<T_Physic>::form >::value * _nbrDofsByElements], &_jacobians[i * 9 * nbrGaussPoints + j * 9], &_determinants[i * nbrGaussPoints + j], _nbrDofsByElements);

                            values[i * nbrGaussPoints + j] += (valueBasis * vecDof).squaredNorm();
                        }
                    }
                }
            }
        };
    } // namespace function
}; // namespace gmshfem

template<Physic T_Physic>
ModelMonoFunction autocorrelate(const WaveMultiField<T_Physic>& u, const std::vector<unsigned int>& index)
{
    return gmshfem::function::Function< std::complex<double>, gmshfem::Degree::Degree0 >( new gmshfem::function::WaveMultiFieldNode< gmshfem::function::Autocorrelate< T_Physic >,T_Physic >( gmshfem::function::Autocorrelate< T_Physic >(index), &u) );
};

template<Physic T_Physic>
ModelMonoFunction autocorrelate(const WaveMultiField<T_Physic>& u)
{
    std::vector<unsigned int> idx(u.size());
    std::iota(idx.begin(), idx.end(), 0);
    return autocorrelate(u,idx);
};

#endif // H_COMMON_WAVE_CORRELATION
