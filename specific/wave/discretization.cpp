// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard library
//GmshFem library
#include "Exception.h"
#include "Message.h"
//FWI
#include "discretization.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::field;

namespace wave
{

    /*
    * to_functionSpaceType
    */

    /* acoustic */
    template<>
    FunctionSpaceOfForm< propertiesOf<Physic::acoustic>::form > to_functionSpaceType<Physic::acoustic>(std::string str)
    {
        FunctionSpaceOfForm< propertiesOf<Physic::acoustic>::form > out;
        if(str=="Lagrange"){out = FunctionSpaceTypeForm0::Lagrange;}
        else if(str=="HierarchicalH1"){out = FunctionSpaceTypeForm0::HierarchicalH1;}
        else { throw common::Exception("Invalid function space type for acoustics."); }
        return out;
    };

    /* electromagnetic */
    template<>
    FunctionSpaceOfForm< propertiesOf<Physic::electromagnetic>::form > to_functionSpaceType<Physic::electromagnetic>(std::string str)
    {
        FunctionSpaceOfForm< propertiesOf<Physic::electromagnetic>::form > out;
        if(str=="HierarchicalHCurl"){out = FunctionSpaceTypeForm1::HierarchicalHCurl;}
        else { throw common::Exception("Invalid function space type for electromagnetics."); }
        return out;
    };

    /* elastodynamic */
    template<>
    FunctionSpaceOfForm< propertiesOf<Physic::elastodynamic>::form > to_functionSpaceType<Physic::elastodynamic>(std::string str)
    {
        FunctionSpaceOfForm< propertiesOf<Physic::elastodynamic>::form > out;
        if(str=="Lagrange"){out = FunctionSpaceTypeForm0::Lagrange;}
        else if(str=="HierarchicalH1"){out = FunctionSpaceTypeForm0::HierarchicalH1;}
        else { throw common::Exception("Invalid function space type for elastodynamics."); }
        return out;
    };

    /*
    *  class Discretization
    */
    template<Physic T_physic>
    Discretization<T_physic>::Discretization(const GmshFem& gmshFem, std::string suffix)
    {
        msg::print << "Read wave discretization parameters" << msg::endl;
        std::string str_buffer;
        if(!(
                (
                    gmshFem.userDefinedParameter(_functionSpaceDegree, "wave_FunctionSpaceDegree")
                    ||
                    gmshFem.userDefinedParameter(_functionSpaceDegree, "wave_FunctionSpaceDegree"+suffix)
                )
                &&
                (
                    gmshFem.userDefinedParameter(str_buffer, "wave_FunctionSpaceType")
                    ||
                    gmshFem.userDefinedParameter(str_buffer, "wave_FunctionSpaceType"+suffix)
                )
        ))
        {
          throw common::Exception("A discretization parameter could not be found.");
        }
        _functionSpaceType = to_functionSpaceType<T_physic>(str_buffer);
    }

    template class Discretization<Physic::acoustic>;
    template class Discretization<Physic::electromagnetic>;
    template class Discretization<Physic::elastodynamic>;

} // namespace wave
