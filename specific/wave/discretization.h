// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_WAVE_DISCRETIZATION
#define H_WAVE_DISCRETIZATION

//Standard library
#include <string>

//GmshFEM library
#include "GmshFem.h"
#include "FunctionSpace.h"
#include "Message.h"
#include "FieldObject.h"

//GmshFWI
#include "../physic.h"

namespace wave
{
    template<Physic T_physic>
    class Discretization
    {
    private:
        gmshfem::field::FunctionSpaceOfForm< propertiesOf<T_physic>::form > _functionSpaceType;
        unsigned int _functionSpaceDegree;
    public:
        Discretization(const gmshfem::common::GmshFem& gmshFem, std::string suffix = "");
        gmshfem::field::FunctionSpaceOfForm< propertiesOf<T_physic>::form > functionSpaceType() const {return _functionSpaceType;};
        unsigned int functionSpaceDegree() const {return _functionSpaceDegree;};
        void functionSpaceDegree(unsigned int order) {_functionSpaceDegree=order;};
    };

    template<Physic T_physic>
    gmshfem::field::FunctionSpaceOfForm< propertiesOf<T_physic>::form > to_functionSpaceType(std::string str);
}; // namespace wave

#endif // H_DATA_DISCRETIZATION
