// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_WAVE_ELEMENT
#define H_SPECIFIC_WAVE_ELEMENT

//GmshFem Library
#include "FieldInterface.h"
#include "Function.h"
#include "Post.h"

//FWI Library
#include "discretization.h"

/*
* WaveField
*/
/* acoustic and electromagnetic */
template<Physic T_Physic>
class WaveField : public gmshfem::field::Field< std::complex<double> , propertiesOf<T_Physic>::form >
{
public:
    WaveField<T_Physic>(): gmshfem::field::Field< std::complex<double> , propertiesOf<T_Physic>::form >(){};
    WaveField<T_Physic>(std::string name, gmshfem::domain::Domain domain, std::string gmodel, const wave::Discretization<T_Physic>& w_discret): gmshfem::field::Field< std::complex<double> , propertiesOf<T_Physic>::form >(name,domain,w_discret.functionSpaceType(),w_discret.functionSpaceDegree(),gmodel){};
    WaveField<T_Physic>(const gmshfem::field::Field< std::complex<double> , propertiesOf<T_Physic>::form >& other) : gmshfem::field::Field< std::complex<double> , propertiesOf<T_Physic>::form >(other) {};
    void write(std::string name, std::string type="pos", std::string path="") const {gmshfem::post::save(*this,this->domain(),name,type,path);}

    unsigned int degree() const {return this->getFunctionSpace()->order();};
};
template class WaveField<Physic::acoustic>;
template class WaveField<Physic::electromagnetic>;

/* elastodynamic */
#define T_NumFields_Elastodynamic 3

template<>
class WaveField<Physic::elastodynamic> : public gmshfem::field::CompoundField< std::complex<double> , propertiesOf<Physic::elastodynamic>::form , T_NumFields_Elastodynamic >
{
public:
    WaveField<Physic::elastodynamic>(): gmshfem::field::CompoundField< std::complex<double> , propertiesOf<Physic::elastodynamic>::form , T_NumFields_Elastodynamic >(){};
    WaveField<Physic::elastodynamic>(std::string name, gmshfem::domain::Domain domain, std::string gmodel, const wave::Discretization<Physic::elastodynamic>& w_discret): gmshfem::field::CompoundField< std::complex<double> , propertiesOf<Physic::elastodynamic>::form , T_NumFields_Elastodynamic >(name,domain,w_discret.functionSpaceType(),w_discret.functionSpaceDegree(),gmodel){};
    WaveField<Physic::elastodynamic>(const gmshfem::field::CompoundField< std::complex<double> , propertiesOf<Physic::elastodynamic>::form , T_NumFields_Elastodynamic >& other) : gmshfem::field::CompoundField< std::complex<double> , propertiesOf<Physic::elastodynamic>::form , T_NumFields_Elastodynamic >(other) {};
    void write(std::string name, std::string type="pos", std::string path="") const {gmshfem::post::save(*this,this->domain(),name,type,path);}

    unsigned int degree() const {return this->getFunctionSpace()->order();};
};

/*
* WaveFunction
*/
template<Physic T_Physic>
class WaveFunction final : public gmshfem::function::Function< std::complex<double> , propertiesOf<T_Physic>::degree >
{
    public:
      WaveFunction<T_Physic>(const gmshfem::function::Function< std::complex<double> , propertiesOf<T_Physic>::degree >& other) : gmshfem::function::Function< std::complex<double> , propertiesOf<T_Physic>::degree >(other) {};
};

#endif // H_SPECIFIC_WAVE_ELEMENT
