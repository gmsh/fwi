// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard library

//GmshFem library
#include "Message.h"
#include "Exception.h"
#include "Function.h"
#include "Post.h"

//GmshFWI Library
#include "helmholtz.h"
#include "../../model/parametrization/helmholtz.h"
#include "../../configuration/green0_preconditioner.h"
#include "../correlation.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::equation;
using namespace gmshfem::function;
using namespace gmshfem::post;


static const std::complex< double > im = std::complex< double >(0., 1.);

/*
* Helmholtz (acoustics)
*/
#define u ws.state(Type::FS)
#define du ws.state(Type::PFS)
#define lambda ws.state(Type::AS)
#define dlambda ws.state(Type::PAS)

#define nu0 _config->m0_natural(_pulsation)[0]
#define K0 _config->m0_natural(_pulsation)[1]

#define nu ms.state(Type::FS)[0]
#define K ms.state(Type::FS)[1]

#define dnu ms.state(Type::PFS)[0]
#define dK ms.state(Type::PFS)[1]

#define _w_degree _v.degree()

namespace helmholtz
{
    Equation::Equation(unsigned int f_idx,double pulsation,const ConfigurationInterface* const config, const wave::Discretization<Physic::acoustic>& w_discret,const gmshfem::common::GmshFem& gmshFem, std::string suffix) : DifferentialEquationInterface<Physic::acoustic>(config,w_discret,gmshFem,suffix), _f_idx(f_idx), _pulsation(pulsation)
    {
        unsigned int gn = 0;
        if(!gmshFem.userDefinedParameter(gn, "gaussNewton"))
        {
            msg::warning << "Gauss-Newton approximation (equation) flag could not be found. Approximation is not used (default)." << msg::endl;
        }
        _gaussNewton = ((bool) gn);

        unsigned int gp = 1;
        if(!gmshFem.userDefinedParameter(gp, "equation_greenPreconditioner"))
        {
            msg::warning << "Green preconditionner (equation) flag could not be found. Green preconditioner is used (default)." << msg::endl;
        }
        _greenPreconditioner = ((bool) gp);

        if(!_greenPreconditioner)
        {
            if(!gmshFem.userDefinedParameter(_const_preconditioner, "equation_const_preconditioner"))
            {
                throw Exception("Impossible to find constant preconditioner (equation)");
            }
        }
    }

    bool Equation::modelIsObsolete()
    {
        _systemIsUpToDate=false;
        _systemIsFactorized=false;
        _geAreUpToDate=false;
        _grAreUpToDate=false;
        return _greenPreconditioner;
    }

    bool Equation::compatible(const ParametrizationInterface* const parametrization) const
    {
        return
        (
            nullptr != dynamic_cast<const natural::Parametrization* const>(parametrization)
            /* ||
            nullptr != dynamic_cast<const density_bulkmodulus::Parametrization* const>(parametrization) */
        );
    }
    double Equation::wavelength(std::vector<std::complex<double>> mc) const
    {
        return 2. * M_PI / _pulsation * std::real(std::sqrt(mc[0]/mc[1]));
    }
    void Equation::setLHS(const ModelStateEvaluator& ms)
    {
        _formulation.integral(-1.*nu0*grad(dof(_v)), grad(tf(_v)), _config->model_known(Support::BLK),integrationType(_integrationDegreeBlk));
        _formulation.integral(_pulsation * _pulsation * K0 * dof(_v), tf(_v), _config->model_known(Support::BLK), integrationType(_integrationDegreeBlk));
        _formulation.integral(-1. * im * _pulsation * sqrt(K0*nu0) * dof(_v), tf(_v), _config->model_known(Support::BND), integrationType(_integrationDegreeBnd)); //-1 from sign conventions
        if(!_config->model_unknown(Support::BLK).isEmpty())
        {
            _formulation.integral(-1.*nu*grad(dof(_v)), grad(tf(_v)), _config->model_unknown(Support::BLK),integrationType(_integrationDegreeBlk));
            _formulation.integral(_pulsation * _pulsation * K * dof(_v), tf(_v), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
        }
        if(!_config->model_unknown(Support::BND).isEmpty())
        {
            _formulation.integral(-1. * im * _pulsation * sqrt(K*nu) *  dof(_v), tf(_v), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd)); //-1 from sign conventions
        }
    }
    void Equation::setRHS(Type type, unsigned int s, const DataStateEvaluator<Physic::acoustic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<Physic::acoustic>& ws)
    {
        switch (type)
        {
            case Type::FS:
                for (unsigned int e = 0; e < _config->ne(s); e++)
                {
                  _formulation.integral(-1.,tf(_v), _config->emitter(s,e), integrationType(0));
                }
                break;
            case Type::AS:
                for (unsigned int r = 0; r < _config->nr(s); r++)
                {
                  _formulation.integral(-ds.state(Type::AS).value(_f_idx,s,r),tf(_v), _config->receiver(s,r), integrationType(0));
                }
                break;
            case Type::PFS:
                _formulation.integral(-1.*dnu*grad(u[s]), grad(tf(_v)), _config->model_unknown(Support::BLK),integrationType(_integrationDegreeBlk));
                _formulation.integral(u[s] * _pulsation * _pulsation * dK, tf(_v), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                if(_boundary)
                {
                    _formulation.integral(-im/2. * u[s] * _pulsation * (sqrt(K/nu)*dnu + sqrt(nu/K)*dK), tf(_v), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd)); //-1 from sign conventions
                }
                break;
            case Type::PAS:
                for (unsigned int r = 0; r < _config->nr(s); r++)
                {
                    _formulation.integral(-ds.state(Type::PAS).value(_f_idx,s,r), tf(_v), _config->receiver(s,r), integrationType(0));
                }
                if(!_gaussNewton)
                {
                    _formulation.integral(-1.*dnu*grad(lambda[s]), grad(tf(_v)), _config->model_unknown(Support::BLK),integrationType(_integrationDegreeBlk));
                    _formulation.integral(lambda[s] * _pulsation * _pulsation * dK, tf(_v), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                    if(_boundary)
                    {
                        _formulation.integral(-im/2. * lambda[s] * _pulsation * (sqrt(K/nu)*dnu + sqrt(nu/K)*dK), tf(_v), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd)); //-1 from sign conventions
                    }
                }
                break;
            case Type::DS: default:
                throw Exception("Impossible to set RHS other than F,PF,A,PA");
                break;
        }
    }
    void Equation::setGreenRHS(unsigned int p)
    {
        _formulation.integral(-1.,tf(_v), _config->point(p), integrationType(0));
    }

    Sensitivity Equation::update_sensitivity(Order order, Support support, const DataStateEvaluator<Physic::acoustic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<Physic::acoustic>& ws)
    {
        ModelFunction fct(2,0.);
        switch (order)
        {
            case Order::FST:
                switch (support)
                {
                    case Support::BLK:
                        fct[0] =  correlate_derivative(u,lambda,DerivativeCorrelationTrace::GRAD);
                        fct[1] =  - _pulsation * _pulsation * correlate(u,lambda);
                        break;
                    case Support::BND:
                        fct[0] =  im/2. * _pulsation * sqrt(K/nu) * correlate(u,lambda);
                        fct[1] =  im/2. * _pulsation * sqrt(nu/K) * correlate(u,lambda);
                        break;
                }
                break;
            case Order::SCD:
                switch (support)
                {
                    case Support::BLK:
                        if(!_gaussNewton)
                        {
                            fct[0] =  correlate_derivative(du,lambda,DerivativeCorrelationTrace::GRAD)
                            + correlate_derivative(u,dlambda,DerivativeCorrelationTrace::GRAD);
                            fct[1] =  - _pulsation * _pulsation *
                            (
                                correlate(du,lambda)
                                +
                                correlate(u,dlambda)
                            );
                        }
                        else
                        {
                            fct[0] = correlate_derivative(u,dlambda,DerivativeCorrelationTrace::GRAD);
                            fct[1] =  - _pulsation * _pulsation * correlate(u,dlambda);
                        }
                        break;
                    case Support::BND:
                        if(!_gaussNewton)
                        {
                            fct[0] =  im/2. * _pulsation * sqrt(K/nu) *
                            (
                                (dK/K - dnu/nu)/2.*correlate(u,lambda)
                                +correlate(du,lambda)
                                +correlate(u,dlambda)
                            );
                            fct[1] =  im/2. * _pulsation * sqrt(nu/K) *
                            (
                                (dnu/nu - dK/K)/2.*correlate(u,lambda)
                                +correlate(du,lambda)
                                +correlate(u,dlambda)
                            );
                        }
                        else
                        {
                            fct[0] =  im/2. * _pulsation * sqrt(K/nu) *
                            (
                                correlate(u,dlambda)
                            );
                            fct[1] =  im/2. * _pulsation * sqrt(nu/K) *
                            (
                                correlate(u,dlambda)
                            );
                        }
                        break;
                }
                break;
            case Order::DIAG:
                switch (support)
                {
                    case Support::BLK:
                        return Sensitivity(2,_const_preconditioner);
                    case Support::BND:
                        break;
                }
                break;
        }
        return fct;
    }
};
