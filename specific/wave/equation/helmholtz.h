// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_WAVE_EQUATION_HELMHOLTZ
#define H_SPECIFIC_WAVE_EQUATION_HELMHOLTZ

//GmshFem Library
#include "GmshFem.h"
//FWI Library
#include "../../../common/wave/equation/equation.h"

/*
* Helmholtz (acoustics)
*/
namespace helmholtz
{
    class Equation final: public DifferentialEquationInterface<Physic::acoustic>
    {
    private:
        const unsigned int _f_idx;
        double _pulsation;
        bool _gaussNewton;
        bool _greenPreconditioner;
        double _const_preconditioner;
        using EquationInterface<Physic::acoustic>::_boundary;
    public:
        Equation(unsigned int f_idx, double pulsation,const ConfigurationInterface* const config, const wave::Discretization<Physic::acoustic>& w_discret,const gmshfem::common::GmshFem& gmshFem, std::string suffix = "");

        virtual bool compatible(const ParametrizationInterface* const parametrization) const;
        double wavelength(std::vector<std::complex<double>> mc) const;

        //Model related
        virtual Sensitivity update_sensitivity(Order order, Support support, const DataStateEvaluator<Physic::acoustic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<Physic::acoustic>& ws);
    private:
        virtual bool modelIsObsolete() override;//Returns true, if sensitivity[DIAG] depends on model
        double wavelength(double m_ref);
        virtual void setLHS(const ModelStateEvaluator& ms);
        virtual void setRHS(Type type,unsigned int s, const DataStateEvaluator<Physic::acoustic>& d, const ModelStateEvaluator& ms, const WaveStateEvaluator<Physic::acoustic>& ws);
        virtual void setGreenRHS(unsigned int p);
    };
};

#endif //H_SPECIFIC_WAVE_EQUATION_HELMHOLTZ
