// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard library

//GmshFem library
#include "Message.h"
#include "Exception.h"
#include "Function.h"
#include "Post.h"

//GmshFWI Library
#include "maxwell.h"
#include "../../model/parametrization/simplewave.h"
#include "../../configuration/green0_preconditioner.h"
#include "../correlation.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::equation;
using namespace gmshfem::function;
using namespace gmshfem::post;


static const std::complex< double > im = std::complex< double >(0., 1.);

/*
* Maxwell (electromagnetic)
*/
#define u ws.state(Type::FS)
#define du ws.state(Type::PFS)
#define lambda ws.state(Type::AS)
#define dlambda ws.state(Type::PAS)

#define m0 _config->m0_natural(_pulsation)[0]
#define m ms.state(Type::FS)[0]
#define dm ms.state(Type::PFS)[0]

#define _w_degree _v.degree()

namespace maxwell
{
    Equation::Equation(unsigned int f_idx, double pulsation,const ConfigurationInterface* const config, const wave::Discretization<Physic::electromagnetic>& w_discret,const gmshfem::common::GmshFem& gmshFem, std::string suffix) : DifferentialEquationInterface<Physic::electromagnetic>(config,w_discret,gmshFem,suffix), _f_idx(f_idx), _pulsation(pulsation)
    {
        unsigned int gn = 0;
        if(!gmshFem.userDefinedParameter(gn, "gaussNewton"))
        {
            msg::warning << "Gauss-Newton approximation (equation) flag could not be found. Approximation is not used (default)." << msg::endl;
        }
        _gaussNewton = ((bool) gn);

        if(!gmshFem.userDefinedParameter(_const_preconditioner, "equation_const_preconditioner"))
        {
            msg::warning << "Impossible to find constant preconditioner (equation). Dafault value (1) is used." << msg::endl;
            _const_preconditioner=1.;
        }
    }

    bool Equation::modelIsObsolete()
    {
        _systemIsUpToDate=false;
        _systemIsFactorized=false;
        _geAreUpToDate=false;
        _grAreUpToDate=false;
        return false;
    }

    bool Equation::compatible(const ParametrizationInterface* const parametrization) const
    {
        return
        (
            nullptr != dynamic_cast<const simplewave::natural::Parametrization* const>(parametrization)
            ||
            nullptr != dynamic_cast<const simplewave::slowness2_real::Parametrization* const>(parametrization)
            ||
            nullptr != dynamic_cast<const simplewave::ln_slowness2_real::Parametrization* const>(parametrization)
            ||
            nullptr != dynamic_cast<const simplewave::permcond_bireal::Parametrization* const>(parametrization)
        );
    }
    double Equation::wavelength(std::vector<std::complex<double>> mc) const
    {
        return 2. * M_PI / (_pulsation * std::real(std::sqrt(mc[0])));
    }
    void Equation::setLHS(const ModelStateEvaluator& ms)
    {
        _formulation.integral(-1.*curl(dof(_v)), curl(tf(_v)), _config->wave_omega(Support::BLK),integrationType(2*_w_degree));
        _formulation.integral(_pulsation * _pulsation * m0 * dof(_v), tf(_v), _config->model_known(Support::BLK), integrationType(_integrationDegreeBlk));
        VectorFunction< std::complex< double > > n = normal< std::complex< double > >();
        _formulation.integral(-im * _pulsation * sqrt(m0) * n % (dof(_v) % n), tf(_v), _config->model_known(Support::BND), integrationType(_integrationDegreeBnd)); //-1 from sign conventions

        if(!_config->model_unknown(Support::BLK).isEmpty())
        {
            _formulation.integral(_pulsation * _pulsation * m * dof(_v), tf(_v), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
        }
        if(!_config->model_unknown(Support::BND).isEmpty())
        {
            _formulation.integral(-im * _pulsation * sqrt(m)* n % (dof(_v) % n), tf(_v), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd)); //-1 from sign conventions
        }
    }
    void Equation::setRHS(Type type, unsigned int s, const DataStateEvaluator<Physic::electromagnetic>& d, const ModelStateEvaluator& ms, const WaveStateEvaluator<Physic::electromagnetic>& ws)
    {
        switch (type)
        {
            case Type::FS:
                for (unsigned int e = 0; e < _config->ne(s); e++)
                {
                  _formulation.integral(-VectorFunction< std::complex< double > >(_config->emitter_polarization(s,e)),tf(_v), _config->emitter(s,e), integrationType(1));
                }
                break;
            case Type::AS:
                for (unsigned int r = 0; r < _config->nr(s); r++)
                {
                  _formulation.integral(-VectorFunction< std::complex< double > >(d.state(Type::AS).value(_f_idx,s,r)),tf(_v), _config->receiver(s,r), integrationType(1));
                }
                break;
            case Type::PFS:
                _formulation.integral( u[s] * _pulsation * _pulsation * dm, tf(_v), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                if(_boundary)
                {
                    VectorFunction< std::complex< double > > n = normal< std::complex< double > >();
                    _formulation.integral(-im * _pulsation  / 2. / sqrt(m)* dm * n % (u[s] % n), tf(_v), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd)); //-1 from sign conventions
                }
                break;
            case Type::PAS:
                for (unsigned int r = 0; r < _config->nr(s); r++)
                {
                    _formulation.integral(-VectorFunction< std::complex< double > >(d.state(Type::PAS).value(_f_idx,s,r)), tf(_v), _config->receiver(s,r), integrationType(0));
                }
                if(!_gaussNewton)
                {
                    _formulation.integral( lambda[s] * _pulsation * _pulsation * dm, tf(_v), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                    if(_boundary)
                    {
                        VectorFunction< std::complex< double > > n = normal< std::complex< double > >();
                        _formulation.integral(-im * _pulsation  / 2. / sqrt(m)* dm * n % (lambda[s] % n), tf(_v), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd)); //-1 from sign conventions
                    }
                }
                break;
            case Type::DS: default:
                throw Exception("Impossible to set RHS other than F,PF,A,PA");
                break;
        }
    }
    void Equation::setGreenRHS(unsigned int p)
    {
        throw Exception("setGreenRHS not implemented for Maxwell");
    }

    Sensitivity Equation::update_sensitivity(Order order, Support support, const DataStateEvaluator<Physic::electromagnetic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<Physic::electromagnetic>& ws)
    {
        ModelMonoFunction fct = 0.;
        switch (order)
        {
            case Order::FST:
                switch (support)
                {
                    case Support::BLK:
                        fct =  - _pulsation * _pulsation * correlate(u,lambda);
                        break;
                    case Support::BND:
                        fct =  im * _pulsation / 2. / sqrt(m) * correlate(u,lambda,CorrelationTrace::TANGENT_T);
                        break;
                }
                break;
            case Order::SCD:
                switch (support)
                {
                    case Support::BLK:
                        if(!_gaussNewton)
                        {
                            fct =  - _pulsation * _pulsation *
                            (
                                correlate(u,dlambda)
                                +correlate(du,lambda)
                            );
                        }
                        else
                        {
                            fct =  - _pulsation * _pulsation *
                            (
                                correlate(u,dlambda)
                            );
                        }
                        break;
                    case Support::BND:
                        if(!_gaussNewton)
                        {
                            fct =  im * _pulsation / 2. /  sqrt(m) *
                            (
                                correlate(u,dlambda,CorrelationTrace::TANGENT_T)
                                +correlate(du,lambda,CorrelationTrace::TANGENT_T)
                                -dm / 2. / m * correlate(u,lambda,CorrelationTrace::TANGENT_T)
                            );
                        }
                        else
                        {
                            fct =  im * _pulsation / 2. /  sqrt(m) *
                            (
                                correlate(u,dlambda,CorrelationTrace::TANGENT_T)
                            );
                        }
                        break;
                }
                break;
            case Order::DIAG:
                switch (support)
                {
                    case Support::BLK:
                        fct = _const_preconditioner;
                        break;
                    case Support::BND:
                        fct = 0;
                        break;
                }
                break;
        }
        return Sensitivity(1,fct);
    }
};
