// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

//Standard library

//GmshFEM library
#include "Message.h"
#include "Exception.h"
#include "Function.h"
#include "Post.h"
#include "Function.h"

//GmshFWI Library
#include "navier.h"
#include "../../model/parametrization/navier.h"
#include "../correlation.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::equation;
using namespace gmshfem::function;
using namespace gmshfem::post;


static const std::complex< double > im = std::complex< double >(0., 1.);

/*
* Maxwell (elastodynamic)
*/
#define u ws.state(Type::FS)
#define du ws.state(Type::PFS)
#define lambda ws.state(Type::AS)
#define dlambda ws.state(Type::PAS)

#define rho0 _config->m0_natural(_pulsation)[0]
#define lame_la0 _config->m0_natural(_pulsation)[1]
#define lame_mu0 _config->m0_natural(_pulsation)[2]

#define rho ms.state(Type::FS)[0]
#define lame_la ms.state(Type::FS)[1]
#define lame_mu ms.state(Type::FS)[2]
#define vp sqrt((lame_la+2.*lame_mu)/rho)
#define vs sqrt(lame_mu/rho)

#define drho ms.state(Type::PFS)[0]
#define dlame_la ms.state(Type::PFS)[1]
#define dlame_mu ms.state(Type::PFS)[2]

#define _w_degree _v.degree()

namespace navier
{
    Equation::Equation(unsigned int f_idx,double pulsation,const ConfigurationInterface* const config, const wave::Discretization<Physic::elastodynamic>& w_discret,const gmshfem::common::GmshFem& gmshFem, std::string suffix) : DifferentialEquationInterface<Physic::elastodynamic>(config,w_discret,gmshFem,suffix), _f_idx(f_idx), _pulsation(pulsation)
    {
        unsigned int gn = 0;
        if(!gmshFem.userDefinedParameter(gn, "gaussNewton"))
        {
            msg::warning << "Gauss-Newton approximation (equation) flag could not be found. Approximation is not used (default)." << msg::endl;
        }
        _gaussNewton = ((bool) gn);

        if(!gmshFem.userDefinedParameter(_const_preconditioner, "equation_const_preconditioner"))
        {
            msg::warning << "Impossible to find constant preconditioner (equation). Dafault value (1) is used." << msg::endl;
            _const_preconditioner=1.;
        }
    }

    bool Equation::modelIsObsolete()
    {
        _systemIsUpToDate=false;
        _systemIsFactorized=false;
        _geAreUpToDate=false;
        _grAreUpToDate=false;
        return false;
    }

    bool Equation::compatible(const ParametrizationInterface* const parametrization) const
    {
        return
        (
            nullptr != dynamic_cast<const navier::natural::Parametrization* const>(parametrization)
        );
    }
    double Equation::wavelength(std::vector<std::complex<double>> mc) const
    {
        // p-wavelength
        double vpc = std::real(std::sqrt((mc[1]+2.*mc[2])/mc[0]));
        return 2. * M_PI / _pulsation * vpc;
        // s-wavelength
        //double vs = std::real(std::sqrt(mc[2]/mc[0]));
        //return 2. * M_PI / _pulsation * vs;
    }
    void Equation::setLHS(const ModelStateEvaluator& ms)
    {
        TensorFunction< std::complex< double > > C0_xx = tensor< std::complex< double > >
        (
            lame_la0+2.*lame_mu0,0.,0.,
            0., lame_la0, 0.,
            0., 0., 0.
        );
        TensorFunction< std::complex< double > > C0_xy = tensor< std::complex< double > >
        (
            0., lame_mu0, 0.,
            lame_mu0, 0., 0.,
            0., 0., 0.
        );
        TensorFunction< std::complex< double > > C0_yx = tensor< std::complex< double > >
        (
            0., lame_mu0, 0.,
            lame_mu0, 0., 0.,
            0., 0., 0.
        );
        TensorFunction< std::complex< double > > C0_yy = tensor< std::complex< double > >
        (
            lame_la0, 0., 0.,
            0., lame_la0+2.*lame_mu0, 0.,
            0., 0., 0.
        );
        TensorFunction< std::complex< double > > null = tensor< std::complex< double > >
        (
            0., 0., 0.,
            0., 0., 0.,
            0., 0., 0.
        );

        TensorFunction< std::complex< double >, 4 > C0 = tensor< std::complex< double >, 4 >
        (
            C0_xx, C0_xy, null,
            C0_yx, C0_yy, null,
            null, null, null
        );

        ScalarFunction< std::complex< double > > nX = xComp(normal< std::complex< double > >());
        ScalarFunction< std::complex< double > > nY = yComp(normal< std::complex< double > >());
        TensorFunction< std::complex< double > > normal_proj = tensor< std::complex< double > >
        (
            nX * nX, nX * nY, 0.,
            nX * nY, nY * nY, 0.,
             0., 0., 0.
         );
         TensorFunction< std::complex< double > > tangential_proj = tensor< std::complex< double > >
         (
             nY * nY, - (nX * nY), 0.,
             - (nY * nX), nX * nX, 0.,
             0., 0., 0.
         );

         _formulation.integral(C0 * grad(dof(_v)), -grad(tf(_v)), _config->model_known(Support::BLK), integrationType(_integrationDegreeBlk));
         _formulation.integral(_pulsation * _pulsation * rho0 * dof(_v), tf(_v), _config->model_known(Support::BLK), integrationType(_integrationDegreeBlk));

         // Lysmer Kuhlemeyer ABC
         //p-wave
         _formulation.integral(-im * _pulsation * sqrt(rho0 * (lame_la0 + 2. * lame_mu0) ) * normal_proj * dof(_v), tf(_v), _config->model_known(Support::BND), integrationType(_integrationDegreeBnd));
         //s-wave
         _formulation.integral(-im * _pulsation * sqrt(rho0 * lame_mu0) * tangential_proj * dof(_v), tf(_v), _config->model_known(Support::BND), integrationType(_integrationDegreeBnd));


         if(!_config->model_unknown(Support::BLK).isEmpty())
         {
             TensorFunction< std::complex< double > > C_xx = tensor< std::complex< double > >
             (
                 lame_la+2.*lame_mu,0.,0.,
                 0., lame_la, 0.,
                 0., 0., 0.
             );
             TensorFunction< std::complex< double > > C_xy = tensor< std::complex< double > >
             (
                 0., lame_mu, 0.,
                 lame_mu, 0., 0.,
                 0., 0., 0.
             );
             TensorFunction< std::complex< double > > C_yx = tensor< std::complex< double > >
             (
                 0., lame_mu, 0.,
                 lame_mu, 0., 0.,
                 0., 0., 0.
             );
             TensorFunction< std::complex< double > > C_yy = tensor< std::complex< double > >
             (
                 lame_la, 0., 0.,
                 0., lame_la+2.*lame_mu, 0.,
                 0., 0., 0.
             );

             TensorFunction< std::complex< double >, 4 > C = tensor< std::complex< double >, 4 >
             (
                 C_xx, C_xy, null,
                 C_yx, C_yy, null,
                 null, null, null
             );
              _formulation.integral(C * grad(dof(_v)), -grad(tf(_v)), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
             _formulation.integral(_pulsation * _pulsation * rho * dof(_v), tf(_v), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
        }
        if(!_config->model_unknown(Support::BND).isEmpty())
        {
            // Lysmer Kuhlemeyer ABC
            //p-wave
            _formulation.integral(-im * _pulsation * sqrt(rho * (lame_la + 2. * lame_mu) ) * normal_proj * dof(_v), tf(_v), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd));
            //s-wave
            _formulation.integral(-im * _pulsation * sqrt(rho * lame_mu) * tangential_proj * dof(_v), tf(_v), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd));
        }
    }
    void Equation::setRHS(Type type, unsigned int s, const DataStateEvaluator<Physic::elastodynamic>& d, const ModelStateEvaluator& ms, const WaveStateEvaluator<Physic::elastodynamic>& ws)
    {
        switch (type)
        {
            case Type::FS:
                for (unsigned int e = 0; e < _config->ne(s); e++)
                {
                  _formulation.integral(-VectorFunction< std::complex< double > >(_config->emitter_polarization(s,e)),tf(_v), _config->emitter(s,e), integrationType(1));
                }
                break;
            case Type::AS:
                for (unsigned int r = 0; r < _config->nr(s); r++)
                {
                  _formulation.integral(-VectorFunction< std::complex< double > >(d.state(Type::AS).value(_f_idx,s,r)),tf(_v), _config->receiver(s,r), integrationType(1));
                }
                break;
            case Type::PFS:case Type::PAS:
            {
                TensorFunction< std::complex< double > > null = tensor< std::complex< double > >
                (
                    0., 0., 0.,
                    0., 0., 0.,
                    0., 0., 0.
                );

                TensorFunction< std::complex< double > > dC_xx = tensor< std::complex< double > >
                (
                    dlame_la+2.*dlame_mu,0.,0.,
                    0., dlame_la, 0.,
                    0., 0., 0.
                );
                TensorFunction< std::complex< double > > dC_xy = tensor< std::complex< double > >
                (
                    0., dlame_mu, 0.,
                    dlame_mu, 0., 0.,
                    0., 0., 0.
                );
                TensorFunction< std::complex< double > > dC_yx = tensor< std::complex< double > >
                (
                    0., dlame_mu, 0.,
                    dlame_mu, 0., 0.,
                    0., 0., 0.
                );
                TensorFunction< std::complex< double > > dC_yy = tensor< std::complex< double > >
                (
                    dlame_la, 0., 0.,
                    0., dlame_la+2.*dlame_mu, 0.,
                    0., 0., 0.
                );

                TensorFunction< std::complex< double >, 4 > dC = tensor< std::complex< double >, 4 >
                (
                    dC_xx, dC_xy, null,
                    dC_yx, dC_yy, null,
                    null, null, null
                );

                ScalarFunction< std::complex< double > > nX = xComp(normal< std::complex< double > >());
                ScalarFunction< std::complex< double > > nY = yComp(normal< std::complex< double > >());
                TensorFunction< std::complex< double > > normal_proj = tensor< std::complex< double > >
                (
                    nX * nX, nX * nY, 0.,
                    nX * nY, nY * nY, 0.,
                    0., 0., 0.
                );
                TensorFunction< std::complex< double > > tangential_proj = tensor< std::complex< double > >
                (
                    nY * nY, - (nX * nY), 0.,
                    - (nY * nX), nX * nX, 0.,
                    0., 0., 0.
                );
                if(type==Type::PFS)
                {
                    _formulation.integral( dC * -grad(u[s]), grad(tf(_v)), _config->model_unknown(Support::BLK),integrationType(_integrationDegreeBlk));

                    _formulation.integral(_pulsation * _pulsation * drho * u[s], tf(_v), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                    if(_boundary)
                    {
                        // Lysmer Kuhlemeyer ABC
                        // p-wave
                        _formulation.integral(-im * _pulsation * (vp*drho/2. + dlame_la/vp/2. + dlame_mu/vp) * normal_proj * u[s], tf(_v), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd));
                        //s-wave
                        _formulation.integral(-im * _pulsation * (vs/2.*drho + dlame_mu/vs/2.) * tangential_proj * u[s], tf(_v), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd));
                    }
                }
                if(type==Type::PAS)
                {
                    for (unsigned int r = 0; r < _config->nr(s); r++)
                    {
                        _formulation.integral(-VectorFunction< std::complex< double > >(d.state(Type::PAS).value(_f_idx,s,r)), tf(_v), _config->receiver(s,r), integrationType(1));
                    }
                    if(!_gaussNewton)
                    {
                        _formulation.integral(dC * -grad(lambda[s]), grad(tf(_v)), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                        _formulation.integral(_pulsation * _pulsation * drho * lambda[s], tf(_v), _config->model_unknown(Support::BLK), integrationType(_integrationDegreeBlk));
                        if(_boundary)
                        {
                            // Lysmer Kuhlemeyer ABC
                            // p-wave
                            _formulation.integral(-im * _pulsation * (vp*drho/2. + dlame_la/vp/2. + dlame_mu/vp) * normal_proj * lambda[s], tf(_v), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd));
                            //s-wave
                            _formulation.integral(-im * _pulsation * (vs/2.*drho + dlame_mu/vs/2.) * tangential_proj * lambda[s], tf(_v), _config->model_unknown(Support::BND), integrationType(_integrationDegreeBnd));
                        }
                    }
                }
                break;
            }
            case Type::DS: default:
                throw Exception("Impossible to set RHS other than F,PF,A,PA");
                break;
        }
    }
    void Equation::setGreenRHS(unsigned int p)
    {
        throw Exception("setGreenRHS not implemented for Maxwell");
    }

    Sensitivity Equation::update_sensitivity(Order order, Support support, const DataStateEvaluator<Physic::elastodynamic>& ds, const ModelStateEvaluator& ms, const WaveStateEvaluator<Physic::elastodynamic>& ws)
    {
        ModelFunction fct(3,0.);
        switch (order)
        {
            case Order::FST:
                switch (support)
                {
                    case Support::BLK:
                        fct[0] =  - _pulsation * _pulsation * correlate(u,lambda);

                        fct[1] = correlate_derivative(u,lambda,DerivativeCorrelationTrace::DIV);

                        fct[2] = 2.*correlate_derivative(u,lambda,DerivativeCorrelationTrace::SYM_GRAD);

                        break;
                    case Support::BND:
                        fct[0] =  im * _pulsation / 2. *
                        (
                                vp*correlate(u,lambda,CorrelationTrace::NORMAL)
                                +
                                vs*correlate(u,lambda,CorrelationTrace::TANGENT_T)
                        );
                        fct[1] =  im * _pulsation / 2. *
                        (
                                1./vp*correlate(u,lambda,CorrelationTrace::NORMAL)
                        );
                        fct[2] =  im * _pulsation / 2. *
                        (
                                2./vp*correlate(u,lambda,CorrelationTrace::NORMAL)
                                +
                                1./vs*correlate(u,lambda,CorrelationTrace::TANGENT_T)
                        );

                        break;
                }
                break;
            case Order::SCD:
                switch (support)
                {
                    case Support::BLK:
                        if(!_gaussNewton)
                        {
                            fct[0] =  - _pulsation * _pulsation *
                            (
                                correlate(du,lambda)
                                +correlate(u,dlambda)
                            );

                            fct[1] = correlate_derivative(du,lambda,DerivativeCorrelationTrace::DIV)
                            +correlate_derivative(u,dlambda,DerivativeCorrelationTrace::DIV);

                            fct[2] = 2.*correlate_derivative(du,lambda,DerivativeCorrelationTrace::SYM_GRAD)
                            +2.*correlate_derivative(u,dlambda,DerivativeCorrelationTrace::SYM_GRAD);
                        }
                        else
                        {
                            fct[0] =  - _pulsation * _pulsation *
                            (
                                correlate(u,dlambda)
                            );

                            fct[1] = correlate_derivative(u,dlambda,DerivativeCorrelationTrace::DIV);

                            fct[2] = 2.*correlate_derivative(u,dlambda,DerivativeCorrelationTrace::SYM_GRAD);
                        }
                        break;
                    case Support::BND:
                    {
                        ScalarFunction< std::complex< double > > dvp = (-0.5*vp*drho + 0.5*dlame_la/vp +dlame_mu/vp)/rho;
                        ScalarFunction< std::complex< double > > dvs = (-0.5*vs*drho + 0.5*dlame_mu/vs)/rho;
                        if(!_gaussNewton)
                        {
                            fct[0] =  im * _pulsation / 2. *
                            (
                                    dvp*correlate(u,lambda,CorrelationTrace::NORMAL)
                                    +
                                    vp*correlate(du,lambda,CorrelationTrace::NORMAL)
                                    +
                                    vp*correlate(u,dlambda,CorrelationTrace::NORMAL)
                                    +
                                    dvs*correlate(u,lambda,CorrelationTrace::TANGENT_T)
                                    +
                                    vs*correlate(du,lambda,CorrelationTrace::TANGENT_T)
                                    +
                                    vs*correlate(u,dlambda,CorrelationTrace::TANGENT_T)
                            );
                            fct[1] =  im * _pulsation / 2. *
                            (
                                    -1./pow(vp,2)*dvp*correlate(u,lambda,CorrelationTrace::NORMAL)
                                    +
                                    1./vp*correlate(du,lambda,CorrelationTrace::NORMAL)
                                    +
                                    1./vp*correlate(u,dlambda,CorrelationTrace::NORMAL)
                            );
                            fct[2] =  im * _pulsation / 2. *
                            (
                                    -2./pow(vp,2)*dvp*correlate(u,lambda,CorrelationTrace::NORMAL)
                                    +
                                    2./vp*correlate(du,lambda,CorrelationTrace::NORMAL)
                                    +
                                    2./vp*correlate(u,dlambda,CorrelationTrace::NORMAL)
                                    +
                                    -1./pow(vs,2)*dvs*correlate(u,lambda,CorrelationTrace::TANGENT_T)
                                    +
                                    1./vs*correlate(du,lambda,CorrelationTrace::TANGENT_T)
                                    +
                                    1./vs*correlate(u,dlambda,CorrelationTrace::TANGENT_T)
                            );
                        }
                        else
                        {
                            fct[0] =  im * _pulsation / 2. *
                            (
                                    vp*correlate(u,dlambda,CorrelationTrace::NORMAL)
                                    +
                                    vs*correlate(u,dlambda,CorrelationTrace::TANGENT_T)
                            );
                            fct[1] =  im * _pulsation / 2. *
                            (
                                    vp*correlate(u,dlambda,CorrelationTrace::NORMAL)
                            );
                            fct[2] =  im * _pulsation / 2. *
                            (
                                    2./vp*correlate(u,dlambda,CorrelationTrace::NORMAL)
                                    +
                                    1./vs*correlate(u,dlambda,CorrelationTrace::TANGENT_T)
                            );
                        }
                        break;
                    }
                }
                break;
            case Order::DIAG:
                switch (support)
                {
                    case Support::BLK:
                        return Sensitivity(3,_const_preconditioner);
                        break;
                    case Support::BND:
                        break;
                }
                break;
        }
        return fct;
    }
};
