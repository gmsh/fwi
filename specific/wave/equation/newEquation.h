// GmshFWI - Copyright (C) 2019-2023, X. Adriaens, C. Geuzaine, Université de Liège
// See the LICENSE.txt file for license information. Please report all issues on https://gitlab.onelab.info/gmsh/fwi/issues

#ifndef H_SPECIFIC_WAVE_NEWEQUATION
#define H_SPECIFIC_WAVE_NEWEQUATION

//Standard Library
#include <string>

//GmshFem Library
#include "GmshFem.h"
#include "Exception.h"

//FWI Library
#include "simplewave.h"
#include "helmholtz.h"
#include "maxwell.h"
#include "navier.h"

template<Physic T_Physic>
EquationInterface<T_Physic>* newEquation(unsigned int f_idx, double pulsation, const ConfigurationInterface* const config, const wave::Discretization<T_Physic>& w_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix = "");

/* acoustic */
template<>
EquationInterface<Physic::acoustic>* newEquation(unsigned int f_idx, double pulsation, const ConfigurationInterface* const config, const wave::Discretization<Physic::acoustic>& w_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix)
{
    std::string equation;
    if(!gmshFem.userDefinedParameter(equation, "equation"))
    {
      throw gmshfem::common::Exception("Equation type could not be found.");
    }
    if(equation=="simplewave"){return new simplewave::Equation(f_idx,pulsation,config,w_discret,gmshFem,suffix);}
    else if(equation=="helmholtz"){return new helmholtz::Equation(f_idx,pulsation,config,w_discret,gmshFem,suffix);}
    else
    {
        throw gmshfem::common::Exception("Equation " + equation + " is not valid in acoustics.");
        return nullptr;
    }
};

/* electromagnetic */
template<>
EquationInterface<Physic::electromagnetic>* newEquation(unsigned int f_idx, double pulsation, const ConfigurationInterface* const config, const wave::Discretization<Physic::electromagnetic>& w_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix)
{
    std::string equation;
    if(!gmshFem.userDefinedParameter(equation, "equation"))
    {
      throw gmshfem::common::Exception("Equation type could not be found.");
    }
    if(equation=="maxwell"){return new maxwell::Equation(f_idx,pulsation,config,w_discret,gmshFem,suffix);}
    else
    {
        throw gmshfem::common::Exception("Equation " + equation + " is not valid in electromagnetics.");
        return nullptr;
    }
};

/* elastodynamic */
template<>
EquationInterface<Physic::elastodynamic>* newEquation(unsigned int f_idx, double pulsation, const ConfigurationInterface* const config, const wave::Discretization<Physic::elastodynamic>& w_discret, const gmshfem::common::GmshFem& gmshFem, std::string suffix)
{
    std::string equation;
    if(!gmshFem.userDefinedParameter(equation, "equation"))
    {
      throw gmshfem::common::Exception("Equation type could not be found.");
    }
    if(equation=="navier"){return new navier::Equation(f_idx,pulsation,config,w_discret,gmshFem,suffix);}
    else
    {
        throw gmshfem::common::Exception("Equation " + equation + " is not valid in elastodynamics.");
        return nullptr;
    }
};

#endif //H_SPECIFIC_WAVE_NEWEQUATION
