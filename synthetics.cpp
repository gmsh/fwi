//GmshFem Library
#include "GmshFem.h"
#include "Post.h"
using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::post;

//FWI Library
#include "common/data/updater.h"
#include "common/wave/updater.h"
#include "specific/physic.h"
#include "specific/configuration/newConfiguration.h"
#include "specific/model/parametrization/newParametrization.h"
#include "common/wave/equation/equation.h"

template<Physic T_Physic>
int synthetics(const GmshFem& gmshFem)
{
    std::string name = "";
    gmshFem.userDefinedParameter(name, "prename");
    std::string suffix = "noname";
    gmshFem.userDefinedParameter(suffix, "name");
    name += suffix;

    const ParametrizationInterface* const parametrization = newParametrization<T_Physic>(gmshFem);

    auto configuration = newConfiguration(name, parametrization, gmshFem);

    ModelState ms(configuration.get());

    unsigned int n_freq = 1;
    gmshFem.userDefinedParameter(n_freq, "n_freq");
    msg::print << "Generate synthetic data" << msg::endl;
    msg::indent();

    unsigned int write_model_field = 0;
    gmshFem.userDefinedParameter(write_model_field, "write_model_fields");
    if(write_model_field)
    {
        for (unsigned int c = 0; c < parametrization->size(); c++)
        {
            save(configuration->m0()[c], configuration->model_known(Support::BLK), name+"_m_blkc"+std::to_string(c), "pos", "");
            save(configuration->m0()[c], configuration->model_known(Support::BND), name+"_m_bndc"+std::to_string(c), "pos", "");
        }
    }

    for (unsigned int f = 0; f < n_freq; f++)
    {
        std::string suffix = std::to_string(f);

        double frequency;
        if(!gmshFem.userDefinedParameter(frequency, "frequency"+suffix))
        {
            throw Exception("Frequency "+suffix+" could not be found.");
        }
        msg::print << "--- Frequency #"+suffix+" = " << frequency << " --- " << msg::endl;
        msg::indent();

        wave::Discretization<T_Physic> w_discret(gmshFem,suffix);
        EquationInterface<T_Physic>* const pequation = new ParametrizedEquation<T_Physic>(parametrization,0,2.*M_PI*frequency,configuration.get(),w_discret,gmshFem,suffix);

        WaveUpdater<T_Physic> wu(configuration.get(),nullptr,pequation);
        DataUpdater<T_Physic> du(std::vector<double>(1,frequency),configuration.get(),&wu,nullptr);

        std::array<bool,5> dataNeedUpToDate = {true,false,false,false,false};
        du.get(dataNeedUpToDate,ms).write(Type::FS,name+"_data"+suffix);

        unsigned int write_data_fields = 0;
        gmshFem.userDefinedParameter(write_data_fields, "write_data_fields");
        if(write_data_fields)
        {
            du.get(dataNeedUpToDate,ms).write(Type::FS,name+"_data_field"+suffix,"pos");
        }

        unsigned int write_wave_fields = 0;
        gmshFem.userDefinedParameter(write_wave_fields, "write_wave_fields");
        std::array<bool,4> waveNeedUpToDate = {true,false,false,false};
        if(write_wave_fields)
        {
            wu.get(waveNeedUpToDate,ms).write(Type::FS,name+"_u"+suffix);
        }
        unsigned int ns = 0;
        if(gmshFem.userDefinedParameter(ns, "write_single_wave_fields"))
        {
            for (unsigned int s = 0; s < ns; s++)
            {
                unsigned int idx;
                if(!gmshFem.userDefinedParameter(idx, "write_single_wave_field"+std::to_string(s)))
                {
                    throw Exception("Wave field index #"+std::to_string(s)+" to be written could not be found.");
                }
                (wu.get(waveNeedUpToDate,ms).state(Type::FS,idx)).write(name+"_u"+suffix+"s"+std::to_string(idx),"pos","");
            }
        }

        delete pequation;
        msg::unindent();
    }
    msg::unindent();
    delete parametrization;

    return 0;
}

int main(int argc, char **argv)
{
  GmshFem gmshFem(argc, argv);
  Physic T_Physic = to_physic(gmshFem);
  switch (T_Physic)
  {
    case Physic::acoustic: default:
      return synthetics<Physic::acoustic>(gmshFem);
    case Physic::electromagnetic:
      return synthetics<Physic::electromagnetic>(gmshFem);
    case Physic::elastodynamic:
      return synthetics<Physic::elastodynamic>(gmshFem);
  }
}
