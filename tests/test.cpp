#define BOOST_TEST_MODULE fwi_tests
#include <boost/test/unit_test.hpp>

#include <cstring>

#include <gmsh.h>
#include <gmshfem/Formulation.h>
#include <gmshfem/GmshFem.h>
#include "../common/wave/element.h"
#include "../common/data/element.h"
#include "../specific/data/element.h"

#include "dummyAcquisition.h"

#define INIT_GMSHFEM()                  \
    namespace geo = gmsh::model::geo;   \
    namespace model = gmsh::model;      \
    namespace mesh = gmsh::model::mesh; \
    using namespace gmshfem;            \
    using namespace gmshfem::common;    \
    using namespace gmshfem::problem;   \
    using namespace gmshfem::domain;    \
    using namespace gmshfem::field;     \
    using namespace gmshfem::function;  \
    using namespace gmshfem::post;      \
    using namespace gmshfem::equation;  \
    gmshfem::common::GmshFem fem(0, nullptr);

BOOST_AUTO_TEST_SUITE(WaveMultiFieldTest);

BOOST_AUTO_TEST_CASE(TestHomoegenuousDirichletWMF)
{
    INIT_GMSHFEM();
    fem.getArgsManager()->addUserDefinedParameter("wave_FunctionSpaceDegree", "2");
    fem.getArgsManager()->addUserDefinedParameter("wave_FunctionSpaceType", "HierarchicalH1");

    double lc = 0.1;
    model::add("Poisson");

    geo::addPoint(0.0, 0.0, 0.0, lc, 1);
    geo::addPoint(1.0, 0.0, 0.0, lc, 2);
    geo::addPoint(1.0, 1.0, 0.0, lc, 3);
    geo::addPoint(0.0, 1.0, 0.0, lc, 4);

    geo::addLine(1, 2);
    geo::addLine(2, 3);
    geo::addLine(3, 4);
    geo::addLine(4, 1);

    geo::addCurveLoop({1,2,3,4});
    geo::addPlaneSurface({1});
    geo::synchronize();
    mesh::generate();

    model::addPhysicalGroup(1, {4}, 1);
    model::setPhysicalName(1, 1, "gammaLeft");

    model::addPhysicalGroup(2, {1}, 1);
    model::setPhysicalName(2, 1, "omega");

    Formulation<std::complex<double>> formulation("Poisson");
    Domain omega("omega");
    Domain gammaLeft("gammaLeft");
    
    // Allocate an Auxiliary Field that will serve as proxy to write in the WMF
    WaveAuxilaryField<Physic::acoustic> v("v", omega, "Poisson", wave::Discretization<Physic::acoustic>(fem));
    v.addConstraint(gammaLeft, 0.); // Apply Dirichlet *before* preprocessing, to get correct DOF dictionary

    // Assemble LHS to get the correct number of DOFs
    formulation.integral(grad(dof(v)), grad(tf(v)), omega, "Gauss6");
    formulation.pre();

    WaveMultiField<Physic::acoustic> wmf(2, "v_multi");
    wmf.initializeField(v);
    v.setField(&wmf);

    // First solve
    v.setIndex(0);
    formulation.integral(-1., tf(v), omega, "Gauss6");
    formulation.assemble();
    formulation.solve(true);
        
    // Second solve
    formulation.setRHSToZero();

    v.setIndex(1);
    formulation.removeTerms();
    formulation.integral(-2., tf(v), omega, "Gauss6");
    formulation.assemble();
    formulation.solve(true);

    // Check analytical solution to tolerance.
    double f = 1;
    auto expected = f * x<std::complex<double>>() * (1. - 0.5 * x<std::complex<double>>());
    
    // Quadratic solution -> should be exact on a coarse mesh.
    auto integralV = abs(integrate(v, omega, "Gauss4"));
    auto integralErr1 = abs(integrate(expected - wmf[0], omega, "Gauss4"));
    auto integralErr2 = abs(integrate(2. * expected - wmf[1], omega, "Gauss4"));
    BOOST_TEST(integralV < 1e-8, "AuxiliaryWaveField should be empty. Integral of absolute value is " << integralV);
    BOOST_TEST(integralErr1 < 1e-8, "First WMF has the wrong value. Error norm: " << integralErr1);
    BOOST_TEST(integralErr2 < 1e-8, "Second WMF has the wrong value. Error norm: " << integralErr2);
}
BOOST_AUTO_TEST_CASE(dummy_test)
{
    BOOST_CHECK(1 + 1 == 2);
}

BOOST_AUTO_TEST_SUITE_END()