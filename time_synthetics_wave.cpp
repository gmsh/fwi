//Standard Library
#include <numeric>

//GmshFEM Library
#include "Formulation.h"
#include "GmshFem.h"
#include "Post.h"
#include "Matrix.h"
#include "Vector.h"

#include "petsc.h"

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;
using namespace gmshfem::equation;
using namespace gmshfem::algebra;

//GmshFWI Library
#include "common/data/element.h"
#include "specific/configuration/newConfiguration.h"
#include "specific/model/parametrization/newParametrization.h"

#define s2 config->m0_natural(1.)[0]
#define eps real(config->m0_natural(1.)[0])
#define sig imag(-config->m0_natural(1.)[0])

/*
* TimeExcitationInterface
*/
class TimeExcitationInterface
{
public:
    virtual double operator()(double t) const = 0;
    virtual bool isActive(double t) const = 0;
};
namespace ricker
{
    class TimeExcitation : public TimeExcitationInterface
    {
    public:
        TimeExcitation(const GmshFem& gmshFem)
        {
            if(!(
                gmshFem.userDefinedParameter(_offset, "excitation_offset") &&
                gmshFem.userDefinedParameter(_fm, "excitation_fm")
            ))
            {
                throw Exception("A time excitation parameter could not be found.");
            }
            _t0 = _offset;
        }
        virtual double operator()(double time) const
        {
            double t = time - _offset;
            double T = M_PI*_fm*t;
            return (1.-2.*T*T)*std::exp(-T*T);
        }
        virtual bool isActive(double t) const
        {
            return ((_offset-_t0)<=t) && (t<=(_offset+_t0));
        }
    private:
        double _offset;
        double _fm;
        double _t0;
    };
};

void Vec2Std(Vec v, std::vector<std::complex<double>> &s)
{
  PetscInt N;
  VecGetSize(v, &N);
  PetscScalar *array;
  VecGetArray(v, &array);
  s.resize(N);
  for(int i = 0; i < N; i++) s[i] = array[i];
  VecRestoreArray(v, &array);
}

int main(int argc, char **argv)
{
    GmshFem gmshFem(argc, argv);

    std::string name = "";
    gmshFem.userDefinedParameter(name, "prename");
    std::string suffix = "noname";
    gmshFem.userDefinedParameter(suffix, "name");
    name += suffix;

    const ParametrizationInterface* const parametrization = newParametrization<Physic::acoustic>(gmshFem);

    auto configuration = newConfiguration(name, parametrization, gmshFem);

    ModelState ms(config);

    unsigned int write_model_fields = 0;
    gmshFem.userDefinedParameter(write_model_fields, "write_model_fields");

    msg::print << "Generate synthetic data" << msg::endl;
    msg::indent();

    unsigned int write_model_field = 0;
    gmshFem.userDefinedParameter(write_model_field, "write_model_field");
    if(write_model_fields || write_model_field)
    {
        for (unsigned int c = 0; c < parametrization->size(); c++)
        {
            save(config->m0()[c], config->model_known(Support::BLK), name+"_m_blkc"+std::to_string(c), "pos", "");
            save(config->m0()[c], config->model_known(Support::BND), name+"_m_bndc"+std::to_string(c), "pos", "");
        }
    }

    unsigned int write_wave_field = 0;
    gmshFem.userDefinedParameter(write_wave_field, "write_wave_field");
    unsigned int write_interval = 1;
    gmshFem.userDefinedParameter(write_interval, "write_interval");

    wave::Discretization<Physic::acoustic> w_discret(gmshFem);
    unsigned int w_degree = w_discret.functionSpaceDegree();

    /* Time excitation */
    const TimeExcitationInterface* const excitation = new ricker::TimeExcitation(gmshFem);

    /* Time scheme */
    double newmark_beta = 0.25; //read from gmshfem
    double newmark_gamma = 0.5;
    double dt = 1.;
    double T = 0.;
    if(!(
        gmshFem.userDefinedParameter(newmark_beta, "newmark_beta") &&
        gmshFem.userDefinedParameter(newmark_gamma, "newmark_gamma") &&
        gmshFem.userDefinedParameter(dt, "newmark_dt") &&
        gmshFem.userDefinedParameter(T, "newmark_T")
    ))
    {
        throw Exception("A Newmark time scheme parameter could not be found.");
    }
    unsigned int num_timesteps = std::ceil(T/dt);

    /* Simple wave */
    Field< std::complex<double>, Form::Form0 > p("p", config->wave_evaldomain(), w_discret.functionSpaceType(), w_degree);
    std::string integrationType;
    unsigned int integrationDegreeBlk, integrationDegreeBnd;
    if(!gmshFem.userDefinedParameter(integrationType, "integration_type"))
    {
        throw Exception("Integration type could not be found.");
    }
    if(!gmshFem.userDefinedParameter(integrationDegreeBlk, "equation_integration_degree_blk"))
    {
        throw Exception("Equation integration degree (bulk) could not be found.");
    }
    if(!gmshFem.userDefinedParameter(integrationDegreeBnd, "equation_integration_degree_bnd"))
    {
        throw Exception("Equation integration degree (boundary) could not be found.");
    }

    Formulation<std::complex<double>> formulation("time_wave");

    if(
        (dynamic_cast<const simplewave::ln_slowness2_real::Parametrization* const>(parametrization) != nullptr) ||
        (dynamic_cast<const simplewave::slowness2_real::Parametrization* const>(parametrization) != nullptr)
    )
    {
        /* LHS */
        formulation.integral(s2 * dt2_dof(p), tf(p), config->model_known(Support::BLK), integrationType+std::to_string(integrationDegreeBlk));

        formulation.integral(grad(dof(p)), grad(tf(p)), config->wave_omega(Support::BLK),integrationType+std::to_string(2*w_degree));

        formulation.integral(sqrt(s2) * dt_dof(p), tf(p), config->model_known(Support::BND), integrationType+std::to_string(integrationDegreeBnd));
    }
    else if(
        (dynamic_cast<const simplewave::ln_permcond_bireal::Parametrization* const>(parametrization) != nullptr) ||
        (dynamic_cast<const simplewave::permcond_bireal::Parametrization* const>(parametrization) != nullptr)
    )
    {
        /* LHS */
        formulation.integral(eps * dt2_dof(p), tf(p), config->model_known(Support::BLK), integrationType+std::to_string(integrationDegreeBlk));

        formulation.integral(sig * dt_dof(p), tf(p), config->model_known(Support::BLK), integrationType+std::to_string(integrationDegreeBlk));

        formulation.integral(grad(dof(p)), grad(tf(p)), config->wave_omega(Support::BLK),integrationType+std::to_string(2*w_degree));

        formulation.integral(sqrt(eps) * dt_dof(p), tf(p), config->model_known(Support::BND), integrationType+std::to_string(integrationDegreeBnd));
    }
    else
    {
        throw Exception("Time wave only implemented for slowness and permcond_bireal");
    }

    /* RHS */
    formulation.integral(-1.,tf(p), config->emitter(0,0), integrationType+"0");

    formulation.pre();
    formulation.assemble(true);
    formulation.removeTerms();


    MatrixCRS< std::complex<double> > mass, damping, stiffness;
    formulation.getMass(mass);
    formulation.getDamping(damping);
    formulation.getStiffness(stiffness);

    Mat mK = stiffness.getPetsc();
    Mat mC = damping.getPetsc();
    Mat mM = mass.getPetsc();
    Mat mA, mIter1, mIter2;


    /* Make global system matrix */
    MatDuplicate(mM, MAT_COPY_VALUES, &mA);
    MatAXPY(mA, newmark_gamma * dt, mC, UNKNOWN_NONZERO_PATTERN);
    MatAXPY(mA, newmark_beta * dt * dt, mK, UNKNOWN_NONZERO_PATTERN);
    /* Make x_n update matrix */
    MatDuplicate(mM, MAT_COPY_VALUES, &mIter1);
    MatScale(mIter1, 2.0);
    MatAXPY(mIter1, -(1-2.0*newmark_gamma)*dt, mC, UNKNOWN_NONZERO_PATTERN);
    MatAXPY(mIter1, -(0.5 + newmark_gamma - 2.0*newmark_beta)*dt*dt, mK, UNKNOWN_NONZERO_PATTERN);

    /* Make x_{n-1} update matrix */
    MatDuplicate(mM, MAT_COPY_VALUES, &mIter2);
    MatScale(mIter2, -1.0);
    MatAXPY(mIter2, -(newmark_gamma - 1.0)*dt, mC, UNKNOWN_NONZERO_PATTERN);
    MatAXPY(mIter2, -(0.5 - newmark_gamma + newmark_beta)*dt*dt, mK, UNKNOWN_NONZERO_PATTERN);

    Vector< std::complex<double> > source;
    formulation.getRHS(source);
    formulation.setRHSToZero();
    Vec vSource;
    vSource = source.getPetsc();
    VecZeroEntries(vSource);

    Vec vx_prev, vx_curr, vx_next;
    Vec vb_prev, vb_curr, vb_next;
    Vec vRhs;
    VecDuplicate(vSource, &vRhs); VecZeroEntries(vRhs);
    VecDuplicate(vSource, &vx_prev); VecZeroEntries(vx_prev);
    VecDuplicate(vSource, &vx_curr); VecZeroEntries(vx_curr);
    VecDuplicate(vSource, &vx_next); VecZeroEntries(vx_next);

    KSP ksp;
    KSPCreate(PETSC_COMM_SELF, &ksp);
    KSPSetReusePreconditioner(ksp, PETSC_TRUE);
    KSPSetOperators(ksp, mA, mA);
    KSPSetType(ksp, "preonly");

    PC pc;
    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCCHOLESKY);
    PCFactorSetMatSolverType(pc, "mumps");

    /* RHS */
    std::vector<double> time(num_timesteps);
    std::iota(time.begin(), time.end(), 0);
    Data<Physic::acoustic> data(time,config);

    for (unsigned int s = 0; s < config->ns(); s++)
    {
        msg::print << "Shot #" << s << msg::endl;
        for (unsigned int e = 0; e < config->ne(s); e++)
        {
            formulation.integral(-1.,tf(p), config->emitter(s,e), integrationType+"0");
        }
        formulation.assemble(true);
        formulation.removeTerms();

        formulation.getRHS(source);
        formulation.setRHSToZero();
        vSource = source.getPetsc();

        double current_time = 0.;

        VecDuplicate(vSource, &vb_prev); VecScale(vb_prev, (*excitation)(current_time-dt));
        VecDuplicate(vSource, &vb_curr); VecScale(vb_curr, (*excitation)(current_time));
        VecDuplicate(vSource, &vb_next); VecScale(vb_next, (*excitation)(current_time+dt));

        for(unsigned int n = 0; n < num_timesteps; n++)
        {
            MatMult(mIter1, vx_curr, vRhs);
            MatMultAdd(mIter2, vx_prev, vRhs, vRhs);
            if(excitation->isActive(current_time))
            {
                VecAXPY(vRhs, dt*dt*newmark_beta, vb_next);
                VecAXPY(vRhs, dt*dt*(0.5 + newmark_gamma - 2.0*newmark_beta), vb_curr);
                VecAXPY(vRhs, dt*dt*(0.5 - newmark_gamma + newmark_beta), vb_prev);
            }

            // solve for x^{n+1}
            KSPSolve(ksp, vRhs, vx_next);

            // store solution back in the formulation
            std::vector<std::complex<double>> sol;
            Vec2Std(vx_next, sol);
            formulation.setSolutionIntoFields(sol);

            VecCopy(vx_curr, vx_prev);
            VecCopy(vx_next, vx_curr);

            current_time += dt;
            if(excitation->isActive(current_time))
            {
                VecCopy(vb_curr, vb_prev);
                VecCopy(vb_next, vb_curr);
                VecCopy(vSource, vb_next); VecScale(vb_next, (*excitation)(current_time+dt));
            }

            for (unsigned int r = 0; r < config->nr(s); r++)
            {
                data.value(n,s,r, integrate(p,config->receiver(s,r),"Gauss0"));
            }

            if( write_wave_field && (n % write_interval == 0) )
            {
                save(p, config->model_known(Support::BLK), name+"_u_s"+std::to_string(s)+"n"+std::to_string(n), "pos", "");
            }
        }
        VecZeroEntries(vRhs);
        VecZeroEntries(vx_prev);
        VecZeroEntries(vx_curr);
        VecZeroEntries(vx_next);
    }
    data.write(name+"_data");

    msg::unindent();
    delete config;
    delete parametrization;

    return 0;
}
