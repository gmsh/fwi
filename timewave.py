#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 12:32:05 2022

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
rc('font',**{'family':'serif','sans-serif':['Times New Roman'],'size':15})
rc('text', usetex=True)
#Value for 1 width figure
H = 3.;
W = 8.;

"""
# Marmousi
path = "extended_marmousi_data/"
prename = "timewave_synthetics_data"
T=8.0
Δt=0.004
Nr=242
Ns = 122

snr = 2.**2
noise_tag = "snr2"

#source
offset = 0.3
fm = 6.

prename2 = "paper2_extended_synthetics_data"
Nf=3
freqf = np.array([4.,6.,8.]) #[Hz]
"""

"""
# Concrete 3
path = "concrete3_data/"
prename = "timewave_synthetics_data"
T=0.2
Δt=1e-4
Nr=226
Ns=227

snr = 2.**(-3)
noise_tag = "snrm3"

#source
offset = 0.0075
fm = 150.

prename2 = "paper2_concrete3_synthetics_data"
Nf=9
freqf = np.linspace(100,300,9) #[Hz]
"""

# Cross
path = "cross_data/"
prename = "cross_time_synthetics_data"
T=100.
Δt=1e-2
Nr=119
Ns=40
amplitude = 0.01

snr = 2.**(6)
noise_tag = "snr6"

#source
offset = 3.75
fm = 0.33

prename2 = "cross_synthetics_data"
Nf=7
freqf =  np.array( [ 0.17, 0.2, 0.23, 0.27, 0.33, 0.5, 0.67 ] ) # [Hz] ?

#
# Load
#
N=np.int(T/Δt)
time = np.arange(0,N,1)*Δt #[s]
freq = np.fft.rfftfreq(time.shape[-1],d=Δt) #[Hz]
data = np.zeros([Ns,Nr,N]);

for n in range(0,N):
    tmp = np.loadtxt(path+prename+"f"+str(n)+".csv",delimiter=";");
    data[:,:,n] = tmp[0:Ns,0:2*Nr:2]
signal_nrm = np.linalg.norm(np.linalg.norm(data,axis=2))**2 / Ns / Nr  * Δt

#
# Noise
#
B=2.5*fm
σ = np.sqrt(signal_nrm/(2*B*Δt*T*snr))
white_noise = np.random.normal(0.0,σ,[Ns,Nr,N])

freq_noise = np.zeros([Ns,Nr,np.int(N/2)+1])*1j;
noise = np.zeros([Ns,Nr,N])
freq_cut_idx = np.int(np.ceil(B*T))
for s in range(0,Ns):
    for r in range(0,Nr):
        freq_noise[s,r,0:(freq_cut_idx+1)] = np.fft.rfft(white_noise[s,r,:])[0:(freq_cut_idx+1)]
        noise[s,r,:] = np.fft.irfft(freq_noise[s,r,:])     
noise_nrm = np.linalg.norm(np.linalg.norm(noise,axis=2))**2 / Ns / Nr  * Δt
        
noisy_data = data + noise

for n in range(0,N):
    np.savetxt(path+prename+"_"+noise_tag+"f"+str(n)+".csv",(noisy_data[:,:,n]+0j).view(float),delimiter=";",fmt='%.18e');

#
# Fourier transform
#
freq = np.fft.rfftfreq(time.shape[-1],d=Δt) #[Hz]

excitation = 2. / np.sqrt(np.pi) * freq**2 / fm**3 * np.exp(-freq**2/fm**2) * np.exp(-2j * np.pi * freq * offset)

freq_noisy_data = np.zeros([Ns,Nr,np.int(N/2)+1])*1j;
freq_data = np.zeros([Ns,Nr,np.int(N/2)+1])*1j;
for s in range(0,Ns):
    for r in range(0,Nr):
        freq_noisy_data[s,r,:] = np.fft.rfft(noisy_data[s,r,:]) * Δt
        freq_data[s,r,:] = np.fft.rfft(data[s,r,:]) * Δt

#
# Frequency domain
#
freq_dataf = np.zeros([Ns,Nr,Nf])*1j;

for f in range(0,Nf):
    tmp = np.loadtxt(path+prename2+str(f)+".csv",delimiter=";");
    freq_dataf[:,:,f] = tmp[0:Ns,0:2*Nr:2] + tmp[0:Ns,1:2*Nr:2]*1j 


#write
for f in range(0,np.size(freqf)):
    idx = np.where(freq == freqf[f])[0][0]
    np.savetxt(path+prename2+"_"+noise_tag+"f"+str(f)+".csv",freq_noisy_data[:,:,idx]/excitation[idx],fmt=['%.18e;%.18e']*Nr, delimiter=";");
