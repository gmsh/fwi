import yaml

def generate_yaml(num_points, x_step, dist = 1):
    coordinates = []
    shots = []
    total_receivers = 0

    for i in range(num_points):
        x = i * x_step + 0.75
        y = -0.1
        coordinates.append([x, y])

    for i in range(num_points):
        emitters = [i]
        receivers = []
        for j in range(num_points):
            if abs(j - i) >= dist and j != i:
                receivers.append(j)
        shots.append({'emitters': emitters, 'receivers': receivers})
        total_receivers += len(receivers)  # Add the count of receivers in this shot

    data = {'coordinates': coordinates, 'shots': shots}
    return yaml.dump(data, default_flow_style=False), total_receivers

# Example usage:
num_points = 10
x_step = 5 / (num_points-1)

# Write YAML data to a file
for d in [0]:
    with open(f'config_{num_points}_offset_{d}.yml', 'w') as file:
        yaml_data, n_data = generate_yaml(num_points, x_step, d)
        print(f"With d = {d}, there are {n_data} measurements")
        file.write(yaml_data)
