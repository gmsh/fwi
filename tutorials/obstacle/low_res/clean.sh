#!/usr/bin/bash
set -e
set -x

rm obstacle_*data*.csv -v -f
rm *.msh -v -f
rm *.pos -v -f
rm obstacle*.csv -v -f