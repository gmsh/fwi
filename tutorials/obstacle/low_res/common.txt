# Geometry, configuration, parametrization
prename = obstacle_low_res_
configuration = flexible_acquisition
physic = acoustic
equation = simplewave
L = 6
H = 2
Hsub = 0.2
# Initial parameters
Re(m_superc0) = 4e-7
Im(m_superc0) = 0
Re(m_subc0) = 8e-8
Im(m_subc0) = 0
# Subsurface configured in synthetics or inversion
#Discretization
integration_type = Gauss
equation_integration_degree_blk = 14
equation_integration_degree_bnd = 14
#Wave
wave_FunctionSpaceType = HierarchicalH1
#top_bc = Neumann
shots_config = ../config_25_offset_0.yml
equation_boundary=1
