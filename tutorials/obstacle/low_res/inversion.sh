#!/usr/bin/bash
set -e
set -x
INVERSION_PATH=~/deps/fwi/build/inversion
$INVERSION_PATH common.txt freq.txt inversion.txt optim_trust.txt -verbose 2
