#!/usr/bin/bash
set -e
set -x
SYNTHETICS_PATH=~/deps/fwi/build/synthetics
$SYNTHETICS_PATH common.txt freq.txt synthetics.txt -verbose 2
