#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 25 14:43:28 2022

@author: xavier
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
start_color = '#060689'
mid_color = '#FFFFFF'
end_color = '#890606'
mycmap = mcolors.LinearSegmentedColormap.from_list('mycmap', [start_color, mid_color, end_color])

N=750
interval = 10
Nr=242
amplitude = 0.04
s = 0

for i in range(0,5):
    path = "it"+str(i)+"/data/"
    prename = "model_iteration"+str(i)+"_data";

    #
    # Time domain
    #
    
    data = np.zeros([N,Nr]);
    
    my_dpi=96
    plt.figure(figsize=(1532/my_dpi, 520/my_dpi), dpi=my_dpi,tight_layout=False)
    plt.gca().invert_yaxis()
    plt.axis('off')
    plt.subplots_adjust(top=1.,right=1.,bottom=0.,left=0.)
    
    for n in range(0,N):
        tmp = np.loadtxt(path+prename+"f"+str(n)+".csv",delimiter=";",ndmin=2);
        data[n,:] = tmp[s,0:2*Nr:2]
        if n % interval == 0:
            plt.imshow(data,vmin=-amplitude,vmax=amplitude,cmap=mycmap, aspect='auto')
            plt.savefig(path+"dataf"+str(n)+".png")
            plt.cla()
    plt.close()
